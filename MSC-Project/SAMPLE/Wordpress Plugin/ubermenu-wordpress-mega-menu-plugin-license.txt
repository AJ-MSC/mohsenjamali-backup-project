LICENSE CERTIFICATE : Envato Marketplace Item
==============================================

This document certifies the purchase of:
ONE REGULAR LICENSE
as defined in the standard terms and conditions on the Envato Marketplaces.

Licensor's Author Username: sevenspark
Licensee: MAVAJ SUN CO

For the item:
UberMenu - WordPress Mega Menu Plugin

http://codecanyon.net/item/ubermenu-wordpress-mega-menu-plugin/154703
Item ID: 154703

Item Purchase Code: caba42a7-4926-49ac-9a54-ebe3b58e4527

Purchase Date: 2014-01-14 21:24:31 UTC

For any queries related to this document or license please contact Envato Support via http://support.envato.com/index.php?/Live/Tickets/Submit

Envato Pty. Ltd. (ABN 11 119 159 741)
PO Box 16122, Collins Street West, VIC 8007, Australia

==== THIS IS NOT A TAX RECEIPT OR INVOICE ====
