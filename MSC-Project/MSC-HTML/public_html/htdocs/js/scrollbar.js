
function makeScrollbar(content,scrollbar,handle,ignoreMouse){
    if(content){
        var steps = (content.getSize().scrollSize.y - content.getSize().size.y);
    	var slider = new Slider(scrollbar, handle, {	
    		steps: steps,
    		mode: 'vertical',
    		onChange: function(step){
    			// Scrolls the content element in x or y direction.
    			var x = 0;
    			var y = step;
    			content.scrollTo(x,y);
    		}
    	}).set(0);
    	if( !(ignoreMouse) ){
    		// Scroll the content element when the mousewheel is used within the 
    		// content or the scrollbar element.
    		$$(content, scrollbar).addEvent('mousewheel', function(e){	
    			e = new Event(e).stop();
    			var step = slider.step - e.wheel * 30;	
    			slider.set(step);					
    		});
    	}
    	// Stops the handle dragging process when the mouse leaves the document body.
    	$(document.body).addEvent('mouseleave',function(){slider.drag.stop()});
    }
}