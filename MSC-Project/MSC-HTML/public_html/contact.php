<!--
 * Copyright (c) 1978-2028 MAVAJ SUN CO, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of MAVAJ SUN
 * CO, Inc. ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the terms
 * of the license agreement you entered into with MAVAJ SUN CO.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND . ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. MAVAJ SUN CO AND ITS LICENSORS SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING ,
 * MODIFYING OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL
 * MAVAJ SUN CO OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR
 * DATA,OR FOR DIRECT,INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE
 * DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING
 * OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear facility.
 * Licensee represents and warrants that it will not use or redistribute the
 * Software for such purposes.
 *
 *
 * @author  : MavajSunCo (Ali Jamali)
 * @Email   : info@MavajSunCo.com
 * @Website : www.MavajSunCo.com
 * @Date    : 2011-01-01
 * @version : 1
 *
 * @Decsriptoin : 
 *
-->
<!DOCTYPE html>
<html>
    <head>
        <title>MAVAJ SUN CO | Contact </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="robots" content="index, follow" />
        <meta name="DC.title" content="Mavaj Sun Co" />
        <meta name="geo.region" content="AE-DU" />
        <meta name="geo.placename" content="Dubai" />
        <meta name="geo.position" content="25.043184;55.136961" />
        <meta name="ICBM" content="25.043184, 55.136961" />
        <meta name="keywords" content="MavajSunCo , contact , MAVAJ SUN CO , Social , Media , Links" />
        <meta name="description" content="Mavaj Sun Co. Contact from & social media links. " />
        <meta name="generator" content="Mavaj Sun Co." />
        <link rel="shortcut icon" href="htdocs/img/base/favicon.png" />
        <script type="text/javascript" src="htdocs/js/google-analytics.js"></script>

        <script type="text/javascript">
            v_fields = new Array('sender_email', 'sender_message');
            alert_on = true;
            thanks_on = true;
            thanks_message = "Thank you to contact MAVAJ SUN CO . Your message has been sent.";
            function validateForm() {

                //alert(v_fields);

                //init errors
                var err = "";

                //start checking fields
                for (i = 0; i < v_fields.length; i++) {

                    //store the field value
                    var _thisfield = eval("document.contact." + v_fields[i] + ".value");

                    //check the field value
                    if (v_fields[i] === "sender_name") {
                        if (!isAlpha(_thisfield)) {
                            err += "Please enter a valid name\n";
                        }
                    } else if (v_fields[i] === "sender_subject") {
                        if (!isAlpha(_thisfield)) {
                            err += "Please enter a valid subject\n";
                        }
                    } else if (v_fields[i] === "sender_email") {
                        if (!isEmail(_thisfield)) {
                            err += "Please enter a valid email address\n";
                        }
                    } else if (v_fields[i] === "sender_url") {
                        if (!isURL(_thisfield)) {
                            err += "Please enter a valid URL\n";
                        }
                    } else if (v_fields[i] === "sender_phone") {
                        if (!isPhone(_thisfield)) {
                            err += "Please enter a valid phone number\n";
                        }
                    } else if (v_fields[i] === "sender_message") {
                        if (!isText(_thisfield)) {
                            err += "Please enter a valid message\n";
                        }
                    }

                }//end for

                if (err !== "") {
                    if (alert_on) {
                        alert("The following errors have occurred\n" + err);
                    } else {
                        showErrors(err);
                    }
                    return false;
                }
                return true;
            }

            //function to show errors in HTML
            function showErrors(str) {
                var err = str.replace(/\n/g, "<br />");
                document.getElementById("form_errors").innerHTML = err;
                document.getElementById("form_errors").style.display = "block";
            }

            //function to show thank you message in HTML
            function showThanks(str) {
                var tym = str.replace(/\n/g, "<br />");
                document.getElementById("form_thanks").innerHTML = tym;
                document.getElementById("form_thanks").style.display = "block";

            }

            function isEmail(str) {
                if (str === "")
                    return false;
                var regex = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;
                return regex.test(str);
            }

            function isText(str) {
                if (str === "")
                    return false;
                return true;
            }

            function isURL(str) {
                var regex = /[a-zA-Z0-9\.\/:]+/;
                return regex.test(str);
            }

            // returns true if the number is formatted in the following ways:
            // (000)000-0000, (000) 000-0000, 000-000-0000, 000.000.0000, 000 000 0000, 0000000000
            function isPhone(str) {
                var regex = /^\(?[2-9]\d{2}[\)\.-]?\s?\d{3}[\s\.-]?\d{4}$/;
                return regex.test(str);
            }

            // returns true if the string contains A-Z, a-z or 0-9 or . or # only
            function isAddress(str) {
                var regex = /[^a-zA-Z0-9\#\.]/g;
                if (regex.test(str))
                    return true;
                return false;
            }

            // returns true if the string is 5 digits
            function isZip(str) {
                var regex = /\d{5,}/;
                if (regex.test(str))
                    return true;
                return false;
            }

            // returns true if the string contains A-Z or a-z only
            function isAlpha(str) {
                var regex = /[a-zA-Z]/g;
                if (regex.test(str))
                    return true;
                return false;
            }

            // returns true if the string contains A-Z or a-z or 0-9 only
            function isAlphaNumeric(str) {
                var regex = /[^a-zA-Z0-9]/g;
                if (regex.test(str))
                    return false;
                return true;
            }
        </script>
    </head>
    <body>

        <div id="article_content">
            <div id="one_column">
                <!--fieldset style="float:left;"-->
                <p id="form_errors"></p>
                <p id="form_thanks"></p>
                <form name="contact" action="/contact.php" method="post" onsubmit="return validateForm();">
                    <table board="0" >
                        <tr>
                            <td>
                                <label>Name <span class="req">*</span></label>
                            </td>
                            <td>
                                <input type="text" class="txt_input" name="name" />
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Your E-Mail <span class="req">*</span></label>
                            </td>
                            <td>
                                <input type="text" class="txt_input" name="sender_email" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Phone </label>
                            </td>
                            <td>
                                <input type="text" class="txt_input" name="phone" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Message <span class="req">*</span></label>
                            </td>
                            <td>
                                <textarea name="sender_message"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" name="submitForm" value="Send message" />
                            </td>
                        </tr>
                    </table>
                </form>
                <!--/fieldset-->
                <br/>

                <h2>Social Network</h2>
                <ul>
                    <li><a href="https://www.facebook.com/MavajSunCo" class="popup" title="Facebook Fan Page" target="_blank">      <span><img src="htdocs/img/base/facebook.png" height="64px" width="64px"/>  Facebook    </span></a></li>
                    <li><a href="https://plus.google.com/+Mavajsunco" class="popup" title="Google +" target="_blank" >              <span><img src="htdocs/img/base/google.png" height="64px" width="64px"/>    Google      </span></a></li>
                    <li><a href="https://www.youtube.com/MavajSunCo" class="popup" title="Watch youtube our channel" target="_blank"><span><img src="htdocs/img/base/youtube.png" height="64px" width="64px"/>  Youtube     </span></a></li>
                    <li><a href="https://vimeo.com/MavajSunCo" class="popup" title="Watch vimeo our channel" target="_blank">        <span><img sr="htdocs/img/base/facebook.png" height="64px" width="64px"/>  Vimeo       </span></a></li>
                    <li><a href="https://www.linkedin.com/company/mavaj-sun-co" class="popup" title="linkedin" target="_blank">     <span><img src="htdocs/img/base/linkedin.png" height="64px" width="64px"/>  Linkedin    </span></a></li>      
                    <li><a href="https://www.flickr.com/photos/MavajSunCo/" class="popup" title="Flickr" target="_blank">           <span><img src="htdocs/img/base/linkedin.png" height="64px" width="64px"/>  Flickr      </span></a></li>
                    <li><a href="http://instagram.com/MavajSunCo/" class="popup" title="Instagram" target="_blank">                 <span><img src="htdocs/img/base/linkedin.png" height="64px" width="64px"/>  Instagram   </span></a></li>
                    <li><a href="http://mavajsunco.tumblr.com/" class="popup" title="Tumblr" target="_blank">                       <span><img src="htdocs/img/base/linkedin.png" height="64px" width="64px"/>  Tumblr      </span></a></li>
                    <li><a href="http://www.pinterest.com/MavajSunCo/" class="popup" title="Pinterest" target="_blank">             <span><img src="htdocs/img/base/linkedin.png" height="64px" width="64px"/>  Pinterest   </span></a></li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>