// JavaScript TimeLine Powered by MAVAJ SUN CO.

var IE = (navigator.appName === "Microsoft Internet Explorer") ? true : false;
var mouseX = 0;
var images = new Array(13);

function loadImages() {
    for (i = 0; i < 13; i++) {
        images[i] = new Image();
        if (i < 9) {
            images[i] = "htdocs/img/Moon.000" + (i + 1) + ".jpg";
            document.getElementById('dS').src = images[i];
        } else {
            images[i] = "htdocs/img/Moon.00" + (i + 1) + ".jpg";
            document.getElementById('dS').src = images[i];
        }
    }
}

function getDetails(event, element) {
    if (!IE) {
        mouseX = event.pageX - element.offsetLeft;
    } else {
        mouseX = window.event.clientX - element.offsetLeft;
        ;
    }

    if (mouseX > 0 && mouseX < 45) {
        document.getElementById('dS').src = images[12];
    } else if (mouseX > 45 && mouseX < 90) {
        document.getElementById('dS').src = images[11];
    } else if (mouseX > 90 && mouseX < 135) {
        document.getElementById('dS').src = images[10];
    } else if (mouseX > 135 && mouseX < 180) {
        document.getElementById('dS').src = images[9];
    } else if (mouseX > 180 && mouseX < 225) {
        document.getElementById('dS').src = images[8];
    } else if (mouseX > 225 && mouseX < 270) {
        document.getElementById('dS').src = images[7];
        ;
    } else if (mouseX > 270 && mouseX < 315) {
        document.getElementById('dS').src = images[6];
    } else if (mouseX > 315 && mouseX < 360) {
        document.getElementById('dS').src = images[5];
    } else if (mouseX > 360 && mouseX < 405) {
        document.getElementById('dS').src = images[4];
    } else if (mouseX > 405 && mouseX < 450) {
        document.getElementById('dS').src = images[3];
    } else if (mouseX > 450 && mouseX < 495) {
        document.getElementById('dS').src = images[2];
    } else if (mouseX > 485 && mouseX < 540) {
        document.getElementById('dS').src = images[1];
    } else if (mouseX > 540 && mouseX < 585) {
        document.getElementById('dS').src = images[0];
    }
}      