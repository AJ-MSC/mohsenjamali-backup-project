// JavaScript Document
/**
 * @Decsriptoin : To build a 360 degree view , the main idea is to display images of different angles of the product to the user one at a time when the user is interacting with the application and moving the mouse left or right.
 * @author      : MavajSunCo ( Ali Jamali )
 * @Date        : 8-May-2012
 * @Email       : info@MavajSunCo.com
 * @Website     : www.MavajSunCo.com
 **/
var imageHolder = null;
var distance = 0;
var initialX, initialY, finalX, finalY, presentX, presentY = 0;
var isMouseDown = false;
var counter = 0;
var noOfImages = 23;
function init() {
    console.log("init()");
    imageHolder = document.getElementById("imageHolder");
    imageHolder.addEventListener("mousedown", handleMouseDown, false);
    imageHolder.addEventListener("mousemove", handleMouseMove, false);
    imageHolder.addEventListener("mouseup", handleMouseUp, false);
}

function handleMouseMove(event) {
    if (isMouseDown) {
        presentX = event.pageX;
        presentY = event.pageY;
        distance = parseInt((presentX - initialX));
        console.log("Move: " + distance + " Present: " + presentX);
        //console.log(imageHolder.style.width);
        //now check whether distance is +ve or -ve
        if (distance <= -1) {
            initialX = event.pageX;
            moveForward();
        }
        else if (distance >= 1) {
            initialX = event.pageX;
            moveBackward();
        }
    }
}

function moveForward() {
    ++counter;
    if (counter == noOfImages) {
        counter = 0;
    }

    document.getElementById("imageHolder").style.backgroundPosition = "0px " + (-counter * 360) + "px";
    //imageHolder.setStyle("background-position", "0px " + (-i * 376) + "px");
    //console.log("Counter: " + counter);
    //console.log("Test: " + document.getElementById("imageHolder").style.backgroundPosition);
}

function moveBackward() {
    counter--;
    if (counter == -1) {
        counter = noOfImages - 1;
    }

    document.getElementById("imageHolder").style.backgroundPosition = "0px " + (-counter * 360) + "px";
    //imageHolder.setStyle("background-position", "0px " + (-i * 376) + "px");
    //console.log("Counter: " + counter);
    //console.log("Test: " + document.getElementById("imageHolder").style.backgroundPosition);
}

function handleMouseDown(event) {
    event.preventDefault();
    isMouseDown = true;
    initialX = event.pageX;
    //initialY = event.pageY;

    console.log("Mouse Down: " + initialX);
}
function handleMouseUp(event) {
    if (isMouseDown) {
        isMouseDown = false;
    }
    finalX = event.pageX;
    //finalY = event.pageY;
    console.log("Mouse Up: " + finalX);
}