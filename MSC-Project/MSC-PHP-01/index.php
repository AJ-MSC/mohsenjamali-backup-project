<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
</div>

<!-- Full Page Image Background Carousel Header -->
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="http://placehold.it/1300x1300" alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>111111111111</h1>
                    <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="http://placehold.it/1300x1300" alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Another example headline.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="http://placehold.it/1300x1300" alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>One more for good measure.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->
<section id="section-01" class="">
    <div class="container">
        <!-- Page Content -->
        <div class="row">
            <div class="col-md-7 featurette">
                <h2 class="featurette-heading">GET ON THE PATH TO A PERFECT <span class="text-muted">WEBSITE</span></h2>
                <p class="lead">Experts in our field, we design and custom build eye-catching and user-friendly websites that achieve the results that our clients want them to.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="500x500" src="https://chichesterdesign.co.uk/img/map.png" data-holder-rendered="true">
            </div>
        </div>
    </div>
</section>

<section id="section-02" class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>All plans include</h2>
            </div>
        </div>
        <!-- /row -->
        <br><br>
        <div class="row">
            <div class="col-md-4">
                <i class="fa fa-globe"></i>
                <p class="title">Domains</p>
                <p>A domain manager that makes life easy. Simple to add, update, manage, and transfer all your domains in a central place. </p>
            </div>

            <div class="col-md-4">
                <i class="fa fa-refresh"></i>
                <p class="title">Free Migration</p>
                <p>We handle the entire migration from your existing host. Our team works hard to ensure a worry-free transfer with no downtime. </p>
            </div>
            <div class="col-md-4">
                <i class="fa fa-wrench"></i>
                <p class="title">Apps &amp; Tools</p>
                <p>From Wordpress, Joomla!, and Drupal to over 300+ free open-source applications installed in 1-click. There’s no limit to what you can achieve.
                </p>
            </div>
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-4">
                <i class="fa fa-gear"></i>
                <p class="title">Control Panel</p>
                <p>It's easy to manage your websites, domains, files, and more, all in one location with the industry-leading cPanel control panel.</p>
            </div>
            <div class="col-md-4">
                <i class="fa fa-bolt"></i>
                <p class="title">FTP &amp; SFTP</p>
                <p>Both FTP &amp; SFTP access is available on all web hosting plans for simple secure access to your website.</p>
            </div>
            <div class="col-md-4">
                <i class="fa fa-graduation-cap"></i>
                <p class="title">Advanced Capabilities</p>
                <p>Advanced capabilities such as server side includes log file access and web-based managers for files, databases, email, and more. </p>
            </div>

        </div>
        <!--/row-->
    </div>
</section>

<section id="section-03" class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="seal"><span>Award Winning Support</span></div>
                <h2>Let our Squad take care of you</h2>
                <h3>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 pull-right">
                <div class="row">
                    <div class="col-md-6" id="section">
                        <p class="title" ><i class="fa fa-support"></i> Always available
                        </p>
                        <p>With 24/7/365 support, our squad team is always here to help. We offer email, chat, and telephone support on all plans.</p>
                    </div>
                    <div class="col-md-6 ca-only">
                        <p class="title"><i class="fa fa-dashboard"></i> One-on-One Support
                        </p>
                        <p>Need extra assistance? Book a 30-minute dedicated one-on-one session with one of our squad experts. Included for free with our Business Pro and Unlimited Pro plans.</p>

                    </div>

                </div><!-- /row -->
                <div class="row">
                    <div class="col-md-6">
                        <p class="title"><i class="fa fa-flag"></i> Multi-language Support
                        </p><p>Our squad features representatives from all around the world. We currently offer support in English, French, Spanish, and German. </p>
                    </div>
                    <div class="col-md-6">
                        <p class="title"><i class="fa fa-book"></i> Educational Tools
                        </p>
                        <p>We offer a complete knowledgebase of helpful articles and tutorials. Our instructional videos will also help you set up your website, email, and more.</p>
                    </div>
                </div><!-- /row -->
            </div>
            <div class="col-md-4 rep">
                <img data-original="http://www.hostpapa.co.uk/assets/rep.png" class="img-responsive" src="http://www.hostpapa.co.uk/assets/rep.png" style="display: block;"></div>

        </div>
</section>

<section id="section-04" class="">
    <div class="container">
        <div class="row">
            <h2>our <span>process</span></h2>
            <p style="width: 800px; text-align: center; margin: 10px auto; padding: 15px 0 15px 0;">We let our work speak for itself and our clients speak for us. Our focus is to provide an unrivalled service whoever the customer. Almost all of our work comes from referrals and we want to keep it that way.</p>
            <div class="col-sm-1"></div>
            <div class="col-sm-2">
                <img width="100" height="100" class="img-responsive" src="http://www.wearesweet.co.uk/images/home/knob-plan.png" alt="">
                <h2>Plan</h2>
                <p>Planning is key: we work closely with our clients from the outset of every project. Discussing ideas and possibilities before identifying the perfect plan of action.</p>
            </div>
            <div class="col-sm-2">
                <img width="100" height="100" class="img-responsive" src="http://www.wearesweet.co.uk/images/home/knob-design.png" alt="">
                <h2>Design</h2>
                <p>We believe beauty and function can exist simultaneously. We focus on the tiny details which set you apart from your competitors.</p>
            </div>
            <div class="col-sm-2">
                <img width="100" height="100" class="img-responsive" src="http://www.wearesweet.co.uk/images/home/knob-build.png" alt="">
                <h2>Build</h2>
                <p>By constructing our websites in proven frameworks and content management systems we create secure, efficient, high-quality platforms.</p>
            </div>
            <div class="col-sm-2">
                <img width="100" height="100" class="img-responsive" src="http://www.wearesweet.co.uk/images/home/knob-launch.png" alt="">
                <h2>Lunch</h2>
                <p>After rigorous quality-control, your project moves into its live environment. We ensure that your project is accessible on large desktop displays, laptops, tablets and mobile devices.</p>
            </div>
            <div class="col-sm-2">
                <img width="100" height="100" class="img-responsive" src="http://www.wearesweet.co.uk/images/home/knob-grow.png" alt="">
                <h2>Grow</h2>
                <p>The web is a competitive arena and your website must continue to stand out. Our robust, scalable systems and personal aftercare allow you to stay up to date and expand your business.</p>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <!-- /.row -->
    </div>
</section>

<section id="section-05" class="">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h2>What We Do</h2>
                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et molestiae similique eligendi reiciendis sunt distinctio odit? Quia, neque, ipsa, adipisci quisquam ullam deserunt accusantium illo iste exercitationem nemo voluptates asperiores.</p>
                <p>
                    <a class="btn btn-default btn-lg" href="#">Call to Action &raquo;</a>
                </p>
            </div>
            <div class="col-sm-4">
                <h2>Newsletter</h2>
                <strong>Subscribe</strong>
                <br><br>
                <p>Sign up for our newsletter to receive notifications about the latest news, exclusive offers, giveaways, promotions and more!</p>
                <br>
                <div id="mc_embed_signup">
                    <form role="form" action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                        <div class="input-group input-group-lg">
                            <input type="email" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Email address...">
                            <span class="input-group-btn">
                                <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-default">Subscribe!</button>
                            </span>
                        </div>
                        <div id="mce-responses">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                    </form>
                    <br><br>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
</section>
<?php
//End section
include_once $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?> 