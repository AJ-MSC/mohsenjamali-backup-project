<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<div id="page-container">
    <div id="page-wrapper">

        <!-- ### Template /products/hosting/web_email_hosting/linux_hosting_plans.html starts here ### -->
        <div class="row-gray row-indent">
            <div id="plans-masthead">
                <img class="banner-img" src="//cdnassets.com/getImage.php?url=webhost.mavajsunco.com&amp;src=thumb-lnx-hosting.png&amp;t=1378478507">

                <h2 class="ui-heading">Powerful <em>Linux Hosting</em></h2>
                <ol>
                    <li>State-of-the-Art Hosting Infrastructure</li>
                    <li>99.9% Uptime Guarantee</li>
                    <li>30-Day Money-Back Guarantee</li>
                </ol>
            </div>
            <div class="techi-bar">
                <span>Supports:</span>
                <div class="img-wrp"><img src="//cdnassets.com/getImage.php?url=webhost.mavajsunco.com&amp;src=tech-spe-1.jpg&amp;t=1386682362"></div>
            </div>
        </div>

        <div class="row-indent">
            <h2 class="ui-subheading hosting-select">Select Your <em>Plan</em></h2>
        </div>
        <div id="plans-container">
            <br><br>
            <div class="plans-columns-wrp">
                <div class="server_loc_tabs" style="left: 97px;">
                    <ul class="country_specific_tabs">

                        <li class="tab_US sel first" country="US">US Hosting<div class="sel_tab"></div></li>
                        <li class="tab_IN" country="IN">IN Hosting</li>
                        <li class="tab_UK" country="UK">UK Hosting</li>
                        <li class="tab_TR" country="TR">TR Hosting</li>
                        <li class="tab_HK last" country="HK">HK Hosting</li>
                    </ul>
                </div>
                <div class="plans-columns">

                    <ul class="plan-list us_plan " style="">
                        <form name="hostingplan" id="hostingplan_3916546" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="linux">
                            <input type="hidden" name="location" value="us">
                            <input type="hidden" id="planid_id" name="planid" value="3916546">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Unlimited Hosting Plan <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                4.61
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Single Domain
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="3916546_duration">

                                    <option value="12">
                                        1  Year at $ 4.61/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('3916546', 'linux');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list us_plan plan-featured" style="">
                        <form name="hostingplan" id="hostingplan_1" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="us">
                            <input type="hidden" id="planid_id" name="planid" value="1">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Business <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                6.47
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    3 Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="1_duration">

                                    <option value="3">
                                        3  Months at $ 6.47/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('1', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list us_plan " style="">
                        <form name="hostingplan" id="hostingplan_2" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="us">
                            <input type="hidden" id="planid_id" name="planid" value="2">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Pro <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                8.39
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Unlimited Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="2_duration">

                                    <option value="3">
                                        3  Months at $ 8.39/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('2', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list in_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_3916539" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="linux">
                            <input type="hidden" name="location" value="in">
                            <input type="hidden" id="planid_id" name="planid" value="3916539">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Unlimited Hosting Plan <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                4.61
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Single Domain
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="3916539_duration">

                                    <option value="12">
                                        1  Year at $ 4.61/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('3916539', 'linux');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list in_plan plan-featured" style="display: none;">
                        <form name="hostingplan" id="hostingplan_25" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="in">
                            <input type="hidden" id="planid_id" name="planid" value="25">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">MDH-Linux-India Plan2 <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                6.59
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    3 Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="25_duration">

                                    <option value="3">
                                        3  Months at $ 6.59/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('25', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list in_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_24" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="in">
                            <input type="hidden" id="planid_id" name="planid" value="24">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">MDH-Linux-India Plan1 <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                9.23
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Unlimited Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="24_duration">

                                    <option value="3">
                                        3  Months at $ 9.23/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('24', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list uk_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_3916543" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="linux">
                            <input type="hidden" name="location" value="uk">
                            <input type="hidden" id="planid_id" name="planid" value="3916543">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Unlimited Hosting Plan <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                5.27
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Single Domain
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="3916543_duration">

                                    <option value="12">
                                        1  Year at $ 5.27/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('3916543', 'linux');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list uk_plan plan-featured" style="display: none;">
                        <form name="hostingplan" id="hostingplan_48" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="uk">
                            <input type="hidden" id="planid_id" name="planid" value="48">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">MDH-Linux-UK Plan1 <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                9.59
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Unlimited Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="48_duration">

                                    <option value="3">
                                        3  Months at $ 9.59/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('48', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list uk_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_49" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="uk">
                            <input type="hidden" id="planid_id" name="planid" value="49">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">MDH-Linux-UK Plan2 <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                8.57
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    3 Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="49_duration">

                                    <option value="3">
                                        3  Months at $ 8.57/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('49', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list tr_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_3916547" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="linux">
                            <input type="hidden" name="location" value="tr">
                            <input type="hidden" id="planid_id" name="planid" value="3916547">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Platinum Hosting Plan <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                5.27
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Single Domain
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="3916547_duration">

                                    <option value="12">
                                        1  Year at $ 5.27/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('3916547', 'linux');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list tr_plan plan-featured" style="display: none;">
                        <form name="hostingplan" id="hostingplan_113" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="tr">
                            <input type="hidden" id="planid_id" name="planid" value="113">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Biz <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                5.27
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    3 Domains
                                </strong></li>
                            <li class="p-feat">
                                15 GB Disk Space
                            </li>
                            <li class="p-feat">
                                100 GB Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="113_duration">

                                    <option value="3">
                                        3  Months at $ 5.27/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('113', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list tr_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_114" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="tr">
                            <input type="hidden" id="planid_id" name="planid" value="114">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Pro <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                7.91
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    10 Domains
                                </strong></li>
                            <li class="p-feat">
                                Unlimited Disk Space
                            </li>
                            <li class="p-feat">
                                Unlimited Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="114_duration">

                                    <option value="3">
                                        3  Months at $ 7.91/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('114', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list hk_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_3916544" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="linux">
                            <input type="hidden" name="location" value="hk">
                            <input type="hidden" id="planid_id" name="planid" value="3916544">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Personal Hosting Plan <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                12.00
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    Single Domain
                                </strong></li>
                            <li class="p-feat">
                                15 GB Disk Space
                            </li>
                            <li class="p-feat">
                                50 GB Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="3916544_duration">

                                    <option value="12">
                                        1  Year at $ 12.00/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('3916544', 'linux');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list hk_plan plan-featured" style="display: none;">
                        <form name="hostingplan" id="hostingplan_110" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="hk">
                            <input type="hidden" id="planid_id" name="planid" value="110">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Pro <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                14.90
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    10 Domains
                                </strong></li>
                            <li class="p-feat">
                                15 GB Disk Space
                            </li>
                            <li class="p-feat">
                                50 GB Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="110_duration">

                                    <option value="3">
                                        3  Months at $ 14.90/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('110', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>
                    <ul class="plan-list hk_plan " style="display: none;">
                        <form name="hostingplan" id="hostingplan_109" method="POST">
                            <input type="hidden" name="action" value="add">
                            <input type="hidden" name="type" id="type_id" value="multidomainhosting">
                            <input type="hidden" name="location" value="hk">
                            <input type="hidden" id="planid_id" name="planid" value="109">
                            <input type="hidden" name="domain_name" value="">
                            <input type="hidden" name="otherdomain" value="">

                            <input type="hidden" name="orderid" value="">
                            <input type="hidden" name="upgrade" value="">
                            <input type="hidden" name="upgradeprice" value="">
                            <input type="hidden" name="old_plan_name" value="">
                            <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                            <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">

                            <li class="p-name">
                                <span class="pl-title">Biz <b></b></span>
                            </li>
                            <li class="p-pricing">
                                <small class="p-currency">
                                    $
                                </small>
                                7.51
                                <small class="p-duration">/MO</small>
                            </li>
                            <li class="p-feat"><strong>
                                    3 Domains
                                </strong></li>
                            <li class="p-feat">
                                5 GB Disk Space
                            </li>
                            <li class="p-feat">
                                15 GB Data Transfer
                            </li>
                            <li class="p-feat last">
                                Unlimited Email Accounts
                            </li>
                            <li class="p-dropdown">
                                <select name="duration" id="109_duration">

                                    <option value="3">
                                        3  Months at $ 7.51/month
                                    </option>
                                </select>
                            </li>
                            <li class="p-button">
                                <a class="txt-button" onclick="populate_hostingplan_with_domain_form('109', 'multidomainhosting');
                    return false;">Buy Now</a>
                            </li>
                            <span class="p-cutout"></span>
                        </form>
                    </ul>

                </div>

                <script type="text/javascript">var hosting_plan_preferred_server_location = 'us';</script>
                <script type="text/javascript" src="//cdnassets.com//ui/supersite/en/js/hosting_plans_common_functions.js?t=1407752289"></script>
            </div>
        </div>

        <div class="row-white row-indent"> 

            <!-- addon bar -->
            <h2 class="ui-subheading centertext">Install these softwares in just 1 - click!</h2>
            <div class="addons-bar"><img src="//cdnassets.com/getImage.php?url=webhost.mavajsunco.com&amp;src=addons.gif&amp;t=1378478507"></div>
            <!-- addon bar -->
            <div class="div-spacer">&nbsp;</div>
            <div class="switch-nav">
                <div class="tab-wrp tab1" id="tabs">
                    <div class="first" id="felist"><a class="fea-active">Features</a></div>
                    <div class="last" id="faqlist"><a class="">FAQs</a></div>
                </div>
            </div>
            <!-- Features/Faq -->
            <div class="features-wrp" id="fea-list" style="display: block;">

                <!-- Features -->
                <div class="features lfloat">

                    <!-- list -->
                    <div class="list-cont ic-1 first" data-hosting="default" style="display: block;">
                        <h3>Unlimited Everything!</h3>
                        <ul class="features">
                            <li>Unlimited Disk Space</li>
                            <li>Unlimited Bandwidth</li>
                            <li>Unlimited Domains &amp; Sub-Domains</li>
                            <li>Unlimited Email Accounts, Forwarders, Auto Responders</li>
                            <li>Unlimited FTP Accounts</li>
                            <li>Unlimited MySQL Databases</li>
                        </ul>
                    </div>
                    <div class="list-cont ic-1 first" data-hosting="hk" style="display: none;">
                        <h3>Additionally, with every plan!</h3>
                        <ul class="features">
                            <li>Unlimited Forwarders and Auto Responders</li>
                            <li>Unlimited MySQL Databases</li>
                            <li>5+ FTP Accounts</li>
                        </ul>
                    </div>
                    <!-- / -->
                    <!-- list -->
                    <div class="list-cont ic-2-cpanel">
                        <h3>Powerful Control Panel - cPanel <span style="font-size: 12px;"><a href="/content.php?action=demo&amp;type=lhbus" target="_blank">(View Demo)</a></span></h3>

                        <ul class="features">
                            <li>World's Leading Hosting Control Panel</li>
                            <li>Manage Web Pages, View Website Statistics, Create Email Accounts etc.</li>
                        </ul>
                    </div>
                    <!-- / -->

                    <!-- list -->
                    <div class="list-cont ic-3">
                        <h3>Reliable Support</h3>
                        <ul class="features">
                            <li>99.9% Uptime Guarantee</li>
                            <li>30 Day Money Back Guarantee</li>
                            <li>24x7x365 Support</li>
                        </ul>
                    </div>
                    <!-- / -->

                    <!-- list -->
                    <div class="list-cont ic-4">
                        <h3>Robust Infrastructure</h3>
                        <ul class="features">
                            <li>State-of-the-art Datacenters</li>
                            <li>Dual Quad-Core Xeon powered Servers</li>
                            <li>Redundant Scalable Servers</li>
                        </ul>
                    </div>
                    <!-- / -->

                    <!-- list -->
                    <div class="list-cont ic-5">
                        <h3>Advanced Email Hosting Included</h3>
                        <ul class="features">
                            <li>Unlimited POP3 Email Accounts with SMTP</li>
                            <li>IMAP Support</li>
                            <li>Easy to use Webmail Interface</li>
                        </ul>
                    </div>
                    <!-- / -->

                    <!-- list -->
                    <div class="list-cont ic-6">
                        <h3>eCommerce ready</h3>
                        <ul class="features">
                            <li>Private SSL Available</li>
                            <li>Add-on Dedicated IP</li>
                            <li>Supports osCommerce, CubeCart, Zen Cart</li>
                        </ul>
                    </div>
                    <!-- / -->

                </div>
                <!-- / -->
                <!-- sidebar -->
                <div class="faq lfloat">

                    <h4>Server Specifications</h4>
                    <ul class="bullet-list">
                        <li>Dual E5530 2.40GHz Xeon quad core hyperthreaded processors</li>
                        <li>24 GB RAM</li>
                        <li>250 GB RAID 1 (mirrored) OS drive</li>
                        <li>1 TB RAID 1 (mirrored) customer data drive cached</li>
                        <li>Battery backed, RAID controller for all drives</li>
                        <li>Redundant Power, HVAC &amp; Fire-Detection Systems</li>
                    </ul>

                    <div class="divider">&nbsp;</div>

                    <h4>Programming Support</h4>
                    <ul class="bullet-list">
                        <li>PHP 5.3x, 5.4x, Perl, Python, RoR, GD, cURL, CGI, mcrypt</li>
                        <li>Apache 2.2x</li>
                        <li>MySQL 5</li>
                        <li>Ruby On Rails</li>
                        <li>Zend Optimizer, Zend Engine, ionCube Loader</li>
                    </ul>

                    <div class="divider">&nbsp;</div>

                    <h4>Security &amp; Access</h4>
                    <ul class="bullet-list">
                        <li>Anti Spam &amp; Virus Protection</li>
                        <li>Password Protect Directories</li>
                        <li>Secure FTP Access</li>
                        <li>IP Blocking</li>
                        <li>phpMyAdmin Access</li>
                        <li>Hotlink &amp; Leech Protection</li>
                        <li>Cron Jobs for Scheduled Tasks</li>
                        <li>Customizable Error Page</li>
                        <li>Website Statistics with AWstats</li>
                    </ul>

                    <div class="divider">&nbsp;</div>

                    <h4>Email &amp; Domain Name Features</h4>
                    <ul class="bullet-list">
                        <li>Outlook, Thunderbird and Windows Mail compatible</li>
                        <li>Blackberry, iPhone, Android and PDA support</li>
                        <li>Feature packed Webmail</li>
                        <li>Advance Spam/Virus Protection</li>
                        <li>Mail Forwards, Email Aliases, Auto Responders</li>
                        <li>Unlimited Mailing Lists, Catch All Accounts, Mail Spam Filters</li>
                        <li>Parked Domains</li>
                        <li>Free DNS Management</li>
                        <li>Domain Forwarding, Path Forwarding, URL Masking</li>
                    </ul>


                </div>
                <!-- / -->

            </div>


            <!-- Faqs -->
            <div class="faqs-wrp" id="que-ans" style="display: none;">

                <div class="que">Q. What is Shared Web hosting?</div>
                <div class="ans">
                    In Shared Web Hosting, multiple clients are hosted on a single server i.e. the clients share the server's resources. This helps reduce the cost, since the cost of the server and its resources are spread over all the clients/packages hosted on the server. Shared Hosting is perfect for personal websites, small and mid-sized businesses that do not require all the resources of a server.
                </div>

                <div class="que">Q. Can I host multiple Web sites within one Shared Hosting plan?</div>
                <div class="ans">
                    Yes! Our Pro and Business shared hosting plans allow you to host more than one Website, by adding secondary domains through your hosting control panel i.e. cPanel.
                </div>

                <div class="que">Q. Is there a Money Back Guarantee?</div>
                <div class="ans">
                    Yes, we offer a 100% Risk Free, 30 day Money Back Guarantee.
                </div>

                <div class="que">Q. Is Email hosting included in my package?</div>
                <div class="ans">
                    Yes, all our Hosting packages come with Unlimited Email Hosting.
                </div>

                <div class="que">Q. Can I upgrade to a higher plan?</div>
                <div class="ans">
                    Yes, you can easily upgrade to one of our higher plans at any time.
                </div>

                <div class="que">Q. Is my data safe? Do you take backups?</div>
                <div class="ans">
                    Yes, your data is a 100% secure and is backed-up every 5 days.
                </div>

                <div class="que">Q. Do you include protection from viruses?</div>
                <div class="ans">
                    Yes, all our servers are protected by Clam AV.
                </div>

                <div class="que">Q. Can I divide my Shared Hosting package and resell it?</div>
                <div class="ans">
                    While a Shared Hosting package cannot be used for this purpose, you can easily resell custom packages with our Reseller Hosting. To view our Reseller Hosting plans, <a href="/reseller-hosting.php"> click here. </a>
                </div>

                <div class="que">Q. Do you offer SSH access?</div>
                <div class="ans">
                    No, at the moment, we do not provide ssh access. However, most of the tasks can be easily achieved using your Hosting Control Panel - cPanel.
                </div>

                <div class="que">Q. Who do I get in touch with if I need help?</div>
                <div class="ans">
                    Our Support team is always at hand to assist you. You can take a look at all our contact details <a href="/support/contact-us.php">here</a>.
                </div>

            </div>
            <!-- /Faqs -->

            <div class="clear"></div>
            <!-- Features/Faq -->


        </div>

        <script>
            $('#felist').click(function () {
                $('#tabs').addClass("tab1")
                $('#tabs').removeClass("tab2")
                $("#felist a").addClass("fea-active");
                $('#faqlist a').removeClass("faq-active");
                $('#fea-list').show();
                $('#que-ans').hide();
            });
            $('#faqlist').click(function () {
                $('#tabs').addClass("tab2")
                $('#tabs').removeClass("tab1")
                $("#felist a").removeClass("fea-active");
                $('#faqlist a').addClass("faq-active");
                $('#fea-list').hide();
                $('#que-ans').show();
            });

            function populate_hostingplan_with_domain_form(obj, type) {
                plan_id = escapeStr(obj);
                var form_name = 'hostingplan_' + plan_id;
                var location = $('#' + form_name + ' input[name="location"]').val();
                var planid = $('#' + form_name + ' input[name="planid"]').val();
                var duration = $('#' + form_name + ' select[name="duration"]').val();
                $("#hostingplan_with_domain input[name='location']").val(location);
                $("#hostingplan_with_domain input[name='type']").val(type);
                $("#hostingplan_with_domain input[name='planid']").val(planid);
                $("#hostingplan_with_domain input[name='duration']").val(duration);
                showModal();
            }
            $('.features-group').click(function () {
                $(this).toggleClass("closed");
                $(this).next('ul').slideToggle('slow');
            });
        </script>
        <!-- <[/HOSTING PLANS PAGE]> -->

        <div id="modal_div" class="modal-wrapper" style="display:none">
            <div class="modal_overlay" style="width: 1349px; height: 3271px; background: rgb(0, 0, 0);">
            </div>
            <div id="select-domain-wrapper" class="modal-wrapper">
                <div id="select-domain-modal" class="wide_modal modal_content">

                    <div id="select-domain-content" class="hosting-modal">

                        <div class="inner-content">

                            <h1 class="ms-modal-title">Specify a domain name for your order</h1>

                            <a href="#" onclick="closeModal('modal_div');
                    return false;" class="modal_close"></a>

                            <p class="lfloat">
                                <input type="radio" name="map-domainname" class="option-map-domain" id="existing-domain" value="use_existing_domain_name" checked="checked">
                                <label class="frm-label" for="existing-domain">I already have a Domain Name</label>
                            </p>

                            <p class="lfloat">
                                <input type="radio" name="map-domainname" class="option-map-domain" id="new-domain" value="new_domain_name">
                                <label class="frm-label" for="new-domain">I want to buy a new Domain Name</label>
                            </p>

                            <br class="clear">

                            <div class="use-existing">
                                <form onsubmit="return continue_to_add_order();">
                                    <div>

                                        <input type="text" id="existing_domain_name" class="wide-textbox textbox frm-field" name="domain_name" value="www.">

                                        <div id="error" style="display: none"></div>
                                    </div>
                                </form>
                            </div>

                            <div class="register-new" style="display: none;">
                                <form name="hostingplan_with_domain" id="hostingplan_with_domain" action="/domain-registration/index.php" method="POST" onsubmit="return check_domainname_submit();">
                                    <input type="hidden" name="action" value="check_availability">
                                    <input type="hidden" name="hosting_add" value="true">
                                    <input type="hidden" name="type" value="linux">
                                    <input type="hidden" name="location" value="">
                                    <input type="hidden" id="planid_id" name="planid" value="">
                                    <input type="hidden" name="duration" value="">
                                    <input type="hidden" name="no-of-accounts" value="">
                                    <input type="hidden" name="email_account_range" value="">
                                    <input type="hidden" name="upsell_sitelock" id="upsell_sitelock" value="false">
                                    <input type="hidden" name="upsell_codeguard" id="upsell_codeguard" value="false">
                                    <div class="form-field-wrapper">
                                        <input type="text" id="new-domain-name" class="textbox frm-field" name="txtDomainName">
                                        <div id="domain_error" class="error" style="display: none"></div>
                                    </div>
                                </form>
                            </div>


                            <div class="continue-button-container">
                                <input id="continue_action" type="submit" name="submit" value="Continue to checkout">
                            </div>

                        </div>


                        <div class="gray-bgcolor">
                            <div class="inner-content">
                                <h2>Building a website for your business? Don't risk it - safeguard your website with our must-have website tools</h2>

                                <div class="lfloat purchase-sitelock">
                                    <span class="inline-block sitelock-small-logo"><span class="inline-block"></span></span>
                                    <p>Over 5000 websites get attacked everyday. Get SiteLock and secure your website from hackers, viruses and malware.</p>
                                    <p class="purchase-security"><input type="checkbox" value="" name="sitelock-basic" id="upsell_sitelock_plan" class="inline-block"><label class="inline-block" for="upsell_sitelock_plan">Get SiteLock for just $15.84/yr</label></p>
                                </div>



                                <div class="lfloat purchase-codeguard">
                                    <span class="inline-block codeguard-small-logo"><span class="inline-block"></span></span>
                                    <p>Protect yourself from unexpected website crashes. Add CodeGuard and get automatic cloud backup for your website and database.</p>
                                    <p class="purchase-security"><input type="checkbox" value="" name="codeguard-basic" id="upsell_codeguard_plan" class="inline-block"><label class="inline-block" for="upsell_codeguard_plan">Get CodeGuard for just $1.28/mn</label></p>
                                </div>


                                <br class="clear">
                            </div>
                        </div>    

                    </div>

                </div>

                <div id="hosting_modal_upgrade_id" class="extra_wide_modal modal_content" style="display:none">
                    <div class="hosting-modal">
                        <h1 class="hosting-modal-title">Do you want to upgrade from your existing Email Hosting Plan?</h1>
                        <a href="#" onclick="closeUpgradeConflictDiv();
            closeModal();
            return false;" class="modal_close">Close [x]</a>
                        <div class="hosting-modal-body-upgrade compact-modal-body">
                            <h3 class="sub-title">You will receive a credit for this upgrade</h3>
                            <div class="conflict-resolver">
                                <div class="conflict-blurb">
                                    <h4>Your current plan</h4>
                                    <div class="blurb_body">
                                        <div class="current_plan">
                                            <div id="current_plan_id"><h5></h5></div>
                                            <strong>Valid Till:</strong> <div id="currentplan_valid_till_id"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-separator"></div>
                                <div class="conflict-blurb">
                                    <h4>Your selected plan</h4>
                                    <div class="blurb_body">
                                        <div class="selected_plan">
                                            <div id="new_plan_id"><h5></h5></div>
                                            <div>
                                                <strong>Valid Till:</strong> <div id="newplan_valid_till_id"></div>
                                            </div>
                                            <div>
                                                <strong>Upgrade Price:</strong> <div>$ <del><span id="modified_upgrade_price_id"></span></del>
                                                    <span id="new_upgrade_price_id" class="new-price"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="modal-footer">
                                You will not lose any existing emails/data.
                            </div>
                            <div class="actionRow">
                                <a href="#" onclick="closeUpgradeConflictDiv();
                    return false;" class="left-action">« Choose another domain</a>
                                <button class="ui-button ui-button-2" id="continue_id" onclick="upgrade_product();">
                                    <span><span>
                                            <strong>Continue</strong>
                                            <br>
                                            Upgrade this plan</span></span>
                                </button>
                            </div>
                        </div>

                    </div>

                    <script type="text/javascript">

                        function closeUpgradeConflictDiv() {
                            $("#hosting_modal_upgrade_id").hide();
                            $("#step1-select-domain").show();
                        }
                        function upgrade_product() {
                            $('#' + form_name + ' input[name="action"]').val('add');
                            $('#' + form_name).submit();
                        }
                    </script>
                </div>
                <script type="text/javascript" src="/includes/hosting/hosting_modal_select_domain.js.php"></script>
                <script type="text/javascript" src="//cdnassets.com//ui/supersite/en/js/hosting_modal_select_domain.js?t=1411480245"></script>
            </div>
            <div id="cart_conflict" class="extra_wide_modal modal_content" style="display:none">
                <div class="hosting-modal">
                    <h1 class="hosting-modal-title">You have already added <span id="product_type"></span> product to your cart for this domain name</h1>
                    <a href="#" onclick="closeCartConflictDiv();
        closeModal();
        return false;" class="modal_close">Close [x]</a>
                    <div class="hosting-modal-body">
                        <h3 class="sub-title">You can only buy one Email Hosting, Web Hosting, or Website Builder plan per domain name.</h3>
                        <div class="txt-m">Please select from the two options below.</div>
                        <div class="conflict-chooser">
                            <div class="conflict-blurb">
                                <h4>Currently in your cart</h4>
                                <div class="blurb_body">
                                    <div class="current_plan">
                                        <h5 id="cart_plan_name"></h5>
                                        <div id="cart_details"></div>
                                        <div id="cart_duration_price"></div>
                                        <button class="ui-button" type="button" onclick="select_cart_product();"><span><span>Select this Plan</span></span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="vertical-or-seperator">OR</div>
                            <div class="conflict-blurb">
                                <h4>Your selected plan</h4>
                                <div class="blurb_body">
                                    <div class="selected_plan">
                                        <h5 id="new_plan_name"></h5>
                                        <div id="new_details"></div>
                                        <div id="new_duration_price"></div>
                                        <button class="ui-button" type="button" onclick="select_new_product();"><span><span>Select this Plan</span></span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="modal-footer">
                            <a href="#" onclick="closeCartConflictDiv();
                                 return false;">« Choose another domain</a>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    function closeCartConflictDiv() {
                        $("#cart_conflict").hide();
                        $("#select_domain").show();
                    }

                    function select_new_product() {
                        $('#' + form_name + ' input[name="action"]').val('add');
                        $('#' + form_name).submit();
                    }

                    function select_cart_product() {
                        location.href = '/checkout.php';
                    }


                    function populate_details_in_conflict_modal(cart_product_details, new_product_details) {
                        $("#cart_plan_name").html(cart_product_details['plan_name']);
                        $("#cart_details").html(cart_product_details['details']);
                        $("#cart_duration_price").html(cart_product_details['duration_pricing']);
                        $("#product_type").html(cart_product_details['type']);
                        $("#new_plan_name").html(new_product_details['plan_name']);
                        $("#new_details").html(new_product_details['details']);
                        $("#new_duration_price").html(new_product_details['duration_pricing']);

                        if (cart_product_details['type'] == "Web Hosting" || cart_product_details['type'] == "Website Builder") {
                            $(".current_plan").css("background", "transparent url('//cdnassets.com/getImage.php?url=webhost.mavajsunco.com&src=conflict_modal_icons.gif&t=1378478507') no-repeat 6px -190px")
                        }

                        if (new_product_details['type'] == "Email Hosting") {
                            $(".selected_plan").css("background", "transparent url('//cdnassets.com/getImage.php?url=webhost.mavajsunco.com&src=conflict_modal_icons.gif&t=1378478507') no-repeat 6px 0")
                        }
                    }
                </script>
            </div>
            <div style="display:none">
                <form name="upgradeform" action="/upgrade.php" id="upgradeform" method="POST">
                    <input type="hidden" name="action" value="upgrade">
                    <input type="hidden" name="order_id" value="">
                    <input type="hidden" name="type" id="type_id" value="">
                    <input type="hidden" id="planid_id" name="planid" value="">
                    <input type="hidden" name="domain_name" value="">
                </form>

            </div>
        </div>
        <!-- ### Template /products/hosting/web_email_hosting/linux_hosting_plans.html ends here ### -->

    </div>
</div>

<?php
//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?> 