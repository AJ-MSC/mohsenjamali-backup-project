<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<div id="page-container">
    <div id="page-wrapper">

        <!-- ### Template /products/hosting/vps_hosting/vps_hosting_plans.html starts here ### -->
        <div id="plans-masthead" class="vps-masthead">

            <h2 class="vps-heading">VPS HOSTING</h2>
            <div class="vps-features">
                <!--<span>99.99% Server Uptime Guarantee</span>&nbsp; | &nbsp;<span>Fully Managed Support</span>&nbsp; | &nbsp;<span>Advanced SAN Based storage</span>-->      
            </div> 
            <div class="vps-powered-by-wp"> 
                <div class="vps-powered-by">
                    POWERED BY
                </div> 
            </div>

        </div>





        <div class="row-white row-indent">


            <h2 class="ui-subheading ca">Buy VPS Hosting in 4 easy Steps !!</h2>       


            <div id="steps-wrp">
                <div id="vps-step-1" class="vps-step-1"></div>                   
                <div id="vps-step-2" class="vps-step-2"></div>  
                <div class="vps-step-bar1"></div>
                <div class="vps-step-bar2"></div>

                <ul>
                    <li class="vps-s1 sel">Select Plan</li>
                    <li class="vps-s2">Select Add-Ons</li>
                    <li class="vps-s3">Domain Name</li>
                    <li class="vps-s4">Checkout</li>
                </ul>
            </div>

            <br class="clear">

            <div class="server_loc_tabs" id="vps-new-tabs">
                <label for="server-location">Select server location:</label>
                <ul class="country_specific_tabs">

                    <li class="tab_us sel" data-country="us">
                        us
                    </li>
                    <li class="tab_in" data-country="in">
                        in
                    </li>
                </ul>

                <div class="vps-tip" data-country="ae">
                    <p class="hide" data-country="in">TIP: Is your website audience from India? Get 10x faster speeds by choosing our India servers <a href="#" onclick="showModal('modal_div')">Learn More</a></p>
                    <p class="hide" data-country="us">TIP: Is your website audience from USA? Get 10x faster speeds by choosing our USA servers <a href="#" onclick="showModal('modal_div')">Learn More</a></p>
                </div>
            </div>

            <br class="clear">

            <div class="vpsMainWrapper" style="margin-top: 15px; height: 645px;">
                <div id="visible" class="vpsWrapper">
                    <div id="plans" class="innerDiv">
                        <div id="plans-container" class="vps-container">
                            <table class="pricing-tbl">
                                <tbody><tr>
                                        <th class="w">Plans</th>
                                        <th class="w">CPU</th>
                                        <th class="w">RAM</th>
                                        <th class="w">Disk Space</th>
                                        <th class="w">Bandwidth</th>
                                        <th class="w1">Price</th>
                                        <th class="w2">&nbsp;</th>
                                    </tr>
                                </tbody></table>

                            <form name="vpshosting_vps_1" id="vpshosting_us_vps_1" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_1">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 1
                                    </td>
                                    <td class="p-feat w">
                                        0.88 GHz
                                    </td>
                                    <td class="p-feat w">
                                        512 MB
                                    </td>
                                    <td class="p-feat w">
                                        20 GB
                                    </td>
                                    <td class="p-feat w">
                                        500 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 26.40/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_1');">
                                                        1  Month at <span> $ 26.40/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_1');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_2" id="vpshosting_us_vps_2" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_2">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 2
                                    </td>
                                    <td class="p-feat w">
                                        1.17 GHz
                                    </td>
                                    <td class="p-feat w">
                                        768 MB
                                    </td>
                                    <td class="p-feat w">
                                        30 GB
                                    </td>
                                    <td class="p-feat w">
                                        600 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 38.28/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_2');">
                                                        1  Month at <span> $ 38.28/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_2');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_3" id="vpshosting_us_vps_3" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_3">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 3
                                    </td>
                                    <td class="p-feat w">
                                        1.47 GHz
                                    </td>
                                    <td class="p-feat w">
                                        1 GB
                                    </td>
                                    <td class="p-feat w">
                                        40 GB
                                    </td>
                                    <td class="p-feat w">
                                        800 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 54.12/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_3');">
                                                        1  Month at <span> $ 54.12/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_3');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_4" id="vpshosting_us_vps_4" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_4">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 4
                                    </td>
                                    <td class="p-feat w">
                                        1.96 GHz
                                    </td>
                                    <td class="p-feat w">
                                        1.5 GB
                                    </td>
                                    <td class="p-feat w">
                                        60 GB
                                    </td>
                                    <td class="p-feat w">
                                        1000 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 72.60/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_4');">
                                                        1  Month at <span> $ 72.60/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_4');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_5" id="vpshosting_us_vps_5" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_5">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 5
                                    </td>
                                    <td class="p-feat w">
                                        2.43 GHz
                                    </td>
                                    <td class="p-feat w">
                                        2 GB
                                    </td>
                                    <td class="p-feat w">
                                        80 GB
                                    </td>
                                    <td class="p-feat w">
                                        1500 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 91.08/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_5');">
                                                        1  Month at <span> $ 91.08/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_5');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_6" id="vpshosting_us_vps_6" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_6">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 6
                                    </td>
                                    <td class="p-feat w">
                                        3.20 GHz
                                    </td>
                                    <td class="p-feat w">
                                        2.5 GB
                                    </td>
                                    <td class="p-feat w">
                                        100 GB
                                    </td>
                                    <td class="p-feat w">
                                        2000 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 125.40/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_6');">
                                                        1  Month at <span> $ 125.40/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_6');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_8" id="vpshosting_us_vps_8" class="vpshostingplans_us" method="POST" style="display: block;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="us">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_8">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 8
                                    </td>
                                    <td class="p-feat w">
                                        6.40 GHz
                                    </td>
                                    <td class="p-feat w">
                                        5 GB
                                    </td>
                                    <td class="p-feat w">
                                        200 GB
                                    </td>
                                    <td class="p-feat w">
                                        4000 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 196.68/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'us_vps_8');">
                                                        1  Month at <span> $ 196.68/mo</span>
                                                    </li>
                                                    <li class="" value="3" onclick="set_duration(this, 'us_vps_8');">
                                                        3  Months at <span> $ 196.68/mo</span>
                                                    </li>
                                                    <li class="" value="6" onclick="set_duration(this, 'us_vps_8');">
                                                        6  Months at <span> $ 196.68/mo</span>
                                                    </li>
                                                    <li class="" value="12" onclick="set_duration(this, 'us_vps_8');">
                                                        1  Year at <span> $ 196.68/mo</span>
                                                    </li>
                                                    <li class="" value="24" onclick="set_duration(this, 'us_vps_8');">
                                                        2  Years at <span> $ 196.68/mo</span>
                                                    </li>
                                                    <!--<li class="selected=" selected""="" value="36" onclick="set_duration(this, 'us_vps_8');">-->
                                                        3  Years at <span> $ 196.68/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('us_vps_8');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_1" id="vpshosting_in_vps_1" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_1">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 1
                                    </td>
                                    <td class="p-feat w">
                                        0.88 GHz
                                    </td>
                                    <td class="p-feat w">
                                        512 MB
                                    </td>
                                    <td class="p-feat w">
                                        20 GB
                                    </td>
                                    <td class="p-feat w">
                                        100 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 19.79/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_1');">
                                                        1  Month at <span> $ 19.79/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_1');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_2" id="vpshosting_in_vps_2" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_2">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 2
                                    </td>
                                    <td class="p-feat w">
                                        1.17 GHz
                                    </td>
                                    <td class="p-feat w">
                                        768 MB
                                    </td>
                                    <td class="p-feat w">
                                        30 GB
                                    </td>
                                    <td class="p-feat w">
                                        150 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 26.39/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_2');">
                                                        1  Month at <span> $ 26.39/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_2');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_3" id="vpshosting_in_vps_3" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_3">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 3
                                    </td>
                                    <td class="p-feat w">
                                        1.47 GHz
                                    </td>
                                    <td class="p-feat w">
                                        1 GB
                                    </td>
                                    <td class="p-feat w">
                                        40 GB
                                    </td>
                                    <td class="p-feat w">
                                        200 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 32.99/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_3');">
                                                        1  Month at <span> $ 32.99/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_3');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_4" id="vpshosting_in_vps_4" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_4">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 4
                                    </td>
                                    <td class="p-feat w">
                                        1.96 GHz
                                    </td>
                                    <td class="p-feat w">
                                        1.5 GB
                                    </td>
                                    <td class="p-feat w">
                                        60 GB
                                    </td>
                                    <td class="p-feat w">
                                        300 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 52.79/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_4');">
                                                        1  Month at <span> $ 52.79/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_4');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_5" id="vpshosting_in_vps_5" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_5">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 5
                                    </td>
                                    <td class="p-feat w">
                                        2.43 GHz
                                    </td>
                                    <td class="p-feat w">
                                        2 GB
                                    </td>
                                    <td class="p-feat w">
                                        80 GB
                                    </td>
                                    <td class="p-feat w">
                                        400 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 72.59/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_5');">
                                                        1  Month at <span> $ 72.59/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_5');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_6" id="vpshosting_in_vps_6" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_6">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 6
                                    </td>
                                    <td class="p-feat w">
                                        3.20 GHz
                                    </td>
                                    <td class="p-feat w">
                                        2.5 GB
                                    </td>
                                    <td class="p-feat w">
                                        100 GB
                                    </td>
                                    <td class="p-feat w">
                                        500 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 92.39/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_6');">
                                                        1  Month at <span> $ 92.39/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_6');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                            <form name="vpshosting_vps_8" id="vpshosting_in_vps_8" class="vpshostingplans_in" method="POST" style="display: none;">
                                <table class="pricing-tbl">
                                    <tbody><tr>
                                    <input type="hidden" name="action" value="choose_parameters">
                                    <input type="hidden" name="type" id="type_id" value="vpslinux">
                                    <input type="hidden" name="location" value="in">
                                    <input type="hidden" name="planid" id="planid_id" value="vps_8">
                                    <input type="hidden" name="duration" id="plan_duration" value="1">
                                    <td class="p-name w">
                                        VPS 8
                                    </td>
                                    <td class="p-feat w">
                                        6.40 GHz
                                    </td>
                                    <td class="p-feat w">
                                        5 GB
                                    </td>
                                    <td class="p-feat w">
                                        200 GB
                                    </td>
                                    <td class="p-feat w">
                                        1000 GB
                                    </td>
                                    <td class="p-dropdown w1">
                                        <div class="price-dropdown-wrp">
                                            <div class="dropdown-value"> $ 112.19/mo</div>
                                            <div class="price_dropdown">  
                                                <span class="up-arr"></span>
                                                <ul name="duration_dropdown">

                                                    <li class="sel" value="1" onclick="set_duration(this, 'in_vps_8');">
                                                        1  Month at <span> $ 112.19/mo</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </td>
                                    <td class="p-button w2">
                                        <a class="txt-button" onclick="submit_vpshosting_form('in_vps_8');
                      return false;">
                                            Buy Now
                                        </a>
                                    </td>
                                    </tr>
                                    </tbody></table>
                            </form>
                        </div>

                        <!--[if IE]> 
                        <script type="text/javascript">
                        $(function() {
                        var zIndexNumber = 1000;
                        $('.vps-container div').each(function() {
                        $(this).css('zIndex', zIndexNumber);
                        zIndexNumber -= 10;
                        });
                        });
                        </script>
                        <![endif]-->

                        <script type="text/javascript">
                            function set_duration(obj, planid) {
                                $('#vpshosting_' + planid + ' input[name=duration]').val(obj.value);
                            }

                            $('.price-dropdown-wrp').hover(function () {
                                $(this).find('.price_dropdown').css("display", "block");
                            }, function () {
                                $(this).find('.price_dropdown').css("display", "none");
                            });

                            $('.price-dropdown-wrp li').click(function () {
                                $('.price_dropdown').css("display", "none");
                                $(this).siblings().removeClass('sel');
                                $(this).addClass('sel');
                                var t = $(this).find('span').html();
                                $(this).closest('.price-dropdown-wrp').find('.dropdown-value').html(t);
                            });


                        </script>

                    </div>

                    <div id="params" class="innerDiv"></div>

                    <div id="summary" class="innerDiv"></div>
                </div>
            </div>

            <div class="switch-nav" id="adj-cont">
                <div class="tab-wrp tab1" id="tabs">
                    <div class="first" id="felist"><a class="fea-active">
                            Features
                        </a></div>
                    <div class="last" id="faqlist"><a>
                            FAQs
                        </a></div>
                </div>
            </div>
            <!-- Features/Faq -->
            <div class="features-wrp" id="fea-list">
                <!-- Features -->
                <div class="features lfloat">
                    <!-- list -->
                    <div class="list-cont ic-vps-1 first">
                        <h3>
                            First Class Hardware
                        </h3>
                        <ul class="features">
                            <li>
                                State-of-Art datacenter
                            </li>
                            <li>
                                High IO (10k+ iops sustained) &amp; Low Latency SAN utilizing enterprise grade hard drives in Raid
                                DP

                            </li>
                            <li>
                                Deployed in pairs for real-time data replication
                            </li>
                        </ul>
                    </div>
                    <!-- / -->
                    <!-- list -->
                    <div class="list-cont ic-3">
                        <h3>
                            Managed Support
                        </h3>
                        <ul class="features">				
                            <li>Container boot related issues</li>
                            <li>Investigating Network related issues</li>
                            <li>Hardware related issues</li>
                            <li>Issue with deployment of VPS</li>
                            <li>Investigation of any hacking attempts. Any findings and possible solutions will be reported.</li>
                            <li>Setup and re-installation of VPS</li>
                            <li>Core OS updates and patches</li>
                            <li>Container login issues</li>                
                            <li>Reverse DNS setup (since we have the IPs control panel)</li>
                            <li>Installation and support of core software packages</li>
                            <li>Initial installation and basic firewall setup</li>
                            <li>WHMCS installation only </li>
                            <li class="sub-features">
                                <strong class="sub-point">Additionally, with cPanel</strong>
                                <ul class="has-exp-col">
                                    <li class="exp-col">Installation related
                                        <ul>
                                            <li>Initial installation</li>
                                            <li>Initial service optimization</li>
                                            <li>Assistance with version upgrade</li>
                                            <li>Assistance with Repair of cPanel installation</li>
                                            <li>Troubleshooting of automated update and/or backup creation task</li>
                                        </ul>                        
                                    </li>
                                    <li class="exp-col">Mail services
                                        <ul>
                                            <li>Upgrade/reinstall exim MTA</li>
                                            <li>Upgrade/reinstall courier-imap POP3 and IMAP service</li>
                                            <li>Install/upgrade/reinstall/uninstall SpamAssassin</li>
                                            <li>Install/upgrade/reinstall/uninstall ClamAV antivirus</li>
                                            <li>Troubleshoot and repair mail delivery problems
                                                (note: problems caused by 3rd party mail scanning, filtering, products are not supported, custom delivery rules or ACLs are not supported)
                                                Troubleshoot standard Horde,roundcube and Squirrelmail webmail clients</li>
                                        </ul>                        
                                    </li>
                                    <li class="exp-col">Web services
                                        <ul>
                                            <li>Apache web server recompilation</li>
                                            <li>PHP upgrade or recompilation with additional options/modules directly supported by cPanel's easyapache script</li>
                                            <li>PHP configuration modification</li>
                                            <li>ionCube installation/removal/upgrade</li>
                                            <li>Zend Optimizer installation/removal/upgrade</li>
                                            <li>eAccelerator installation/removal/upgrade</li>
                                            <li>Troubleshoot and repair Apache/PHP installation and/or configuration (note: custom Apache configuration or modules are not supported)</li>
                                        </ul>                        	
                                    </li>
                                    <li class="exp-col">Database Services(MySQL)
                                        <ul>
                                            <li>Upgrade MySQL version</li>
                                            <li>Repair MySQL installation</li>
                                            <li>Troubleshoot MySQL startup or connectivity issues</li>
                                        </ul>                        	
                                    </li>
                                    <li class="exp-col">Nameservers or DNS service
                                        <ul>
                                            <li>Repair/reinstall named</li>
                                            <li>Named configuration troubleshooting</li>
                                        </ul>
                                    </li>
                                    <li class="exp-col">FTP Service
                                        <ul>
                                            <li>Upgrade/reinstall proftpd or pure-ftpd FTP server</li>
                                            <li>FTP service configuration troubleshooting (note: custom configuration rules or custom FTP server modules are not supported)</li>
                                        </ul>                        	
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <!-- / -->
                    <!-- list -->
                    <div class="list-cont ic-vps-2">
                        <h3>
                            99.99% Guaranteed Uptime
                        </h3>
                        <ul class="features">
                            <li>
                                Highly redudant SAN powered devices
                            </li>
                            <li>
                                99.99% Guaranteed server uptime
                            </li>
                            <li>
                                99.999% Guaranteed storage uptime
                            </li>
                        </ul>
                    </div>
                    <!-- / -->
                    <!-- list -->
                    <div class="list-cont ic-vps-3">
                        <h3>
                            Powerful and Customizable Addons
                        </h3>
                        <ul class="features">
                            <li>
                                WHM/cPanel
                            </li>
                            <li>
                                WHMCS
                            </li>
                            <li>
                                Free 1 Dedicated IP
                            </li>
                            <li>
                                Many more to come..
                            </li>
                        </ul>
                    </div>
                    <!-- / -->
                </div>
                <!-- / -->
                <!-- sidebar -->
                <div class="faq lfloat">
                    <h4>
                        Summary Of Our Product
                    </h4>
                    <div class="ui-space"></div>
                    <ul class="bullet-list">
                        <li><h3>Server Hardware</h3></li>
                        <ul class="bullet-list-sub">
                            <li>Dell R610, Dual Intel Xeon 3.06GHz Six Core hyper threaded processors with 12M Cache</li>
                            <li>72 GB RAM</li>
                            <li>2x250GB Local HDD in RAID 1</li>
                            <li>Battery backed, RAID controller for all drives</li>
                            <li>Redundant Power, HVAC &amp; Fire-Detection Systems</li>
                        </ul>
                        <li><h3>NetApp Device</h3></li>
                        <ul class="bullet-list-sub">
                            <li>FAS 3240</li>
                            <li>Enterprise grade service</li>
                            <li>99.999% uptime</li>
                            <li>RAID DP</li>
                        </ul>

                        <li><h3>Software</h3></li>
                        <ul class="bullet-list-sub">
                            <li>CentOS 6 64-bit</li>
                            <li>Parallels Virtuozzo Containers (PVC) v4.7</li>
                        </ul>
                        <div class="divider">&nbsp;</div>
                        <li><h3>Goodies And Other Info</h3></li>
                        <ul class="bullet-list-sub">
                            <li>SSH with full root access</li>
                            <li>Free 1 dedicated ip with every package</li>
                            <li>Automatic failovers</li>
                            <li>Full control over local firewall settings for your package</li>
                        </ul>
                    </ul>
                </div>
                <!-- / -->
            </div>
            <!-- Faqs -->
            <div class="faqs-wrp" id="que-ans" style="display:none;">
                <div class="que">
                    Q. What is Virtual Private Server (VPS) Hosting?
                </div>
                <div class="ans">
                    VPS Hosting utilizes a technology called Virtualization, to create isolated an isolated Virtual Machines for
                    you with dedicated resources and full root access. It gives you all the features and functions of Dedicated
                    server, without the additional cost.

                </div>
                <div class="que">
                    Q. What are the advantages of VPS Hosting?
                </div>
                <div class="ans">
                    With VPS Hosting, you have complete isolation. This means that no matter what other users might be doing on
                    the server, your VPS package will be unaffected. With root access, you can install whatever applications you
                    require. VPS also gives you guaranteed resources, so no matter what, the CPU,RAM, HDD and Bandwidth allocated
                    to your VPS package will always be available for your applications.

                </div>
                <div class="que">
                    Q. What virtualization software do you use?
                </div>
                <div class="ans">
                    We use Parallels Virtuozzo Containers (PVC).
                </div>
                <div class="que">
                    Q. What kind of software can I install on my VPS package?
                </div>
                <div class="ans">
                    As you have full root access, you may install any compatible software on your VPS package. However, any
                    software that violates our Acceptable Usage Policy will not be allowed.

                </div>
                <div class="que">
                    Q. Can I install cPanel or Plesk on my VPS package?
                </div>
                <div class="ans">
                    Yes. We have automated installation process configured, so if you choose to buy cPanel for your VPS package,
                    it will be automatically installed for you.

                </div>
                <div class="que">
                    Q. Can I upgrade between VPS plans?
                </div>
                <div class="ans">
                    Yes, you can upgrade your existing plan to a higher plan at any time.
                </div>
                <div class="que">
                    Q. What kind of support do you offer?
                </div>
                <div class="ans">
                    We offer Fully Managed VPS packages. For a list of items that we support, please refer to the features
                    above

                </div>
            </div>
            <!-- /Faqs -->
            <div class="clear"></div>
            <!-- Features/Faq -->

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.exp-col').click(function () {
                        $(this).children('ul').slideToggle(200);
                        $(this).toggleClass('active');
                    });

                });
            </script>
        </div>
        <div id="modal_div" class="modal-wrapper" style="display:none">
            <div class="modal_overlay"></div>
            <div id="learn-more-vps-india" class="wide_modal modal_content">
                <div class="hosting-modal" id="vps-india-info">
                    <div id="hosting_modal_body_id" class="hosting-modal-body">
                        <div class="modalcontent">
                            <h2>Make your website faster by selecting a server close to your users location. Here's how:</h2>
                            <a href="#" onclick="closeModal();
                            return false;" class="modal_close"></a>
                            <p>When a user types your website into a browser, the request travels all the way from her computer to your server and back. The time taken for this is called Latency.</p>
                            <p>Latency is a big reason for the slow performance of a website. In fact, in our internal tests we've found that latency can be as much as 30 times higher for a poorly setup website.</p>
                            <p>The good news is that fixing such issues requires only prudence – simply ensure that your website is hosted on a server closest to where most of your customers are. The faster your website loads, the fewer customers leave your website and the more business you do.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $('.country_specific_tabs li').on('click', function () {
                var country = $(this).data('country');
                var user_country = $('.vps-tip').data('country');

                $('.vpsMainWrapper').animate({"margin-top": "15px"}, "0");
                $('.vps-tip p').slideUp('slow');
                if ((country !== user_country) && ($('.vps-tip p[data-country="' + user_country + '"]').length > 0))
                {
                    $('.vpsMainWrapper').animate({"margin-top": "20px"}, "0");
                    $('.vps-tip p[data-country="' + user_country + '"]').slideDown('slow');

                }

                $('#plans-container form').hide();
                $('.vpshostingplans_' + country).show();
                $('.country_specific_tabs li').removeClass('sel');
                $(this).addClass('sel');

                var addHeight = PlanTableHeightAdjustment();

                var StorePlansHeight = $("#plans").height() + addHeight;
                $(".vpsMainWrapper").animate({"height": StorePlansHeight}, "6000");
            });

            $('.country_specific_tabs li:first').click();

            $('#felist').click(function () {
                $('#tabs').addClass("tab1")
                $('#tabs').removeClass("tab2")
                $("#felist a").addClass("fea-active");
                $('#faqlist a').removeClass("faq-active");
                $('#fea-list').show();
                $('#que-ans').hide();
            });
            $('#faqlist').click(function () {
                $('#tabs').addClass("tab2")
                $('#tabs').removeClass("tab1")
                $("#felist a").removeClass("fea-active");
                $('#faqlist a').addClass("faq-active");
                $('#fea-list').hide();
                $('#que-ans').show();
            });

            function submit_vpshosting_form(obj) {
                plan_id = escapeStr(obj);
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data: $('#' + 'vpshosting_' + plan_id).serialize(),
                    url: "/vps-hosting.php",
                    beforeSend: function () {
                    },
                    success: function (result) {
                        var step = $('#' + 'vpshosting_' + plan_id + ' input[name=action]').val();
                        if (result == null) {
                            alert("Unable to process your request. Please try again later.");
                        }
                        else {
                            if (step == 'choose_parameters') {
                                $('.server_loc_tabs').addClass('hide');
                                $("#visible").animate({"left": -940}, "slow");
                                $("#adj-cont").animate({"margin-top": -10}, "slow");
                                $(".vps-step-2").animate({"width": 265}, "slow");
                                $(".vps-step-bar1").show(1200);
                                $(".vps-s1").removeClass("sel");
                                $(".vps-s2").addClass("sel");
                                $('#params').html(result.html);
                                $(".vpsMainWrapper").animate({"height": 500}, "6000");
                            } else {
                                $("#visible").animate({"left": -1880}, "slow");
                                $(".vps-step-2").animate({"width": 491}, "slow");
                                $(".vps-step-bar2").show(1200);
                                $(".vps-step-bar1").hide();
                                $(".vps-s2").removeClass("sel");
                                $(".vps-s3").addClass("sel");
                                $('#summary').html(result.html);
                                $(".vpsMainWrapper").animate({"height": 500}, "6000");
                            }
                        }
                    },
                    complete: function () {
                    },
                    error: function () {
                    }
                });
            }
            $('.features-group').click(function () {
                $(this).toggleClass("closed");
                $(this).next('ul').slideToggle('slow');
            });
        </script>
        <!-- ### Template /products/hosting/vps_hosting/vps_hosting_plans.html ends here ### -->

    </div>
</div>

<?php
//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?> 