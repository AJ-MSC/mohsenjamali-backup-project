var bottom_opacity = 0.5;
//var bottom_opacity = 1;
if (window.ie) {
    bottom_opacity = 1;
}
if (window.webkit) {
    bottom_opacity = 0.5;
}
var numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];
var GROWFACTOR = 2.2;
var ws = [];

/*****************************************************************
 Position for each box (top,left)
 *****************************************************************/

function getPosition(index) {
    var position = [];
    var abs_position = [[-20, 250], [15, 515], [290, 515], [15, 0], [290, 0], [375, 250], [200, 200]];

    var a = index % 3;
    var b = (index - a) / 3;
    position[1] = (290 * a) + 60;
    position[0] = (290 * b) - 15;
    position[0] = -30 + (400 - 80) * Math.random();
    position[1] = 0 + (400 - 0) * Math.random();

    return abs_position[index];
}




/********************************************
 Determine wich paragraph and image show
 Compare number of images and number of paragraphs
 Returns: number of paragraphs and images
 box_id - id of the box
 link_id - pager id clicked
 ********************************************/
function setPage(box_id, link_id) {
    var paragraphs = 0;
    var images = 0;
    $$("#" + box_id + ' p').each(function (p) {		//number of paragraphs
        p.setStyle('display', 'none');
        paragraphs++;
    });
    $$("#" + box_id + ' img').each(function (img) {		//number of images
        img.setStyle('display', 'none');
        images++;
    });
    $$("#" + box_id + ' a.selected').each(function (al) {		//pager link selected
        al.removeClass('selected');
    });
    $$("#" + box_id + ' p.' + link_id).each(function (element_p) {	// show paragraph clicked
        element_p.setStyle('display', 'block');
    });
    if (paragraphs > images) {								//  more paragraphs than images show first image
        $$("#" + box_id + ' img.zero').each(function (img) {
            img.setStyle('display', 'block');
        });
    } else {													// show image with the paragraph
        $$("#" + box_id + ' img.' + link_id).each(function (img) {
            img.setStyle('display', 'block');
        });
    }
    $$("#" + box_id + ' a.' + link_id).each(function (as) {			// highlight pager link clicked
        as.addClass('selected');
    });
    return [paragraphs, images];
}


/*********************************************************************
 Determines which is the biggest paragraph and set its heights to the others 
 puts a name on each paragraph
 returns number of paragraphs
 box_id  - id box
 *********************************************************************/

function setParagraphs(box_id) {
    var big_p = 0;
    var pages = 0;
    $$("#" + box_id + ' p').each(function (p, index_p) {
        if (p.getSize().size.y > big_p)
            big_p = p.getSize().size.y;					// longest paragraph
    });
    $$("#" + box_id + ' p').each(function (p, index_p) {
        if (index_p === 0) {															//display first paragraph
            p.setStyles({'display': 'block', 'height': big_p});
        } else {
            p.setStyles({'display': 'none', 'height': big_p});							// hide the rest
        }
        p.setStyle('color', '#808080');
        p.addClass(numbers[index_p]);												//name of the paragraph
        pages = index_p;
    });
    $(box_id + '_pager').setStyle('color', '#808080');
    $(box_id).setStyle('cursor', 'pointer');
    return pages;
}

/*********************************************************************
 Grow the size of the paragraph with the box
 box_id  - id box
 *********************************************************************/
p_size = 1.9;       // paragraph factor
function growParagraphs(box_id) {
    $$("#" + box_id + ' p').each(function (p, index_p) {
        var h = p.getStyle("height").toInt() * p_size;
        p.setStyle('height', h);
        p.setStyle('color', '#E6E6E6');
    });
    $(box_id + '_pager').setStyle('color', '#E6E6E6');
    $(box_id).setStyle('cursor', 'move');
}

/*********************************************************************
 Shrink the size of the paragraph with the box
 box_id  - id box
 *********************************************************************/

function shrinkParagraphs(box_id) {
    $$("#" + box_id + ' p').each(function (p, index_p) {
        var h = p.getStyle("height").toInt() / p_size;
        p.setStyle('height', h);
        p.setStyle('color', '#808080');
    });
    $(box_id + '_pager').setStyle('color', '#808080');
    $(box_id).setStyle('cursor', 'pointer');
}


/*********************************************************************
 Validate Email and Name
 *********************************************************************/

function isValidEmail(str) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return reg.test(str);
}

function isValidName(str) {
    return (str !== null) && (str !== "");
}

function isValidOcupation(str) {
    return (str !== null) && (str !== "- Select -");
}


var cloud =
        {
            init: function ()
            {
                var big_top = 100;
                var big_left = 150;
                $$('#draggables div.box').each(function (box, index) {  							   //draggables boxes
                    box.addClass('translucent');        // initial transparency
                    var box_id = box.getProperty('id');												 // initial values of the boxes
                    var id = box_id;				//name of each box
                    var box_size = box.getSize().size.y;
                    var position = getPosition(index);
                    box.setStyle('cursor', 'move');
                    box.setStyle('top', position[0]);
                    box.setStyle('left', position[1]);
                    ws[index] = box.getStyle("width").toInt();										// store each box width

                    var pages = setParagraphs(box_id);												// set paragraphs visible / hidden													

                    $$("#" + box_id + ' img').each(function (img, index_img) {									// show first image 
                        if (index_img === 0) {
                            img.setStyle('display', 'block');
                        } else {
                            img.setStyle('display', 'none');
                        }
                        img.addClass(numbers[index_img]);
                    });

                    if (pages > 0) {																	// 	render pager if needed
                        $(box_id + '_pager').setHTML('next:');
                        new Element('ul').setProperty('id', box_id + '_list').injectAfter(box_id + '_pager');
                        for (var j = 0; j <= pages; j++) {
                            if (j === 0) {
                                new Element('li').setHTML('<a class="pager replaced ' + numbers[j] + ' selected" href="' + numbers[j] + '">' + j + '</a>').injectInside(box_id + '_list');
                            } else {
                                new Element('li').setHTML('<a class="pager replaced ' + numbers[j] + '" href="' + numbers[j] + '">' + j + '</a>').injectInside(box_id + '_list');
                            }
                        }
                    }

                    $$("#" + id + ' a.pager').each(function (a_link, index_a) {              				// add functionality to the pager
                        var link_id = a_link.getProperty('href');
                        if (window.ie) {							// ie fix
                            var tmp_url = link_id.split('/');
                            for (var k = 0; k < tmp_url.length; k++) {
                                link_id = tmp_url[k];
                            }
                        }

                        a_link.addEvent('click', function (e) {									// once pager is clicked set paragraph , image and pager
                            new Event(e).stop();
                            setPage(box_id, link_id);
                            if ($('now_playing')) {												// in case  movie is playing replace with an image
                                var img_data = $('now_playing').getProperty('alt').split(',');
                                $('now_playing').setHTML('<img class="resizable ' + img_data[1] + '" style="display:none;" src="' + img_data[0] + '" alt="Content Image" />');
                                $('now_playing').removeProperty('style');
                                $('now_playing').setStyle('display', 'block');
                                $('now_playing').removeProperty('id');
                            }
                        });
                    });


                    new Drag.Move(box);
                    box.addEvent('emptydrop', function () {									        		// catch drop event
                        if (!box.hasClass('the-focus')) {														//  is the box big?
                            var nu_id = box_id;
                            var nu_top = box.getStyle('top').toInt();
                            var nu_left = box.getStyle('left').toInt();

                            $$('.the-focus').each(function (bigbox) {
                                var big_id = bigbox.getProperty('id');							    		// compare old and new box
                                big_top = bigbox.getStyle('top').toInt();
                                big_left = bigbox.getStyle('left').toInt();
                                if (big_id !== nu_id) {

                                    if ($('now_playing')) {                                                   // REMOVE the movie if playing
                                        var img_data = $('now_playing').getProperty('alt').split(',');
                                        $('now_playing').setHTML('<img class="resizable ' + img_data[1] + '" src="' + img_data[0] + '" alt="Content Image" />');
                                        $('now_playing').removeProperty('style');
                                        $('now_playing').setStyle('display', 'block');
                                        $('now_playing').removeProperty('id');
                                    }

                                    var shrink = bigbox.effects({transition: Fx.Transitions.Expo.easeOut});		// SHRINKING TRANSITION
                                    setPage(big_id, "zero");									// reset box paragraph, image and pager
                                    bigbox.addClass('last-focus');
                                    var ow = bigbox.getStyle("width").toInt();
                                    ow = ws[index];												    		// original width
                                    shrink.start({
                                        'width': ow,
                                        'font-size': '9px',
                                        'letter-spacing': '-1px',
                                        'opacity': bottom_opacity,
                                        'top': nu_top,
                                        'left': nu_left
                                    }).chain(function () {
                                        bigbox.removeClass('last-focus');						// once it has shrinked remove last focus
                                    });
                                    shrinkParagraphs(big_id);									// reduce paragraphs of the big box
                                    //shrinkParagraphs(id);
                                    $$("#" + big_id + ' h2').each(function (h2) {						// reduce headers
                                        h2.removeClass('big');
                                    });
                                    $$("#" + id + ' a.movie').each(function (a_movie) {				//disable movies
                                        a_movie.removeClass('big');
                                    });
                                    bigbox.removeClass('the-focus');							// remove focus from the big box
                                }
                            });

                            if (box.hasClass('menu_on')) {										// is the box positioned as menu
                                var article = new Element('div', {'id': 'article', 'class': 'clearfix'}).setHTML('&nbsp;');
                                $('article').replaceWith(article);								// remove article
                                $$('.menu_on').each(function (menu_box, index) {
                                    $$('#draggables div.box').each(function (bm, ind) {			// catch click on any menu box
                                        var bm_id = bm.getProperty('id');
                                        if (nu_id !== bm_id) {
                                            var position = getPosition(ind);
                                            var backTransition = bm.effects({transition: Fx.Transitions.Expo.easeOut});	// SHRINK AND POSITION TRANSITION FROM MENUBOX
                                            backTransition.start({
                                                'top': position[0],
                                                'left': position[1],
                                                'width': 200
                                            });
                                        }
                                    });
                                    $$('.box_content').each(function (box_content) {									// show boxes content, used when it dissappeared 
                                        box_content.setStyle('display', 'block');
                                    });
                                    var navbar = $('extras');
                                    navbar.removeClass('rolled');
                                    var unroll = navbar.effects({transition: Fx.Transitions.Expo.easeOut});			// return the resources bar to its original size
                                    unroll.start({
                                        'width': 225
                                    });
                                    menu_box.removeClass('menu_on');
                                });
                            }

                            $$('#draggables div.box').each(function (sb) {										//  turn all boxes translucent
                                sb.setStyle('opacity', bottom_opacity);
                            });

                            var w = ws[index] * GROWFACTOR;			                                        //  new width big box

                            var grow = box.effects({transition: Fx.Transitions.Expo.easeOut});  					// GROW TRANSITION
                            grow.start({
                                'width': w,
                                'font-size': '14px',
                                'letter-spacing': '0px',
                                'opacity': '1',
                                'top': big_top,
                                'left': big_left
                            }).chain(function () {
                                $$("#" + id + ' a.movie').each(function (a_movie) {									//set movies ready for play
                                    a_movie.addClass('big');
                                });
                                $('download_button').setStyle('display', 'block');
                            });
                            box.addClass('the-focus');													// focus
                            $$("#" + id + ' h2').each(function (h2) {											// adjust header bullet
                                h2.addClass('big');
                            });
                            growParagraphs(id);															//adjust size of the paragraphs
                        }
                    });

                });

            }

        };


var unroll =
        {
            complete: function () {						// once the article request is completed load article content 
                var tmp = new Element('div').setProperties({id: 'tmp'}).setStyles({display: 'none'}).injectInside(document.body);
                tmp.setHTML(this.content.response.text);
                var slice = $('tmp').getElement('#article_content');
                $('article').setHTML('<div id="article_content">' + slice.innerHTML + '</div><div id="scrollbar" class="scrollbar-vert"><div id="handle" class="handle-vert"></div></div>');
                $('article').setStyle('display', 'none');
                var slideArticle = new Fx.Slide('article');
                slideArticle.hide();
                $('article').setStyle('display', 'block');
                slideArticle.slideIn().chain(function () {
                    makeScrollbar($('article_content'), $('scrollbar'), $('handle'));
                });
                $('tmp').remove();

                if ($('form_subscribe_send')) {
                    $('form_subscribe_send').addEvent('click', function (e) {
                        new Event(e).stop();

                        var errors = 0;
                        var error_msg = [];
                        var name = $('name').getProperty('value');
                        var email = $('email').getProperty('value');
                        var job = $('job').getProperty('value');
                        if (!isValidEmail(email)) {
                            error_msg[errors] = "Please insert a valid email";
                            errors++;
                        }
                        if (!isValidName(name))
                        {
                            error_msg[errors] = "Please insert a valid name";
                            errors++;
                        }
                        if (!isValidOcupation(job)) {
                            error_msg[errors] = "Please select a valid option";
                            errors++;
                        }
                        if (errors) {
                            var text = "<ul>";
                            for (var i = 0; i < errors; i++) {
                                text = text + "<li>" + error_msg[i] + "</li>";
                            }
                            text = text + "</ul>";
                            $('form_error_container').setHTML(text);
                        } else {
                            // Handle Ajax request
                            // $('form_subscribe').submit();
                            //var url = $('form_subscribe').getProperty("action")+"?name="+escape(name)+"&email="+escape(email)+"&ocupation="+escape(ocupation);
                            var url = $('form_subscribe').getProperty("action");
                            //var url = "test.html"
                            $('form_error_container').setHTML("");
                            var response2 = new Ajax(url, {
                                postBody: $('form_subscribe'),
                                method: 'get',
                                autoCancel: true,
                                evalScripts: false
                            }).request();

                            var url = "htdocs/thankyou.html";
                            this.loadHTMLRender(e, url);
                        }
                    }.bind(this));
                }


            },
            completeRender: function () {						// once the article request is completed load article content 
                var tmp = new Element('div').setProperties({id: 'tmp'}).setStyles({display: 'none'}).injectInside(document.body);
                tmp.setHTML(this.content.response.text);
                var slice = $('tmp').getElement('#feedback_content');
                $('article').setHTML('<div id="article_content">' + slice.innerHTML + '</div>');
                //$('article').setStyle('display','none');
                //makeScrollbar( $('article_content'), $('scrollbar'), $('handle') );
                $('tmp').remove();

            },
            loadHTMLRender: function (e, url) {
                var currentTime = new Date();
                var s = currentTime.getTime();
                url = url + "?" + s;			// avoid cache
                this.content = new Ajax(url, {
                    method: 'get',
                    onComplete: this.completeRender.bind(this),
                    autoCancel: true,
                    evalScripts: false
                }).request();
            },
            loadHTML: function (e, url) {
                var currentTime = new Date();
                var s = currentTime.getTime();
                url = url + "?" + s;			// avoid cache
                this.content = new Ajax(url, {
                    method: 'get',
                    onComplete: this.complete.bind(this),
                    autoCancel: true,
                    evalScripts: false
                }).request();
            },
            init: function ()
            {
                $$('.roll').each(function (roll_link, index) {
                    var url = roll_link.getProperty('href');
                    roll_link.addEvent('click', function (e) {
                        new Event(e).stop();
                        var navbar = $('extras');
                        navbar.addClass('rolled');
                        var roll = navbar.effects({transition: Fx.Transitions.Expo.easeOut});
                        var pos_x = 740;				// width of the article container
                        roll.start({
                            'width': pos_x
                        }).chain(function () {			// actions after the container growed
                            var article = new Element('div', {'id': 'article', 'class': 'clearfix'}).setHTML('<img src="htdocs/img/base/loading.gif">');
                            $('article').replaceWith(article);
                            bits = url.split("/");
                            if (bits[1] === "downloads.html") {	// no scrollbar exceptions
                                $('download_button').setStyle('display', 'none');
                                this.loadHTMLRender(e, url);		// load the article
                            } else {
                                $('download_button').setStyle('display', 'block');
                                this.loadHTML(e, url);		// load the article
                            }

                        }.bind(this));
                        this.reduceOpened();
                        this.moveBoxes();
                    }.bind(this));
                }.bind(this));
                var tips_links = new Tips($$('.tooltip'), {//  tipbox setup
                    initialize: function () {
                        this.fx = new Fx.Style(this.toolTip, 'opacity', {duration: 500, wait: false}).set(bottom_opacity);
                    },
                    onShow: function (toolTip) {
                        if ((!$('arrow_left')) && (!$('extras').hasClass('rolled'))) {
                            var h = $('extras').getSize().size.y;
                            var arrow = new Element('div', {'id': 'arrow_left', 'class': 'clearfix', 'style': 'height:' + h + 'px'}).injectAfter('extras');
                        }
                        this.fx.start(1);
                    },
                    onHide: function (toolTip) {
                        if ($('arrow_left')) {
                            $('arrow_left').remove();
                        }
                        this.fx.start(0);
                    }
                });
                tips_links.addEvent('click', function (e) {		// disable tipbox links
                    new Event(e).stop();
                });
            },
            moveBoxes: function ()								// place the boxes into menu order
            {
                var box_number = 0;
                var spacer = 40;
                $$('.box').each(function (box) {
                    var top = spacer * box_number - 15;
                    var left = 5;
                    var boxTransition = box.effects({transition: Fx.Transitions.Expo.easeOut});	// MENU TRANSITION
                    boxTransition.start({
                        'top': top,
                        'left': left,
                        'width': 190
                    });
                    box_number += 1;
                    box.addClass('menu_on');
                    box.setStyle('cursor', 'pointer');
                });
            },
            reduceOpened: function ()						// reduce big box 
            {
                $$('.the-focus').each(function (bigbox, index) {
                    var big_id = bigbox.getProperty('id');
                    var shrink = bigbox.effects({transition: Fx.Transitions.linear});	// SHRINK TRANSITION TO MENU
                    setPage(big_id, "zero");
                    var ow = bigbox.getStyle("width").toInt();
                    ow = ow / GROWFACTOR;														// original width
                    shrink.start({
                        'width': ow,
                        'font-size': '9px',
                        'letter-spacing': '-1px'
                    });

                    if ($('now_playing')) {                                                   // REMOVE the movie if playing
                        var img_data = $('now_playing').getProperty('alt').split(',');
                        $('now_playing').setHTML('<img class="resizable ' + img_data[1] + '" src="' + img_data[0] + '" alt="Content Image" />');
                        $('now_playing').removeProperty('style');
                        $('now_playing').setStyle('display', 'block');
                        $('now_playing').removeProperty('id');
                    }

                    bigbox.removeClass('the-focus');
                    $$('.box').each(function (sbox) {			//  turn into translucent boxes
                        sbox.setStyle('opacity', 1);
                    });
                    $$("#" + big_id + ' h2').each(function (h2) {
                        h2.removeClass('big');
                    });
                    $$("#" + big_id + ' a.movie').each(function (a_movie) {				//disable movies
                        a_movie.removeClass('big');
                    });
                    shrinkParagraphs(big_id);
                });
            }

        };


var subscribeForm =
        {
            complete: function () {
                var tmp = new Element('div').setProperties({id: 'tmp'}).setStyles({display: 'none'}).injectInside(document.body);
                tmp.setHTML(this.content.response.text);
                var slice = $('tmp').getElement('#form_content');
                $('form_container').setHTML('<div id="form_content">' + slice.innerHTML + '</div>');

                $('form_subscribe_close').addEvent('click', function (e) {
                    $('form_container').remove();
                });

                /*  AJAX the subscribe form
                 $('form_subscribe_send').addEvent('click',function(e){
                 new Event(e).stop()
                 // code for sending the variables of the form
                 url = "thankyou.html";
                 this.loadHTML(e,url);
                 alert('send data');
                 
                 }.bind(this));
                 */

                tmp.remove();
            },
            loadHTML: function (e, url) {
                var currentTime = new Date();
                var s = currentTime.getTime();
                url = url + "?" + s;			// avoid cache
                this.content = new Ajax(url, {
                    method: 'get',
                    onComplete: this.complete.bind(this),
                    autoCancel: true,
                    evalScripts: false
                }).request();
            },
            init: function () {
                $('subscribe').addEvent('click', function (e) {
                    var url = $('subscribe').getProperty('href');
                    new Event(e).stop();
                    var left = 734;
                    //var left = $('wrapper').getSize().size.x - $('extras').getSize().size.x + 10;
                    var form_container = new Element('div', {'id': 'form_container', 'class': 'clearfix', 'style': 'position:absolute;top:0px;left:' + left + 'px;'}).setHTML('<img src="htdocs/site-media/img/base/loading.gif">').injectInside('wrapper');
                    this.loadHTML(e, url);
                }.bind(this));

            }


        };

var poplink = // open links in a new window
        {
            init: function () {
                var pops = $$('.popup');
                pops.each(function (pop) {
                    pop.addEvent('click', function (e) {
                        new Event(e).stop();
                        var url = pop.getProperty('href');
                        window.open(url);
                    });
                });
            }
        };

var movieReplace =
        {
            init: function () {
                var movies = $$('a.movie');
                movies.each(function (movie) {
                    movie.addEvent('click', function (e) {
                        new Event(e).stop();
                        if (movie.hasClass('big')) {
                            movie.setProperty('id', 'now_playing');
                            var movie_url = movie.getProperty('href');
                            var image = movie.getElement('img');
                            var image_url = image.getProperty('src');
                            var image_id = image.getProperty('class');
                            image_id = image_id.replace("resizable", "");
                            image_id = image_id.replace(" ", "");
                            movie.setProperty('alt', image_url + ',' + image_id);
                            var image_y = image.getSize().size.y;
                            var image_x = image.getSize().size.x;
                            movie.setStyles({'height': image_y, 'width': image_x, 'display': 'block'});
                            movie.setHTML('<object width="' + image_x + '" height="' + image_y + '" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab"><param name="src" value="' + movie_url + '"><param name="scale" value="toFit"><param name="autoplay" value="true"><param name="controller" value="false"><embed type="video/quicktime" src="' + movie_url + '" width="' + image_x + '" height="' + image_y + '" autoplay="true" scale="toFit" bgcolor="transparent"></embed></object>');
                        }
                    });
                });
            }
        };

function growFirstOne() {
    id = "seven";
    var w = ws[6] * GROWFACTOR;	                                        //  new width big box
    var grow = $(id).effects({transition: Fx.Transitions.Expo.easeOut});  					// GROW TRANSITION
    grow.start({
        'width': w,
        'font-size': '14px',
        'letter-spacing': '0px',
        'opacity': '1',
        'top': 100,
        'left': 150
    }).chain(function () {
        $$("#" + id + ' a.movie').each(function (a_movie) {									//set movies ready for play
            a_movie.addClass('big');
        });
    });
    $(id).addClass('the-focus');													// focus
    $$("#" + id + ' h2').each(function (h2) {											// adjust header bullet
        h2.addClass('big');
    });
    growParagraphs(id);															//adjust size of the paragraphs
}

var downloadAnchor = {
    complete: function () {						// once the article request is completed load article content 
        var tmp = new Element('div').setProperties({id: 'tmp'}).setStyles({display: 'none'}).injectInside(document.body);
        tmp.setHTML(this.content.response.text);
        var slice = $('tmp').getElement('#feedback_content');
        $('article').setHTML('<div id="article_content">' + slice.innerHTML + '</div><div id="scrollbar" class="scrollbar-vert"><div id="handle" class="handle-vert"></div></div>');
        $('article').setStyle('display', 'none');
        var slideArticle = new Fx.Slide('article');
        slideArticle.hide();
        $('article').setStyle('display', 'block');
        slideArticle.slideIn().chain(function () {
            makeScrollbar($('article_content'), $('scrollbar'), $('handle'));
        });
        $('tmp').remove();
    },
    loadHTML: function (url) {
        var currentTime = new Date();
        var s = currentTime.getTime();
        url = url + "?" + s;			// avoid cache
        this.content = new Ajax(url, {
            method: 'get',
            onComplete: this.complete.bind(this),
            autoCancel: true,
            evalScripts: false
        }).request();
    },
    init: function () {
        var location = window.location.href;
        location_bits = location.split('#');
        if (location_bits[1] === 'download' || location_bits[1] === 'downloads') {
            var navbar = $('extras');
            navbar.addClass('rolled');
            var roll = navbar.effects({transition: Fx.Transitions.Expo.easeOut});
            var pos_x = 740;					// width of the article container
            roll.start({
                'width': pos_x
            }).chain(function () {			// actions after the container growed
                var article = new Element('div', {'id': 'article', 'class': 'clearfix'}).setHTML('<img src="htdocs/site-media/img/base/loading.gif">');
                $('article').replaceWith(article);
                this.loadHTML("htdocs/downloads.html");// load the article		
            }.bind(this));
            unroll.reduceOpened();
            unroll.moveBoxes();
            $('download_button').setStyle('display', 'none');
        }
    }
};

//window.addEvent('domready', function () {
//    cloud.init();
//    unroll.init();
//    poplink.init();
//    movieReplace.init();
//    growFirstOne();
//    downloadAnchor.init();
//    //subscribeForm.init();
//    });