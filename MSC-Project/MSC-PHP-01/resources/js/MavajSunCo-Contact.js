$("#frmBusiness").validate({
    // Specify the validation rules
    rules: {
        bInputName: {
            required: true,
        },
        bCompanyName: {
            required: true,
        },
        bPhone: {
            required: true,
            number: true
        },
        bEmail: {
            required: true,
            email: true
        },
        bSubject: {
            required: true
        },
        bMobile: {
            required: true,
            number: true
        },
        bMessage: {
            required: true
        },
        bCaptcha: {
            required: true,
            remote: {
                url: 'validate-captcha.php',
                type: "post",
                data:
                        {
                            section: function () {
                                return $('#frmBusiness :input[name="section"]').val();
                            }
                        }
            }
        }
    },
    // Specify the validation error messages
    messages: {
        bCaptcha: {
            remote: "Your captcha is incorrect"
        }
    },
    submitHandler: function (form) {
        form.submit();
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }

});
//------------------------------------------------------------------------------
$("#frmQuote").validate({
    // Specify the validation rules
    rules: {
        qInputName: {
            required: true,
        },
        qCompanyName: {
            required: true,
        },
        qBudget: {
            required: true,
        },
        qFile: {
            required: true,
        },
        'qSelect[]': {
            required: true,
        },
        qEmail: {
            required: true,
            email: true
        },
        qSubject: {
            required: true
        },
        qMobile: {
            required: true,
            number: true
        },
        qMessage: {
            required: true
        },
        qYourPhone: {
            required: true,
            number: true
        },
        qCaptcha: {
            required: true,
            remote: {
                url: 'validate-captcha.php',
                type: "post",
                data:
                        {
                            section: function () {
                                return $('#frmQuote :input[name="section"]').val();
                            }
                        }
            }
        }
    },
    // Specify the validation error messages
    messages: {
        qCaptcha: {
            remote: "Your captcha is incorrect"
        }
    },
    submitHandler: function (form) {
        form.submit();
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});
//------------------------------------------------------------------------------
$("#frmSupport").validate({
// Specify the validation rules
    rules: {
        sInputName: {
            required: true,
        },
        sCompanyName: {
            required: true,
        },
        sPhone: {
            required: true,
        },
        sMessage: {
            required: true,
        },
        sUrl: {
            required: true,
            url: true
        },
        sEmail: {
            required: true,
            email: true
        },
        sCaptcha: {
            required: true,
            remote: {
                url: 'validate-captcha.php',
                type: "post",
                data:
                        {
                            section: function () {
                                return $('#frmSupport :input[name="section"]').val();
                            }
                        }
            }
        }
    },
    // Specify the validation error messages
    messages: {
        sCaptcha: {
            remote: "Your captcha is incorrect"
        }
    },
    submitHandler: function (form) {
        form.submit();
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});
$(document).ready(function () {
    var randomNum = Math.random() * 1000;
    //$('ul.nav li:nth-child(1)').addClass('active');
    $('ul.nav-tabs li').click(function () {
        $('.alert').hide();
        $(".img-rounded").attr('src', '');
        var section = $(this).find('a').attr('class');
        var section = section.split(' ')[0];

        if (section == "business") {
            $("#bCaptchaImg").attr('src', 'http://www.MavajSunCo.com/resources/phpcaptcha/captcha.php?rand=' + randomNum);
        } else if (section == "quote") {
            $("#qCaptchaImg").attr('src', 'http://www.MavajSunCo.com/resources/phpcaptcha/captcha.php?rand=' + randomNum);
        } else if (section == "support") {
            $("#sCaptchaImg").attr('src', 'http://www.MavajSunCo.com/resources/phpcaptcha/captcha.php?rand=' + randomNum);
        }
    });

    var activeSection = $("#activeSection").attr('class');
    if (activeSection != "") {
        $('.' + activeSection).closest('li').addClass('active');
        $(".tab-pane").removeClass('active in');
        if (activeSection == "business") {
            $("#Business-Inquiries").addClass('active in');
            $("#bCaptchaImg").attr('src', 'http://www.MavajSunCo.com/resources/phpcaptcha/captcha.php?rand=' + randomNum);
        } else if (activeSection == "quote") {
            $("#Get-a-Quote").addClass('active in');
            $("#qCaptchaImg").attr('src', 'http://www.MavajSunCo.com/resources/phpcaptcha/captcha.php?rand=' + randomNum);
        } else {
            $("#Support").addClass('active in');
            $("#sCaptchaImg").attr('src', 'http://www.MavajSunCo.com/resources/phpcaptcha/captcha.php?rand=' + randomNum);
        }
//        $("ul.nav-tabs li").trigger('click');
    } else {
        $('ul.nav-tabs li:nth-child(1)').addClass('active');
    }
    //$("ul.nav li").trigger('click');
});