<?php

/** * ***************************************** **
 * 	@PHP Mail Config         
 *      @version            V1.0.0
 * 	@AUTHOR             MavajSunCo
 * 	@DATE               JUN 18, 2015
 * * ****************************************** * */
// USE SMTP OR mail()
// SMTP is recommended, mail() is disabled on most shared hosting servers.
// IF false : SMTP host/port/user/pass/ssl not used, leave empty or as it is!
$config['use_smtp'] = FALSE;                        // true|false
// SMTP Server Settings
$config['smtp_host'] = 'smtp.strato.de';      // eg.: smtp.mandrillapp.com
$config['smtp_port'] = 465;                         // eg.: 587
$config['smtp_user'] = 'timo@timo.net';             // you@gmail.com
$config['smtp_pass'] = 'timotimo';                  // password
$config['smtp_ssl'] = false;                        // should remain false
// Who receive all emails?
$config['send_to'] = 'contact@timo.net';                 // destination of all emails sent throught contact form
// Email Subject
$config['subject'] = 'TimO Contact Form';           // subject of emails you receive
?>