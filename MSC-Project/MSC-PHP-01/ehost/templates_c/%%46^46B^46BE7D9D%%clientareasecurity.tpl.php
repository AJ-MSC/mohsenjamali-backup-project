<?php /* Smarty version 2.6.28, created on 2015-09-20 16:00:45
         compiled from /home/sites/mavajsunco.com/public_html/ehost/templates/default/clientareasecurity.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['clientareanavsecurity'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/clientareadetailslinks.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['successful']): ?>
<div class="alert alert-success">
    <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['errormessage']): ?>
<div class="alert alert-error">
    <p class="bold"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
</p>
    <ul>
        <?php echo $this->_tpl_vars['errormessage']; ?>

    </ul>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['twofaavailable']): ?>

<?php if ($this->_tpl_vars['twofaactivation']): ?>

<script><?php echo '
function dialogSubmit() {
    $(\'div#twofaactivation form\').attr(\'method\', \'post\');
    $(\'div#twofaactivation form\').attr(\'action\', \'clientarea.php\');
    $(\'div#twofaactivation form\').attr(\'onsubmit\', \'\');
    $(\'div#twofaactivation form\').submit();
    return true;
}
'; ?>
</script>

<div id="twofaactivation">
    <?php echo $this->_tpl_vars['twofaactivation']; ?>

</div>

<script type="text/javascript">
$("#twofaactivation input:text:visible:first,#twofaactivation input:password:visible:first").focus();
</script>

<?php else: ?>

<h2><?php echo $this->_tpl_vars['LANG']['twofactorauth']; ?>
</h2>

<p><?php echo $this->_tpl_vars['LANG']['twofaactivationintro']; ?>
</p>

<br />

<form method="post" action="clientarea.php?action=security">
<input type="hidden" name="2fasetup" value="1" />
<p align="center">
<?php if ($this->_tpl_vars['twofastatus']): ?>
<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['twofadisableclickhere']; ?>
" class="btn btn-danger" />
<?php else: ?>
<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['twofaenableclickhere']; ?>
" class="btn btn-success" />
<?php endif; ?>
</p>
</form>

<br /><br />

<?php endif; ?>

<?php endif; ?>

<?php if ($this->_tpl_vars['securityquestionsenabled'] && ! $this->_tpl_vars['twofaactivation']): ?>

<br/><br/>

<h2><?php echo $this->_tpl_vars['LANG']['clientareanavsecurityquestions']; ?>
</h2>

<form class="form-horizontal" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=changesq">

  <fieldset class="onecol">

<?php if (! $this->_tpl_vars['nocurrent']): ?>
    <div class="control-group">
        <label class="control-label" for="currentans"><?php echo $this->_tpl_vars['currentquestion']; ?>
</label>
        <div class="controls">
            <input type="password" name="currentsecurityqans" id="currentans" />
        </div>
    </div>
<?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="securityqid"><?php echo $this->_tpl_vars['LANG']['clientareasecurityquestion']; ?>
</label>
        <div class="controls">
            <select name="securityqid" id="securityqid">
            <?php $_from = $this->_tpl_vars['securityquestions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['question']):
?>
                <option value=<?php echo $this->_tpl_vars['question']['id']; ?>
><?php echo $this->_tpl_vars['question']['question']; ?>
</option>
            <?php endforeach; endif; unset($_from); ?>
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="securityqans"><?php echo $this->_tpl_vars['LANG']['clientareasecurityanswer']; ?>
</label>
        <div class="controls">
            <input type="password" name="securityqans" id="securityqans" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="securityqans2"><?php echo $this->_tpl_vars['LANG']['clientareasecurityconfanswer']; ?>
</label>
        <div class="controls">
            <input type="password" name="securityqans2" id="securityqans2" />
        </div>
    </div>

  </fieldset>

  <div class="form-actions">
    <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareasavechanges']; ?>
" />
    <input class="btn" type="reset" value="<?php echo $this->_tpl_vars['LANG']['cancel']; ?>
" />
  </div>

</form>

<?php endif; ?>