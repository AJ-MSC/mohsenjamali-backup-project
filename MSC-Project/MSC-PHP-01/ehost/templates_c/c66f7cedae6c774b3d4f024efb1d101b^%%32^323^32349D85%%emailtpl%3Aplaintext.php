<?php /* Smarty version 2.6.28, created on 2014-11-24 00:00:08
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p><?php if ($this->_tpl_vars['days_until_expiry']): ?>The domain(s) listed below are due to expire within the next <?php echo $this->_tpl_vars['days_until_expiry']; ?>
 days.<?php else: ?>The domain(s) listed below are going to expire in <?php echo $this->_tpl_vars['domain_days_until_expiry']; ?>
 days. Renew now before it's too late...<?php endif; ?></p>
<p><?php if ($this->_tpl_vars['expiring_domains']): ?><?php $_from = $this->_tpl_vars['expiring_domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?><?php echo $this->_tpl_vars['domain']['name']; ?>
 - <?php echo $this->_tpl_vars['domain']['nextduedate']; ?>
 <strong>(<?php echo $this->_tpl_vars['domain']['days']; ?>
 Days)</strong><br /><?php endforeach; endif; unset($_from); ?><?php elseif ($this->_tpl_vars['domains']): ?><?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?><?php echo $this->_tpl_vars['domain']['name']; ?>
 - <?php echo $this->_tpl_vars['domain']['nextduedate']; ?>
<br /><?php endforeach; endif; unset($_from); ?><?php else: ?><?php echo $this->_tpl_vars['domain_name']; ?>
 - <?php echo $this->_tpl_vars['domain_next_due_date']; ?>
 <strong>(<?php echo $this->_tpl_vars['domain_days_until_nextdue']; ?>
 Days)</strong><?php endif; ?></p>
<p>To ensure the domain does not expire, you should renew it now. You can do this from the domains management section of our client area here: <?php echo $this->_tpl_vars['whmcs_link']; ?>
</p>
<p>Should you allow the domain to expire, you will be able to renew it for up to 30 days after the renewal date. During this time, the domain will not be accessible so any web site or email services associated with it will stop working.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>