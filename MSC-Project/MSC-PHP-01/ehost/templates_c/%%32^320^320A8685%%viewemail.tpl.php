<?php /* Smarty version 2.6.28, created on 2014-12-14 10:32:13
         compiled from default/viewemail.tpl */ ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $this->_tpl_vars['LANG']['clientareaemails']; ?>
 - <?php echo $this->_tpl_vars['companyname']; ?>
</title>

    <link href="templates/<?php echo $this->_tpl_vars['template']; ?>
/css/bootstrap.css" rel="stylesheet">
    <link href="templates/<?php echo $this->_tpl_vars['template']; ?>
/css/whmcs.css" rel="stylesheet">

  </head>

  <body class="popupwindow">

<h2><?php echo $this->_tpl_vars['subject']; ?>
</h2>

<div class="popupcontainer"><?php echo $this->_tpl_vars['message']; ?>
</div>

<p class="textcenter"><input type="button" value="<?php echo $this->_tpl_vars['LANG']['closewindow']; ?>
" class="btn btn-primary" onclick="window.close()" /></p>

  </body>
</html>