<?php /* Smarty version 2.6.28, created on 2015-08-31 21:40:41
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>Thank you for your domain renewal order. Your domain renewal request for the domain listed below has now been completed.</p>
<p>Domain: <?php echo $this->_tpl_vars['domain_name']; ?>
<br />Renewal Length: <?php echo $this->_tpl_vars['domain_reg_period']; ?>
<br />Renewal Price: <?php echo $this->_tpl_vars['domain_recurring_amount']; ?>
<br />Next Due Date: <?php echo $this->_tpl_vars['domain_next_due_date']; ?>
</p>
<p>You may login to your client area at <?php echo $this->_tpl_vars['whmcs_url']; ?>
 to manage your domain.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>