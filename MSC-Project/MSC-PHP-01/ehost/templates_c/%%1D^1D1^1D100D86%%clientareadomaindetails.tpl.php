<?php /* Smarty version 2.6.28, created on 2015-05-15 07:01:29
         compiled from /home/sites/mavajsunco.com/public_html/ehost/templates/default/clientareadomaindetails.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', '/home/sites/mavajsunco.com/public_html/ehost/templates/default/clientareadomaindetails.tpl', 1, false),array('modifier', 'replace', '/home/sites/mavajsunco.com/public_html/ehost/templates/default/clientareadomaindetails.tpl', 89, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['LANG']['managing'])) ? $this->_run_mod_handler('cat', true, $_tmp, ' ') : smarty_modifier_cat($_tmp, ' ')))) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['domain']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['domain'])))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['updatesuccess']): ?>
<div class="alert alert-success">
    <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
</div>
<?php elseif ($this->_tpl_vars['registrarcustombuttonresult'] == 'success'): ?>
<div class="alert alert-success">
    <p><?php echo $this->_tpl_vars['LANG']['moduleactionsuccess']; ?>
</p>
</div>
<?php elseif ($this->_tpl_vars['error']): ?>
<div class="alert alert-error">
    <p><?php echo $this->_tpl_vars['error']; ?>
</p>
</div>
<?php elseif ($this->_tpl_vars['registrarcustombuttonresult']): ?>
<div class="alert alert-error">
    <p><strong><?php echo $this->_tpl_vars['LANG']['moduleactionfailed']; ?>
:</strong> <?php echo $this->_tpl_vars['registrarcustombuttonresult']; ?>
</p>
</div>
<?php elseif ($this->_tpl_vars['lockstatus'] == 'unlocked'): ?>
<div class="alert alert-error">
    <p><strong><?php echo $this->_tpl_vars['LANG']['domaincurrentlyunlocked']; ?>
</strong> <?php echo $this->_tpl_vars['LANG']['domaincurrentlyunlockedexp']; ?>
</p>
</div>
<?php endif; ?>

<div id="tabs">
    <ul class="nav nav-tabs">
        <li class="active" id="tab1nav"><a href="#tab1"><?php echo $this->_tpl_vars['LANG']['information']; ?>
</a></li>
        <li id="tab2nav"><a href="#tab2"><?php echo $this->_tpl_vars['LANG']['domainsautorenew']; ?>
</a></li>
        <?php if ($this->_tpl_vars['rawstatus'] == 'active' && $this->_tpl_vars['managens']): ?><li id="tab3nav"><a href="#tab3"><?php echo $this->_tpl_vars['LANG']['domainnameservers']; ?>
</a></li><?php endif; ?>
        <?php if ($this->_tpl_vars['lockstatus']): ?><?php if ($this->_tpl_vars['tld'] != "co.uk" && $this->_tpl_vars['tld'] != "org.uk" && $this->_tpl_vars['tld'] != "ltd.uk" && $this->_tpl_vars['tld'] != "plc.uk" && $this->_tpl_vars['tld'] != "me.uk"): ?><li id="tab4nav"><a href="#tab4"><?php echo $this->_tpl_vars['LANG']['domainregistrarlock']; ?>
</a></li><?php endif; ?><?php endif; ?>
        <?php if ($this->_tpl_vars['releasedomain']): ?><li id="tab5nav"><a href="#tab5"><?php echo $this->_tpl_vars['LANG']['domainrelease']; ?>
</a></li><?php endif; ?>
        <?php if ($this->_tpl_vars['addonscount']): ?><li id="tab6nav"><a href="#tab6"><?php echo $this->_tpl_vars['LANG']['clientareahostingaddons']; ?>
</a></li><?php endif; ?>
        <?php if ($this->_tpl_vars['managecontacts'] || $this->_tpl_vars['registerns'] || $this->_tpl_vars['dnsmanagement'] || $this->_tpl_vars['emailforwarding'] || $this->_tpl_vars['getepp']): ?><li class="dropdown"><a data-toggle="dropdown" href="#" class="dropdown-toggle"><?php echo $this->_tpl_vars['LANG']['domainmanagementtools']; ?>
&nbsp;<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <?php if ($this->_tpl_vars['managecontacts']): ?><li><a href="clientarea.php?action=domaincontacts&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['domaincontactinfo']; ?>
</a></li><?php endif; ?>
                <?php if ($this->_tpl_vars['registerns']): ?><li><a href="clientarea.php?action=domainregisterns&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['domainregisterns']; ?>
</a></li><?php endif; ?>
                <?php if ($this->_tpl_vars['dnsmanagement']): ?><li><a href="clientarea.php?action=domaindns&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['clientareadomainmanagedns']; ?>
</a></li><?php endif; ?>
                <?php if ($this->_tpl_vars['emailforwarding']): ?><li><a href="clientarea.php?action=domainemailforwarding&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['clientareadomainmanageemailfwds']; ?>
</a></li><?php endif; ?>
                <?php if ($this->_tpl_vars['getepp']): ?><li class="divider"></li>
                <li><a href="clientarea.php?action=domaingetepp&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['domaingeteppcode']; ?>
</a></li><?php endif; ?>
                <?php if ($this->_tpl_vars['registrarcustombuttons']): ?><li class="divider"></li>
                <?php $_from = $this->_tpl_vars['registrarcustombuttons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['command']):
?>
                <li><a href="clientarea.php?action=domaindetails&amp;id=<?php echo $this->_tpl_vars['domainid']; ?>
&amp;modop=custom&amp;a=<?php echo $this->_tpl_vars['command']; ?>
"><?php echo $this->_tpl_vars['label']; ?>
</a></li>
                <?php endforeach; endif; unset($_from); ?><?php endif; ?>
            </ul>
        </li><?php endif; ?>
    </ul>
</div>

<div data-toggle="tab" id="tab1" class="tab-content active">

    <div class="row">

        <div class="col30">
            <div class="internalpadding">
                <div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['information']; ?>
</h2></div>
                <p><?php echo $this->_tpl_vars['LANG']['domaininfoexp']; ?>
</p>
                <br />
                <p><input type="button" value="<?php echo $this->_tpl_vars['LANG']['backtodomainslist']; ?>
" class="btn" onclick="window.location='clientarea.php?action=domains'" /></p>
            </div>
        </div>
        <div class="col70">
            <div class="internalpadding">
                <div class="row">
                <div class="col2half">
                    <h4><strong><?php echo $this->_tpl_vars['LANG']['clientareahostingdomain']; ?>
:</strong></h4> <?php echo $this->_tpl_vars['domain']; ?>
 <span class="label <?php echo $this->_tpl_vars['rawstatus']; ?>
"><?php echo $this->_tpl_vars['status']; ?>
</span>
                </div>
                <div class="col2half">
                    <h4><strong><?php echo $this->_tpl_vars['LANG']['firstpaymentamount']; ?>
:</strong></h4> <span><?php echo $this->_tpl_vars['firstpaymentamount']; ?>
</span>
                </div>
                </div>
                <div class="row">
                <div class="col2half">
                    <h4><strong><?php echo $this->_tpl_vars['LANG']['clientareahostingregdate']; ?>
:</strong></h4> <span><?php echo $this->_tpl_vars['registrationdate']; ?>
</span>
                </div>
                <div class="col2half">
                    <p><h4><strong><?php echo $this->_tpl_vars['LANG']['recurringamount']; ?>
:</strong></h4> <?php echo $this->_tpl_vars['recurringamount']; ?>
 <?php echo $this->_tpl_vars['LANG']['every']; ?>
 <?php echo $this->_tpl_vars['registrationperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
<?php if ($this->_tpl_vars['renew']): ?> &nbsp; <a href="cart.php?gid=renewals" class="btn btn-mini"><?php echo $this->_tpl_vars['LANG']['domainsrenewnow']; ?>
</a><?php endif; ?></p>
                </div>
                </div>
                <div class="row">
                <div class="col2half">
                    <p><h4><strong><?php echo $this->_tpl_vars['LANG']['clientareahostingnextduedate']; ?>
:</strong></h4> <?php echo $this->_tpl_vars['nextduedate']; ?>
</p>
                </div>
                <div class="col2half">
                    <p><h4><strong><?php echo $this->_tpl_vars['LANG']['orderpaymentmethod']; ?>
:</strong></h4> <?php echo $this->_tpl_vars['paymentmethod']; ?>
</p>
                </div>
                </div>
                <div class="clear"></div>
                <?php if ($this->_tpl_vars['registrarclientarea']): ?><div class="moduleoutput"><?php echo ((is_array($_tmp=$this->_tpl_vars['registrarclientarea'])) ? $this->_run_mod_handler('replace', true, $_tmp, 'modulebutton', 'btn') : smarty_modifier_replace($_tmp, 'modulebutton', 'btn')); ?>
</div><?php endif; ?>
                <br />
                <br />
                <br />
            </div>
        </div>

    </div>

</div>
<div data-toggle="tab" id="tab2" class="tab-content">

    <div class="row">

        <div class="col30">
            <div class="internalpadding">
                <div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['domainsautorenew']; ?>
</h2></div>
                <p><?php echo $this->_tpl_vars['LANG']['domainrenewexp']; ?>
</p>
            </div>
        </div>
        <div class="col70">
            <div class="internalpadding">
                <h4><strong><?php echo $this->_tpl_vars['LANG']['domainautorenewstatus']; ?>
:</strong></h4>
                <div class="internalpadding">
                    <p><strong><?php if ($this->_tpl_vars['autorenew']): ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewenabled']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewdisabled']; ?>
<?php endif; ?></strong></p>
                </div>
                <hr />
                <br />
                <div class="internalpadding">
                    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails" class="form-horizontal">
                    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
                    <?php if ($this->_tpl_vars['autorenew']): ?>
                    <input type="hidden" name="autorenew" value="disable">
                    <p><input type="submit" class="btn btn-large btn-danger" value="<?php echo $this->_tpl_vars['LANG']['domainsautorenewdisable']; ?>
" /></p>
                    <?php else: ?>
                    <input type="hidden" name="autorenew" value="enable">
                    <p><input type="submit" class="btn btn-large btn-success" value="<?php echo $this->_tpl_vars['LANG']['domainsautorenewenable']; ?>
" /></p>
                    <?php endif; ?>
                    </form>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>

    </div>

</div>
<div data-toggle="tab" id="tab3" class="tab-content">

    <div class="row">

        <div class="col30">
            <div class="internalpadding">
                <div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['domainnameservers']; ?>
</h2></div>
                <p><?php echo $this->_tpl_vars['LANG']['domainnsexp']; ?>
</p>
            </div>
        </div>
        <div class="col70">
            <div class="internalpadding">
                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails" class="form-horizontal">
                <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
" />
                <input type="hidden" name="sub" value="savens" />
                <p><label class="full control-label"><input type="radio" class="radio inline" name="nschoice" value="default" onclick="disableFields('domnsinputs',true)"<?php if ($this->_tpl_vars['defaultns']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['nschoicedefault']; ?>
</label>
                <label class="full control-label"><input type="radio" class="radio inline" name="nschoice" value="custom" onclick="disableFields('domnsinputs','')"<?php if (! $this->_tpl_vars['defaultns']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['nschoicecustom']; ?>
</label></p>
                <br />
                <fieldset class="control-group">
                    <div class="control-group">
                        <label class="control-label" for="ns1"><?php echo $this->_tpl_vars['LANG']['domainnameserver1']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns1" name="ns1" type="text" value="<?php echo $this->_tpl_vars['ns1']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns2"><?php echo $this->_tpl_vars['LANG']['domainnameserver2']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns2" name="ns2" type="text" value="<?php echo $this->_tpl_vars['ns2']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns3"><?php echo $this->_tpl_vars['LANG']['domainnameserver3']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns3" name="ns3" type="text" value="<?php echo $this->_tpl_vars['ns3']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns4"><?php echo $this->_tpl_vars['LANG']['domainnameserver4']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns4" name="ns4" type="text" value="<?php echo $this->_tpl_vars['ns4']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns5"><?php echo $this->_tpl_vars['LANG']['domainnameserver5']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns5" name="ns5" type="text" value="<?php echo $this->_tpl_vars['ns5']; ?>
" />
                        </div>
                    </div>
                    <div class="internalpadding">
                        <p class="textcenter"><input type="submit" class="btn btn-large btn-primary" value="<?php echo $this->_tpl_vars['LANG']['changenameservers']; ?>
" /></p>
                    </div>                    
                </fieldset>
                </form>

            </div>
        </div>

    </div>

</div>
<div data-toggle="tab" id="tab4" class="tab-content">

    <div class="row">

        <div class="col30">
            <div class="internalpadding">
                <div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['domainregistrarlock']; ?>
</h2></div>
                <p><?php echo $this->_tpl_vars['LANG']['domainlockingexp']; ?>
</p>
            </div>
        </div>
        <div class="col70">
            <div class="internalpadding">
                <h4><strong><?php echo $this->_tpl_vars['LANG']['domainreglockstatus']; ?>
:</strong></h4>
                <p><strong><?php if ($this->_tpl_vars['lockstatus'] == 'locked'): ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewenabled']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewdisabled']; ?>
<?php endif; ?></strong></p>
                <hr />
                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
                <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
" />
                <input type="hidden" name="sub" value="savereglock" />
                <?php if ($this->_tpl_vars['lockstatus'] == 'locked'): ?>
                <p><input type="submit" class="btn btn-danger" value="<?php echo $this->_tpl_vars['LANG']['domainreglockdisable']; ?>
" /></p>
                <?php else: ?>
                <p><input type="submit" class="btn btn-success" name="reglock" value="<?php echo $this->_tpl_vars['LANG']['domainreglockenable']; ?>
" /></p>
                <?php endif; ?>
                </form>
            </div>
        </div>

    </div>

</div>
<div data-toggle="tab" id="tab5" class="tab-content">

    <div class="row">

        <div class="col30">
            <div class="internalpadding">
                <div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['domainrelease']; ?>
</h2></div>
                <p><?php echo $this->_tpl_vars['LANG']['domainreleasedescription']; ?>
</p>
            </div>
        </div>
        <div class="col70">
            <div class="internalpadding">
                <?php if ($this->_tpl_vars['releasedomain']): ?>
                <p><strong>&nbsp;&raquo;&nbsp;&nbsp;<?php echo $this->_tpl_vars['LANG']['domainrelease']; ?>
</strong></p>
                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
                <input type="hidden" name="sub" value="releasedomain">
                <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
                <?php echo $this->_tpl_vars['LANG']['domainreleasetag']; ?>
: <input type="text" name="transtag" size="20" />
                <p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainrelease']; ?>
" class="buttonwarn" /></p>
                </form>
                <?php endif; ?>
            </div>
        </div>

    </div>

</div>
<div data-toggle="tab" id="tab6" class="tab-content">

    <div class="row">

        <div class="col30">
            <div class="internalpadding">
                <div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['domainaddons']; ?>
</h2></div>
                <p><?php echo $this->_tpl_vars['LANG']['domainaddonsinfo']; ?>
</p>
            </div>
        </div>
        <div class="col70">
            <div class="internalpadding">
                <?php if ($this->_tpl_vars['addons']['idprotection']): ?>
                <div class="row">
                    <div class="domaddonimg">
                        <img src="images/idprotect.png" />
                    </div>
                    <div class="col70">
                        <strong><?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
</strong><br />
                        <?php echo $this->_tpl_vars['LANG']['domainaddonsidprotectioninfo']; ?>
<br />
                        <?php if ($this->_tpl_vars['addonstatus']['idprotection']): ?>
                        <a href="clientarea.php?action=domainaddons&id=<?php echo $this->_tpl_vars['domainid']; ?>
&disable=idprotect&token=<?php echo $this->_tpl_vars['token']; ?>
"><?php echo $this->_tpl_vars['LANG']['disable']; ?>
</a>
                        <?php else: ?>
                        <a href="clientarea.php?action=domainaddons&id=<?php echo $this->_tpl_vars['domainid']; ?>
&buy=idprotect&token=<?php echo $this->_tpl_vars['token']; ?>
"><?php echo $this->_tpl_vars['LANG']['domainaddonsbuynow']; ?>
 <?php echo $this->_tpl_vars['addonspricing']['idprotection']; ?>
</a>
                        <?php endif; ?>
                    </div>
                </div>
                <br />
                <?php endif; ?>
                <?php if ($this->_tpl_vars['addons']['dnsmanagement']): ?>
                <div class="row">
                    <div class="domaddonimg">
                        <img src="images/dnsmanagement.png" />
                    </div>
                    <div class="col70">
                        <strong><?php echo $this->_tpl_vars['LANG']['domainaddonsdnsmanagement']; ?>
</strong><br />
                        <?php echo $this->_tpl_vars['LANG']['domainaddonsdnsmanagementinfo']; ?>
<br />
                        <?php if ($this->_tpl_vars['addonstatus']['dnsmanagement']): ?>
                        <a href="clientarea.php?action=domaindns&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['manage']; ?>
</a> | <a href="clientarea.php?action=domainaddons&id=<?php echo $this->_tpl_vars['domainid']; ?>
&disable=dnsmanagement&token=<?php echo $this->_tpl_vars['token']; ?>
"><?php echo $this->_tpl_vars['LANG']['disable']; ?>
</a>
                        <?php else: ?>
                        <a href="clientarea.php?action=domainaddons&id=<?php echo $this->_tpl_vars['domainid']; ?>
&buy=dnsmanagement&token=<?php echo $this->_tpl_vars['token']; ?>
"><?php echo $this->_tpl_vars['LANG']['domainaddonsbuynow']; ?>
 <?php echo $this->_tpl_vars['addonspricing']['dnsmanagement']; ?>
</a>
                        <?php endif; ?>
                    </div>
                </div>
                <br />
                <?php endif; ?>
                <?php if ($this->_tpl_vars['addons']['emailforwarding']): ?>
                <div class="row">
                    <div class="domaddonimg">
                        <img src="images/emailfwd.png" />
                    </div>
                    <div class="col70">
                        <strong><?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
</strong><br />
                        <?php echo $this->_tpl_vars['LANG']['domainaddonsemailforwardinginfo']; ?>
<br />
                        <?php if ($this->_tpl_vars['addonstatus']['emailforwarding']): ?>
                        <a href="clientarea.php?action=domainemailforwarding&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
"><?php echo $this->_tpl_vars['LANG']['manage']; ?>
</a> | <a href="clientarea.php?action=domainaddons&id=<?php echo $this->_tpl_vars['domainid']; ?>
&disable=emailfwd&token=<?php echo $this->_tpl_vars['token']; ?>
"><?php echo $this->_tpl_vars['LANG']['disable']; ?>
</a>
                        <?php else: ?>
                        <a href="clientarea.php?action=domainaddons&id=<?php echo $this->_tpl_vars['domainid']; ?>
&buy=emailfwd&token=<?php echo $this->_tpl_vars['token']; ?>
"><?php echo $this->_tpl_vars['LANG']['domainaddonsbuynow']; ?>
 <?php echo $this->_tpl_vars['addonspricing']['emailforwarding']; ?>
</a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>

    </div>

</div>
