<?php /* Smarty version 2.6.28, created on 2015-05-15 06:42:32
         compiled from /home/sites/mavajsunco.com/public_html/ehost/templates/default/pwresetvalidation.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sprintf2', '/home/sites/mavajsunco.com/public_html/ehost/templates/default/pwresetvalidation.tpl', 17, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['pwreset'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['invalidlink']): ?>

  <div class="alert alert-error">
    <p class="textcenter"><?php echo $this->_tpl_vars['invalidlink']; ?>
</p>
  </div>
  <br /><br /><br /><br />

<?php elseif ($this->_tpl_vars['success']): ?>

  <br />
  <div class="alert alert-success">
    <p class="textcenter bold"><?php echo $this->_tpl_vars['LANG']['pwresetvalidationsuccess']; ?>
</p>
  </div>

  <p class="textcenter"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['pwresetsuccessdesc'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, '<a href="clientarea.php">', '</a>') : smarty_modifier_sprintf2($_tmp, '<a href="clientarea.php">', '</a>')); ?>
</p>

  <br /><br /><br /><br />

<?php else: ?>

<?php if ($this->_tpl_vars['errormessage']): ?>

  <div class="alert alert-error">
    <p class="textcenter"><?php echo $this->_tpl_vars['errormessage']; ?>
</p>
  </div>

<?php endif; ?>

<form class="form-horizontal" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=pwreset">
<input type="hidden" name="key" id="key" value="<?php echo $this->_tpl_vars['key']; ?>
" />

<br /><h4><?php echo $this->_tpl_vars['LANG']['pwresetenternewpw']; ?>
</h4><br />

  <fieldset class="onecol">

    <div class="control-group">
        <label class="control-label" for="password"><?php echo $this->_tpl_vars['LANG']['newpassword']; ?>
</label>
        <div class="controls">
            <input type="password" name="newpw" id="password" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="confirmpw"><?php echo $this->_tpl_vars['LANG']['confirmnewpassword']; ?>
</label>
        <div class="controls">
            <input type="password" name="confirmpw" id="confirmpw" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="passstrength"><?php echo $this->_tpl_vars['LANG']['pwstrength']; ?>
</label>
        <div class="controls">
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pwstrength.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div>
    </div>

  </fieldset>

  <div class="form-actions">
    <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareasavechanges']; ?>
" />
    <input class="btn" type="reset" value="<?php echo $this->_tpl_vars['LANG']['cancel']; ?>
" />
  </div>

</form>

<?php endif; ?>