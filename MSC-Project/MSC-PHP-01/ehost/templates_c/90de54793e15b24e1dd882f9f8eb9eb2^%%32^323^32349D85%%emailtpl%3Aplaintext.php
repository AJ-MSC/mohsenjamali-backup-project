<?php /* Smarty version 2.6.28, created on 2014-12-07 14:17:20
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>As you requested, your password for our client area has now been reset.</p>
<p>If it was not at your request, then please contact support immediately.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>