<?php /* Smarty version 2.6.28, created on 2015-06-06 12:35:17
         compiled from /home/sites/mavajsunco.com/public_html/ehost/templates/default/clientareadomaincontactinfo.tpl */ ?>
<?php echo '
<script language="javascript">
function usedefaultwhois(id) {
    jQuery("."+id.substr(0,id.length-1)+"customwhois").attr("disabled", true);
    jQuery("."+id.substr(0,id.length-1)+"defaultwhois").attr("disabled", false);
    jQuery(\'#\'+id.substr(0,id.length-1)+\'1\').attr("checked", "checked");
}
function usecustomwhois(id) {
    jQuery("."+id.substr(0,id.length-1)+"customwhois").attr("disabled", false);
    jQuery("."+id.substr(0,id.length-1)+"defaultwhois").attr("disabled", true);
    jQuery(\'#\'+id.substr(0,id.length-1)+\'2\').attr("checked", "checked");
}
</script>
'; ?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['domaincontactinfo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="alert alert-block alert-info">
    <p><?php echo $this->_tpl_vars['LANG']['domainname']; ?>
: <strong><?php echo $this->_tpl_vars['domain']; ?>
</strong></p>
</div>

<?php if ($this->_tpl_vars['successful']): ?>
<div class="alert alert-success">
    <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['error']): ?>
    <div class="alert alert-error">
        <p class="bold textcenter"><?php echo $this->_tpl_vars['error']; ?>
</p>
    </div>
<?php endif; ?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaincontacts" class="form-horizontal">

<input type="hidden" name="sub" value="save" />
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
" />

<?php $_from = $this->_tpl_vars['contactdetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['contactdetails'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['contactdetails']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['contactdetail'] => $this->_tpl_vars['values']):
        $this->_foreach['contactdetails']['iteration']++;
?>

<h3><a name="<?php echo $this->_tpl_vars['contactdetail']; ?>
"></a><?php echo $this->_tpl_vars['contactdetail']; ?>
<?php if (($this->_foreach['contactdetails']['iteration'] <= 1)): ?><?php $_from = $this->_tpl_vars['contactdetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['contactsx'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['contactsx']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['contactdetailx'] => $this->_tpl_vars['valuesx']):
        $this->_foreach['contactsx']['iteration']++;
?><?php if (! ($this->_foreach['contactsx']['iteration'] <= 1)): ?> - <a href="clientarea.php?action=domaincontacts&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
#<?php echo $this->_tpl_vars['contactdetailx']; ?>
"><?php echo $this->_tpl_vars['LANG']['jumpto']; ?>
 <?php echo $this->_tpl_vars['contactdetailx']; ?>
</a><?php endif; ?><?php endforeach; endif; unset($_from); ?><?php else: ?> - <a href="clientarea.php?action=domaincontacts&domainid=<?php echo $this->_tpl_vars['domainid']; ?>
#"><?php echo $this->_tpl_vars['LANG']['top']; ?>
</a><?php endif; ?></h3>

<p><label class="full control-label"><input type="radio" class="radio inline" name="wc[<?php echo $this->_tpl_vars['contactdetail']; ?>
]" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
1" value="contact" onclick="usedefaultwhois(id)"<?php if ($this->_tpl_vars['defaultns']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domaincontactusexisting']; ?>
</label></p>

<fieldset class="onecol" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
defaultwhois">

    <div class="control-group">
        <label class="control-label" for="<?php echo $this->_tpl_vars['contactdetail']; ?>
3"><?php echo $this->_tpl_vars['LANG']['domaincontactchoose']; ?>
</label>
        <div class="controls">
            <select class="<?php echo $this->_tpl_vars['contactdetail']; ?>
defaultwhois" name="sel[<?php echo $this->_tpl_vars['contactdetail']; ?>
]" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
3" onclick="usedefaultwhois(id)">
            <option value="u<?php echo $this->_tpl_vars['clientsdetails']['userid']; ?>
"><?php echo $this->_tpl_vars['LANG']['domaincontactprimary']; ?>
</option>
            <?php $_from = $this->_tpl_vars['contacts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['contact']):
?>
            <option value="c<?php echo $this->_tpl_vars['contact']['id']; ?>
"><?php echo $this->_tpl_vars['contact']['name']; ?>
</option>
            <?php endforeach; endif; unset($_from); ?>
          </select>
        </div>
    </div>

</fieldset>

<p><label class="full control-label"><input type="radio" class="radio inline" name="wc[<?php echo $this->_tpl_vars['contactdetail']; ?>
]" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
2" value="custom" onclick="usecustomwhois(id)"<?php if (! $this->_tpl_vars['defaultns']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domaincontactusecustom']; ?>
</label></p>

<fieldset class="onecol" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
defaultwhois">

<?php $_from = $this->_tpl_vars['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['value']):
?>
    <div class="control-group">
        <label class="control-label" for="<?php echo $this->_tpl_vars['contactdetail']; ?>
3"><?php echo $this->_tpl_vars['contactdetailstranslations'][$this->_tpl_vars['name']]; ?>
</label>
        <div class="controls">
            <input type="text" name="contactdetails[<?php echo $this->_tpl_vars['contactdetail']; ?>
][<?php echo $this->_tpl_vars['name']; ?>
]" value="<?php echo $this->_tpl_vars['value']; ?>
" size="30" class="<?php echo $this->_tpl_vars['contactdetail']; ?>
customwhois" />
        </div>
    </div>
<?php endforeach; endif; unset($_from); ?>

</fieldset>

<?php endforeach; endif; unset($_from); ?>

<p class="textcenter"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareasavechanges']; ?>
" class="btn btn-primary" /></p>

</form>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
" />
<p><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareabacklink']; ?>
" class="btn" /></p>
</form>