<?php /* Smarty version 2.6.28, created on 2014-12-18 08:27:12
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>This message is to confirm that your domain purchase has been successful. The details of the domain purchase are below:</p>
<p>Registration Date: <?php echo $this->_tpl_vars['domain_reg_date']; ?>
<br /> Domain: <?php echo $this->_tpl_vars['domain_name']; ?>
<br /> Registration Period: <?php echo $this->_tpl_vars['domain_reg_period']; ?>
<br /> Amount: <?php echo $this->_tpl_vars['domain_first_payment_amount']; ?>
<br /> Next Due Date: <?php echo $this->_tpl_vars['domain_next_due_date']; ?>
</p>
<p>You may login to your client area at <?php echo $this->_tpl_vars['whmcs_url']; ?>
 to manage your new domain.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>