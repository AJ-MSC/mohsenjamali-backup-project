<?php /* Smarty version 2.6.28, created on 2015-04-05 18:04:48
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>This is a notification that your service has now been suspended. The details of this suspension are below:</p>
<p>Product/Service: <?php echo $this->_tpl_vars['service_product_name']; ?>
<br /><?php if ($this->_tpl_vars['service_domain']): ?>Domain: <?php echo $this->_tpl_vars['service_domain']; ?>
<br /><?php endif; ?>Amount: <?php echo $this->_tpl_vars['service_recurring_amount']; ?>
<br />Due Date: <?php echo $this->_tpl_vars['service_next_due_date']; ?>
<br />Suspension Reason: <strong><?php echo $this->_tpl_vars['service_suspension_reason']; ?>
</strong></p>
<p>Please contact us as soon as possible to get your service reactivated.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>