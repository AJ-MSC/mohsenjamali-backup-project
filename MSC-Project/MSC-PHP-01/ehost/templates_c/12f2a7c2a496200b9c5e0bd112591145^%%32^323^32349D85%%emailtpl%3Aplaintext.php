<?php /* Smarty version 2.6.28, created on 2015-04-15 14:17:32
         compiled from emailtpl:plaintext */ ?>
<p align="center"><strong>PLEASE READ THIS EMAIL IN FULL AND PRINT IT FOR YOUR RECORDS</strong></p>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>Thank you for your order from us! Your shoutcast account has now been setup and this email contains all the information you will need in order to begin using your account.</p>
<p><strong>New Account Information</strong></p>
<p>Domain: <?php echo $this->_tpl_vars['service_domain']; ?>
<br /> Username: <?php echo $this->_tpl_vars['service_username']; ?>
<br /> Password: <?php echo $this->_tpl_vars['service_password']; ?>
</p>
<p><strong>Server Information</strong></p>
<p>Server Name: <?php echo $this->_tpl_vars['service_server_name']; ?>
<br /> Server IP: <?php echo $this->_tpl_vars['service_server_ip']; ?>
<br /> Nameserver 1: <?php echo $this->_tpl_vars['service_ns1']; ?>
<br /> Nameserver 1 IP: <?php echo $this->_tpl_vars['service_ns1_ip']; ?>
<br /> Nameserver 2: <?php echo $this->_tpl_vars['service_ns2']; ?>
 <br /> Nameserver 2 IP: <?php echo $this->_tpl_vars['service_ns2_ip']; ?>
</p>
<p>Thank you for choosing us.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>