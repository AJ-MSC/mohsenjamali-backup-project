<?php /* Smarty version 2.6.28, created on 2015-09-20 15:58:16
         compiled from /home/sites/mavajsunco.com/public_html/ehost/modules/addons/newtlds/newtlds.tpl */ ?>
  <!-- <?php echo $this->_tpl_vars['NEWTLDS_PLUGINVERSION']; ?>
 -->

<?php if ($this->_tpl_vars['NEWTLDS_ERRORS']): ?>
    <div class="alert alert-error">
      <p class="bold"><?php echo $this->_tpl_vars['clientareaerrors']; ?>
</p>
      <ul><?php echo $this->_tpl_vars['NEWTLDS_ERRORS']; ?>
</ul>
    </div>
  <?php endif; ?>

    <?php if ($this->_tpl_vars['loggedin']): ?>
        <?php if ($this->_tpl_vars['NEWTLDS_ENABLED']): ?>
              <?php if ($this->_tpl_vars['NEWTLDS_PORTALACCOUNT']): ?>
                <!-- START WATCHLIST -->
                <div id="tldportal-root"></div>
                <script src="https://<?php echo $this->_tpl_vars['NEWTLDS_URLHOST']; ?>
/api/embed?token=<?php echo $this->_tpl_vars['NEWTLDS_LINK']; ?>
" type="text/javascript"></script>
                <!-- END WATCHLIST -->
              <?php else: ?>
                <strong><?php echo $this->_tpl_vars['NEWTLDS_NOPORTALACCT']; ?>
</strong>
                <br /><br /><br />
              <?php endif; ?>
        <?php else: ?>
          <strong><?php echo $this->_tpl_vars['NEWTLDS_NOTENABLED']; ?>
</strong>
          <br /><br /><br />
        <?php endif; ?>
    <?php else: ?>
      <strong><?php echo $this->_tpl_vars['NEWTLDS_NOTLOGGEDIN']; ?>
</strong>
      <br /><br /><br />
    <?php endif; ?>
