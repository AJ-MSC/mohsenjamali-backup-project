<?php /* Smarty version 2.6.28, created on 2015-08-25 10:10:01
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>This is the second billing notice that your invoice no. <?php echo $this->_tpl_vars['invoice_num']; ?>
 which was generated on <?php echo $this->_tpl_vars['invoice_date_created']; ?>
 is now overdue.</p>
<p>Your payment method is: <?php echo $this->_tpl_vars['invoice_payment_method']; ?>
</p>
<p>Invoice: <?php echo $this->_tpl_vars['invoice_num']; ?>
<br /> Balance Due: <?php echo $this->_tpl_vars['invoice_balance']; ?>
<br /> Due Date: <?php echo $this->_tpl_vars['invoice_date_due']; ?>
</p>
<p>You can login to your client area to view and pay the invoice at <?php echo $this->_tpl_vars['invoice_link']; ?>
</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>