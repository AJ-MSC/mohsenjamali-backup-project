<?php /* Smarty version 2.6.28, created on 2014-11-24 00:00:08
         compiled from emailtpl:emailmessage */ ?>
<div class="msg-body inner  undoreset" role="main" aria-label="Message body" >
    <div >
        <title>MAVAJ SUN CO</title>
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
                <tbody>
                    <tr>
                        <td style="padding:20px 11px 40px 11px;background-color:#ffffff;" >
                            <table width="648" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;background-color:#ffffff;" bgcolor="#ffffff" >
                                <tbody>
                                    <tr>
                                        <td >
                                            <img src="http://mavajsunco.com/htdocs/img/email/top.gif" alt="" width="648" height="122" border="0" style="display:block;margin:0;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="630" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;background-color:#f1f1f1;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table width="490" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;background-color:#f1f1f1;border-top:1px solid #cccccc;">
                                                <tbody>
                                                    <tr>
                                                        <td width="490" align="left" style="padding:0 0 22px 0;">
                                                            <div style="font-family:Lucida Grande, Lucida Sans, Lucida Sans Unicode, Arial, Helvetica, Verdana, sans-serif;color:#333333;font-size:12px;line-height:1.25em;">
                                                                <p style="text-align: justify;"><br/><br/>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p><?php if ($this->_tpl_vars['days_until_expiry']): ?>The domain(s) listed below are due to expire within the next <?php echo $this->_tpl_vars['days_until_expiry']; ?>
 days.<?php else: ?>The domain(s) listed below are going to expire in <?php echo $this->_tpl_vars['domain_days_until_expiry']; ?>
 days. Renew now before it's too late...<?php endif; ?></p>
<p><?php if ($this->_tpl_vars['expiring_domains']): ?><?php $_from = $this->_tpl_vars['expiring_domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?><?php echo $this->_tpl_vars['domain']['name']; ?>
 - <?php echo $this->_tpl_vars['domain']['nextduedate']; ?>
 <strong>(<?php echo $this->_tpl_vars['domain']['days']; ?>
 Days)</strong><br /><?php endforeach; endif; unset($_from); ?><?php elseif ($this->_tpl_vars['domains']): ?><?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?><?php echo $this->_tpl_vars['domain']['name']; ?>
 - <?php echo $this->_tpl_vars['domain']['nextduedate']; ?>
<br /><?php endforeach; endif; unset($_from); ?><?php else: ?><?php echo $this->_tpl_vars['domain_name']; ?>
 - <?php echo $this->_tpl_vars['domain_next_due_date']; ?>
 <strong>(<?php echo $this->_tpl_vars['domain_days_until_nextdue']; ?>
 Days)</strong><?php endif; ?></p>
<p>To ensure the domain does not expire, you should renew it now. You can do this from the domains management section of our client area here: <?php echo $this->_tpl_vars['whmcs_link']; ?>
</p>
<p>Should you allow the domain to expire, you will be able to renew it for up to 30 days after the renewal date. During this time, the domain will not be accessible so any web site or email services associated with it will stop working.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>
</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:40px;" >
                                            <img src="http://mavajsunco.com/htdocs/img/email/btm.gif" alt="" width="630" height="21" border="0" style="display:block;margin:0;">
                                        </td></tr>
                                </tbody>
                            </table>

                            <table width="490" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="padding:20px 20px 10px 0;">
                                            <div style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;">TM and copyright &copy; 2014 MAVAJ SUN CO</div>
                                            <div style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;">
                                                <a rel="nofollow" target="_blank" href="http://www.MavajSunCo.com/" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">All Rights Reserved</a>
                                                / <a rel="nofollow" target="_blank" href="http://www.MavajSunCo.com/ehost" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">Keep Informed</a>
                                                / <a rel="nofollow" target="_blank" href="http://www.facebook.com/MavajSunCo" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">Facebook</a>
                                                / <a rel="nofollow" target="_blank" href="http://www.twitter.com/MavajSunCo" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">Twitter</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>