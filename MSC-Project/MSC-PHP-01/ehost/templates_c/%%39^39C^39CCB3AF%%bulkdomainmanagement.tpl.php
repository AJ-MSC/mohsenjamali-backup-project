<?php /* Smarty version 2.6.28, created on 2015-06-27 10:22:39
         compiled from /home/sites/mavajsunco.com/public_html/ehost/templates/default/bulkdomainmanagement.tpl */ ?>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=bulkdomain" class="form-horizontal">
<input type="hidden" name="update" value="<?php echo $this->_tpl_vars['update']; ?>
">
<input type="hidden" name="save" value="1">
<?php $_from = $this->_tpl_vars['domainids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domainid']):
?>
<input type="hidden" name="domids[]" value="<?php echo $this->_tpl_vars['domainid']; ?>
" />
<?php endforeach; endif; unset($_from); ?>

<?php if ($this->_tpl_vars['update'] == 'nameservers'): ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['domainmanagens'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['save']): ?>
    <?php if ($this->_tpl_vars['errors']): ?>
        <div class="alert alert-error">
            <p class="bold"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
</p>
            <ul>
                <?php $_from = $this->_tpl_vars['errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['error']):
?>
                <li><?php echo $this->_tpl_vars['error']; ?>
</li>
            <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>
    <?php else: ?>
        <div class="alert alert-success">
            <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
        </div>
    <?php endif; ?>
<?php endif; ?>

<p><?php echo $this->_tpl_vars['LANG']['domainbulkmanagementchangesaffect']; ?>
</p>

<br />

<blockquote>
<br />
<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?>
&raquo; <?php echo $this->_tpl_vars['domain']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
<br />
</blockquote>

<p><label class="full control-label"><input type="radio" class="radio inline" name="nschoice" value="default" onclick="disableFields('domnsinputs',true)" checked /> <?php echo $this->_tpl_vars['LANG']['nschoicedefault']; ?>
</label><br />
<label class="full control-label"><input type="radio" class="radio inline" name="nschoice" value="custom" onclick="disableFields('domnsinputs','')" checked /> <?php echo $this->_tpl_vars['LANG']['nschoicecustom']; ?>
</label></p>

                <fieldset class="control-group">
                    <div class="control-group">
                        <label class="control-label" for="ns1"><?php echo $this->_tpl_vars['LANG']['domainnameserver1']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns1" name="ns1" type="text" value="<?php echo $this->_tpl_vars['ns1']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns2"><?php echo $this->_tpl_vars['LANG']['domainnameserver2']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns2" name="ns2" type="text" value="<?php echo $this->_tpl_vars['ns2']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns3"><?php echo $this->_tpl_vars['LANG']['domainnameserver3']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns3" name="ns3" type="text" value="<?php echo $this->_tpl_vars['ns3']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns4"><?php echo $this->_tpl_vars['LANG']['domainnameserver4']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns4" name="ns4" type="text" value="<?php echo $this->_tpl_vars['ns4']; ?>
" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ns5"><?php echo $this->_tpl_vars['LANG']['domainnameserver5']; ?>
</label>
                        <div class="controls">
                            <input class="input-xlarge domnsinputs" id="ns5" name="ns5" type="text" value="<?php echo $this->_tpl_vars['ns5']; ?>
" />
                        </div>
                    </div>
                    <p align="center"><input type="submit" class="btn btn-primary btn-large" value="<?php echo $this->_tpl_vars['LANG']['changenameservers']; ?>
" /></p>
                </fieldset>

<?php elseif ($this->_tpl_vars['update'] == 'autorenew'): ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['domainautorenewstatus'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['save']): ?>
    <div class="alert alert-success">
        <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
    </div>
<?php endif; ?>

<p><?php echo $this->_tpl_vars['LANG']['domainautorenewinfo']; ?>
</p>
<br />
<p><?php echo $this->_tpl_vars['LANG']['domainautorenewrecommend']; ?>
</p>
<br />
<p><?php echo $this->_tpl_vars['LANG']['domainbulkmanagementchangeaffect']; ?>
</p>

<br />

<blockquote>
<br />
<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?>
&raquo; <?php echo $this->_tpl_vars['domain']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
<br />
</blockquote>

<br />

<p align="center"><input type="submit" name="enable" value="<?php echo $this->_tpl_vars['LANG']['domainsautorenewenable']; ?>
" class="btn btn-success btn-large" /> &nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" name="disable" value="<?php echo $this->_tpl_vars['LANG']['domainsautorenewdisable']; ?>
" class="btn btn-danger btn-large" /></p>

<?php elseif ($this->_tpl_vars['update'] == 'reglock'): ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['domainreglockstatus'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['save']): ?>
    <?php if ($this->_tpl_vars['errors']): ?>
        <div class="alert alert-error">
            <p class="bold"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
</p>
            <ul>
                <?php $_from = $this->_tpl_vars['errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['error']):
?>
                <li><?php echo $this->_tpl_vars['error']; ?>
</li>
            <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>
    <?php else: ?>
        <div class="alert alert-success">
            <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
        </div>
    <?php endif; ?>
<?php endif; ?>

<p><?php echo $this->_tpl_vars['LANG']['domainreglockinfo']; ?>
</p>
<br />
<p><?php echo $this->_tpl_vars['LANG']['domainreglockrecommend']; ?>
</p>
<br />
<p><?php echo $this->_tpl_vars['LANG']['domainbulkmanagementchangeaffect']; ?>
</p>

<blockquote>
<br />
<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?>
&raquo; <?php echo $this->_tpl_vars['domain']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
<br />
</blockquote>

<br />

<p align="center"><input type="submit" name="enable" value="<?php echo $this->_tpl_vars['LANG']['domainreglockenable']; ?>
" class="btn btn-success btn-large" /> &nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" name="disable" value="<?php echo $this->_tpl_vars['LANG']['domainreglockdisable']; ?>
" class="btn btn-danger btn-large" /></p>

<?php elseif ($this->_tpl_vars['update'] == 'contactinfo'): ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['domaincontactinfoedit'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['save']): ?>
    <?php if ($this->_tpl_vars['errors']): ?>
        <div class="alert alert-error">
            <p class="bold"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
</p>
            <ul>
                <?php $_from = $this->_tpl_vars['errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['error']):
?>
                <li><?php echo $this->_tpl_vars['error']; ?>
</li>
            <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>
    <?php else: ?>
        <div class="alert alert-success">
            <p><?php echo $this->_tpl_vars['LANG']['changessavedsuccessfully']; ?>
</p>
        </div>
    <?php endif; ?>
<?php endif; ?>

<?php echo '
<script language="javascript">
function usedefaultwhois(id) {
    jQuery("."+id.substr(0,id.length-1)+"customwhois").attr("disabled", true);
    jQuery("."+id.substr(0,id.length-1)+"defaultwhois").attr("disabled", false);
    jQuery(\'#\'+id.substr(0,id.length-1)+\'1\').attr("checked", "checked");
}
function usecustomwhois(id) {
    jQuery("."+id.substr(0,id.length-1)+"customwhois").attr("disabled", false);
    jQuery("."+id.substr(0,id.length-1)+"defaultwhois").attr("disabled", true);
    jQuery(\'#\'+id.substr(0,id.length-1)+\'2\').attr("checked", "checked");
}
</script>
'; ?>


<p><?php echo $this->_tpl_vars['LANG']['domainbulkmanagementchangesaffect']; ?>
</p>

<br />

<blockquote>
<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?>
&raquo; <?php echo $this->_tpl_vars['domain']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
</blockquote>

<?php $_from = $this->_tpl_vars['contactdetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['contactdetails'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['contactdetails']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['contactdetail'] => $this->_tpl_vars['values']):
        $this->_foreach['contactdetails']['iteration']++;
?>

<h3><a name="<?php echo $this->_tpl_vars['contactdetail']; ?>
"></a><?php echo $this->_tpl_vars['contactdetail']; ?>
</strong><?php if (($this->_foreach['contactdetails']['iteration'] <= 1)): ?><?php $_from = $this->_tpl_vars['contactdetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['contactsx'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['contactsx']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['contactdetailx'] => $this->_tpl_vars['valuesx']):
        $this->_foreach['contactsx']['iteration']++;
?><?php if (! ($this->_foreach['contactsx']['iteration'] <= 1)): ?> - <a href="clientarea.php?action=bulkdomain#<?php echo $this->_tpl_vars['contactdetailx']; ?>
"><?php echo $this->_tpl_vars['LANG']['jumpto']; ?>
 <?php echo $this->_tpl_vars['contactdetailx']; ?>
</a><?php endif; ?><?php endforeach; endif; unset($_from); ?><?php else: ?> - <a href="clientarea.php?action=bulkdomain#"><?php echo $this->_tpl_vars['LANG']['top']; ?>
</a><?php endif; ?></h3>

<p></p>

<p><label class="full control-label"><input type="radio" class="radio inline" name="wc[<?php echo $this->_tpl_vars['contactdetail']; ?>
]" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
1" value="contact" onclick="usedefaultwhois(id)" /> <?php echo $this->_tpl_vars['LANG']['domaincontactusexisting']; ?>
</label></p>

<fieldset class="onecol" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
defaultwhois">

    <div class="control-group">
        <label class="control-label" for="<?php echo $this->_tpl_vars['contactdetail']; ?>
3"><?php echo $this->_tpl_vars['LANG']['domaincontactchoose']; ?>
</label>
        <div class="controls">
            <select class="<?php echo $this->_tpl_vars['contactdetail']; ?>
defaultwhois" name="sel[<?php echo $this->_tpl_vars['contactdetail']; ?>
]" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
3" onclick="usedefaultwhois(id)">
            <option value="u<?php echo $this->_tpl_vars['clientsdetails']['userid']; ?>
"><?php echo $this->_tpl_vars['LANG']['domaincontactprimary']; ?>
</option>
            <?php $_from = $this->_tpl_vars['contacts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['contact']):
?>
            <option value="c<?php echo $this->_tpl_vars['contact']['id']; ?>
"><?php echo $this->_tpl_vars['contact']['name']; ?>
</option>
            <?php endforeach; endif; unset($_from); ?>
          </select>
        </div>
    </div>

</fieldset>

<p><label class="full control-label"><input type="radio" class="radio inline" name="wc[<?php echo $this->_tpl_vars['contactdetail']; ?>
]" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
2" value="custom" onclick="usecustomwhois(id)" checked /> <?php echo $this->_tpl_vars['LANG']['domaincontactusecustom']; ?>
</label></p>

<fieldset class="onecol" id="<?php echo $this->_tpl_vars['contactdetail']; ?>
defaultwhois">

<?php $_from = $this->_tpl_vars['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['value']):
?>
    <div class="control-group">
        <label class="control-label" for="<?php echo $this->_tpl_vars['contactdetail']; ?>
3"><?php echo $this->_tpl_vars['name']; ?>
</label>
        <div class="controls">
            <input type="text" name="contactdetails[<?php echo $this->_tpl_vars['contactdetail']; ?>
][<?php echo $this->_tpl_vars['name']; ?>
]" value="<?php echo $this->_tpl_vars['value']; ?>
" size="30" class="<?php echo $this->_tpl_vars['contactdetail']; ?>
customwhois" />
        </div>
    </div>
<?php endforeach; endif; unset($_from); ?>

</fieldset>

<?php endforeach; else: ?>

<div class="alert alert-error">
    <p><?php echo $this->_tpl_vars['LANG']['domainbulkmanagementnotpossible']; ?>
</p>
</div>

<?php endif; unset($_from); ?>

<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareasavechanges']; ?>
" class="btn btn-primary" /></p>

<?php endif; ?>

<p><input type="button" value="<?php echo $this->_tpl_vars['LANG']['clientareabacklink']; ?>
" onclick="window.location='clientarea.php?action=domains'" class="btn" /></p>

<br /><br />

</form>