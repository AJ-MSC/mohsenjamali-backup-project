<?php /* Smarty version 2.6.28, created on 2014-12-14 09:16:53
         compiled from emailtpl:emailmessage */ ?>
<div class="msg-body inner  undoreset" role="main" aria-label="Message body" >
    <div >
        <title>MAVAJ SUN CO</title>
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
                <tbody>
                    <tr>
                        <td style="padding:20px 11px 40px 11px;background-color:#ffffff;" >
                            <table width="648" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;background-color:#ffffff;" bgcolor="#ffffff" >
                                <tbody>
                                    <tr>
                                        <td >
                                            <img src="http://mavajsunco.com/htdocs/img/email/top.gif" alt="" width="648" height="122" border="0" style="display:block;margin:0;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="630" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;background-color:#f1f1f1;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table width="490" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;background-color:#f1f1f1;border-top:1px solid #cccccc;">
                                                <tbody>
                                                    <tr>
                                                        <td width="490" align="left" style="padding:0 0 22px 0;">
                                                            <div style="font-family:Lucida Grande, Lucida Sans, Lucida Sans Unicode, Arial, Helvetica, Verdana, sans-serif;color:#333333;font-size:12px;line-height:1.25em;">
                                                                <p style="text-align: justify;"><br/><br/>
<p>Dear Ali Abdullah (Property Yours),</p>
<p>This is a notice that an invoice has been generated on Sunday, December 7th, 2014.</p>
<p>Your payment method is: PayPal</p>
<p>Invoice #15<br /> Amount Due: 1525.00 AED<br /> Due Date: Sunday, December 21st, 2014</p>
<p><strong>Invoice Items</strong></p>
<p>Unlimited Pro - propertyyours.ae 1200.00 AED<br /> propertyyours.ae 325.00 AED<br /> ------------------------------------------------------<br /> Sub Total: 1525.00 AED<br /> Credit: 0.00 AED<br /> Total: 1525.00 AED <br /> ------------------------------------------------------</p>
<p>You can login to your client area to view and pay the invoice at <a href="http://mavajsunco.com/ehost/viewinvoice.php?id=15">http://mavajsunco.com/ehost/viewinvoice.php?id=15</a></p>
<p><br /><br /><br /> Thanks,<br /><br /> MAVAJ SUN CO. Customer Support<br /><br /></p>
</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:40px;" >
                                            <img src="http://mavajsunco.com/htdocs/img/email/btm.gif" alt="" width="630" height="21" border="0" style="display:block;margin:0;">
                                        </td></tr>
                                </tbody>
                            </table>

                            <table width="490" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="padding:20px 20px 10px 0;">
                                            <div style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;">TM and copyright &copy; 2014 MAVAJ SUN CO</div>
                                            <div style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;">
                                                <a rel="nofollow" target="_blank" href="http://www.MavajSunCo.com/" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">All Rights Reserved</a>
                                                / <a rel="nofollow" target="_blank" href="http://www.MavajSunCo.com/ehost" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">Keep Informed</a>
                                                / <a rel="nofollow" target="_blank" href="http://www.facebook.com/MavajSunCo" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">Facebook</a>
                                                / <a rel="nofollow" target="_blank" href="http://www.twitter.com/MavajSunCo" style="font-family:Geneva, Verdana, Arial, Helvetica, sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline;">Twitter</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>