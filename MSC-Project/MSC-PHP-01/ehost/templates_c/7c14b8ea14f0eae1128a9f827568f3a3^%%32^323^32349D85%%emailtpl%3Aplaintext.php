<?php /* Smarty version 2.6.28, created on 2014-11-29 09:52:36
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p align="center"><strong>PLEASE READ THIS EMAIL IN FULL AND PRINT IT FOR YOUR RECORDS</strong></p>
<p>Thank you for your order from us! Your hosting account has now been setup and this email contains all the information you will need in order to begin using your account.</p>
<p>If you have requested a domain name during sign up, please keep in mind that your domain name will not be visible on the internet instantly. This process is called propagation and can take up to 48 hours. Until your domain has propagated, your website and email will not function, we have provided a temporary url which you may use to view your website and upload files in the meantime.</p>
<p><strong>New Account Information</strong></p>
<p>Hosting Package: <?php echo $this->_tpl_vars['service_product_name']; ?>
<br />Domain: <?php echo $this->_tpl_vars['service_domain']; ?>
<br />First Payment Amount: <?php echo $this->_tpl_vars['service_first_payment_amount']; ?>
<br />Recurring Amount: <?php echo $this->_tpl_vars['service_recurring_amount']; ?>
<br />Billing Cycle: <?php echo $this->_tpl_vars['service_billing_cycle']; ?>
<br />Next Due Date: <?php echo $this->_tpl_vars['service_next_due_date']; ?>
</p>
<p><strong>Login Details</strong></p>
<p>Username: <?php echo $this->_tpl_vars['service_username']; ?>
<br />Password: <?php echo $this->_tpl_vars['service_password']; ?>
</p>
<p>Control Panel URL: http://cpanel.mavajsunco.com/<br />Once your domain has propogated, you may also use <a href="http://www.<?php echo $this->_tpl_vars['service_domain']; ?>
:2082/">http://www.<?php echo $this->_tpl_vars['service_domain']; ?>
/</a></p>
<p><strong>Server Information</strong></p>
<p>Server Name: <?php echo $this->_tpl_vars['service_server_name']; ?>
<br />Server IP: <?php echo $this->_tpl_vars['service_server_ip']; ?>
</p>
<p>If you are using an existing domain with your new hosting account, you will need to update the nameservers to point to the nameservers listed below.</p>
<p>Nameserver 1: <?php echo $this->_tpl_vars['service_ns1']; ?>
 (<?php echo $this->_tpl_vars['service_ns1_ip']; ?>
)<br />Nameserver 2: <?php echo $this->_tpl_vars['service_ns2']; ?>
 (<?php echo $this->_tpl_vars['service_ns2_ip']; ?>
)<?php if ($this->_tpl_vars['service_ns3']): ?><br />Nameserver 3: <?php echo $this->_tpl_vars['service_ns3']; ?>
 (<?php echo $this->_tpl_vars['service_ns3_ip']; ?>
)<?php endif; ?><?php if ($this->_tpl_vars['service_ns4']): ?><br />Nameserver 4: <?php echo $this->_tpl_vars['service_ns4']; ?>
 (<?php echo $this->_tpl_vars['service_ns4_ip']; ?>
)<?php endif; ?></p>
<p><strong>Uploading Your Website</strong></p>
<p>Temporarily you may use one of the addresses given below to manage your web site:</p>
<p>Temporary FTP Hostname: <?php echo $this->_tpl_vars['service_server_ip']; ?>
<br />Temporary Webpage URL: <a href="http://<?php echo $this->_tpl_vars['service_server_ip']; ?>
/~<?php echo $this->_tpl_vars['service_username']; ?>
/">http://<?php echo $this->_tpl_vars['service_server_ip']; ?>
/~<?php echo $this->_tpl_vars['service_username']; ?>
/</a></p>
<p>And once your domain has propagated you may use the details below:</p>
<p>FTP Hostname: <?php echo $this->_tpl_vars['service_domain']; ?>
<br />Webpage URL: <a href="http://www.<?php echo $this->_tpl_vars['service_domain']; ?>
">http://www.<?php echo $this->_tpl_vars['service_domain']; ?>
</a></p>
<p><strong>Email Settings</strong></p>
<p>For email accounts that you setup, you should use the following connection details in your email program:</p>
<p><span>POP3 Host Address: mail.<?php echo $this->_tpl_vars['service_domain']; ?>
   Port : 995  ( Always use a secure connection (SSL)  )</span><br /><span>SMTP Host Address: mail.<?php echo $this->_tpl_vars['service_domain']; ?>
   Port : 465  ( Always use a secure connection (SSL)  )   </span><br />Username: The email address you are checking email for<br />Password: As specified in your control panel<br /><span>Webmail : http://webmail.mavajsunco.com/</span></p>
<p>Thank you for choosing us.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>