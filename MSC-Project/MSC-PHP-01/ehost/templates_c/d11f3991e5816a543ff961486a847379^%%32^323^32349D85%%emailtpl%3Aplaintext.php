<?php /* Smarty version 2.6.28, created on 2014-11-29 09:48:33
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>As you requested, your password for our client area has now been reset. Your new login details are as follows:</p>
<p><?php echo $this->_tpl_vars['whmcs_link']; ?>
<br />Email: <?php echo $this->_tpl_vars['client_email']; ?>
<br />Password: <?php echo $this->_tpl_vars['client_password']; ?>
</p>
<p>To change your password to something more memorable, after logging in go to My Details &gt; Change Password.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>