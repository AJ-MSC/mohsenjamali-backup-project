
<div class="banner">
  <ul class="liststyle1">
    <li>UNLIMITED POP Mailboxes</li>
    <li>UNLIMITED MySQL Databases</li>
    <li>FREE Domain Name for life</li>
    <li>UNLIMITED Domain Name</li>
  </ul>
  <div class="clr"></div>
  <div class="bannerBtn">
    <ul>
      <li><a href="sharedhosting.php">Personal<br />
        Starter</a></li>
      <li><a href="sharedhosting.php">Business<br />
        Solution</a></li>
      <li><a href="sharedhosting.php">Website<br />
        Hosting</a></li>
      <li><a href="domainchecker.php">Domain<br />
        Names</a></li>
    </ul>
  </div>
  <div class="bannerRightBtn">
    <div class="getstarted"><a href="register.php">Get Started Now!</a></div>
    <br />
    <a href="sharedhosting.php">Read More &gt;&gt; </a></div>
</div>
<div class="clr"></div>
</div>
</div>
<div class="wrapper">
<div id="leftCon">
  <div class="planCon">
    <div class="plan">
      <h1>Linux Hosting<span class="big">$3.95<span class="small">/mo</span></span></h1>
      <div class="planimage1"></div>
      <ul>
        <li><strong class="colorStyle1">UNLIMITED</strong> POP Mailboxes</li>
        <li><strong class="colorStyle1">UNLIMITED</strong> MySQL Databases</li>
        <li><strong class="colorStyle1">FREE</strong> Domain Name for life</li>
      </ul>
      <div class="planbutton">
        <div class="learnmore"><a href="sharedhosting.php">Learn More</a></div>
        <div class="PlanOrderNow"><a href="cart.php">Order Now</a></div>
      </div>
    </div>
    <div class="plan floatRight">
      <h1>Window Hosting<span class="big">$4.95<span class="small">/mo</span></span></h1>
      <div class="planimage2"></div>
      <ul>
        <li><strong class="colorStyle1">UNLIMITED</strong> POP Mailboxes</li>
        <li><strong class="colorStyle1">UNLIMITED</strong> MySQL Databases</li>
        <li><strong class="colorStyle1">FREE</strong> Domain Name for life</li>
      </ul>
      <div class="planbutton">
        <div class="learnmore"><a href="sharedhosting.php">Learn More</a></div>
        <div class="PlanOrderNow"><a href="cart.php">Order Now</a></div>
      </div>
    </div>
    <div class="clr"></div>
  </div>
  <div class="box1">
    <h1>Powerful Dedicated Servers</h1>
    <div class="box1Content">
      <ul>
        <li><strong>20GB</strong> Storage</li>
        <li><strong>220GB</strong> Bandwidth</li>
        <li><strong>Unlimited</strong> Domains</li>
      </ul>
      <ul>
        <li><strong>365/24/7 Support</strong></li>
        <li>Softaculous Auto Installer</li>
        <li>Fantastico Auto Installer</li>
      </ul>
      <div class="clr"></div>
      <div class="planbutton">
        <div class="learnmore"><a href="sharedhosting.php">Learn More</a></div>
        <div class="PlanOrderNow"><a href="cart.php">Order Now</a></div>
      </div>
    </div>
  </div>
  <div class="box2">
    <div class="box2Top"></div>
    <div class="box2Con">
      <h1>Top Reasons why to choose us ?</h1>
      <div class="reason">
        <h2>Raid Protected Disk Space</h2>
        <ul>
          <li>Our servers include</li>
          <li>Setup for your extra</li>
          <li>Setup for your extra protection! </li>
        </ul>
      </div>
      <div class="reason">
        <h2>Custom Web Templates</h2>
        <ul>
          <li>Our servers include</li>
          <li>Setup for your extra</li>
          <li>Setup for your extra protection! </li>
        </ul>
      </div>
      <div class="reason nomarginRt">
        <h2>Real 24/7 Support</h2>
        <ul>
          <li>Our servers include</li>
          <li>Setup for your extra</li>
          <li>Setup for your extra protection! </li>
        </ul>
      </div>
      <div class="clr"></div>
      <strong>We provide reliable hosting services <a href="sharedhosting.php">learn more>></a></strong></div>
  </div>
  <div class="clr"></div>
  <div class="partner"></div>
</div>
<!--EOF LeftCon-->
<div id="rightCon">
<div class="sidebox1">
  <h1>Our Guarantee!</h1>
  <ul>
    <li class="noborderTop uptime"><span class="big">99</span><span class="colorStyle1">.9%</span> Network<br />
      Uptime Guarantee!</li>
    <li class="support"><span class="big">24</span><span class="colorStyle1">x7</span> Support<br />
      Guarantee!</li>
    <li class="noborderBtm money"><span class="big">30</span> Day Money<br />
      Back Guarantee!</li>
  </ul>
</div>
<div class="sidebox2">
  <h1>We include:</h1>
  <ul>
    <li>Unlimited Premimum web</li>
    <li>Unlimited Data Transfer</li>
    <li>Free $50 on Google Adwords</li>
    <li>Free $25 on yahoo Credits</li>
    <li>Unlimited Hosted Domains</li>
    <li>Host unlimited Domains</li>
    <li>Free Blog, Forum, board</li>
  </ul>
</div>
<div class="websitetransfer"><a href="cart.php"></a></div>
<div class="testimonial"><a href="announcements.php"></a></div>
