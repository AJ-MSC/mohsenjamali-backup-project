<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset={$charset}" />
<title>{$companyname} - {$pagetitle}{if $kbarticle.title} - {$kbarticle.title}{/if}</title>
{if $systemurl}<base href="{$systemurl}" />{/if}
<link rel="stylesheet" type="text/css" href="templates/{$template}/style.css" />
<script type="text/javascript" src="includes/jscript/jquery.js"></script>
{$headoutput}
{if $livehelpjs}{$livehelpjs}{/if}
<link rel="shortcut icon" type="image/x-icon" href="https://www.whmcsthemes.com/favicon.png" />
<link href="templates/{$template}/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="templates/{$template}/resources/css/superfish.css" rel="stylesheet" type="text/css" />
</head>

<body>
{$headeroutput}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td class="leftTD" width="50%">&nbsp;</td>
  <td width="950px"><div class="headerWrapper">
      <div class="header">
        <div class="logo"><a href="index.php"></a></div>
        <div class="menuCon">
          <div class="phoneNo">123-456-789-000</div>
          <div class="livechat">Live Chat <strong class="color1">Online</strong></div>
          <div id="menu">
            <ul class="sf-menu">
              <li class="home"><a href="index.php"></a></li>
              <li><a href="sharedhosting.php"><span></span>Hosting</a></li>
              <li><a href="domainchecker.php"><span></span>Domains</a></li>
              <li><a href="knowledgebase.php"><span></span>F.A.Q's</a></li>
              <li><a href="announcements.php"><span></span>News</a></li>
              <li><a href="clientportal.php"><span></span>Client Portal</a></li>
              <li><a href="contact.php"><span></span>Contact Us</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="banner">
      <div class="bannerCon">
        <p>Choose your ideal web hosting tools from three simple options. </p>
        <ul>
          <li><span class="color2">UNLIMITED</span> WebSpace</li>
          <li> <span class="color2">UNLIMITED</span> Bandwidth</li>
          <li> <span class="color2">UNLIMITED</span> POP E-Mail ID </li>
        </ul>
        <ul>
          <li>99.9% Uptime <span class="color2">GUARANTEE</span><br />
          </li>
          <li>1 Click Script Installs</li>
        </ul>
        <div class="bannerBtnCon">
          <div class="bannerBtn"><a href="sharedhosting.php">Get Started Now!</a></div>
        </div>
      </div>
    </div></td>
  <td class="rightTD" width="50%">&nbsp;</td>
</tr>
<tr>
  <td class="bgLeft">&nbsp;</td>
  <td><div class="bodyCon">
    <div class="bodyleft">
{if $filename eq 'index' or $pagetype eq 'planpage'}
{else}
    <div class="blankBox">
    <div class="blankBoxTop"></div>
    <div class="blankBoxContent">
    <h1 class="headingBlank"><span class="color2">{$pagetitle}</span></h1>
{/if}