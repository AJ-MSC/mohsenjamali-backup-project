{if $filename eq 'index'}
{else}
{if $pagetype eq 'planpage'}
{else}
{if $langchange}<div align="right">{$setlanguage}</div><br />{/if}
</div>
</div>

<div class="clr"></div>
{/if}
</div>
<div class="bodyRight">
  <div class="siderBox01Con">
    <div class="siderBox01Top">Client Menu</div>
    <div class="siderBox01Content">
      <ul class="ulStyle03">
      {if $loggedin}
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php" title="{$LANG.clientareanavhome}">{$LANG.clientareanavhome}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php?action=details" title="{$LANG.clientareanavdetails}">{$LANG.clientareanavdetails}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php?action=products" title="{$LANG.clientareanavservices}">{$LANG.clientareanavservices}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php?action=domains" title="{$LANG.clientareanavdomains}">{$LANG.clientareanavdomains}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php?action=quotes" title="{$LANG.quotestitle}">{$LANG.quotestitle}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php?action=invoices" title="{$LANG.invoices}">{$LANG.invoices}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="supporttickets.php" title="{$LANG.clientareanavsupporttickets}">{$LANG.clientareanavsupporttickets}</a></li>
      {else}
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientportal.php" title="{$LANG.globalsystemname}">{$LANG.globalsystemname}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="register.php" title="{$LANG.clientregistertitle}">{$LANG.clientregistertitle}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="clientarea.php" title="{$LANG.clientareatitle}">{$LANG.clientareatitle}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="announcements.php" title="{$LANG.announcementstitle}">{$LANG.announcementstitle}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="knowledgebase.php" title="{$LANG.knowledgebasetitle}">{$LANG.knowledgebasetitle}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="submitticket.php" title="{$LANG.supportticketspagetitle}">{$LANG.supportticketssubmitticket}</a></li>
      <li><img src="templates/{$template}/images/play.png" align="absmiddle" border="0px" /><a href="downloads.php" title="{$LANG.downloadstitle}">{$LANG.downloadstitle}</a></li>
      {/if}
      </ul>
      <p>&nbsp;</p>
    </div>
  </div>


{if $loggedin}
  <div class="siderBox01Con">
    <div class="siderBox01Top">{$LANG.accountinfo}</div>
    <div class="siderBox01Content">
      <div class="planContent">
<p><strong>{$clientsdetails.firstname} {$clientsdetails.lastname} {if $clientsdetails.companyname}({$clientsdetails.companyname}){/if}</strong><br />
{$clientsdetails.address1}, {$clientsdetails.address2}<br />
{$clientsdetails.city}, {$clientsdetails.state}, {$clientsdetails.postcode}<br />
{$clientsdetails.countryname}<br />
{$clientsdetails.email}<br /><br />
{if $condlinks.addfunds}<img src="templates/{$template}/images/icons/money.gif" alt="Add Funds" width="22" height="22" border="0" class="absmiddle" /> <a href="clientarea.php?action=addfunds">{$LANG.addfunds}</a>{/if}</p>
      </div>
	</div>
  </div>
  <div class="siderBox01Con">
    <div class="siderBox01Top">{$LANG.accountstats}</div>
    <div class="siderBox01Content">
      <div class="planContent">
    <p>{$LANG.statsnumproducts}: <strong>{$clientsstats.productsnumactive}</strong> ({$clientsstats.productsnumtotal})<br />
{$LANG.statsnumdomains}: <strong>{$clientsstats.numactivedomains}</strong> ({$clientsstats.numdomains})<br />
{$LANG.statsnumtickets}: <strong>{$clientsstats.numtickets}</strong><br />
{$LANG.statsnumreferredsignups}: <strong>{$clientsstats.numaffiliatesignups}</strong><br />
{$LANG.statscreditbalance}: <strong>{$clientsstats.creditbalance}</strong><br />
{$LANG.statsdueinvoicesbalance}: <strong>{if $clientsstats.numdueinvoices>0}<span class="red">{/if}{$clientsstats.dueinvoicesbalance}{if $clientsstats.numdueinvoices>0}</span>{/if}</strong></p>
      </div>
	</div>
  </div>
{else}
  <div class="siderBox01Con">
    <div class="siderBox01Top">{$LANG.clientlogin}</div>
    <div class="siderBox01Content">
      <div class="planContent">
<form method="post" action="{$systemsslurl}dologin.php">
  <p><strong>{$LANG.email}</strong><br />
    <input name="username" type="text" size="25" />
  </p>
  <p><strong>{$LANG.loginpassword}</strong><br />
    <input name="password" type="password" size="25" />
  </p>
  <p>
    <input type="checkbox" name="rememberme" />
    {$LANG.loginrememberme}</p>
  <p>
    <input type="submit" class="submitbutton" value="{$LANG.loginbutton}" />
  </p>
</form>
      </div>
	</div>
  </div>
  <div class="siderBox01Con">
    <div class="siderBox01Top">{$LANG.knowledgebasesearch}</div>
    <div class="siderBox01Content">
      <div class="planContent">
<form method="post" action="knowledgebase.php?action=search">
  <p>
    <input name="search" type="text" size="25" /><br /><br />
    <select name="searchin">
      <option value="Knowledgebase">{$LANG.knowledgebasetitle}</option>
      <option value="Downloads">{$LANG.downloadstitle}</option>
    </select>
    <input type="submit" value="{$LANG.go}" />
  </p>
</form>
      </div>
	</div>
  </div>
{/if}
{if $twitterusername}
  <div class="siderBox01Con">
    <div class="siderBox01Top">{$LANG.twitterfollow}</div>
    <div class="siderBox01Content">
      <div class="planContent">
<p align="center"><a href="http://twitter.com/{$twitterusername}" target="_blank"><img src="templates/{$template}/images/twitter.png" border="0" alt="{$LANG.twitterfollow}" /></a></p>
      </div>
	</div>
  </div>
{/if}
{/if}
</div>
</div>
</td>
<td class="bgRight">&nbsp;</td>
</tr>
<tr>
  <td class="footerL">&nbsp;</td>
  <td class="footer"><div class="wrapper">
      <div class="siteMapCon">
        <div class="siteMap">
      <h4>Clients</h4>
      <ul>
        <li><a href="clientarea.php">Client Area</a></li>
        <li><a href="register.php">Register</a></li>
        <li><a href="pwreset.php">Reset Password</a></li>
      </ul>
        </div>
        <div class="siteMap">
      <h4>Hosting</h4>
      <ul>
        <li><a href="sharedhosting.php">Shared Hosting</a></li>
        <li><a href="domainchecker.php">Domain Names</a></li>
        <li><a href="cart.php">Order Now</a></li>
      </ul>
        </div>
        <div class="siteMap">
      <h4>Partners</h4>
      <ul>
        <li><a href="http://themeforest.net/?ref=ZumadaLimited" target="_blank">Theme Forest</a></li>
        <li><a href="http://graphicriver.net/?ref=ZumadaLimited" target="_blank">Graphic River</a></li>
        <li><a href="http://codecanyon.net/?ref=ZumadaLimited" target="_blank">Code Canyon</a></li>
      </ul>
        </div>
        <div class="siteMap noMR">
      <h4>WHMCS</h4>
      <ul>
        <li><a href="http://www.whmcsthemes.com/premium-whmcs-templates.php" target="_blank">Premium Themes</a></li>
        <li><a href="http://www.whmcsthemes.com/whmcs-templates.php" target="_blank">Low Cost Themes</a></li>
        <li><a href="http://www.freewhmcstemplates.com/" target="_blank">Free Themes</a></li>
      </ul>
        </div>
        <div class="socailCon">
          <h6>Connect with us!</h6>
          <div class="socailNet"><a href="http://www.facebook.com/whmcsthemes" target="_blank"></a><a href="http://www.twitter.com/whmcsthemes" target="_blank"></a><a href="http://www.linkedin.com" target="_blank" class="noMR"></a> </div>
          <p>Copyright © 2012 - All Rights Reserved.<br /><a href="http://www.whmcsthemes.com" target="_blank" style="color:#fff;">WHMCSThemes.com</a></p>
        </div>
      </div>
    </div></td>
  <td class="footerR">&nbsp;</td>
</tr>
</table>
{$footeroutput}
</body></html>