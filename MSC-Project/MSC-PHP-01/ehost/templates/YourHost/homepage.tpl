        <div class="boxCon">
          <div class="box01"><span class="color3">365x7x24</span><br />
            LIVECHAT</div>
          <div class="box02">FREE<br />
            DOMAIN FOR LIFE</div>
          <div class="box03">GREEN<br />
            WEB HOSTING</div>
        </div>
        <div class="clr"></div>
        <div class="planCon">
          <div class="personal">
            <div class="priceCon"><span class="Text36">$</span><span class="Text100">7</span>.<span class="Text36">96</span>/Month</div>
            <div class="planUlCon">
              <ul>
                <li>1000 MB of Disk Space</li>
                <li>5000 MB of Bandwidth</li>
                <li>15  Email Accounts</li>
              </ul>
              <div class="orderBtn01 ordernow03"><a href="cart.php">Order Now</a></div>
            </div>
          </div>
          <div class="business">
            <div class="priceCon priceCon02"><span class="Text36">$</span><span class="Text99">14</span>.<span class="Text36">96</span>/Month</div>
            <div class="planUlCon">
              <ul>
                <li>1000 MB of Disk Space</li>
                <li>5000 MB of Bandwidth</li>
                <li>15  Email Accounts</li>
              </ul>
              <div class="orderBtn01 ordernow03"><a href="cart.php">Order Now</a></div>
            </div>
          </div>
        </div>
        <div class="box04">
          <div class="box04Top"></div>
          <div class="box04Content">
            <h1 class="headingStyle01"><span class="color2">Welcome to</span> Your Host</h1>
            <p class="pStyle01">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.consequat.nonummy nibh euismod tincidunt ut laoreet dolore magna</p>
            <p> <span class="pStyle01">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</span><br />
              <a href="sharedhosting.php" class="anchorStyle01">Click here to get started!</a></p>
          </div>
        </div>
      </div>
      <div class="bodyRight">
        <div class="siderBoxCon">
          <div class="siderBoxTop">WHY CHOOSE US!</div>
          <div class="siderBoxContent">
            <div class="featureBox">
              <div class="imgCon img01">&nbsp;</div>
              <h3>100% Uptime</h3>
              <p>You can enter some more text here.</p>
            </div>
            <div class="featureBox">
              <div class="imgCon img02">&nbsp;</div>
              <h3>Money Back Guarantee</h3>
              <p>You can enter some more text here.</p>
            </div>
            <div class="featureBox">
              <div class="imgCon img03">&nbsp;</div>
              <h3>New Offers</h3>
              <p>You can enter some more text here.</p>
            </div>
            <div class="featureBox">
              <div class="imgCon img04">&nbsp;</div>
              <h3>Website Statistics</h3>
              <p>You can enter some more text here.</p>
            </div>
          </div>
        </div>
        <div class="testimonials">
          <h4>TESTIMONIALS</h4>
          <p>Lorem ipsum dolor sit amet,<br />
            constuer adipiscing elit, sed diam nonummy nibh euismo tincidunt ut laoreet dolore magna enim. constuer adipiscing elit, </p>
          \
          <div class="anchorCon">John Doe</div><br />

        </div>
