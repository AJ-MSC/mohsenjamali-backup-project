        <div class="clr"></div>
        <div class="information">
          <div class="basic">
            <div class="basic_header">Basic Information</div>
            <ul class="BorderStyleLt">
              <li>Web Space</li>
              <li>Monthly Bandwidth</li>
              <li>Order Now &nbsp;</li>
            </ul>
          </div>
          <div class="c1">
            <div class="c1_header">Basic </div>
            <ul>
              <li>2GB</li>
              <li>10GB </li>
              <li class="Style03">$8.95</li>
              <li class="NoBorder">
                <div class="OrderNowBtn"><a href="cart.php">Order Now</a></div>
              </li>
            </ul>
          </div>
          <div class="c1">
            <div class="c1_header">Standard</div>
            <ul>
              <li>2GB</li>
              <li>10GB </li>
              <li class="Style03">$8.95</li>
              <li class="NoBorder">
                <div class="OrderNowBtn"><a href="cart.php">Order Now</a></div>
              </li>
            </ul>
          </div>
          <div class="c1">
            <div class="c1_header Style04">Advance </div>
            <ul>
              <li>2GB</li>
              <li>10GB </li>
              <li class="Style03">$8.95</li>
              <li class="NoBorder">
                <div class="OrderNowBtn"><a href="cart.php">Order Now</a></div>
              </li>
            </ul>
          </div>
          <div class="clr"></div>
          <div class="feature">
            <div class="feature_header Style05">FEATURES</div>
            <ul class="BorderStyleLt">
              <li>Monthly Price</li>
              <li>Annual Price</li>
              <li>Setup Fee</li>
              <li>Multi-domain Hosting</li>
              <li>Subdomains</li>
              <li>Bandwidth</li>
              <li>Web Disk Space</li>
              <li>Email Disk Space</li>
              <li>MySQL Databases</li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header">&nbsp;</div>
            <ul>
              <li>15</li>
              <li>100GB</li>
              <li class="check">&nbsp;</li>
              <li>5</li>
              <li class="check">&nbsp;</li>
              <li>DirectAdmin</li>
              <li><strong>Unlimited</strong></li>
              <li>N/A</li>
              <li><strong>Unlimited</strong></li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header">&nbsp;</div>
            <ul>
              <li>15</li>
              <li>100GB</li>
              <li class="check">&nbsp;</li>
              <li>5</li>
              <li class="check">&nbsp;</li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header Style04">&nbsp;</div>
            <ul>
              <li>15</li>
              <li>100GB</li>
              <li class="check">&nbsp;</li>
              <li>5</li>
              <li class="check">&nbsp;</li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
            </ul>
          </div>
          <div class="feature">
            <div class="feature_header Style05">EMAIL FEATURES</div>
            <ul class="BorderStyleLt">
              <li>Monthly Price</li>
              <li>Annual Price</li>
              <li>Setup Fee</li>
              <li>Multi-domain Hosting</li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header">&nbsp;</div>
            <ul>
              <li>DirectAdmin</li>
              <li><strong>Unlimited</strong></li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header">&nbsp;</div>
            <ul>
              <li>DirectAdmin</li>
              <li><strong>Unlimited</strong></li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header Style04">&nbsp;</div>
            <ul>
              <li>DirectAdmin</li>
              <li><strong>Unlimited</strong></li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
            </ul>
          </div>
          <div class="feature">
            <div class="feature_header Style05">DATABASE</div>
            <ul class="BorderStyleLt">
              <li>Monthly Price</li>
              <li>Annual Price</li>
              <li>Setup Fee</li>
              <li>Multi-domain Hosting</li>
              <li>Subdomains</li>
              <li>Bandwidth</li>
              <li>Web Disk Space</li>
              <li>Order Now </li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header">&nbsp;</div>
            <ul>
              <li>5GB</li>
              <li>5GB</li>
              <li>2&nbsp;</li>
              <li>DirectAdmin</li>
              <li class="check">&nbsp;</li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
              <li class="Style03">$8.95</li>
              <li class="NoBorder">
                <div class="OrderNowBtn"><a href="cart.php">Order Now</a></div>
              </li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header">&nbsp;</div>
            <ul>
              <li>5GB</li>
              <li>5GB</li>
              <li>2&nbsp;</li>
              <li>DirectAdmin</li>
              <li class="check">&nbsp;</li>
              <li class="check">&nbsp;</li>
              <li><strong>Unlimited</strong></li>
              <li class="Style03">$8.95</li>
              <li class="NoBorder">
                <div class="OrderNowBtn"><a href="cart.php">Order Now</a></div>
              </li>
            </ul>
          </div>
          <div class="c2">
            <div class="feature_header Style04">&nbsp;</div>
            <ul >
              <li>5GB</li>
              <li>5GB</li>
              <li>2&nbsp;</li>
              <li>DirectAdmin</li>
              <li class="check">&nbsp;</li>
              <li>N/A</li>
              <li><strong>Unlimited</strong></li>
              <li class="Style03">$8.95</li>
              <li class="NoBorder">
                <div class="OrderNowBtn"><a href="cart.php">Order Now</a></div>
              </li>
            </ul>
          </div>
        </div>
