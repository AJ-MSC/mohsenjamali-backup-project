<?php
session_start();

if ($_POST['section'] == "business") {
    if (empty($_SESSION['captcha_code']) || strcasecmp($_SESSION['captcha_code'], $_POST['bCaptcha']) != 0) {
        echo "false";
    } else {// Captcha verification is Correct. Final Code Execute here!		
        echo "true";
    }
}

if ($_POST['section'] == "quote") {
    if (empty($_SESSION['captcha_code']) || strcasecmp($_SESSION['captcha_code'], $_POST['qCaptcha']) != 0) {
        echo "false";
    } else {// Captcha verification is Correct. Final Code Execute here!		
        echo "true";
    }
}

if ($_POST['section'] == "support") {
    if (empty($_SESSION['captcha_code']) || strcasecmp($_SESSION['captcha_code'], $_POST['sCaptcha']) != 0) {
        echo "false";
    } else {// Captcha verification is Correct. Final Code Execute here!		
        echo "true";
    }
}