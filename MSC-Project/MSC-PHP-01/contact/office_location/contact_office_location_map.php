<header id="page-title" class="nopadding">
    <div id="gmap"><!-- google map --></div>
    <script type="text/javascript">
        var $googlemap_latitude = <?= $lang['GOOGLEMAP_LATITUDE'] ?>,
                $googlemap_longitude = <?= $lang['GOOGLEMAP_LONGITUDE'] ?>,
                $googlemap_zoom = <?= $lang['GOOGLEMAP_ZOOM'] ?>
    </script>
</header>