<?php
// GLOBAL VALUABLE -------------------------------------------------------------
$rootHost = $_SERVER['HTTP_HOST'] . "/new/";
$rootDocument = $_SERVER['DOCUMENT_ROOT'] . "/new";
//Start Page -------------------------------------------------------------------
include_once $rootDocument . "/common/language_selector.php";
include_once $rootDocument . "/common/header.php";
?>
<!-- WRAPPER -->
<div id="wrapper">
    <?php
    //include_once $rootDocument . "/common/breadcrumb_toolbar.php";
    include_once $rootDocument . "/common/switcher.php";
    include_once $rootDocument . "/contact/office_location/contact_office_location_map.php";
    ?>
    <div  style="background:url(http://<?php echo $rootHost; ?>/assets/plugins/revolution-slider/assets/shadow3.png) no-repeat; width: 100%; left: 0px; background-size:100%;  height:60px; margin-top: -63px;"></div>
    <section id="contact" class="container">
        <div class="row">   
            <?php
            include_once $rootDocument . "/contact/office_location/contact_office_location_form.php";
            include_once $rootDocument . "/contact/office_location/contact_office_location_details.php";
            ?>
        </div>
    </section>
</div>
<!-- /WRAPPER -->
<?php
include_once $rootDocument . "/common/footer.php";
?>