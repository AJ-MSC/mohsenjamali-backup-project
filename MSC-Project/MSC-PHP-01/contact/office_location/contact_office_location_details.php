<!-- INFO -->
<div class="col-md-4">

    <h2><?= $lang['CONTACT_DETAILS_TITLE'] ?></h2>

    <p><?= $lang['CONTACT_DETAILS'] ?></p>

    <div class="divider half-margins"><!-- divider -->
        <i class="fa fa-star"></i>
    </div>

    <p>
        <span class="block"><strong><i class="fa fa-map-marker"></i> <?= $lang['GLOBAL_ADDRESS_TITLE'] ?> :</strong> <?= $lang['GLOBAL_ADDRESS'] ?></span>
        <span class="block"><strong><i class="fa fa-phone"></i> <?= $lang['GLOBAL_PHONE_TITLE'] ?> :</strong> <?= $lang['GLOBAL_PHONE'] ?></span>
        <span class="block"><strong><i class="fa fa-envelope"></i> <?= $lang['GLOBAL_EMAIL_TITLE'] ?> :</strong> <a href="mailto:<?= $lang['GLOBAL_EMAIL'] ?>"><?= $lang['GLOBAL_EMAIL'] ?></a></span>
    </p>

    <div class="divider half-margins"><!-- divider -->
        <i class="fa fa-star"></i>
    </div>

    <h4 class="font300"><?= $lang['CONTACT_BUSINESSHOURS_TITLE'] ?></h4>
    <p>
        <span class="block"><strong><?= $lang['CONTACT_BUSINESSHOURS_NORMALDAY'] ?> :</strong> <?= $lang['CONTACT_BUSINESSHOURS_NORMALDAY_TIME'] ?></span>
        <span class="block"><strong><?= $lang['CONTACT_DETAILS_HOLIDAYS'] ?> :</strong> <?= $lang['CONTACT_DETAILS_HOLIDAYS_POSITION'] ?></span>
    </p>

</div>
<!-- /INFO -->