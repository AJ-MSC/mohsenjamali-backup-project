<!-- FORM -->
<div class="col-md-8">

    <h2><?= $lang['CONTACT_FORM_TITILE'] ?></h2>

    <!-- SENT OK -->
    <div id="_sent_ok_" class="alert alert-success fade in fsize16 hide">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?= $lang['CONTACT_FORM_SUCCESSFULMASSAGE'] ?>
    </div>
    <!-- /SENT OK -->

    <!-- SENT FAILED -->
    <div id="_sent_required_" class="alert alert-danger fade in fsize16 hide">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?= $lang['CONTACT_FORM_ERRORMASSAGE'] ?>
    </div>
    <!-- /SENT FAILED -->

    <form id="contactForm" class="white-row" action="http://<?php echo $rootHost; ?>utility/contact.php" method="post">
        <div class="row">
            <div class="form-group">
                <div class="col-md-4">
                    <label><?= $lang['CONTACT_FORM_FULLNAME'] ?> *</label>
                    <input type="text" value="" data-msg-required="<?= $lang['CONTACT_FORM_FULLNAME_MSG'] ?>" maxlength="100" class="form-control" name="contact_name" id="contact_name">
                </div>
                <div class="col-md-4">
                    <label><?= $lang['CONTACT_FORM_EMAIL'] ?> *</label>
                    <input type="email" value="" data-msg-required="<?= $lang['CONTACT_FORM_EMAIL_MSG'] ?>" data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="contact_email" id="contact_email">
                </div>
                <div class="col-md-4">
                    <label><?= $lang['CONTACT_FORM_PHONE'] ?></label>
                    <input type="text" value="" data-msg-required="<?= $lang['CONTACT_FORM_PHONE_MSG'] ?>" data-msg-email="<?= $lang['CONTACT_FORM_PHONE_MSG'] ?>" maxlength="100" class="form-control" name="contact_phone" id="contact_phone">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <label><?= $lang['CONTACT_FORM_SUBJECT'] ?></label>
                    <input type="text" value="" data-msg-required="<?= $lang['CONTACT_FORM_SUBJECT_MSG'] ?>" maxlength="100" class="form-control" name="contact_subject" id="contact_subject">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <label><?= $lang['CONTACT_FORM_MESSEAGE'] ?> *</label>
                    <textarea maxlength="5000" data-msg-required="<?= $lang['CONTACT_FORM_MESSEAGE_MSG'] ?>" rows="10" class="form-control" name="contact_message" id="contact_message"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <span class="pull-right"><!-- captcha -->
                    <label class="block text-right fsize12"><?= $lang['CONTACT_FORM_CAPTCHA'] ?></label>
                    <img alt="" rel="nofollow,noindex" width="50" height="18" src="http://<?php echo $rootHost; ?>utility/captcha.php?bgcolor=ffffff&amp;txtcolor=000000">
                    <input type="text" name="contact_captcha" id="contact_captcha" value="" data-msg-required="<?= $lang['CONTACT_FORM_CAPTCHA_MESSEAGE'] ?>" maxlength="6" style="width:100px; margin-left:10px;">
                </span>

                <input id="contact_submit" type="submit" value="<?= $lang['CONTACT_FORM_SUBMIT'] ?>" class="btn btn-primary btn-lg" data-loading-text="<?= $lang['CONTACT_FORM_SUBMIT_WAIT'] ?>">
            </div>
        </div>
    </form>

</div>
<!-- /FORM -->