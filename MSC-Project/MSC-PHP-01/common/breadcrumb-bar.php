!-- PAGE TITLE -->
<?php
if (is_array($breadcrumb[$page]['POSITION'])) {
    echo '<header id="page-title"><div class="container"><h4>' . $title . '</h4>'; 
    echo '<ul class="breadcrumb">';
    foreach ($breadcrumb[$page]['POSITION'] as $key => $val) {
        if ($key === $page) {
            echo '<li class="active">' . $val . '</li>';
        } else {
            echo '<li><a href="' . $key . '">' . $val . '</a></li>';
        }
    }
    echo '</ul></div></header>';
}
?>

//<?php
//
//	function getPageTitle()
//	{
//		$page = basename( $_SERVER['PHP_SELF'] );
//		if($page === 'index.php')
//			return 'Home';
//		else {
//			$title = implode( ' ', explode( '-', explode('.',$page)[0] ) );
//			return ucwords(strtolower($title));
//		}
//	}
//
//?>
