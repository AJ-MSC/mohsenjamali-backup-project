<!-- FOOTER LATEST POSTS -->
<div class="column col-md-4 text-right">
    <h3><?=$lang['BLOG_TITLE']?></h3>

    <?php
    $url = "http://www.MavajSunCo.com/blog/feed/";
    $rss = simplexml_load_file($url);
    if ($rss) {
        $items = $rss->channel->item;
        $counter = 0;
        foreach ($items as $item) {
            if ($counter == 3) {
                break;
            }
            $counter++;
            $title = $item->title;
            $link = $item->link;
            $published_on = $item->pubDate;
            $description = $item->description;

            echo '<div class="post-item"><small>' . date("l, d F Y", strtotime($published_on))  . '</small>';
            echo '<h3><a href="' . $link . '">' . $title . '</a></h3> </div>';
        }
    }
    ?> 
    <a href="http://www.MavajSunCo.com/blog/" class="view-more pull-right"><?=$lang['BLOG_MORE']?> <i class="fa fa-arrow-right"></i></a>

</div>
<!-- /FOOTER LATEST POSTS -->