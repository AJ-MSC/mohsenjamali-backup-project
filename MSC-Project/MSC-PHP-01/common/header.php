<!--
 * Copyright (c) 1978-2028 MAVAJ SUN CO, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of MAVAJ SUN
 * CO, Inc. ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the terms
 * of the license agreement you entered into with MAVAJ SUN CO.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND . ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. MAVAJ SUN CO AND ITS LICENSORS SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING ,
 * MODIFYING OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL
 * MAVAJ SUN CO OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR
 * DATA,OR FOR DIRECT,INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE
 * DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING
 * OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear facility.
 * Licensee represents and warrants that it will not use or redistribute the
 * Software for such purposes.
 *
 *
 * @author  : MavajSunCo
 * @Email   : info@MavajSunCo.com
 * @Website : www.MavajSunCo.com
 * @Date    : 2014-01-01
 * @version : 3
 *
 * @Decsriptoin :
 *
-->
<?php
// Handles the Breadcrumb Part
$page = basename($_SERVER['PHP_SELF']);
$rootHTTP ='http://'. $_SERVER['HTTP_HOST'];
$rootDOC = $_SERVER['DOCUMENT_ROOT'] ;
include_once $rootDOC."/common/breadcrumb.php";

if (is_array($breadcrumb[$page])) {
    $title = $breadcrumb[$page]['TITLE'];
    $keyword = $breadcrumb[$page]['KEYWORDS'];
    $description = $breadcrumb[$page]['DESCRIPTION'];
}
//include_once  "/common/breadcrumb-bar.php";
//$myArray = include '/common/breadcrumb.php';
//
//if (is_array($myArray[$page])) {
//    $breadcrumb = '<ul class="breadcrumb">';
//
//    foreach (array_slice($myArray[$page], 0, -3, true) as $key => $val) {
//        if ($key === $page) {
//            $breadcrumb .= '<li class="active">' . $val . '</li>';
//        } else {
//            $breadcrumb .= '<li><a href="' . $key . '">' . $val . '</a></li>';
//        }
//    }
//    $breadcrumb .= '</ul>';
//
//    $title = $myArray[$page]['title'];
//    $keyword = $myArray[$page]['keyword'];
//    $description = $myArray[$page]['description'];
//}
?>
<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots"         content="index, follow" />
        <meta name="geo.region"     content="AE-DU" />
        <meta name="geo.placename"  content="Dubai" />
        <meta name="geo.position"   content="25.043184;55.136961" />
        <meta name="ICBM"           content="25.043184, 55.136961" />
        <meta name="keywords"       content="<?= $keyword; ?> " />
        <meta name="description"    content="<?= $description; ?> " />
        <meta name="generator"      content="MAVAJ SUN CO" />
        <meta name="author"         content="www.MavajSunCo.com">
        <title><?= $title; ?></title>
        <link rel="shortcut icon" href="http://www.MavajSunCo.com/resources/img/favicon.png" />
        <!-- Bootstrap -->
        <link  rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Yamm 3 Megamenu Style -->
        <link rel="stylesheet" href="http://www.MavajSunCo.com/resources/css/yamm.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:900,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://www.MavajSunCo.com/resources/css/MavajSunCo.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>  
        <div class="container">
            <h1 id="logo">MAVAJ SUN CO</h1> 
        </div>
        <?php include_once $rootDOC.'/common/main-menu.php'; ?>
        <div class="container"><!-- Container Section -->