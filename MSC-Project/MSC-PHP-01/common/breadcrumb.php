<?php

$breadcrumb = array();

//--------- Start Breadcrumb ---------------------------------------------------
// --- Home Page 
$breadcrumb['index.php']['NAME']                                                = 'Home';
$breadcrumb['index.php']['POSITION']                                            = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['index.php']['TITLE']                                               = 'HOME - MAVAJ SUN CO';
$breadcrumb['index.php']['KEYWORDS']                                            = 'Website design , Website development , Web Application Development , Web hosting , Domain Name registration , Software development , Mobile app development , Online marketing , IT consulting , training , Application Development , software services outsourcing , software development company , software company , quality assurance testing, content management system';
$breadcrumb['index.php']['DESCRIPTION']                                         = 'M.S.C ( Mavaj Sun Co ) provides website design , website development , web hosting , Domain Name registration , software development , Mobile app development , online marketing , IT consulting & training through offshore development centers , serving Software Development Companies and Online Businesses . ';
$breadcrumb['index.php']['URL']                                                 = $rootHTTP.'/index.php';
// --- Home Page /
// 
// --- Sitemap Page 
$breadcrumb['sitemap.php']['NAME']                                                = 'Home';
$breadcrumb['sitemap.php']['POSITION']                                            = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['sitemap.php']['TITLE']                                               = '';
$breadcrumb['sitemap.php']['KEYWORDS']                                            = '';
$breadcrumb['sitemap.php']['DESCRIPTION']                                         = '';
$breadcrumb['sitemap.php']['URL']                                                 = $rootHTTP.'/sitemap.php';
// --- Sitemap Page /

// -------------------------------------------------------------------- SERVICES        
// --- Services Page 
$breadcrumb['services.php']['NAME']                                             = 'Services';
$breadcrumb['services.php']['POSITION']                                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['services.php']['TITLE']                                            = 'Services';
$breadcrumb['services.php']['KEYWORDS']                                         = 'Services';
$breadcrumb['services.php']['DESCRIPTION']                                      = 'Services';
$breadcrumb['services.php']['URL']                                              = $rootHTTP.'/services/services.php';
// --- Services Page /

// ---------------------------------------------------------------- STRATEGY ---
$breadcrumb['strategy.php']['NAME']                                             = 'Strategy';

// --- Website Strategy & Planning Page 
$breadcrumb['website-strategy-and-planning.php']['NAME']                        = 'Website Strategy & Planning';
$breadcrumb['website-strategy-and-planning.php']['POSITION']                    = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['website-strategy-and-planning.php']['TITLE']                       = 'Website Strategy & Planning';
$breadcrumb['website-strategy-and-planning.php']['KEYWORDS']                    = 'Website Strategy & Planning';
$breadcrumb['website-strategy-and-planning.php']['DESCRIPTION']                 = 'Website Strategy & Planning';
$breadcrumb['website-strategy-and-planning.php']['URL']                         = $rootHTTP.'/services/strategy/website-strategy-and-planning.php';
// --- Website Strategy & Planning Page /      
        
// --- User Interface Designs Page 
$breadcrumb['user-interface-designs.php']['NAME']                               = 'User Interface Designs';
$breadcrumb['user-interface-designs.php']['POSITION']                           = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['user-interface-designs.php']['TITLE']                              = 'User Interface Designs';
$breadcrumb['user-interface-designs.php']['KEYWORDS']                           = 'User Interface Designs';
$breadcrumb['user-interface-designs.php']['DESCRIPTION']                        = 'User Interface Designs';
$breadcrumb['user-interface-designs.php']['URL']                                = $rootHTTP.'/services/strategy/user-interface-designs.php';
// --- User Interface Designs Page /
        
// --- Information Architecture Page 
$breadcrumb['information-architecture.php']['NAME']                             = 'Information Architecture';
$breadcrumb['information-architecture.php']['POSITION']                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['information-architecture.php']['TITLE']                            = 'Information Architecture';
$breadcrumb['information-architecture.php']['KEYWORDS']                         = 'Information Architecture';
$breadcrumb['information-architecture.php']['DESCRIPTION']                      = 'Information Architecture';
$breadcrumb['information-architecture.php']['URL']                              = $rootHTTP.'/services/strategy/information-architecture.php';
// --- Information Architecture Page /

// --- Online Marketing Strategy Page 
$breadcrumb['online-marketing-strategy.php']['NAME']                            = 'Online Marketing Strategy';
$breadcrumb['online-marketing-strategy.php']['POSITION']                        = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['online-marketing-strategy.php']['TITLE']                           = 'Online Marketing Strategy';
$breadcrumb['online-marketing-strategy.php']['KEYWORDS']                        = 'Online Marketing Strategy';
$breadcrumb['online-marketing-strategy.php']['DESCRIPTION']                     = 'Online Marketing Strategy';
$breadcrumb['online-marketing-strategy.php']['URL']                             = $rootHTTP.'/services/strategy/online-marketing-strategy.php';
// --- Online Marketing Strategy Page /
        
// --- Social Media Planning Page 
$breadcrumb['social-media-planning.php']['NAME']                                = 'Social Media Planning';
$breadcrumb['social-media-planning.php']['POSITION']                            = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['social-media-planning.php']['TITLE']                               = 'Social Media Planning';
$breadcrumb['social-media-planning.php']['KEYWORDS']                            = 'Social Media Planning';
$breadcrumb['social-media-planning.php']['DESCRIPTION']                         = 'Social Media Planning';
$breadcrumb['social-media-planning.php']['URL']                                 = $rootHTTP.'/services/strategy/social-media-planning.php';
// --- Social Media Planning Page /

// ------------------------------------------------------------------ CREATE ---
$breadcrumb['create.php']['NAME']                                               = 'Create';

// --- Website Design Page 
$breadcrumb['web-design.php']['NAME']                                           = 'Website Design';
$breadcrumb['web-design.php']['POSITION']                                       = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['web-design.php']['TITLE']                                          = 'Website Design';
$breadcrumb['web-design.php']['KEYWORDS']                                       = 'Website Design';
$breadcrumb['web-design.php']['DESCRIPTION']                                    = 'Website Design';
$breadcrumb['web-design.php']['URL']                                            = $rootHTTP.'/services/create/web-design.php';
// --- Website Design Page /

// --- Web Development Page 
$breadcrumb['web-development.php']['NAME']                                      = 'Web Development';
$breadcrumb['web-development.php']['POSITION']                                  = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['web-development.php']['TITLE']                                     = 'Web Development';
$breadcrumb['web-development.php']['KEYWORDS']                                  = 'Web Development';
$breadcrumb['web-development.php']['DESCRIPTION']                               = 'Web Development';
$breadcrumb['web-development.php']['URL']                                       = $rootHTTP.'/services/create/web-development.php';
// --- Web Development Page /
        
// --- Content Management Systems Page 
$breadcrumb['content-management-systems.php']['NAME']                           = 'Content Management Systems';
$breadcrumb['content-management-systems.php']['POSITION']                       = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['content-management-systems.php']['TITLE']                          = 'Content Management Systems';
$breadcrumb['content-management-systems.php']['KEYWORDS']                       = 'Content Management Systems';
$breadcrumb['content-management-systems.php']['DESCRIPTION']                    = 'Content Management Systems';
$breadcrumb['content-management-systems.php']['URL']                            = $rootHTTP.'/services/create/content-management-systems.php';
// --- Content Management Systems Page /
        
// --- E-Commerce Applications Page 
$breadcrumb['e-commerce.php']['NAME']                                           = 'E-Commerce Applications';
$breadcrumb['e-commerce.php']['POSITION']                                       = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['e-commerce.php']['TITLE']                                          = 'E-Commerce Applications';
$breadcrumb['e-commerce.php']['KEYWORDS']                                       = 'E-Commerce Applications';
$breadcrumb['e-commerce.php']['DESCRIPTION']                                    = 'E-Commerce Applications';
$breadcrumb['e-commerce.php']['URL']                                            = $rootHTTP.'/services/create/e-commerce.php';
// --- E-Commerce Applications Page /

// --- Mobile Website Design Page 
$breadcrumb['mobile-website-design.php']['NAME']                                = 'Mobile Website Design';
$breadcrumb['mobile-website-design.php']['POSITION']                            = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['mobile-website-design.php']['TITLE']                               = 'Mobile Website Design';
$breadcrumb['mobile-website-design.php']['KEYWORDS']                            = 'Mobile Website Design';
$breadcrumb['mobile-website-design.php']['DESCRIPTION']                         = 'Mobile Website Design';
$breadcrumb['mobile-website-design.php']['URL']                                 = $rootHTTP.'/services/create/mobile-website-design.php';
// --- Mobile Website Design Page /
            
// --- Mobile Apps Development Page 
$breadcrumb['mobile-apps-development.php']['NAME']                              = 'Mobile Apps Development';
$breadcrumb['mobile-apps-development.php']['POSITION']                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['mobile-apps-development.php']['TITLE']                             = 'Mobile Apps Development';
$breadcrumb['mobile-apps-development.php']['KEYWORDS']                          = 'Mobile Apps Development';
$breadcrumb['mobile-apps-development.php']['DESCRIPTION']                       = 'Mobile Apps Development';
$breadcrumb['mobile-apps-development.php']['URL']                               = $rootHTTP.'/services/create/mobile-apps-development.php';
// --- Mobile Apps Development Page /
           
// --- Blog Development Page 
$breadcrumb['blog-development.php']['NAME']                                     = 'Blog Development';
$breadcrumb['blog-development.php']['POSITION']                                 = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['blog-development.php']['TITLE']                                    = 'Blog Development';
$breadcrumb['blog-development.php']['KEYWORDS']                                 = 'Blog Development';
$breadcrumb['blog-development.php']['DESCRIPTION']                              = 'Blog Development';
$breadcrumb['blog-development.php']['URL']                                      = $rootHTTP.'/services/create/blog-development.php';
// --- Blog Development Page /

// ----------------------------------------------------------------- PROMOTE ---
$breadcrumb['promote.php']['NAME']                                               = 'Promote';

// --- Search Engine Optimization Page 
$breadcrumb['search-engine-optimisation.php']['NAME']                           = 'Search Engine Optimization';
$breadcrumb['search-engine-optimisation.php']['POSITION']                       = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['search-engine-optimisation.php']['TITLE']                          = 'Search Engine Optimization';
$breadcrumb['search-engine-optimisation.php']['KEYWORDS']                       = 'Search Engine Optimization';
$breadcrumb['search-engine-optimisation.php']['DESCRIPTION']                    = 'Search Engine Optimization';
$breadcrumb['search-engine-optimisation.php']['URL']                            = $rootHTTP.'/services/promote/search-engine-optimisation.php';
// --- Search Engine Optimization Page /
        
// --- Social Media Marketing Page 
$breadcrumb['social-media-marketing.php']['NAME']                               = 'Social Media Marketing';
$breadcrumb['social-media-marketing.php']['POSITION']                           = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['social-media-marketing.php']['TITLE']                              = 'Social Media Marketing';
$breadcrumb['social-media-marketing.php']['KEYWORDS']                           = 'Social Media Marketing';
$breadcrumb['social-media-marketing.php']['DESCRIPTION']                        = 'Social Media Marketing';
$breadcrumb['social-media-marketing.php']['URL']                                = $rootHTTP.'/services/promote/social-media-marketing.php';
// --- Social Media Marketing Page /
        
// --- Email Marketing & Newsletters Page 
$breadcrumb['email-marketing-and-newsletters.php']['NAME']                      = 'Email Marketing & Newsletters';
$breadcrumb['email-marketing-and-newsletters.php']['POSITION']                  = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['email-marketing-and-newsletters.php']['TITLE']                     = 'Email Marketing & Newsletters';
$breadcrumb['email-marketing-and-newsletters.php']['KEYWORDS']                  = 'Email Marketing & Newsletters';
$breadcrumb['email-marketing-and-newsletters.php']['DESCRIPTION']               = 'Email Marketing & Newsletters';
$breadcrumb['email-marketing-and-newsletters.php']['URL']                       = $rootHTTP.'/services/promote/email-marketing-and-newsletters.php';
// --- Email Marketing & Newsletters Page /
        
// --- Pay Per Click Campaign Advertising Page 
$breadcrumb['pay-per-click-campain-advertising.php']['NAME']                    = 'Pay Per Click Campaign Advertising';
$breadcrumb['pay-per-click-campain-advertising.php']['POSITION']                = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['pay-per-click-campain-advertising.php']['TITLE']                   = 'Pay Per Click Campaign Advertising';
$breadcrumb['pay-per-click-campain-advertising.php']['KEYWORDS']                = 'Pay Per Click Campaign Advertising';
$breadcrumb['pay-per-click-campain-advertising.php']['DESCRIPTION']             = 'Pay Per Click Campaign Advertising';
$breadcrumb['pay-per-click-campain-advertising.php']['URL']                     = $rootHTTP.'/services/promote/pay-per-click-campain-advertising.php';
// --- Pay Per Click Campaign Advertising Page /
        
// -------------------------------------------------------------------- PRODUCTS 
// --- Products Page 
$breadcrumb['products.php']['NAME']                                             = 'Products';
$breadcrumb['products.php']['POSITION']                                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['products.php']['TITLE']                                            = 'Products';
$breadcrumb['products.php']['KEYWORDS']                                         = 'Products';
$breadcrumb['products.php']['DESCRIPTION']                                      = 'Products';
$breadcrumb['products.php']['URL']                                              = $rootHTTP.'/products/products.php';
// --- Services Page /
// ----------------------------------------------------------------- DOMAINS ---
$breadcrumb['domains.php']['NAME']                                               = 'Domains';

// --- Registration Page 
$breadcrumb['registration.php']['NAME']                                         = 'Registration';
$breadcrumb['registration.php']['POSITION']                                     = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['registration.php']['TITLE']                                        = 'Registration';
$breadcrumb['registration.php']['KEYWORDS']                                     = 'Registration';
$breadcrumb['registration.php']['DESCRIPTION']                                  = 'Registration';
$breadcrumb['registration.php']['URL']                                          = $rootHTTP.'/products/domain/registration.php';
// --- Registration Page /
        
// --- Register a Domain Page 
$breadcrumb['register-a-domain.php']['NAME']                                    = 'Register a Domain';
$breadcrumb['register-a-domain.php']['POSITION']                                = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['register-a-domain.php']['TITLE']                                   = 'Register a Domain';
$breadcrumb['register-a-domain.php']['KEYWORDS']                                = 'Register a Domain';
$breadcrumb['register-a-domain.php']['DESCRIPTION']                             = 'Register a Domain';
$breadcrumb['register-a-domain.php']['URL']                                     = $rootHTTP.'/products/domain/register-a-domain.php';
// --- Register a Domain Page /

// --- Bulk Domain Registration Page 
$breadcrumb['bulk-domain-registration.php']['NAME']                             = 'Bulk Domain Registration';
$breadcrumb['bulk-domain-registration.php']['POSITION']                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['bulk-domain-registration.php']['TITLE']                            = 'Bulk Domain Registration';
$breadcrumb['bulk-domain-registration.php']['KEYWORDS']                         = 'Bulk Domain Registration';
$breadcrumb['bulk-domain-registration.php']['DESCRIPTION']                      = 'Bulk Domain Registration';
$breadcrumb['bulk-domain-registration.php']['URL']                              = $rootHTTP.'/products/domain/bulk-domain-registration.php';
// --- Bulk Domain Registration Page /
                
// --- Domain Extensions Page 
$breadcrumb['new-domain-extensions.php']['NAME']                                = 'Domain Extensions';
$breadcrumb['new-domain-extensions.php']['POSITION']                            = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['new-domain-extensions.php']['TITLE']                               = 'Domain Extensions';
$breadcrumb['new-domain-extensions.php']['KEYWORDS']                            = 'Domain Extensions';
$breadcrumb['new-domain-extensions.php']['DESCRIPTION']                         = 'Domain Extensions';
$breadcrumb['new-domain-extensions.php']['URL']                                 = $rootHTTP.'/products/domain/new-domain-extensions.php';
// --- Domain Extensions Page /
        
// --- Sunrise Domains Page 
$breadcrumb['sunrise-domains.php']['NAME']                                      = 'Sunrise Domains';
$breadcrumb['sunrise-domains.php']['POSITION']                                  = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['sunrise-domains.php']['TITLE']                                     = 'Sunrise Domains';
$breadcrumb['sunrise-domains.php']['KEYWORDS']                                  = 'Sunrise Domains';
$breadcrumb['sunrise-domains.php']['DESCRIPTION']                               = 'Sunrise Domains';
$breadcrumb['sunrise-domains.php']['URL']                                       = $rootHTTP.'/products/domain/sunrise-domains.php';
// --- Sunrise Domains Page /
        
// --- IDN Domain Registration Page 
$breadcrumb['idn-domain-registration.php']['NAME']                              = 'IDN Domain Registration';
$breadcrumb['idn-domain-registration.php']['POSITION']                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['idn-domain-registration.php']['TITLE']                             = 'IDN Domain Registration';
$breadcrumb['idn-domain-registration.php']['KEYWORDS']                          = 'IDN Domain Registration';
$breadcrumb['idn-domain-registration.php']['DESCRIPTION']                       = 'IDN Domain Registration';
$breadcrumb['idn-domain-registration.php']['URL']                               = $rootHTTP.'/products/domain/idn-domain-registration.php';
// --- IDN Domain Registration Page /
        
// --- Transfer Page 
$breadcrumb['transfer.php']['NAME']                                             = 'Transfer';
$breadcrumb['transfer.php']['POSITION']                                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['transfer.php']['TITLE']                                            = 'Transfer';
$breadcrumb['transfer.php']['KEYWORDS']                                         = 'Transfer';
$breadcrumb['transfer.php']['DESCRIPTION']                                      = 'Transfer';
$breadcrumb['transfer.php']['URL']                                              = $rootHTTP.'/products/domain/transfer.php';
// --- Transfer Page /
        
// --- Transfer your Domain Page 
$breadcrumb['transfer-your-domain.php']['NAME']                                 = 'Transfer your Domain';
$breadcrumb['transfer-your-domain.php']['POSITION']                             = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['transfer-your-domain.php']['TITLE']                                = 'Transfer your Domain';
$breadcrumb['transfer-your-domain.php']['KEYWORDS']                             = 'Transfer your Domain';
$breadcrumb['transfer-your-domain.php']['DESCRIPTION']                          = 'Transfer your Domain';
$breadcrumb['transfer-your-domain.php']['URL']                                  = $rootHTTP.'/products/domain/transfer-your-domain.php';
// --- Transfer your Domain Page /
        
// --- Bulk Domain Transfer Page 
$breadcrumb['bulk-domain-transfer.php']['NAME']                                 = 'Bulk Domain Transfer';
$breadcrumb['bulk-domain-transfer.php']['POSITION']                             = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['bulk-domain-transfer.php']['TITLE']                                = 'Bulk Domain Transfer';
$breadcrumb['bulk-domain-transfer.php']['KEYWORDS']                             = 'Bulk Domain Transfer';
$breadcrumb['bulk-domain-transfer.php']['DESCRIPTION']                          = 'Bulk Domain Transfer';
$breadcrumb['bulk-domain-transfer.php']['URL']                                  = $rootHTTP.'/products/domain/bulk-domain-transfer.php';
// --- Bulk Domain Transfer Page /
        
// --- Add-ons Page 
$breadcrumb['add-ons.php']['NAME']                                              = 'Add-ons';
$breadcrumb['add-ons.php']['POSITION']                                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['add-ons.php']['TITLE']                                             = 'Add-ons';
$breadcrumb['add-ons.php']['KEYWORDS']                                          = 'Add-ons';
$breadcrumb['add-ons.php']['DESCRIPTION']                                       = 'Add-ons';
$breadcrumb['add-ons.php']['URL']                                               = $rootHTTP.'/products/domain/add-ons.php';
// --- Add-ons Page /
        
// --- Free with every domain Page 
$breadcrumb['free-with-every-domain.php']['NAME']                               = 'Free with every domain';
$breadcrumb['free-with-every-domain.php']['POSITION']                           = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['free-with-every-domain.php']['TITLE']                              = 'Free with every domain';
$breadcrumb['free-with-every-domain.php']['KEYWORDS']                           = 'Free with every domain';
$breadcrumb['free-with-every-domain.php']['DESCRIPTION']                        = 'Free with every domain';
$breadcrumb['free-with-every-domain.php']['URL']                                = $rootHTTP.'/products/domain/free-with-every-domain.php';
// --- Free with every domain Page /
        
// --- Name suggestion tool Page 
$breadcrumb['name-suggestion-tool.php']['NAME']                                 = 'Name suggestion tool';
$breadcrumb['name-suggestion-tool.php']['POSITION']                             = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['name-suggestion-tool.php']['TITLE']                                = 'Name suggestion tool';
$breadcrumb['name-suggestion-tool.php']['KEYWORDS']                             = 'Name suggestion tool';
$breadcrumb['name-suggestion-tool.php']['DESCRIPTION']                          = 'Name suggestion tool';
$breadcrumb['name-suggestion-tool.php']['URL']                                  = $rootHTTP.'/products/domain/name-suggestion-tool.php';
// --- Name suggestion tool Page /
        
// --- Whois lookup Page 
$breadcrumb['whois-lookup.php']['NAME']                                         = 'Whois lookup';
$breadcrumb['whois-lookup.php']['POSITION']                                     = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['whois-lookup.php']['TITLE']                                        = 'Whois lookup';
$breadcrumb['whois-lookup.php']['KEYWORDS']                                     = 'Whois lookup';
$breadcrumb['whois-lookup.php']['DESCRIPTION']                                  = 'Whois lookup';
$breadcrumb['whois-lookup.php']['URL']                                          = $rootHTTP.'/products/domain/whois-lookup.php';
// --- Whois lookup Page /

// ----------------------------------------------------------------- HOSTING ---
$breadcrumb['hosting.php']['NAME']                                              = 'Hosting';   

// --- Shared Hosting Page 
$breadcrumb['shared-hosting.php']['NAME']                                       = 'Shared Hosting';
$breadcrumb['shared-hosting.php']['POSITION']                                   = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['shared-hosting.php']['TITLE']                                      = 'Shared Hosting';
$breadcrumb['shared-hosting.php']['KEYWORDS']                                   = 'Shared Hosting';
$breadcrumb['shared-hosting.php']['DESCRIPTION']                                = 'Shared Hosting ';
$breadcrumb['shared-hosting.php']['URL']                                        = $rootHTTP.'/products/hositing/shared-hosting/shared-hosting.php';
// --- Shared Hosting Page /

// --- Linux Shared Hosting Page 
$breadcrumb['linux-shared-hosting.php']['NAME']                                 = 'Linux Shared Hosting';
$breadcrumb['linux-shared-hosting.php']['POSITION']                             = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['linux-shared-hosting.php']['TITLE']                                = 'Linux Shared Hosting';
$breadcrumb['linux-shared-hosting.php']['KEYWORDS']                             = 'Linux Shared Hosting';
$breadcrumb['linux-shared-hosting.php']['DESCRIPTION']                          = 'Linux Shared Hosting';
$breadcrumb['linux-shared-hosting.php']['URL']                                  = $rootHTTP.'/products/hositing/shared-hosting/linux-shared-hosting.php';
// --- Linux Shared Hosting Page /

// --- Windows Shared Hosting Page 
$breadcrumb['windows-shared-hosting.php']['NAME']                               = 'Windows Shared Hosting';
$breadcrumb['windows-shared-hosting.php']['POSITION']                           = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['windows-shared-hosting.php']['TITLE']                              = 'Windows Shared Hosting';
$breadcrumb['windows-shared-hosting.php']['KEYWORDS']                           = 'Windows Shared Hosting';
$breadcrumb['windows-shared-hosting.php']['DESCRIPTION']                        = 'Windows Shared Hosting';
$breadcrumb['windows-shared-hosting.php']['URL']                                = $rootHTTP.'/products/hositing/shared-hosting/windows-shared-hosting.php';
// --- Windows Shared Hosting Page /
   
// --- Servers Page 
$breadcrumb['server.php']['NAME']                                              = 'Servers';
$breadcrumb['server.php']['POSITION']                                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['server.php']['TITLE']                                             = 'Servers';
$breadcrumb['server.php']['KEYWORDS']                                          = 'Servers';
$breadcrumb['server.php']['DESCRIPTION']                                       = 'Servers';
$breadcrumb['server.php']['URL']                                               = $rootHTTP.'/products/hositing/server/server.php';
// --- Servers Page /

// --- VPS Page 
$breadcrumb['vps-hosting.php']['NAME']                                          = 'VPS';
$breadcrumb['vps-hosting.php']['POSITION']                                      = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['vps-hosting.php']['TITLE']                                         = 'VPS';
$breadcrumb['vps-hosting.php']['KEYWORDS']                                      = 'VPS';
$breadcrumb['vps-hosting.php']['DESCRIPTION']                                   = 'VPS';
$breadcrumb['vps-hosting.php']['URL']                                           = $rootHTTP.'/products/hositing/server/vps-hosting.php';
// --- VPS Page /

// --- Dedicated Servers Page 
$breadcrumb['dedicated-servers.php']['NAME']                                    = 'Dedicated Servers';
$breadcrumb['dedicated-servers.php']['POSITION']                                = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['dedicated-servers.php']['TITLE']                                   = 'Dedicated Servers';
$breadcrumb['dedicated-servers.php']['KEYWORDS']                                = 'Dedicated Servers';
$breadcrumb['dedicated-servers.php']['DESCRIPTION']                             = 'Dedicated Servers';
$breadcrumb['dedicated-servers.php']['URL']                                     = $rootHTTP.'/products/hositing/server/dedicated-servers.php';
// --- Dedicated Servers Page /

// --- Managed Servers Page 
$breadcrumb['managed-servers.php']['NAME']                                      = 'Managed Servers';
$breadcrumb['managed-servers.php']['POSITION']                                  = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['managed-servers.php']['TITLE']                                     = 'Managed Servers';
$breadcrumb['managed-servers.php']['KEYWORDS']                                  = 'Managed Servers';
$breadcrumb['managed-servers.php']['DESCRIPTION']                               = 'Managed Servers';
$breadcrumb['managed-servers.php']['URL']                                       = $rootHTTP.'/products/hositing/server/managed-servers.php';
// --- Managed Servers Page /

// --- Reseller Hosting Page 
$breadcrumb['reseller-hosting.php']['NAME']                                     = 'Reseller Hosting';
$breadcrumb['reseller-hosting.php']['POSITION']                                 = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['reseller-hosting.php']['TITLE']                                    = 'Reseller Hosting';
$breadcrumb['reseller-hosting.php']['KEYWORDS']                                 = 'Reseller Hosting';
$breadcrumb['reseller-hosting.php']['DESCRIPTION']                              = 'Reseller Hosting';
$breadcrumb['reseller-hosting.php']['URL']                                      = $rootHTTP.'/products/hositing/reseller-hosting/reseller-hosting.php';
// --- Reseller Hosting Page /

// --- Linux Reseller Hosting Page 
$breadcrumb['linux-reseller-hosting.php']['NAME']                               = 'Linux Reseller Hosting';
$breadcrumb['linux-reseller-hosting.php']['POSITION']                           = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['linux-reseller-hosting.php']['TITLE']                              = 'Linux Reseller Hosting';
$breadcrumb['linux-reseller-hosting.php']['KEYWORDS']                           = 'Linux Reseller Hosting';
$breadcrumb['linux-reseller-hosting.php']['DESCRIPTION']                        = 'Linux Reseller Hosting';
$breadcrumb['linux-reseller-hosting.php']['URL']                                = $rootHTTP.'/products/hositing/reseller-hosting/linux-reseller-hosting.php';
// --- Linux Reseller Hosting Page /
      
// --- Windows Reseller Hosting Page 
$breadcrumb['windows-reseller-hosting.php']['NAME']                             = 'Windows Reseller Hosting';
$breadcrumb['windows-reseller-hosting.php']['POSITION']                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['windows-reseller-hosting.php']['TITLE']                            = 'Windows Reseller Hosting';
$breadcrumb['windows-reseller-hosting.php']['KEYWORDS']                         = 'Windows Reseller Hosting';
$breadcrumb['windows-reseller-hosting.php']['DESCRIPTION']                      = 'Windows Reseller Hosting';
$breadcrumb['windows-reseller-hosting.php']['URL']                              = $rootHTTP.'/products/hositing/reseller-hosting/windows-reseller-hosting.php';
// --- Windows Reseller Hosting Page /
// ------------------------------------------------------------------- EMAIL ---
$breadcrumb['email.php']['NAME']                                                = 'Email';  

// --- Business Email Page 
$breadcrumb['business-email.php']['NAME']                                       = 'Business Email';
$breadcrumb['business-email.php']['POSITION']                                   = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['business-email.php']['TITLE']                                      = 'Business Email';
$breadcrumb['business-email.php']['KEYWORDS']                                   = 'Business Email';
$breadcrumb['business-email.php']['DESCRIPTION']                                = 'Business Email';
$breadcrumb['business-email.php']['URL']                                        = $rootHTTP.'/products/email/business-email.php';
// --- Business Email Page /

// --- Enterprise Email Page 
$breadcrumb['enterprise-email.php']['NAME']                                     = 'Enterprise Email';
$breadcrumb['enterprise-email.php']['POSITION']                                 = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['enterprise-email.php']['TITLE']                                    = 'Enterprise Email';
$breadcrumb['enterprise-email.php']['KEYWORDS']                                 = 'Enterprise Email';
$breadcrumb['enterprise-email.php']['DESCRIPTION']                              = 'Enterprise Email';
$breadcrumb['enterprise-email.php']['URL']                                      = $rootHTTP.'/products/email/enterprise-email.php';
// --- Enterprise Email Page /

// ---------------------------------------------------------------- SECURITY ---
$breadcrumb['security.php']['NAME']                                             = 'Security';  

// --- Security SSL Page 
$breadcrumb['security-ssl.php']['NAME']                                         = 'Security SSL';
$breadcrumb['security-ssl.php']['POSITION']                                     = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['security-ssl.php']['TITLE']                                        = 'Security SSL';
$breadcrumb['security-ssl.php']['KEYWORDS']                                     = 'Security SSL';
$breadcrumb['security-ssl.php']['DESCRIPTION']                                  = 'Security SSL';
$breadcrumb['security-ssl.php']['URL']                                          = $rootHTTP.'/products/security/security-ssl.php';
// --- Security SSL Page /

// --- SiteLock Malware Detector Page 
$breadcrumb['sitelock-malware-detector.php']['NAME']                            = 'SiteLock Malware Detector';
$breadcrumb['sitelock-malware-detector.php']['POSITION']                        = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['sitelock-malware-detector.php']['TITLE']                           = 'SiteLock Malware Detector';
$breadcrumb['sitelock-malware-detector.php']['KEYWORDS']                        = 'SiteLock Malware Detector';
$breadcrumb['sitelock-malware-detector.php']['DESCRIPTION']                     = 'SiteLock Malware Detector';
$breadcrumb['sitelock-malware-detector.php']['URL']                             = $rootHTTP.'/products/security/sitelock-malware-detector.php';
// --- SiteLock Malware Detector Page /

// --- Codeguard Website Backup Page 
$breadcrumb['codeguard-website-backup.php']['NAME']                             = 'Codeguard Website Backup';
$breadcrumb['codeguard-website-backup.php']['POSITION']                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['codeguard-website-backup.php']['TITLE']                            = 'Codeguard Website Backup';
$breadcrumb['codeguard-website-backup.php']['KEYWORDS']                         = 'Codeguard Website Backup';
$breadcrumb['codeguard-website-backup.php']['DESCRIPTION']                      = 'Codeguard Website Backup';
$breadcrumb['codeguard-website-backup.php']['URL']                              = $rootHTTP.'/products/security/codeguard-website-backup.php';
// --- Codeguard Website Backup Page /




// ----------------------------------------------------------------- THE COMPANY
$breadcrumb['company.php']['NAME']                                              = 'The Company';

// --- About MavajSunCo Page 
$breadcrumb['about-MavajSunCo.php']['NAME']                                     = 'About';
$breadcrumb['about-MavajSunCo.php']['POSITION']                                 = array('index.php' => $breadcrumb['index.php']['NAME'] , 'the-company.php' => 'The Company','about-us.php' => $breadcrumb['about-MavajSunCo.php']['NAME'] );
$breadcrumb['about-MavajSunCo.php']['TITLE']                                    = 'About Us';
$breadcrumb['about-MavajSunCo.php']['KEYWORDS']                                 = 'About Us';
$breadcrumb['about-MavajSunCo.php']['DESCRIPTION']                              = 'About Us';
$breadcrumb['about-MavajSunCo.php']['URL']                                      = $rootHTTP.'/company/about-MavajSunCo.php';
// --- About MavajSunCo Page /
        
// --- Our Mission Page 
$breadcrumb['our-mission.php']['NAME']                                          = 'Our Mission';
$breadcrumb['our-mission.php']['POSITION']                                      = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['our-mission.php']['TITLE']                                         = 'Our Mission';
$breadcrumb['our-mission.php']['KEYWORDS']                                      = 'Our Mission';
$breadcrumb['our-mission.php']['DESCRIPTION']                                   = 'Our Mission';
$breadcrumb['our-mission.php']['URL']                                           = $rootHTTP.'/company/our-mission.php';
// --- Our Mission Page /

// --- Why Us Page 
$breadcrumb['why-us.php']['NAME']                                                = 'Why Us';
$breadcrumb['why-us.php']['POSITION']                                            = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['why-us.php']['TITLE']                                               = 'Why Us';
$breadcrumb['why-us.php']['KEYWORDS']                                            = 'Why Us';
$breadcrumb['why-us.php']['DESCRIPTION']                                         = 'Why Us';
$breadcrumb['why-us.php']['URL']                                                 = $rootHTTP.'/company/why-us.php';
// --- Why Us Page /
        
// --- How we work Page 
$breadcrumb['how-we-work.php']['NAME']                                          = 'How we work';
$breadcrumb['how-we-work.php']['POSITION']                                      = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['how-we-work.php']['TITLE']                                         = 'How we work';
$breadcrumb['how-we-work.php']['KEYWORDS']                                      = 'How we work';
$breadcrumb['how-we-work.php']['DESCRIPTION']                                   = 'How we work';
$breadcrumb['how-we-work.php']['URL']                                           = $rootHTTP.'/company/how-we-work.php';
// --- How we work Page /
        
// --- Our team Page 
$breadcrumb['our-team.php']['NAME']                                             = 'Our team';
$breadcrumb['our-team.php']['POSITION']                                         = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['our-team.php']['TITLE']                                            = 'Our team';
$breadcrumb['our-team.php']['KEYWORDS']                                         = 'Our team';
$breadcrumb['our-team.php']['DESCRIPTION']                                      = 'Our team';
$breadcrumb['our-team.php']['URL']                                              = $rootHTTP.'/company/our-team.php';
// --- Our team Page /
        
// --------------------------------------------------------------------- CLIENTS
// --- Clients Page 
$breadcrumb['clients.php']['NAME']                                              = 'Clients';
$breadcrumb['clients.php']['POSITION']                                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['clients.php']['TITLE']                                             = 'Clients';
$breadcrumb['clients.php']['KEYWORDS']                                          = 'Clients';
$breadcrumb['clients.php']['DESCRIPTION']                                       = 'Clients';
$breadcrumb['clients.php']['URL']                                               = $rootHTTP.'/clients/clients.php';
// --- Clients Page /

// --------------------------------------------------------------------- CAREERS        
// --- Careers Page 
$breadcrumb['careers.php']['NAME']                                              = 'Careers';
$breadcrumb['careers.php']['POSITION']                                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['careers.php']['TITLE']                                             = 'Careers';
$breadcrumb['careers.php']['KEYWORDS']                                          = 'Careers';
$breadcrumb['careers.php']['DESCRIPTION']                                       = 'Careers';
$breadcrumb['careers.php']['URL']                                               = $rootHTTP.'/contact/careers.php';
// --- Careers Page /
 
// --------------------------------------------------------------------- CONTACT   
// --- Contact Page 
$breadcrumb['contact.php']['NAME']                                              = 'Contact';
$breadcrumb['contact.php']['POSITION']                                          = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['contact.php']['TITLE']                                             = 'Contact';
$breadcrumb['contact.php']['KEYWORDS']                                          = 'Contact';
$breadcrumb['contact.php']['DESCRIPTION']                                       = 'Contact';
$breadcrumb['contact.php']['URL']                                               = $rootHTTP.'/contact/contact.php';
// --- Contact Page /




// --------------------------------------------------------------------- 
// --- Home Page 
$breadcrumb['1111111111.php']['NAME']                                           = 'Home';
$breadcrumb['1111111111.php']['POSITION']                                       = array('index.php' => $breadcrumb['index.php']['NAME'] );
$breadcrumb['1111111111.php']['TITLE']                                          = '';
$breadcrumb['1111111111.php']['KEYWORDS']                                       = '';
$breadcrumb['1111111111.php']['DESCRIPTION']                                    = '';
$breadcrumb['1111111111.php']['URL']                                            = '1111111111.php';
// --- Home Page /

//--------- Start Breadcrumb /
?>
