<div class="yamm navbar  navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?= $breadcrumb['index.php']['URL'] ?>" class="navbar-brand"><?= $breadcrumb['index.php']['NAME'] ?></a>
        </div>
        <div id="navbar-collapse-1" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

                <!-- Start Services Menu -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<?= $breadcrumb['services.php']['URL'] ?>"><?= $breadcrumb['services.php']['NAME'] ?><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div id="accordion" class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> 
                                                        <p>
                                                            <img src="http://www.MavajSunCo.com/resources/img/global/menu-icon-strategy.png" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['strategy.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li><a href="<?= $breadcrumb['website-strategy-and-planning.php']['URL'] ?>"><?= $breadcrumb['website-strategy-and-planning.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['user-interface-designs.php']['URL'] ?>"><?= $breadcrumb['user-interface-designs.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['information-architecture.php']['URL'] ?>"><?= $breadcrumb['information-architecture.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['online-marketing-strategy.php']['URL'] ?>"><?= $breadcrumb['online-marketing-strategy.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['social-media-planning.php']['URL'] ?>"><?= $breadcrumb['social-media-planning.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> 
                                                        <p>
                                                            <img src="http://www.MavajSunCo.com/resources/img/global/menu-icon-create.png" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['create.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li><a href="<?= $breadcrumb['web-design.php']['URL'] ?>"><?= $breadcrumb['web-design.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['web-development.php']['URL'] ?>"><?= $breadcrumb['web-development.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['content-management-systems.php']['URL'] ?>"><?= $breadcrumb['content-management-systems.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['e-commerce.php']['URL'] ?>"><?= $breadcrumb['e-commerce.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['mobile-website-design.php']['URL'] ?>"><?= $breadcrumb['mobile-website-design.php']['NAME'] ?> </a></li>
                                                        <li><a href="<?= $breadcrumb['mobile-apps-development.php']['URL'] ?>"><?= $breadcrumb['mobile-apps-development.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['blog-development.php']['URL'] ?>"><?= $breadcrumb['blog-development.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> 
                                                        <p>
                                                            <img src="http://www.MavajSunCo.com/resources/img/global/menu-icon-promote.png" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['promote.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li><a href="<?= $breadcrumb['search-engine-optimisation.php']['URL'] ?>"><?= $breadcrumb['search-engine-optimisation.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['social-media-marketing.php']['URL'] ?>"><?= $breadcrumb['social-media-marketing.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['email-marketing-and-newsletters.php']['URL'] ?>"><?= $breadcrumb['email-marketing-and-newsletters.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['pay-per-click-campain-advertising.php']['URL'] ?>"><?= $breadcrumb['pay-per-click-campain-advertising.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Start Services Menu -->

                <!-- Start Products Menu -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<?= $breadcrumb['products.php']['URL'] ?>"><?= $breadcrumb['products.php']['NAME'] ?><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div id="accordion2" class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#ProductsOne"> 
                                                        <p>
                                                            <img src="http://placehold.it/25x25" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['domains.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="ProductsOne" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li class="list-group-item-info"><a href="<?= $breadcrumb['registration.php']['URL'] ?>"><?= $breadcrumb['registration.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['register-a-domain.php']['NAME'] ?>"><?= $breadcrumb['register-a-domain.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['bulk-domain-registration.php']['URL'] ?>"><?= $breadcrumb['bulk-domain-registration.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['new-domain-extensions.php']['URL'] ?>"><?= $breadcrumb['new-domain-extensions.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['sunrise-domains.php']['URL'] ?>"><?= $breadcrumb['sunrise-domains.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['idn-domain-registration.php']['URL'] ?>"><?= $breadcrumb['idn-domain-registration.php']['NAME'] ?></a></li>
                                                        <li class="list-group-item-info"><a href="<?= $breadcrumb['transfer.php']['URL'] ?>"><?= $breadcrumb['transfer.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['transfer-your-domain.php']['URL'] ?>"><?= $breadcrumb['transfer-your-domain.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['bulk-domain-transfer.php']['URL'] ?>"><?= $breadcrumb['bulk-domain-transfer.php']['NAME'] ?></a></li>
                                                        <li class="list-group-item-info"><a href="<?= $breadcrumb['add-ons.php']['URL'] ?>"><?= $breadcrumb['add-ons.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['free-with-every-domain.php']['URL'] ?>"><?= $breadcrumb['free-with-every-domain.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['name-suggestion-tool.php']['URL'] ?>"><?= $breadcrumb['name-suggestion-tool.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['whois-lookup.php']['URL'] ?>"><?= $breadcrumb['whois-lookup.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#ProductsTwo"> 
                                                        <p>
                                                            <img src="http://placehold.it/25x25" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['hosting.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="ProductsTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li class="list-group-item-info"><a href="<?= $breadcrumb['shared-hosting.php']['URL'] ?>"><?= $breadcrumb['shared-hosting.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['linux-shared-hosting.php']['URL'] ?>"><?= $breadcrumb['linux-shared-hosting.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['windows-shared-hosting.php']['URL'] ?>"><?= $breadcrumb['windows-shared-hosting.php']['NAME'] ?></a></li>
                                                        <li class="list-group-item-info"><a href="<?= $breadcrumb['servers.php']['URL'] ?>"><?= $breadcrumb['servers.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['vps-hosting.php']['URL'] ?>"><?= $breadcrumb['vps-hosting.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['dedicated-servers.php']['URL'] ?>"><?= $breadcrumb['dedicated-servers.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['managed-servers.php']['URL'] ?>"><?= $breadcrumb['managed-servers.php']['NAME'] ?></a></li>
                                                        <li class="list-group-item-info"><a href="<?= $breadcrumb['reseller-hosting.php']['URL'] ?>"><?= $breadcrumb['reseller-hosting.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['linux-reseller-hosting.php']['URL'] ?>"><?= $breadcrumb['linux-reseller-hosting.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['windows-reseller-hosting.php']['URL'] ?>"><?= $breadcrumb['windows-reseller-hosting.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#ProductsThree"> 
                                                        <p>
                                                            <img src="http://placehold.it/25x25" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['email.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="ProductsThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li><a href="<?= $breadcrumb['business-email.php']['URL'] ?>"><?= $breadcrumb['business-email.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['enterprise-email.php']['URL'] ?>"><?= $breadcrumb['enterprise-email.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion2" href="#ProductsFour"> 
                                                        <p>
                                                            <img src="http://placehold.it/25x25" class="img-rounded"alt="" />
                                                            <strong><?= $breadcrumb['security.php']['NAME'] ?></strong>
                                                        </p>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="ProductsFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="col-sm-12 list-unstyled">
                                                        <li><a href="<?= $breadcrumb['security-ssl.php']['URL'] ?>"><?= $breadcrumb['security-ssl.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['sitelock-malware-detector.php']['URL'] ?>"><?= $breadcrumb['sitelock-malware-detector.php']['NAME'] ?></a></li>
                                                        <li><a href="<?= $breadcrumb['codeguard-website-backup.php']['URL'] ?>"><?= $breadcrumb['codeguard-website-backup.php']['NAME'] ?></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Start Products Menu -->
                <!-- Start Company Menu -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="company.php"><?= $breadcrumb['company.php']['NAME'] ?><b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a tabindex="-1" href="<?= $breadcrumb['about-MavajSunCo.php']['URL'] ?>"><?= $breadcrumb['about-MavajSunCo.php']['NAME'] ?></a></li>
                        <li><a tabindex="-1" href="<?= $breadcrumb['our-mission.php']['URL'] ?>"><?= $breadcrumb['our-mission.php']['NAME'] ?></a></li>
                        <li><a tabindex="-1" href="<?= $breadcrumb['why-us.php']['URL'] ?>"><?= $breadcrumb['why-us.php']['NAME'] ?></a></li>
                        <li><a tabindex="-1" href="<?= $breadcrumb['how-we-work.php']['URL'] ?>"><?= $breadcrumb['how-we-work.php']['NAME'] ?></a></li>
                        <li><a tabindex="-1" href="<?= $breadcrumb['our-team.php']['URL'] ?>"><?= $breadcrumb['our-team.php']['NAME'] ?></a></li>
                    </ul>
                </li>
                <!-- End Company Menu -->
                <!--                 Start Resources Menu 
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown"  href="resources.php">Resources<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <div class="yamm-content">
                                                <div class="row">
                                                    <ul class="col-sm-4 list-unstyled">
                                                        <li><a href="">Download</a></li>
                                                        <li><a href="articles.php">Articles </a></li>
                                                        <li><a href="">Portal Development</a></li>
                                                        <li><a href="">Application Development</a></li>
                                                        <li><a href="">Blog</a></li>
                                                        <li><a href="">Events</a></li>
                                                        <li><a href="">News</a></li>
                                                        <li><a href="">My Account</a></li>
                                                        <li><a href="">Member</a></li>
                                                    </ul>
                                                    <ul class="col-sm-4 list-unstyled">
                                                        <li><a href="faq.php">FAQs</a></li>
                                                        <li><a href="forum.php">Forum</a></li>
                                                        <li><a href="">Social Media</a></li>
                                                        <li><a href="">Promotion</a></li>
                                                        <li><a href="">Gallery</a></li>
                                                        <li><a href="">E-Shop</a></li>
                                                        <li><a href="">Sitemap</a></li>
                                                        <li><a href="">Achknowlegde</a></li>
                                                        <li><a href="portfolio.php">Portfolio</a></li>
                                                        <li><a href="">Affiliation</a></li>
                                                    </ul>
                                                    <ul class="col-sm-4 list-unstyled">
                                                        <li><a href="">Newsletter</a></li>
                                                        <li><a href="">Support</a></li>
                                                        <li><a href="">Learning</a></li>
                                                        <li><a href="">Quotation</a></li>
                                                        <li><a href="">Webmail</a></li>
                                                        <li><a href="">Domain Control Panel</a></li>
                                                        <li><a href="">Webhosting Control Panel</a></li>
                                                        <li><a href="">Technology</a></li>
                                                        <li><a href="">Industry</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>-->
                <!-- Start Articles Menu -->
                <li><a href="<?= $breadcrumb['clients.php']['URL'] ?>"><?= $breadcrumb['clients.php']['NAME'] ?></a></li>
                <li><a href="<?= $breadcrumb['careers.php']['URL'] ?>"><?= $breadcrumb['careers.php']['NAME'] ?></a></li>
                <li><a href="<?= $breadcrumb['contact.php']['URL'] ?>"><?= $breadcrumb['contact.php']['NAME'] ?></a></li>
            </ul>
        </div>
    </div>
</div>
