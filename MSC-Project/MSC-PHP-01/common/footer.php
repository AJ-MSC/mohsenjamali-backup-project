</div>
<footer><!-- FOOTER -->
    <div class="container">
        <?php include_once $rootDOC . '/common/footer-menu.php'; ?> 
        <hr/>   
        <div class="row"><!-- Copyright Section -->
            <div class="col-lg-12">
                <p class="pull-right"><a href="#" onclick="return false;" onmousedown="resetScroller('logo');"> <span class="glyphicon glyphicon-circle-arrow-up" aria-hidden="true"></span></a></p>
                <p id="copyright">Copyright (c) <?php echo DATE('Y') ?> MAVAJ SUN CO, All Rights Reserved.</p>
            </div>
        </div> <!-- Copyright Section End-->
    </div>
</footer><!-- FOOTER end -->
</div><!-- Container Section -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script async="true" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script async="true" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script async="true" src="http://www.MavajSunCo.com/resources/js/MavajSunCo.js"></script>
<script async="true" type="text/javascript" src="http://www.MavajSunCo.com/resources/js/google-analytics.js"></script>
</body>
</html>