<?php

function verify_name($string) {
    if (!preg_match("/^[a-zA-Z0-9 ]*$/", varify_input($string))) {
        return "Only English letters allowed";
    }
    return TRUE;
}

function verify_email($email) {
    if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", varify_input($email))) {
        return "Invalid email format";
    }
    return TRUE;
}

function varify_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function sendEmail($data) {
    $to = $data['to'];
    $subject = $data['subject'];
    $message = $data['message'];

    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers
    $headers .= 'From: <contact@MavajSunCo.com>' . "\r\n";
     /*  $headers .= 'Cc: myboss@example.com' . "\r\n"; */

    $sent = mail($to, $subject, $message, $headers);
    $_SESSION['section'] = $data['section'];
    if ($sent) {
        $_SESSION['success_message'] = "Email sent successfully";
    } else {
        $_SESSION['error_message'] = "Email not sent please try again";
    }
}
