<!-- LANGUAGE -->
<div class="btn-group pull-right">
    <button class="language" type="button" ata-toggle="dropdown">
        <?php
        if ($_SESSION['lang'] == 'de') {
            ?>
            <a href="http://<?php echo $rootHost; ?>index.php?lang=en">
                <img src="http://<?php echo $rootHost; ?>assets/images/flags/us.png" width="16" height="11" alt="EN Language" /> English <span class="caret"></span>
            </a>
            <?php
        } else {
            ?>
            <a href="http://<?php echo $rootHost; ?>index.php?lang=de">
                <img src="http://<?php echo $rootHost; ?>assets/images/flags/de.png" width="16" height="11" alt="DE Language" /> German <span class="caret"></span>
            </a>
            <?php
        }
        ?>
    </button>
</div>
<!-- /LANGUAGE -->