<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<!--- Start-->
<p>MAVAJ SUN CO was founded in 1978 and has grown to be premier website design , online marketing , web hosting and domain name provider company.</p>
<p>We are located in Cyprus , with capabilities to service clients across the world. Since 1999 , the professional team at MAVAJ SUN CO have designed and built over 2,000 websites for a diverse range of organisations across many industry groupings , provide hosting server , domain name registration and help them to running their online business seamlessly . </p>
<p>MAVAJ SUN CO will provide you with an innovative, attractive and professional web design that will assist you to open new markets for your business. MAVAJ SUN CO and its high calibre designers will craft the perfect look and feel for your website, our skilled programmers will implement the latest technologies in developing your website and our experienced marketing managers and consultants will drive qualified, targeted sales leads from the major search engines to your website</p>
<p>MAVAJ SUN CO strives to create professional web sites that will effectively promote our clients products or services to their target audience. We listen to our clients and develop a custom made web site that will enhance your overall corporate image. Unlike your standard newspaper or phone book ad, a web site can contain an unlimited amount of information at a fraction of the price, and is accessible by millions world wide. Any aspect of your site can be updated monthly, weekly, or even daily, to have current information for your readers. This information can be accessed with a click of a button from around the entire globe, 24 hours a day. Statistics show that there are over 300 million people on the Internet today. Those numbers speak for themselves.</p>
<p>At MAVAJ SUN CO, we are developing your ideas into reality.</p>
<!--- End-->

<?php
//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>