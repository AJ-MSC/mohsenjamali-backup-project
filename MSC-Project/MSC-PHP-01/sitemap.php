<?php
include_once './common/header.php';
//Start Section
?>
<br>
<!--<? echo $breadcrumb; ?>-->

<p> <h1>Coming Soon - Sitemap </h1></p>
<div>
    <ul>
        <li><a href="<?= $breadcrumb['index.php']['URL'] ?>" ><?= $breadcrumb['index.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['services.php']['URL'] ?>"><?= $breadcrumb['services.php']['NAME'] ?></a></li>
        <li><strong><?= $breadcrumb['strategy.php']['NAME'] ?></strong></li>
        <li><a href="<?= $breadcrumb['website-strategy-and-planning.php']['URL'] ?>"><?= $breadcrumb['website-strategy-and-planning.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['user-interface-designs.php']['URL'] ?>"><?= $breadcrumb['user-interface-designs.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['information-architecture.php']['URL'] ?>"><?= $breadcrumb['information-architecture.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['online-marketing-strategy.php']['URL'] ?>"><?= $breadcrumb['online-marketing-strategy.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['social-media-planning.php']['URL'] ?>"><?= $breadcrumb['social-media-planning.php']['NAME'] ?></a></li>

        <li><strong><?= $breadcrumb['create.php']['NAME'] ?></strong></li>
        <li><a href="<?= $breadcrumb['web-design.php']['URL'] ?>"><?= $breadcrumb['web-design.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['web-development.php']['URL'] ?>"><?= $breadcrumb['web-development.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['content-management-systems.php']['URL'] ?>"><?= $breadcrumb['content-management-systems.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['e-commerce.php']['URL'] ?>"><?= $breadcrumb['e-commerce.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['mobile-website-design.php']['URL'] ?>"><?= $breadcrumb['mobile-website-design.php']['NAME'] ?> </a></li>
        <li><a href="<?= $breadcrumb['mobile-apps-development.php']['URL'] ?>"><?= $breadcrumb['mobile-apps-development.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['blog-development.php']['URL'] ?>"><?= $breadcrumb['blog-development.php']['NAME'] ?></a></li>

        <li><strong><?= $breadcrumb['promote.php']['NAME'] ?></strong></li>
        <li><a href="<?= $breadcrumb['search-engine-optimisation.php']['URL'] ?>"><?= $breadcrumb['search-engine-optimisation.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['social-media-marketing.php']['URL'] ?>"><?= $breadcrumb['social-media-marketing.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['email-marketing-and-newsletters.php']['URL'] ?>"><?= $breadcrumb['email-marketing-and-newsletters.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['pay-per-click-campain-advertising.php']['URL'] ?>"><?= $breadcrumb['pay-per-click-campain-advertising.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['products.php']['URL'] ?>"><?= $breadcrumb['products.php']['NAME'] ?></a></li>
        <li><strong><?= $breadcrumb['domains.php']['NAME'] ?></strong>
        <li><a href="<?= $breadcrumb['registration.php']['URL'] ?>"><?= $breadcrumb['registration.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['register-a-domain.php']['NAME'] ?>"><?= $breadcrumb['register-a-domain.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['bulk-domain-registration.php']['URL'] ?>"><?= $breadcrumb['bulk-domain-registration.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['new-domain-extensions.php']['URL'] ?>"><?= $breadcrumb['new-domain-extensions.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['sunrise-domains.php']['URL'] ?>"><?= $breadcrumb['sunrise-domains.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['idn-domain-registration.php']['URL'] ?>"><?= $breadcrumb['idn-domain-registration.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['transfer.php']['URL'] ?>"><?= $breadcrumb['transfer.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['transfer-your-domain.php']['URL'] ?>"><?= $breadcrumb['transfer-your-domain.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['bulk-domain-transfer.php']['URL'] ?>"><?= $breadcrumb['bulk-domain-transfer.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['add-ons.php']['URL'] ?>"><?= $breadcrumb['add-ons.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['free-with-every-domain.php']['URL'] ?>"><?= $breadcrumb['free-with-every-domain.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['name-suggestion-tool.php']['URL'] ?>"><?= $breadcrumb['name-suggestion-tool.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['whois-lookup.php']['URL'] ?>"><?= $breadcrumb['whois-lookup.php']['NAME'] ?></a></li>
        <li><strong><?= $breadcrumb['hosting.php']['NAME'] ?></strong>
        <li><a href="<?= $breadcrumb['shared-hosting.php']['URL'] ?>"><?= $breadcrumb['shared-hosting.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['linux-shared-hosting.php']['URL'] ?>"><?= $breadcrumb['linux-shared-hosting.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['windows-shared-hosting.php']['URL'] ?>"><?= $breadcrumb['windows-shared-hosting.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['servers.php']['URL'] ?>"><?= $breadcrumb['servers.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['vps-hosting.php']['URL'] ?>"><?= $breadcrumb['vps-hosting.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['dedicated-servers.php']['URL'] ?>"><?= $breadcrumb['dedicated-servers.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['managed-servers.php']['URL'] ?>"><?= $breadcrumb['managed-servers.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['reseller-hosting.php']['URL'] ?>"><?= $breadcrumb['reseller-hosting.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['linux-reseller-hosting.php']['URL'] ?>"><?= $breadcrumb['linux-reseller-hosting.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['windows-reseller-hosting.php']['URL'] ?>"><?= $breadcrumb['windows-reseller-hosting.php']['NAME'] ?></a></li>
        <strong><?= $breadcrumb['email.php']['NAME'] ?></strong>
        <li><a href="<?= $breadcrumb['business-email.php']['URL'] ?>"><?= $breadcrumb['business-email.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['enterprise-email.php']['URL'] ?>"><?= $breadcrumb['enterprise-email.php']['NAME'] ?></a></li>
        <li><strong><?= $breadcrumb['security.php']['NAME'] ?></strong>
        <li><a href="<?= $breadcrumb['security-ssl.php']['URL'] ?>"><?= $breadcrumb['security-ssl.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['sitelock-malware-detector.php']['URL'] ?>"><?= $breadcrumb['sitelock-malware-detector.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['codeguard-website-backup.php']['URL'] ?>"><?= $breadcrumb['codeguard-website-backup.php']['NAME'] ?></a></li>

        <li><a href="company.php"><?= $breadcrumb['company.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['about-MavajSunCo.php']['URL'] ?>"><?= $breadcrumb['about-MavajSunCo.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['our-mission.php']['URL'] ?>"><?= $breadcrumb['our-mission.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['why-us.php']['URL'] ?>"><?= $breadcrumb['why-us.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['how-we-work.php']['URL'] ?>"><?= $breadcrumb['how-we-work.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['our-team.php']['URL'] ?>"><?= $breadcrumb['our-team.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['clients.php']['URL'] ?>"><?= $breadcrumb['clients.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['careers.php']['URL'] ?>"><?= $breadcrumb['careers.php']['NAME'] ?></a></li>
        <li><a href="<?= $breadcrumb['contact.php']['URL'] ?>"><?= $breadcrumb['contact.php']['NAME'] ?></a></li>
    </ul>
</div>

<?php
//End section
include_once './common/footer.php';
?> 