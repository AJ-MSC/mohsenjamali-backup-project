<?php
include_once  $_SERVER['DOCUMENT_ROOT']. '/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<p>Our websites are designed with your business and your target market in mind. Our professional and talented team of graphic artists and designers are highly trained and experienced in web design.</p>
<p>The design of your website is one of the most integral components of your web success. In most cases you have 10 seconds to impress site visitors with your design and professionalism, after which they will decide to read further through your site or simply leave.You will find an instant impact and a positive impression should be synonymous with your web site design.</p>
<p>At MAVAJ SUN CO, prior to starting your design we conduct an analysis of your target market and can provide advice on a design most likely to appeal to your customers. With a MAVAJ SUN CO designed web site we provide you with the highest possible chance of having your site visitors convert from casual visitors to customers. For a free report on creation of a new web site design or for a revamp of your existing design contact us today!</p>

<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT']. '/common/footer.php';
?>