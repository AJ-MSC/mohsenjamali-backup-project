<?php
include_once  $_SERVER['DOCUMENT_ROOT']. '/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<p>At MAVAJ SUN CO , our programming team has developed a multitude of applications which we can offer to our clients.These include, but are not limited to, applications for: real estate management, online shopping, wholesale ordering, newsletter and mailing lists, e-cards and site updating.</p>
<p>MAVAJ SUN CO provides tailored solutions to suite YOUR business</p>
<p>Our programmers can build unlimited numbers of programs and applications.</p>

<p>We have a range of business solutions for</p>

<ui>
    <li>Promotional Sites</li>
    <li>Marketing (Newsletter applications, link management, Search Engine Marketing Google Adwords monitoring)</li>
    <li>Course Booking</li>
    <li>E-Commerce</li>
    <li>Art Gallery and Photography licensing solutions</li>
    <li>Real Estate</li>
    <li>Accommodation</li>
    <li>The skills, and size, to custom build virtually any project</li>
</ui>
<p>Our programming team will ensure that your website has custom designed functions which are suited to both your business type and your target market. All our applications are designed in-house which means they can be customized to fit your individual needs.</p>

<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT']. '/common/footer.php';
?>