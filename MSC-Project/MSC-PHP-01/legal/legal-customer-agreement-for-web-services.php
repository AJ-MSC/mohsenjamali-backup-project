<?php

include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<table class="dataTable" cellspacing="0" width="100%" align="center" border="0">
    <tbody><tr>
            <td><div class="ui-heading">Legal Agreements</div></td>
        </tr>
        <tr align="">
            <td align="">
                <textarea rows="26" name="newcontent" cols="145" readonly="">CUSTOMER WEB SERVICES PRODUCT AGREEMENT EXTENSION

MAVAJ SUN CO (hereinafter referred to as "Parent") AND you (hereinafter referred to as "Customer")

HAVE

entered into a Customer Master Agreement ("Agreement") effective from 13 December, 2014 of which this "Web Services Product Agreement Extension" is a part. 

WHEREAS, Parent provides Domain Forwarding, Mail Forwarding, Managed DNS;

WHEREAS, the Customer wishes to activate through Parent, Domain Forwarding or Mail Forwarding or Managed DNS Services;

NOW, THEREFORE, for and in consideration of the mutual promises, benefits and covenants contained herein and for other good and valuable consideration, the receipt, adequacy and sufficiency of which are hereby acknowledged, Parent and the Customer, intending to be legally bound, hereby agree as follows:

1. Customer Election. Customer hereby elects to activate Domain Forwarding or Mail Forwarding or Managed DNS through Parent.

2. Parent's Acceptance. Parent hereby accepts Customer's election to activate Domain Forwarding or Mail Forwarding or Managed DNS through Parent.
                </textarea>
            </td>
        </tr>

        <tr>
            <td align="center">
                <input type="button" name="back_button" value="Back" class="frmButton" onclick="javascript:history.go(-1);">
            </td>
        </tr>

    </tbody></table>
<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>
