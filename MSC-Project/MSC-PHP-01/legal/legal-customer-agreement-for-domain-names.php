<?php

include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<table class="dataTable" cellspacing="0" width="100%" align="center" border="0">
    <tbody><tr>
            <td><div class="ui-heading">Legal Agreements</div></td>
        </tr>
        <tr align="">
            <td align="">
                <textarea rows="26" name="newcontent" cols="145" readonly="">CUSTOMER DOMAIN REGISTRATION PRODUCT AGREEMENT EXTENSION

MAVAJ SUN CO (hereinafter referred to as "Parent") AND you (hereinafter referred to as "Customer")

HAVE

entered into a Customer Master Agreement ("Agreement") effective from 13 December, 2014 of which this "Domain Registration Product Agreement Extension" is a part.

WHEREAS, Parent is authorized to provide Internet registration and management services for domain names, for the list of TLDs mentioned within APPENDIX 'B';

WHEREAS, the Customer wishes to purchase Registration and/or Management and/or Renewal and/or Transfer for the list of TLDs mentioned within APPENDIX 'B' through Parent;

NOW, THEREFORE, for and in consideration of the mutual promises, benefits and covenants contained herein and for other good and valuable consideration, the receipt, adequacy and sufficiency of which are hereby acknowledged, Parent and the Customer, intending to be legally bound, hereby agree as follows:
1. DEFINITIONS

(1) "TLD" refers to .COM, .NET, .ORG, .BIZ, .INFO, .NAME, .US, .IN, .EU, .UK, .TRAVEL, .WS, .COOP, CentralNIC, .MOBI, .ASIA, .ME, .TEL, .MN, .BZ, .CC, .TV, .CN, .NZ, .CO, .CA, .DE, .ES, .AU, .XXX , .RU, .PRO, .SX, .PW, .IN.NET, .CO.DE, .LA, Donuts, .CLUB, .UNO, .MENU, .BUZZ, .LONDON, .BID, .TRADE, .WEBCAM, Rightside Registry and Radix Registry

(2) "gTLD" refers to .COM, .NET, .ORG, .BIZ, .INFO, .NAME, .TRAVEL, .COOP, .MOBI, .ASIA, .TEL, .XXX, .PRO, .BIKE, .CLOTHING, .GURU, .HOLDINGS, .PLUMBING, .SINGLES, .VENTURES, .CAMERA, .EQUIPMENT, .ESTATE, .GALLERY, .GRAPHICS, .LIGHTING, .PHOTOGRAPHY, .CONSTRUCTION, .CONTRACTORS, .DIRECTORY, .KITCHEN, .LAND, .TECHNOLOGY, .TODAY, .DIAMONDS, .ENTERPRISES, .TIPS, .VOYAGE, .CAREERS, .PHOTOS, .RECIPES, .SHOES, .CAB, .COMPANY, .DOMAINS, .LIMO, .ACADEMY, .CENTER, .COMPUTER, .MANAGEMENT, .SYSTEMS, .BUILDERS, .EMAIL, .SOLUTIONS, .SUPPORT, .TRAINING, .CAMP, .EDUCATION, .GLASS, .INSTITUTE, .REPAIR, .COFFEE, .FLORIST, .HOUSE, .INTERNATIONAL, .SOLAR, .HOLIDAY, .MARKETING, .CODES, .FARM, .VIAJES, .AGENCY, .BARGAINS, .BOUTIQUE, .CHEAP, .ZONE, .COOL, .WATCH, .WORKS, .EXPERT, .FOUNDATION, .EXPOSED, .CRUISES, .FLIGHTS, .RENTALS, .VACATIONS, .VILLAS, .TIENDA, .CONDOS, .PROPERTIES, .MAISON, .DATING, .EVENTS, .PARTNERS, .PRODUCTIONS, .COMMUNITY, .CATERING, .CARDS, .CLEANING, .TOOLS, .INDUSTRIES, .PARTS, .SUPPLIES, .SUPPLY, .FISH, .REPORT, .VISION, .SERVICES, .CAPITAL, .ENGINEERING, .EXCHANGE, .GRIPE, .ASSOCIATES, .LEASE, .MEDIA, .PICTURES, .REISEN, .TOYS, .UNIVERSITY, .TOWN, .WTF, .FAIL, .FINANCIAL, .LIMITED, .CARE, .CLINIC, .SURGERY, .DENTAL, .TAX, .CASH, .FUND, .INVESTMENTS, .FURNITURE, .DISCOUNT, .FITNESS, .SCHULE, .GRATIS, .CLAIMS, .CREDIT, .CREDITCARD, .DIGITAL, .ACCOUNTANTS, .FINANCE, .INSURE, .LOANS, .CHURCH, .LIFE, .GUIDE, .DIRECT, .PLACE, .DEALS, .CITY, .HEALTHCARE, .RESTAURANT, .GIFTS, .CLUB, .UNO, .MENU, .BUZZ, .LONDON, .BID, .TRADE, .WEBCAM, .IMMOBILIEN, .NINJA, .FUTBOL, .REVIEWS, .SOCIAL, .PUB, .MODA, .KAUFEN, .CONSULTING, .DEMOCRAT .DANCE, .PRESS, .HOST and .WEBSITE

(3) "Domain Order" refers to an Order fulfilled by the Customer through the Parent under this Domain Registration Product Agreement Extension.

(4) "Registrant" refers to the registrant/owner of a Domain Order as in the OrderBox Database.

(5) "Registrar" refers to the Registrar of a Domain Order as in the OrderBox Database and/or shown in the Whois Record.

(6) Registry Operator refers to the Organisation/Entity that maintains the registry of a TLD of a Domain Order.

(7) "Whois Record" refers to the collection of all data elements of the Domain Order, specifically its Registrant Contact Information, Administrative Contact Information, Technical Contact Information, Billing Contact Information, Nameservers if any, its Creation and Expiry dates, its Registrar and its current Status in the Registry.
2. OBLIGATIONS OF THE CUSTOMER

(1) The Customer must ensure that the Registrant of each Domain Order must agree to be bound by the terms and conditions laid out by the Registrar of the Domain Name during the term of the Domain Order. The Customer must familiarize himself with such terms. The Customer acknowledges that the Registrar has various rights and powers as mentioned in the Registrar's terms and conditions. Parent is not liable for any action taken by Registrar pursuant to the Registrar's terms and conditions. The Customer acknowledges and agrees that the Customer shall indemnify Parent of, and shall be responsible for any liability resulting from Registrants' noncompliance with such terms and conditions.

(2) The Customer will not make any changes to any information associated with the Domain Order without explicit authorization from the Registrant of that Domain Order.

(3) The Customer must comply with all applicable terms and conditions, standards, policies, procedures, and practices laid down by ICANN, the Registrar and the Registry Operator.
3. RIGHTS OF PARENT

Parent and Service Providers, in their sole discretion, expressly reserve the right to freeze, delete, suspend, deny, cancel, modify, take ownership of or transfer any Domain Order, in order to comply with any applicable Dispute policies, requests of law enforcement, or in compliance with any Court Orders, or if Parent or Service Providers in their sole discretion determine that the information associated with the Domain Order is inaccurate, or has been tampered with, or has been modified without authorization, or if Parent or Service Providers in their sole discretion determine that the Domain Order ownership should belong to another entity, or if Customer/Customer/Registrant does not comply with any applicable terms and conditions, standards, policies, procedures, and practices laid down by Parent, Service Providers, ICANN, the Registrar and the Registry Operator. The Customer agrees that Parent and Service Providers, and the contractors, employees, directors, officers, representatives, agents and affiliates, of Parent and Service Providers, are not liable for loss or damages that may result from any of the above.
4. SURVIVAL

In the event of termination of this Product Agreement Extension for any reason, Sections 2 and 3 shall survive.
APPENDIX 'A'
PRIVACY PROTECTION SERVICE SPECIFIC CONDITIONS
1. DESCRIPTION OF SERVICES

The Privacy Protection Service hides the contact details of the actual owner from appearing in the Whois Lookup Result of his domain name.
2. IMPLEMENTATION DETAILS

(1) Customer acknowledges and agrees that the contact information being displayed in the Whois of a privacy protected Domain Order will be those designated by the Parent, and

(1) any mail received via post at this Address would be rejected;

(2) any telephone call received at this Telephone Number, would be greeted with an electronic answering machine requesting the caller to email the email address listed in the Whois of this privacy protected domain name;

(3) the sender of any email to an email address listed in the Whois of this privacy protected domain name, will get an automated response email asking them to visit the URL http://www.privacyprotect.org/ to contact the Registrant, Administrative, Billing or Technical Contact of a privacy protected domain name through an online form. This message would be relayed as an email message via http://www.privacyprotect.org/ to the actual Registrant, Administrative, Billing or Technical Contact email address in the OrderBox Database.

(2) Customer agrees that we can not guarantee delivery of messages to either the Registrant, Administrative, Billing, Technical Contact, of a privacy protected Domain Order, and that such message may not be delivered in time or at all, for any reason whatsoever. Parent and Service Providers disclaim any and all liability associated with non-delivery of any messages relating to the Domain Order and this service.

(3) Customer understands that the Privacy Protection Service is only available for certain TLDs.

(4) Irrespective of whether Privacy Protection is enabled or not, Customers and Registrants are required to fulfill their obligations of providing true and accurate contact information as detailed in the Agreement.

(5) Customer understands and acknowledges that Parent in its sole, unfettered discretion, can discontinue providing Privacy Protection Services on the Order for any purpose, including but not limited to:

(1) if Parent receives any abuse complaint for the privacy protected domain name, or

(2) pursuant to any applicable laws, government rules or requirements, requests of law enforcement agency, or

(3) for the resolution of disputes concerning the domain name, or

(4) any other reason that Parent in its sole discretion deems appropriate to switch off the Privacy Protection Services.
3. OBLIGATIONS OF CUSTOMER

Customer must ensure that the Registrant of each Domain Order must also acknowledge and agree to be bound by the following terms and conditions. The Customer acknowledges and agrees that the Customer shall indemnify Parent of, and shall be responsible for any liability resulting from Customer's nondisclosure of these terms to Registrant of Domain Order.
4. INDEMNITY

Customer and Registrant agree to release, defend, indemnify and hold harmless Parent, Service Providers, PrivacyProtect.org, and their parent companies, subsidiaries, affiliates, shareholders, agents, directors, officers and employees, from and against any and all claims, demands, liabilities, losses, damages or costs, including reasonable attorney's fees, arising out of or related in any way to the Privacy Protection services provided hereunder.
APPENDIX 'B'
LIST OF TLDS PARENT IS AUTHORIZED TO PROVIDE DOMAIN NAME REGISTRATION AND MANAGEMENT SERVICES

    .COM, .NET (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .ORG (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .BIZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .INFO (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .NAME and .NAME Defensive Registrations and .NAME Mail Forwards (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .US (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .IN (through Registrar Webiq Domains Solutions Pvt Ltd)
    .EU (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .UK (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .TRAVEL (through Registrar Directi Internet Solutions Pvt. Ltd. D/B/A PublicDomainRegistry.com)
    .WS (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .COOP (through Registrar Domains.coop Ltd.)
    CentralNIC (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .MOBI (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .ASIA (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .ME (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .TEL (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .MN, .BZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CC, .TV (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CN (through Registrar PDR Ltd.)
    .NZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CO (through Registrar &lt;#=dotco_serviceprovidername#&gt;)
    .CA (through Registrar PublicDomainRegistry.com Inc)
    .DE (through Registrar Directi Internet Solutions Pvt. Ltd. d/b/a PublicDomainRegistry.com)
    .ES (through Registrar Directi Internet Solutions Pvt. Ltd. d/b/a PublicDomainRegistry.com)
    .AU (through Registrar Public Domain Registry Pty Ltd.)
    .RU (through Registrar RU-Center)
    .XXX (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .PRO (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .SX (through Registrar PDR Ltd.)
    .PW (through Registrar &lt;#=dotpw_serviceprovidername#&gt;)
    .IN.NET (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CO.DE (through Registrar PDR Ltd.)
    .LA (through Registrar &lt;#=centralnicdotla_serviceprovidername#&gt;)
    Donuts (through Registrar PDR Ltd.)
    .CLUB (through Registrar PDR Ltd. d/b/a
PublicDomainRegistry.com)
    .UNO (through Registrar PDR Ltd.)
    .MENU (through Registrar PDR Ltd.)
    .BUZZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.LONDON (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.BID (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.TRADE (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.WEBCAM (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	Rightside Registry (through Registrar PDR Ltd.)
	Radix Registry (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)



APPENDIX 'C'
VERISIGN MOBILEVIEW SERVICE SPECIFIC CONDITIONS
1. DESCRIPTION OF SERVICES

The Verisign MobileView Service creates a mobile-friendly version for your .COM and .NET website.
2. IMPLEMENTATION DETAILS

Registrant agrees that Verisign may store the following data elements about any MobileView-enabled domain name:

(1) General Details (Company Name, Description and Company Logo);

(2) Contacts (Phone, Email and Address);

(3) Social (Facebook and Twitter URL);

(4) Products (Product Name, Description, Price and Image);

(5) Business Hours (Title and Hours of Operation);

(6) Coupons (Name, Description, Disclaimer, Image, Start and End Date).

(7) Usage information:

(1) Number of hits for any given domain name;

(2) User agent (where the request is coming from);

(3) Crawling website to collect website information.
3. DATA USAGE

Registrant agrees and acknowledges the following terms of data usage by Verisign:

(1) The data will be used to display to the end users and not used for any other internal purposes. This data is all publicly available on the internet and/or customer website.

(2) Customer information is used to create and maintain accounts as well as contacting Customers in case of any problems with accounts, provide technical support, conduct surveys and other similar activities.

(3) Verisign may use data for statistical analysis purpose to understand the Verisign MovileView adoption rate, trend by TLD's etc. in order to make improvements to our services.

(4) Verisign may also use data collected under the IMPLEMENTATION DETAILS, in accordance with the terms of their privacy policy as set forth at http://www.verisigninc.com/en_US/privacy/index.xhtml.
4. DATA RETENTION

Registrant acknowledges that Verisign may retain some data that has been anonymized under the IMPLEMENTATION DETAILS, as well as retain statistical information derived from aggregated data, even after the MobileView Service has been cancelled.
5. DATA TRANSFER

Registrant acknowledges that Verisign may transfer data collected under the IMPLEMENTATION DETAILS across International boundaries.
                </textarea>
            </td>
        </tr>

        <tr>
            <td align="center">
                <input type="button" name="back_button" value="Back" class="frmButton" onclick="javascript:history.go(-1);">
            </td>
        </tr>

    </tbody></table>
<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>