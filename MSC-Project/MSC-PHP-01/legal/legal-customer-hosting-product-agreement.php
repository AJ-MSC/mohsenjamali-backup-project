<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<div class="row-indent">
    <table class="dataTable" cellspacing="0" width="100%" align="center" border="0">
        <tbody><tr>
                <td><div class="ui-heading">Legal Agreements</div></td>
            </tr>
            <tr align="">
                <td align="">
                    <textarea rows="26" name="newcontent" cols="145" readonly="">CUSTOMER HOSTING PRODUCT AGREEMENT EXTENSION

MAVAJ SUN CO (hereinafter referred to as "Parent") AND you (hereinafter referred to as "Customer")

HAVE

entered into a Customer Master Agreement effective from 13 December, 2014 of which this "Customer Hosting Product Agreement Extension" is a part.

WHEREAS Parent provides Web, Virtual Private Server (VPS) and Email Hosting Services;

WHEREAS the Customer wishes to place an Order for Web, VPS and/or Email Hosting Services ("Hosting Order") through the Parent;

NOW, THEREFORE, for and in consideration of the mutual promises, benefits and covenants contained herein and for other good and valuable consideration, the receipt, adequacy and sufficiency of which are hereby acknowledged, Parent and the Customer, intending to be legally bound, hereby agree as follows:

1. Rights of Parent
While certain attributes of the Hosting Order may consist of unlimited resources, Customer recognises that the Hosting Order is a shared hosting service, and that the Parent has the right in its sole discretion to apply any hard limits on any specific attribute or resource on the Hosting Order at any given time without notice in order to prevent degradation of its services, or incase of any breach or violation or threatened breach or violation of this Agreement, or incase Parent learns of a possibility of breach or violation of this Agreement which Parent in its sole discretion determines to be appropriate, or to protect the integrity and stability of the Parent Products and the OrderBox, or to avoid any liability, civil or criminal, on the part of Parent and/or Service Providers, or for any other appropriate reason. The Customer agrees that Parent and Service Providers, and the contractors, employees, directors, officers, representatives, agents and affiliates, of Parent and Service Providers, are not liable for loss or damages that may result from any of the above.

2. Terms of Usage
Customer, or its contractors, employees, directors, officers, representatives, agents and affiliates and OrderBox Users, either directly or indirectly, shall not use or permit use of the Hosting Order, in violation of this Agreement, and for any of the activities described below -

A. General Terms

(1) For any unacceptable or inappropriate material as determined by Parent in its sole discretion, including but not limited to Topsites, IRC Scripts/Bots, Proxy Scripts/Anonymizers, Pirated Software/Warez, Image Hosting Scripts (similar to Photobucket or Tinypic), AutoSurf/PTC/PTS/PPC sites, IP Scanners, Bruteforce Programs/Scripts/Applications, Mail Bombers/Spam Scripts, Banner-Ad services (commercial banner ad rotation), File Dump/Mirror Scripts (similar to rapidshare), Commercial Audio Streaming (more than one or two streams), Escrow/Bank Debentures, High-Yield Interest Programs (HYIP) or Related Sites, Investment Sites (FOREX, E-Gold Exchange, Second Life/Linden Exchange, Ponzi, MLM/Pyramid Scheme), Sale of any controlled substance without prior proof of appropriate permit(s), Prime Banks Programs, Lottery Sites, MUDs/RPGs/PPBGs, Hateful/Racist/Harassment oriented sites, Hacker focused sites/archives/programs, Sites promoting illegal activities, Forums and/or websites that distribute or link to warez/pirated/illegal content, Bank Debentures/Bank Debenture Trading Programs, Fraudulent Sites (Including, but not limited to sites listed at aa419.org &amp; escrow-fraud.com), Mailer Pro.

(2) Use over 25% of system resources, including but not limited to Memory, CPU, Disk, Network, and Bandwidth capacity, for longer than 90 seconds in any consecutive 6 hour period.

(3) Execute long-running, stand-alone, unattended server-side processes, bots or daemons.

(4) Run any type of web spiders or indexers.

(5) Run any software that interfaces with an IRC (Internet Relay Chat) network.

(6) Run, host, or store any P2P client, tracker, software, server, files, content or application, including bittorrent.

(7) Participate in any P2P or file-sharing networks.

(8) Use excessive resources which in the Parent's sole discretion result in damage or degradation to the performance, usage, or experience of OrderBox, other users, other orders, and any of Parent's services.

(9) Use the email service for sending or receiving unsolicited emails.

(10) Use the email service for sending or receiving emails through automated scripts hosted on your website. For sending out promotional emails, email campaigns, etc., we recommend using the Mailing Lists feature rather than using your email account. Upon detection of such mails going through the regular mailing system, such mails will get classified as spam even though the recipient might have opted in for receiving such mails. This would lead to immediate cessation of mail sending capabilities for the user or the domain name. Frequent violation would lead to permanent suspension of the domain name.

(11) Sending mails to invalid recipient email addresses. On receipt of too many bounce back messages due to invalid recipient email addresses, the user sending such mails would get blocked. Frequent violation would lead to permanent suspension of the domain name.

(12) Sending mails from an email address that is not valid and which results in triple bounces would result in suspension of the user sending such mails. Frequent violation would lead to permanent suspension of the domain name.

(13) Send emails with malicious content. Such emails could be emanating from user(s) whose machine(s) are infected with a virus or malware and such activity could be happening without the user(s) knowledge or user(s) could be unknowingly sending out emails whose receivers may deem them as unsolicited.

(14) Run cron entries with intervals of less than 15 minutes.

(15) Engage in any activities related to purchase, sale or mining of currencies such as Bitcoin.

B. Web, Email Hosting Specific terms

(1) As a backup/storage device.

(2) Run any gaming servers.

(3) Store over 100,000 files.

(4) Constantly create and delete large numbers of files on a regular basis, or cause file system damage.

(5) Run any MySQL queries longer than 15 seconds.

(6) Divide Multi-Domain Hosting Orders into smaller packages to resell. Multi-Domain Hosting Orders can only be used by a single Company or Customer to host websites that are fully owned by them. Certain relevant Documents, other than domain name whois details, with respect to company and domain names/website ownership will need to be presented when requested. Having the same whois details for all domain names in your Multi-Domain Hosting Order will not be enough to substantiate ownership.

(7) Store a large number of media files (audio, video, etc.), wherein the limit is at Parent's sole discretion.

(8) Send over 100 messages per hour per user and/or 300 messages per hour for a domain name. Receive a high volume of emails, by a user or domain name, in any given period of time.

(9) Purchase/use a Dedicated IP Address without installing an SSL Certificate.

(10) Violate the above Terms of Usage for a Hosting Order which comprises of the Do-It-Yourself website builder powered by Jigsy.com.

(11) Use more than 50% of the website's disk space used by your Hosting order for storing emails.

(12) Use a WHMCS license issued by the Parent with any product/service other than the one for which it was issued.

(13) Store more than two website backup files.

(14) Use more than 5GB per database.
                    </textarea>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <input type="button" name="back_button" value="Back" class="frmButton" onclick="javascript:history.go(-1);">
                </td>
            </tr>

        </tbody></table><br>
</div>

<?php
//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>