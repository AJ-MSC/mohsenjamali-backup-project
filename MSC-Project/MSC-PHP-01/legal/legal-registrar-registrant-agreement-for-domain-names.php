<?php

include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<table class="dataTable" cellspacing="0" width="100%" align="center" border="0">
    <tbody><tr>
            <td><div class="ui-heading">Legal Agreements</div></td>
        </tr>
        <tr align="">
            <td align="">
                <textarea rows="26" name="newcontent" cols="145" readonly="">﻿DOMAIN REGISTRANT AGREEMENT

This Domain Registrant Agreement (hereinafter referred to as the "Agreement") between you ("you", "your" or "Registrant") and the Registrar of the Domain Name, or .NAME Defensive Registration, or .NAME Mail Forward (the "Order") that you have registered/reserved through or transferred to Registrar, sets forth the terms and conditions of Registrar's domain name registration service and other associated services as described herein.

If you are entering into this agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to these terms and conditions, in which case the terms "you", "your" and "Registrant" shall refer to such entity.

This Agreement explains our obligations to you, and your obligations to us in relation to each Domain Name, or .NAME Defensive Registration, or .NAME Mail Forward that you have registered/reserved through or transferred to Registrar ("Order"), directly or indirectly, whether or not you have been notified about Registrar.

This Agreement will become effective when the term of your Order begins with Registrar and will remain in force until the Order remains as an active Order with Registrar. Registrar may elect to accept or reject the Order application for any reason at its sole discretion, such rejection including, but not limited to, rejection due to a request for a prohibited Order.

WHEREAS, Registrar is authorized to provide Internet registration and management services for domain names, for the list of TLDs mentioned within APPENDIX 'U';

AND WHEREAS, the Registrant is the Owner of a registration of a domain name ("the SLD") in any of the TLDs mentioned within APPENDIX 'U', directly or indirectly;

NOW, THEREFORE, for and in consideration of the mutual promises, benefits and covenants contained herein and for other good and valuable consideration, the receipt, adequacy and sufficiency of which are hereby acknowledged, Registrar and the Registrant, intending to be legally bound, hereby agree as follows:

1. DEFINITIONS

(1) "Business Day" refers to a working day between Mondays to Friday excluding all Public Holidays.

(2) "Communications" refers to date, time, content, including content in any link, of all oral / transmitted / written communications / correspondence between Registrar, and the Registrant, and any Artificial Juridical Person, Company, Concern, Corporation, Enterprise, Firm, Individual, Institute, Institution, Organization, Person, Society, Trust or any other Legal Entity acting on their behalf.

(3) "Customer" refers to the customer of the Order as recorded in the OrderBox Database.

(4) "OrderBox" refers to the set of Servers, Software, Interfaces, Registrar Products and API that is provided for use directly or indirectly under this Agreement by Registrar and/or its Service Providers.

(5) "OrderBox Database" is the collection of data elements stored on the OrderBox Servers.

(6) "OrderBox Servers" refer to Machines / Servers that Registrar or its Service Providers maintain to fulfill services and operations of the OrderBox.

(7) "OrderBox User" refers to the Customer and any Agent, Employee, Contractee of the Customer or any other Legal Entity, that has been provided access to the "OrderBox" by the Customer, directly or indirectly.

(8) "Registrar" refers to the Registrar of record as shown in a Whois Lookup for the corresponding Order at the corresponding Registry Operator.

(9) "Registrar Products" refer to all Products and Services of Registrar which it has provided/rendered/sold, or is providing/rendering/selling.

(10) "Registrar Servers" refer to web servers, Mailing List Servers, Database Servers, OrderBox Servers, Whois Servers and any other Machines / Servers that Registrar or its Service Providers Operate, for the OrderBox, the Registrar Website, the Registrar Mailing Lists, Registrar Products and any other operations required to fulfill services and operations of Registrar.

(11) "Registrar Website" refers to the website of the Registrar.

(12) "Registry Operator" refers individually and collectively to any Artificial Juridical Persons, Company, Concern, Corporation, Enterprise, Firm, Individual, Institute, Institution, Organization, Person, Society, Trust or any other Legal Entity that is involved in the management of any portion of the registry of the TLD, including but not limited to policy formation, technical management, business relationships, directly or indirectly as an appointed contractor.

(13) "Resellers" - The Registrant may purchase the Order through a reseller, who in turn may purchase the same through a reseller and so on (collectively known as the "Resellers").

(14) "Service Providers" refers individually and collectively to any Artificial Juridical Persons, Company, Concern, Corporation, Enterprise, Firm, Individual, Institute, Institution, Organization, Person, Society, Trust or any other Legal Entity that the Customer and/or Registrar and/or Service Providers (recursively) may, directly or indirectly, Engage / Employ / Outsource / Contract for the fulfillment / provision / purchase of Registrar Products, OrderBox, and any other services and operations of Registrar.

(15) "Whois" refers to the public service provided by Registrar and Registry Operator whereby anyone may obtain certain information associated with the Order through a "Whois Lookup".

(16) "Whois Record" refers to the collection of all data elements of the Order, specifically its Registrant Contact Information, Administrative Contact Information, Technical Contact Information, Billing Contact Information, Nameservers if any, its Creation and Expiry dates, its Registrar and its current Status in the Registry.

(17) "Prohibited Persons (Countries, Entities, and Individuals)" refers to certain sanctioned countries (each a "Sanctioned Country") and certain individuals, organizations or entities, including without limitation, certain "Specially Designated Nationals" ("SDN") as listed by the government of the United States of America through the Office of Foreign Assets Control ("OFAC"), with whom all or certain commercial activities are prohibited. If you are located in a Sanctioned Country or your details match with an SDN entry, you are prohibited from registering or signing up with, subscribing to, or using any service of Parent.

2. OBLIGATIONS OF THE REGISTRANT

(1) The Registrant agrees to provide and maintain current, complete and accurate information of the Whois Record and all the data elements about the Order in the OrderBox Database and update them within seven (7) days of any change during the term of the Order, including: the full name, postal address, e-mail address, voice telephone number, and fax number if available of the Registered Name Holder; name of authorized person for contact purposes in the case of an Registered Name Holder that is an organization, association, or corporation. Registrant agrees that provision of inaccurate or unreliable information, and/or Registrant's failure to promptly update information provided to Registrar within seven (7) days of any change, or its failure to respond for over seven (7) days to inquiries by Registrar to the email address of the Registrant or any other contact listed for the Order in the OrderBox database concerning the accuracy of contact information associated with the Order shall be constituted as a breach of this Agreement and a basis for freezing, suspending, or deleting that Order.

(2) The Registrant agrees to the automatic email id verification process setup by the Registrar as mandated by ICANN WHOIS ACCURACY PROGRAM (http://www.icann.org/en/resources/registrars/raa/approved-with-specs-27jun13-en.htm#whois-accuracy). Registrants have to verify their email id within fifteen (15) days of receiving notification by the Registrar / Registration Service Provider to the email address of the Registrant by clicking on the verification link. This verification process will be applicable to all new registrants post registration or transfer of a domain name and/or after modifying the email id of an existing registrant contact from Orderbox. Failure to complete the verification for over fifteen (15) days shall result in immediate suspension of

(1) respective domain name and it's associated services;

(2) contact Id associated with the Registrants email id.

(3) The Registrant acknowledges that in the event of any dispute and/or discrepancy concerning the data elements of the Order in the OrderBox Database, the data element in the OrderBox Database records shall prevail.

(4) The Registrant acknowledges that the authentication information for complete control and management of the Order will be accessible to the Registry Operator, Service Providers, Resellers and the Customer. Any modification to the Order by the Resellers, Customer or Service Providers will be treated as if it is authorized by the Registrant directly. Registrar is not responsible for any modification to the Order by the Customer, Resellers, Registry Operator, or Service Providers.

(5) The Registrant acknowledges that all communication about the Order will be only done with the Customer or the Resellers of the Order. Registrar is not required to, and may not directly communicate with the Registrant during the entire term of the Order.

(6) Any Registrant that intends to license use of a domain name to a third party or a privacy/proxy service, is nonetheless the Registered Name holder of record and is responsible for

(1) providing its own complete contact information and for;

(2) providing and updating accurate technical and administrative contact information adequate to facilitate timely resolution of any problems that arise in connection with the registered domain name.

The Registrant licensing use of the registered domain name according to this provision shall accept liability for harm caused by wrongful use of the registered domain name, unless it discloses the current contact information provided by the licensee and the identity of the licensee within seven (7) days to a party providing the registrant reasonable evidence of actionable harm.

(7) Any Registrant that intends to license use of a domain name to a third party or a privacy/proxy service, shall represent that notice has been provided to the licensee or to any third-party individuals whose Personal Data is supplied to Registrar by the Registrant stating -

(1) The purposes for which any Personal Data collected from the licensee or from any third-party individuals;

(2) The intended recipients or categories of recipients of the data (including the Registrar, Registration Service provider, Registry Operator and others who will receive the data from Registry Operator); 

(3) Which data is obligatory and which data is voluntary; and

(4) How the Registrant can access and, if necessary, rectify the data held about them.

(8) The Registrant confirms that they shall consent to the data processing referred to in subsection 2.(6) 

(9) The Registrant confirms that they have obtained consent equivalent to that referred to in subsection 2.(7) from any third party individuals to whom the Registrant is licensing use of the registered domain name.

(10) The Registrant shall comply with all terms or conditions established by Registrar, Registry Operator and/or Service Providers from time to time.

(11) The Registrant must comply with all applicable terms and conditions, standards, policies, procedures, and practices laid down by ICANN (https://www.icann.org/resources/pages/benefits-2013-09-16-en) and the Registry Operator.

(12) During the term of this Agreement and for three years thereafter, the Registrant shall maintain the following records relating to its dealings with Registrar, Resellers and their Agents or Authorized Representatives:

(1) in electronic, paper or microfilm form, all written communications with respect to the Order;

(2) in electronic form, records of the accounts of the Order, including dates and amounts of all payments, discount, credits and refunds.

The Registrant shall make these records available for inspection by Registrar upon reasonable notice not exceeding 14 days.

3. REPRESENTATIONS AND WARRANTIES

Registrar and Registrant represent and warrant that:

(1) They have all requisite power and authority to execute, deliver and perform their obligations under this Agreement.

(2) This Agreement has been duly and validly executed and delivered and constitutes a legal, valid and binding obligation, enforceable against Registrant and Registrar in accordance with its terms.

(3) The execution, delivery, and performance of this Agreement and the consummation by Registrar and the Registrant of the transactions contemplated hereby will not, with or without the giving of notice, the lapse of time, or both, conflict with or violate:

(1) any provision of law, rule, or regulation;

(2) any order, judgment, or decree;

(3) any provision of corporate by-laws or other documents;

(4) any agreement or other instrument.

(4) The execution, performance and delivery of this Agreement has been duly authorized by the Registrant and Registrar.

(5) No consent, approval, or authorization of, or exemption by, or filing with, any governmental authority or any third party is required to be obtained or made in connection with the execution, delivery, and performance of this Agreement or the taking of any other action contemplated hereby.

The Registrant represents and warrants that:

(1) the Registrant has read and understood every clause of this Agreement;

(2) the Registrant has independently evaluated the desirability of the service and is not relying on any representation agreement, guarantee or statement other than as set forth in this agreement; and

(3) the Registrant is eligible, to enter into this Contract according to the laws of his country.

4. RIGHTS OF REGISTRAR, REGISTRY OPERATOR AND SERVICE PROVIDERS

(1) Registrar, Service Providers and Registry Operator may change any information, of the Order, or transfer the Order to another Registrant, or transfer the Order to another Customer, upon receiving any authorization from the Registrant, or the Customer, or Resellers as maybe prescribed by Registrar from time to time.

(2) Registrar, Service Providers and Registry Operator may provide/send any information, about the Registrant, and the Order including Authentication information:

(1) to the Registrant;

(2) to any authorised representative, agent, contractee, employee of the Registrant upon receiving authorization in any form as maybe prescribed by Registrar from time to time;

(3) to the Customer, Resellers, Service Providers and Registry Operator;

(4) to anyone performing a Whois Lookup for the Order.

(3) Registrar in its own discretion can at any point of time with reasonable notification temporarily or permanently cease to sell any Registrar Products.

(4) Registrar and the Registry Operator, in their sole discretion, expressly reserve the right to deny any Order or cancel an Order within 30 days of processing the same. In such case Registrar may refund the fees charged for the Order, after deducting any processing charges for the same.

(5) Notwithstanding anything to the contrary, Registrar, Registry Operator and Service Providers, in their sole discretion, expressly reserve the right to without notice or refund, delete, suspend, deny, cancel, modify, take ownership of or transfer the Order, or to modify, upgrade, suspend, freeze OrderBox, or to publish, transmit, share data in the OrderBox Database with any person or entity, or to contact any entity in the OrderBox Database, in order to recover any Payment from the Registrant, Customer or Resellers, for any service rendered by Registrar including services rendered outside the scope of this agreement for which the Registrant, Customer or Reseller has been notified and requested to remit payment, or to correct mistakes made by Registrar, Registry Operator or Service Providers in processing or executing the Order, or incase of any breach of this Agreement, or incase Registrar learns of a possibility of breach or violation of this Agreement which Registrar in its sole discretion determines to be appropriate, or incase of Termination of this agreement, or if Registrar learns of any such event which Registrar reasonably determines would lead to Termination of this Agreement or would constitute as Breach thereof, or to protect the integrity and stability of the Registrar Products, OrderBox, and the Registry or to comply with any applicable laws, government rules or requirements, requests of law enforcement, or in compliance with any dispute resolution process, or in accordance/compliance with any agreements executed by Registrar including but not limited to agreements with Service Providers, and/or Registry Operator, and/or Customers and/or Resellers, or to avoid any liability, civil or criminal, on the part of Registrar and/or Service Providers, and/or the Registry Operator, as well as their affiliates, subsidiaries, officers, directors and employees, or if the Registrant and/or Agents or any other authorized representatives of the Registrant violate any applicable laws/government rules/usage policies, including but not limited to, intellectual property, copyright, patent, anti-spam, Phishing (identity theft), Pharming (DNS hijacking), distribution of virus or malware, child pornography, using Fast Flux techniques, running Botnet command and control, Hacking (illegal access to another computer or network), network attacks, money laundering schemes (Ponzi, Pyramid, Money Mule, etc.), illegal pharmaceutical distribution, or Registrar learns of the possibility of any such violation or upon appropriate authorization (what constitutes appropriate authorization is at the sole discretion of Registrar) from the Registrant or Customer or Reseller or their authorized representatives, or if Registrar, Registry Operator or Service Providers in their sole discretion determine that the information associated with the Order is inaccurate, or has been tampered with, or has been modified without authorization, or if Registrar or Service Providers in their sole discretion determine that the ownership of the Order should belong to another entity, or if Reseller/Customer/Registrant does not comply with any applicable terms and conditions, standards, policies, procedures, and practices laid down by Registrar, Service Providers, ICANN, the Registrar, the Registry Operator or for any appropriate reason. Registrar or Registry Operator, also reserve the right to freeze the Order during resolution of a dispute. The Registrant agrees that Registrar, Registry Operator and Service Providers, and the contractors, employees, directors, officers, representatives, agents and affiliates, of Registrar, Registry Operator and Service Providers, are not liable for loss or damages that may result from any of the above.

(6) Registrar and Service Providers can choose to redirect an Order to any IP Address including, without limitation, to an IP address which hosts a parking page or a commercial search engine for the purpose of monetization, if an Order has expired, or is suspended, or does not contain valid Name Servers to direct it to any destination. Registrant acknowledges that Registrar and Service Providers cannot and do not check to see whether such a redirection, infringes any legal rights including but not limited to intellectual property rights, privacy rights, trademark rights, of Registrant or any third party, or that the content displayed due to such redirection is inappropriate, or in violation of any federal, state or local rule, regulation or law, or injurious to Registrant or any third party, or their reputation and as such is not responsible for any damages caused directly or indirectly as a result of such redirection.

(7) Registrar and Registry Operator has the right to rectify any mistakes in the data in the OrderBox Database with retrospective effect.

(8) Registrar has the right to change the registrar on record of the order to another registrar.

(9) Registrar shall provide notice to each new or renewed Registered Name Holder stating:

(1) The purposes for which any Personal Data collected from the applicant are intended;

(2) The intended recipients or categories of recipients of the data(including the Registry Operator and others who will receive the data from the Registry Operator);

(3) Which data are obligatory and which data, if any, are voluntary and how the Registrant or data subject can access and, if necessary, rectify the data held about them.

(10) Registrar will not process the Personal Data collected from the Registered Name holder in a way incompatible with the purposes and other limitations about which it has provided notice to the Registered Name holder.

(11) Registrar will take reasonable precautions to protect Personal Data provided by the Registered Name holder from loss, misuse, unauthorized access or disclosure, alteration, or destruction.

(12) Registrar and Service Providers, in their sole discretion, expressly reserve the right to suspend an Order without prior notice, and/or delete an Order without issuing a refund, if the associated Registrant / Administrative / Technical / Billing Contact is located in a Sanctioned Country or his/her details, existing or modified, match with an SDN entry. The Registrant agrees that Registrar and Service Providers, and the contractors, employees, directors, officers, representatives, agents and affiliates, of Registrar and Service Providers, are not liable for loss or damages that may result from any of the above.

5. DOMAIN NAME DISPUTE PROCESS

(1) The Registrant agrees that, if the use of the Order is challenged by a third party, the Registrant will be subject to the provisions of the appropriate Dispute policy for that Order as mentioned in the appropriate Appendix in effect at the time of the dispute. The Registrant agrees that in the event a dispute arises with any third party, the Registrant will indemnify and hold Registrar, Registry Operator and Service Providers harmless in all circumstances, and that Registrar, Registry Operator and Service Providers will have no liability of any kind for any loss or liability resulting from any such dispute, including the decision and final outcome of such dispute. If a complaint has been filed with a judicial or administrative body regarding the Registrant's use of the Order, the Registrant agrees not to make any changes to the Order without Registrar's prior approval. Registrar may not allow the Registrant to make changes to such Order until Registrar is directed to do so by the judicial or administrative body.

(2) Uniform Domain Name Dispute Resolution Policy ("UDRP"), identified on ICANN's website http://www.icann.org/en/help/dndr/udrp/policy, has been adopted by all ICANN-accredited Registrars to resolve dispute proceedings arising from alleged abusive registrations of domain names (for example, cybersquatting). Holder of the trademark can excercise their right by filing a UDRP case with any of ICANN's UDRP Service Providers listed at http://www.icann.org/dndr/udrp/approved-providers.htm to challenge ownership of the gTLD domain names. 

(3) Uniform Rapid Suspension ("URS") System, identified on ICANN's website http://newgtlds.icann.org/en/applicants/urs, is a rights protection mechanism that complements the existing Uniform Domain-Name Dispute Resolution Policy (UDRP) by offering a lower-cost, faster path to relief for rights holders experiencing the most clear-cut cases of trademark infringementfor gTLD domain names. 

6. TERM OF AGREEMENT / RENEWALS

(1) The term of this Agreement shall continue until the registrant of the Order in the OrderBox database continues to be the Registrant and the Order continues to exist and the Order Registration term continues to exist.

(2) Registrant acknowledges that it is the Registrant's responsibility to keep records and maintain reminders regarding the expiry of any Order. However, the Registrar will send domain renewal notifications to the Registrant on record, either directly or through MAVAJ SUN CO, as per the schedule given on http://webhost.mavajsunco.com/domain-registration-pricing. As a convenience to the Registrant, and not as a binding commitment, we may notify the Customer, via an email message sent to the contact information associated with the Customer in the OrderBox database, about the expiry of the Order. Should renewal fees go unpaid for an Order, the Order will expire.

(3) Registrant acknowledges that after expiration of the term of an Order, Registrant has no rights on such Order, or any information associated with such Order, and that ownership of such Order now passes on to the Registrar. Registrar and Service Providers may make any modifications to said Order or any information associated with said Order. Registrar and Service Providers may intercept any network/communication requests to such Order and process them in any manner in their sole discretion. Registrar and Service Providers may choose to monetize such requests in any fashion at their sole discretion. Registrar and Service Providers may choose to display any appropriate message, and/or send any response to any user making a network/communication request, for or concerning said Order. Registrar and Service Providers may choose to delete said Order at anytime after expiry upon their sole discretion. Registrar and Service Providers may choose to transfer the ownership of the Order to any third party in their sole discretion. Registrant acknowledges that Registrar and Service Providers shall not be liable to Registrant or any third party for any action performed under this clause.

(4) Registrar at its sole discretion may allow the renewal of the Order after Order expiry, and such renewal term will start as on the date of expiry of the Order, unless otherwise specified. Such process may be charged separately at the price then prevailing for such a process as determined by the Registrar in its sole discretion. Such renewal after the expiry of the Order may not result in exact reinstatement of the Order in the same form as it was prior to expiry.

(5) Registrar makes no guarantees about the number of days, after deletion of an Order, after which the same Order will once again become available for purchase.

(6) This Agreement shall terminate immediately in the event:

(1) Registrar's contract with the Service Providers for the fulfillment of such Order is terminated or expires without renewal;

(2) Registrar's contract with the Registry Operator is terminated or expires without renewal;

(3) Registry Operator ceases to be the Registry Operator for the particular TLD;

(4) of Registrant-Registrant Transfer as per Section 8;

(5) of Registrar-Registrar Transfer as per Section 9.

(7) Upon Termination of this Agreement, Registrar may delete/suspend/transfer/modify the Order and suspend OrderBox Users' access to the OrderBox with immediate effect, upon the sole discretion of Registrar.

(8) Neither Party shall be liable to the other for damages of any sort resulting solely from terminating this Agreement in accordance with its terms, unless specified otherwise. The Registrant however shall be liable for any damage arising from any breach by it of this Agreement.

7. FEES / RENEWAL

Payment of fees shall be governed as per the Payment Terms and Conditions set out in Appendix 'B.'

8. REGISTRANT - REGISTRANT TRANSFER

(1) Registrar may transfer the Order of the Registrant to another registrant under the following circumstances:

(1) authorization from the Registrant and/or their Agent or Authorized Representative in a manner prescribed by Registrar from time to time;

(2) authorization from the Customer and/or the Reseller in a manner prescribed by Registrar;

(3) on receiving orders from a competent Court or Law Enforcement Agency;

(4) for fulfillment of a decision in a domain dispute resolution;

(5) breach of Contract;

(6) termination of this Agreement;

(7) Registrar learns of any such event, which Registrar reasonably determines would lead to Termination of this Agreement, or would constitute as Breach thereof.

(2) Registrant acknowledges that Registrar cannot verify the authenticity of any information, authorization or instructions received in Section (8)(1). Upon receiving such authorization that Registrar in its absolute unfettered and sole discretion deems to be genuine, Registrar may transfer the Order. Registrar cannot be held liable for any such transfer under any circumstance including but not limited to fraudulent or forged authorization received by Registrar.

(3) In the above circumstances the Registrant shall extend full cooperation to Registrar in transferring the Order of the Registrant to another registrant including without limitation, handing over all data required to be stored by the Registrant as per Section 3(5), and complying with all requirements to facilitate a smooth transfer.

(4) The Registrant's Order may not be transferred until Registrar receives such written assurances or other reasonable assurance that the new registrant has been bound by the contractual terms of this Agreement (such reasonable assurance as determined by Registrar in its sole discretion). If the Transferee fails to be bound in a reasonable fashion (as determined by Registrar in its sole discretion) to the terms and conditions in this Agreement, any such transfer maybe considered by Registrar as null and void in its sole discretion.

9. REGISTRAR-REGISTRAR TRANSFER

(1) The Registrant acknowledge and agree that during the first 60 days after initial registration of the Order, or after expiration of the Order the Registrant may not be able to transfer the Order to another registrar.

(2) Registrar may request the Registrant or any other contact associated with the Order for authorization upon receiving a request to transfer the Order to another registrar. The Registrant agrees to provide such authorization to Registrar. Registrar, in its sole discretion will determine, if such authorization is adequate to allow the transfer.
(3) Registrar may deny or prevent a transfer of an Order to another registrar in situations described in this Agreement including, but not limited to:

(1) a dispute over the identity of the domain name holder;

(2) bankruptcy; and default in the payment of any fees;

(3) any pending dues from the Customer or Resellers' or Registrant for any services rendered, whether under this agreement;

(4) any pending Domain Dispute Resolution process with respect to the Order;

(5) if the Order has been locked or suspended by the Customer or Resellers;

(6) any situation where denying the transfer is permitted under the then applicable process and rules of transfer of domain names as laid out by the Registry Operator, Registrant acknowledges that it is their responsibility to research and acquaint themselves with these rules and any applicable changes from time to time;

(7) any other circumstance described in this Agreement;

(8) for any other appropriate reason;

(4) Registrar may at its sole discretion lock or suspend the Order to prevent a Domain Transfer.

(5) Registrar cannot be held liable for any domain name transferred away to another registrar, or for any denial of a transfer, in accordance with this Section 9 (Registrar-Registrar Transfer).

10. LIMITATION OF LIABILITY

IN NO EVENT WILL REGISTRAR, REGISTRY OPERATOR OR SERVICE PROVIDERS OR CONTRACTORS OR THIRD PARTY BENEFICIARIES BE LIABLE TO THE REGISTRANT FOR ANY LOSS OF REGISTRATION AND USE OF THE ORDER, OR FOR INTERRUPTIONS OF BUSINESS, OR ANY SPECIAL, INDIRECT, ANCILLARY, INCIDENTAL, PUNITIVE, EXEMPLARY OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES RESULTING FROM LOSS OF PROFITS, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, REGARDLESS OF THE FORM OF ACTION WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), OR OTHERWISE, EVEN IF REGISTRAR AND/OR ITS SERVICE PROVIDERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

REGISTRAR FURTHER DISCLAIMS ANY AND ALL LOSS OR LIABILITY RESULTING FROM, BUT NOT LIMITED TO:

(1) LOSS OR LIABILITY RESULTING FROM THE UNAUTHORIZED USE OR MISUSE OF AUTHENTICATION INFORMATION;

(2) LOSS OR LIABILITY RESULTING FROM FORCE MAJEURE EVENTS AS STATED IN SECTION 21 OF THIS AGREEMENT;

(3) LOSS OR LIABILITY RESULTING FROM ACCESS DELAYS OR ACCESS INTERRUPTIONS;

(4) LOSS OR LIABILITY RESULTING FROM NON-DELIVERY OF DATA OR DATA MISS-DELIVERY;

(5) LOSS OR LIABILITY RESULTING FROM ERRORS, OMISSIONS, OR MISSTATEMENTS IN ANY AND ALL INFORMATION OR REGISTRAR PRODUCT(S) PROVIDED UNDER THIS AGREEMENT;

(6) LOSS OR LIABILITY RESULTING FROM THE INTERRUPTION OF SERVICE.

If any legal action or other legal proceeding (including arbitration) relating to the performance under this Agreement or the enforcement of any provision of this Agreement is brought against Registrar by the Registrant, then in no event will the liability of Registrar exceed actual amount received by Registrar for the Order minus direct expenses incurred with respect to the Order.

REGISTRANT ACKNOWLEDGES THAT THE CONSIDERATION RECEIVED BY REGISTRAR IS BASED IN PART UPON THESE LIMITATIONS, AND THAT THESE LIMITATIONS WILL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY REMEDY. IN NO EVENT WILL THE LIABILITY OF REGISTRAR RELATING TO THIS AGREEMENT EXCEED TOTAL AMOUNT RECEIVED BY REGISTRAR IN RELATION TO THE ORDER.

11. INDEMNIFICATION

(1) The Registrant, at its own expense, will indemnify, defend and hold harmless, Registrar, Service Provider, Registry Operator, Resellers and the contactors, employees, directors, officers, representatives, agents and affiliates, of Registrar, Registry Operator, Service Providers, and Resellers against any claim, suit, action, or other proceeding brought against them based on or arising from any claim or alleged claim, of third parties relating to or arising under this Agreement, Registrar Products provided hereunder, or any use of the Registrar Products, including without limitation:

(1) infringement by the Registrant, or someone else using a Registrar Product with the Registrant's computer, of any intellectual property or other proprietary right of any person or entity;

(2) arising out of any breach by the Registrant of this Agreement;

(3) arising out of, or related to, the Order or use of the Order;

(4) relating to any action of Registrar as permitted by this Agreement;

(5) relating to any action of Registrar carried out on behalf of Registrant as described in this Agreement.

However, that in any such case Registrar may serve either of the Registrant with notice of any such claim and upon their written request, Registrar will provide to them all available information and assistance reasonably necessary for them to defend such claim, provided that they reimburse Registrar for its actual costs.

(2) Registrar will not enter into any settlement or compromise of any such indemnifiable claim without Registrant's prior written consent, which shall not be unreasonably withheld.

(3) The Registrant will pay any and all costs, damages, and expenses, including, but not limited to, actual attorneys' fees and costs awarded against or otherwise incurred by Registrar in connection with or arising from any such indemnifiable claim, suit, action or proceeding.

12. INTELLECTUAL PROPERTY

Subject to the provisions of this Agreement, each Party will continue to independently own his/her/its intellectual property, including all patents, trademarks, trade names, domain names, service marks, copyrights, trade secrets, proprietary processes and all other forms of intellectual property. Any improvements to existing intellectual property will continue to be owned by the Party already holding such intellectual property.

Without limiting the generality of the foregoing, no commercial use rights or any licenses under any patent, patent application, copyright, trademark, know-how, trade secret, or any other intellectual proprietary rights are granted by Registrar to the Registrant, or by any disclosure of any Confidential Information to the Registrant under this Agreement.

Registrant shall further ensure that the Registrant does not infringe any intellectual property rights or other rights of any person or entity, or does not publish any content that is libelous or illegal while using services under this Agreement. Registrant acknowledges that Registrar cannot and does not check to see whether any service or the use of the services by the Registrant under this Agreement, infringes legal rights of others.

13. OWNERSHIP AND USE OF DATA

(1) You agree and acknowledge that Registrar owns all data, compilation, collective and similar rights, title and interests worldwide in the OrderBox Database, and all information and derivative works generated from the OrderBox Database.

(2) Registrar, Service Providers and the Registry Operator and their designees/agents have the right to backup, copy, publish, disclose, use, sell, modify, process this data in any form and manner as maybe required for compliance of any agreements executed by Registrar, or Registry Operator or Service Providers, or in order to fulfill services under this Agreement, or for any other appropriate reason.

14. DELAYS OR OMISSIONS; WAIVERS

No failure on the part of any Party to exercise any power, right, privilege or remedy under this Agreement, and no delay on the part of any Party in exercising any power, right, privilege or remedy under this Agreement, shall operate as a waiver of such power, right, privilege or remedy; and no single or partial exercise or waiver of any such power, right, privilege or remedy shall preclude any other or further exercise thereof or of any other power, right, privilege or remedy.

No Party shall be deemed to have waived any claim arising out of this Agreement, or any power, right, privilege or remedy under this Agreement, unless the waiver of such claim, power, right, privilege or remedy is expressly set forth in a written instrument on behalf of such Party; and any such waiver shall not be applicable or have any effect except in the specific instance in which it is given.

No waiver of any of the provisions of this Agreement shall be deemed to constitute a waiver of any other provision (whether or not similar), nor shall such waiver constitute a waiver or continuing waiver unless otherwise expressly provided in writing.

15. RIGHT TO SUBSTITUTE UPDATED AGREEMENT

(1) During the period of this Agreement, the Registrant agrees that Registrar may:

(1) revise the terms and conditions of this Agreement; and

(2) change the services provided under this Agreement

(2) Registrar, or the Registry Operator or any corresponding/designated policy formulating body may revise ANY of the Dispute policies, and eligbility criterias set forth in the various appendices as well as in any of the external URLs referenced within the appendices.

(3) Any such revision or change will be binding and effective immediately on posting of the revision on the Registrar Website or the corresponding URL referenced in this Agreement.

(4) The Registrant agrees to review the Registrar Website and all other URLs referenced in this Agreement, periodically, to be aware of any such revisions.

(5) The Registrant agrees that, continuing use of the services under this Agreement following any revision, will constitute as an acceptance of any such revisions or changes.

(6) The Registrant acknowledges that if the Registrant does not agree to any such modifications, the Registrant may terminate this Agreement within 30 days of such revision. In such circumstance Registrar will not refund any fees paid by the Registrant.

16. PUBLICITY

The Registrant shall not create, publish, distribute, or permit any written / Oral / electronic material that makes reference to us or our Service Providers or uses any of Registrar's registered Trademarks / Service Marks or our Service Providers' registered Trademarks / Service Marks without first submitting such material to us and our Service Providers and receiving prior written consent.

The Registrant gives Registrar the right to use the Registrant names in marketing / promotional material with regards to Registrar Products to Visitors to the Registrar Website, Prospective Clients and existing and new customers.

17. TAXES

The Registrant shall be responsible for sales tax, consumption tax, transfer duty, custom duty, octroi duty, excise duty, income tax, and all other taxes and duties, whether international, national, state or local, however designated, which are levied or imposed or may be levied or imposed, with respect to this Agreement and the Registrar Products.

18. FORCE MAJEURE

Neither party shall be liable to the other for any loss or damage resulting from any cause beyond its reasonable control (a "Force Majeure Event") including, but not limited to, insurrection or civil disorder, riot, war or military operations, national or local emergency, acts or directives or omissions of government or other competent authority, compliance with any statutory obligation or executive order, strike, lock-out, work stoppage, industrial disputes of any kind (whether or not involving either party's employees), any Act of God, fire, lightning, explosion, flood, earthquake, eruption of volcano, storm, subsidence, weather of exceptional severity, equipment or facilities breakages / shortages which are being experienced by providers of telecommunications services generally, or other similar force beyond such Party's reasonable control, and acts or omissions of persons for whom neither party is responsible. Upon occurrence of a Force Majeure Event and to the extent such occurrence interferes with either party's performance of this Agreement, such party shall be excused from performance of its obligations (other than payment obligations) during the first six months of such interference, provided that such party uses best efforts to avoid or remove such causes of non performance as soon as possible.

19. ASSIGNMENT / SUBLICENSE

Except as otherwise expressly provided herein, the provisions of this Agreement shall inure to the benefit of and be binding upon, the successors and assigns of the Parties; provided, however, that any such successor or assign be permitted pursuant to the Articles, Bylaws or policies of Registrar.

The Registrant shall not assign, sublicense or transfer its rights or obligations under this Agreement to any third person/s except as provided for in Section 8 (REGISTRANT - REGISTRANT TRANSFER) or with the prior written consent of Registrar.

Registrant agrees that if Registrant licenses the use of the Order to a third party, the Registrant nonetheless remains the Registrant of record, and remains responsible for all obligations under this Agreement.

20. NO GUARANTY

The Registrant acknowledges that registration or reservation of the Order does not confer immunity from objection to the registration, reservation, or use of the Order.

21. DISCLAIMER

THE ORDERBOX, REGISTRAR SERVERS, OrderBox Servers, Registrar Website AND ANY OTHER SOFTWARE / API / SPECIFICATION / DOCUMENTATION / APPLICATION SERVICES IS PROVIDED ON "AS IS" AND "WHERE IS" BASIS AND WITHOUT ANY WARRANTY OF ANY KIND.

REGISTRAR AND SERVICE PROVIDERS EXPRESSLY DISCLAIM ALL WARRANTIES AND / OR CONDITIONS, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY OR SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS AND QUALITY/AVAILABILITY OF TECHNICAL SUPPORT.

REGISTRAR AND SERVICE PROVIDERS ASSUME NO RESPONSIBILITY AND SHALL NOT BE LIABLE FOR ANY DAMAGES TO, OR VIRUSES THAT MAY AFFECT, YOUR COMPUTER EQUIPMENT OR OTHER PROPERTY IN CONNECTION WITH YOUR ACCESS TO, USE OF, ORDERBOX OR BY ACCESSING REGISTRAR SERVERS. WITHOUT LIMITING THE FOREGOING, REGISTRAR AND SERVICE PROVIDERS DO NOT REPRESENT, WARRANT OR GUARANTEE THAT (A) ANY INFORMATION/DATA/DOWNLOAD AVAILABLE ON OR THROUGH ORDERBOX OR REGISTRAR SERVERS WILL BE FREE OF INFECTION BY VIRUSES, WORMS, TROJAN HORSES OR ANYTHING ELSE MANIFESTING DESTRUCTIVE PROPERTIES; OR (B) THE INFORMATION AVAILABLE ON OR THROUGH THE ORDERBOX/REGISTRAR SERVERS WILL NOT CONTAIN ADULT-ORIENTED MATERIAL OR MATERIAL WHICH SOME INDIVIDUALS MAY DEEM OBJECTIONABLE; OR (C) THE FUNCTIONS OR SERVICES PERFORMED BY REGISTRAR AND SERVICE PROVIDERS WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR THAT DEFECTS IN THE ORDERBOX WILL BE CORRECTED; OR (D) THE SERVICE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS OR (E) THE SERVICES PROVIDED UNDER THIS AGREEMENT OPERATE IN COMBINATION WITH ANY SPECIFIC HARDWARE, SOFTWARE, SYSTEM OR DATA. OR (F) YOU WILL RECEIVE NOTIFICATIONS, REMINDERS OR ALERTS FOR ANY EVENTS FROM THE SYSTEM INCLUDING BUT NOT LIMITED TO ANY MODIFICATION TO YOUR ORDER, ANY TRANSACTION IN YOUR ACCOUNT, ANY EXPIRY OF AN ORDER.

REGISTRAR AND SERVICE PROVIDERS MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE SUITABILITY OF THE INFORMATION AVAILABLE OR WITH RESPECT TO ITS LEGITIMACY, LEGALITY, VALIDITY, QUALITY, STABILITY, COMPLETENESS, ACCURACY OR RELIABILITY. REGISTRAR AND SERVICE PROVIDERS DO NOT ENDORSE, VERIFY OR OTHERWISE CERTIFY THE CONTENT OF ANY SUCH INFORMATION. SOME JURISDICTIONS DO NOT ALLOW THE WAIVER OF IMPLIED WARRANTIES, SO THE FOREGOING EXCLUSIONS, AS TO IMPLIED WARRANTIES, MAY NOT APPLY TO YOU.

FURTHERMORE, REGISTRAR NEITHER WARRANTS NOR MAKES ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE ORDERBOX, ORDERBOX SERVERS, REGISTRAR WEBSITE AND ANY OTHER SOFTWARE / API / SPECIFICATION / DOCUMENTATION / APPLICATION SERVICES IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.

22. JURISDICTION &amp; ATTORNEY'S FEES

This Agreement shall be governed by and interpreted and enforced in accordance with the laws of the Country, State and City where Registrar is incorporated, applicable therein without reference to rules governing choice of laws. Any action relating to this Agreement must be brought in city, state, country where Registrar is incorporated. Registrar reserves the right to enforce the law in the Country/State/District where the Registered/Corporate/Branch Office, or Place of Management/Residence of the Registrant is situated as per the laws of that Country/State/District.

If any legal action or other legal proceeding relating to the performance under this Agreement or the enforcement of any provision of this Agreement is brought against either Party hereto, the prevailing Party shall be entitled to recover reasonable attorneys' fees, costs and disbursements (in addition to any other relief to which the prevailing Party may be entitled.

For the adjudication of disputes concerning or arising from use of the Order, the Registrant shall submit, without prejudice to other potentially applicable jurisdictions, to the jurisdiction of the courts (1) of the Registrant's domicile and (2) the Registrar's country of incorporation.

23. MISCELLANEOUS

(1) Any reference in this Agreement to gender shall include all genders, and words importing the singular number only shall include the plural and vice versa.

(2) There are no representations, warranties, conditions or other agreements, express or implied, statutory or otherwise, between the Parties in connection with the subject matter of this Agreement, except as specifically set forth herein.

(3) The Parties shall attempt to resolve any disputes between them prior to resorting to litigation through mutual understanding or a mutually acceptable Arbitrator.

(4) This Agreement shall inure to the benefit of and be binding upon Registrar and the Registrant as well as all respective successors and permitted assigns.

(5) Survival: In the event of termination of this Agreement for any reason, Sections 1, 2, 4, 5, 6, 7, 10, 11, 12, 13, 14, 16, 17, 20, 21, 22, 9, 10, 11, 12, 13, 14, 16, 17, 18, 21, 22, 23, 23(3), 23(5), 23(7), 23(11), 24(2) and all of Appendix A, and all Sections of Appendix B, and Sections 1, 2, 3 of Appendix W shall survive.

(6) This Agreement does not provide and shall not be construed to provide third parties (i.e. non-parties to this Agreement), with any remedy, claim, and cause of action or privilege against Registrar.

(7) The Registrant, Registrar, its Service Providers, Registry Operator, Resellers, and Customer are independent contractors, and nothing in this Agreement will create any partnership, joint venture, agency, franchise, and sales representative or employment relationship between the parties.

(8) Further Assurances: Each Party hereto shall execute and/or cause to be delivered to the other Party hereto such instruments and other documents, and shall take such other actions, as such other Party may reasonably request for the purpose of carrying out or evidencing any of the transactions contemplated / carried out, by / as a result of, this Agreement.

(9) Construction: The Parties agree that any rule of construction to the effect that ambiguities are to be resolved against the drafting Party shall not be applied in the construction or interpretation of this Agreement.

(10) Entire Agreement; Severability: This Agreement, including all Appendices constitutes the entire agreement between the Parties concerning the subject matter hereof and supersedes any prior agreements, representations, statements, negotiations, understandings, proposals or undertakings, oral or written, with respect to the subject matter expressly set forth herein. If any provision of this Agreement shall be held to be illegal, invalid or unenforceable, each Party agrees that such provision shall be enforced to the maximum extent permissible so as to effect the intent of the Parties, and the validity, legality and enforceability of the remaining provisions of this Agreement shall not in any way be affected or impaired thereby. If necessary to effect the intent of the Parties, the Parties shall negotiate in good faith to amend this Agreement to replace the unenforceable language with enforceable language that reflects such intent as closely as possible.

(11) The division of this Agreement into Sections, Subsections, Appendices, Extensions and other Subdivisions and the insertion of headings are for convenience of reference only and shall not affect or be used in the construction or interpretation of this Agreement.

(12) This agreement may be executed in counterparts.

(13) Language. All notices, designations, and specifications made under this Agreement shall be made in the English Language only.

(14) Dates and Times. All dates and times relevant to this Agreement or its performance shall be computed based on the date and time observed in Mumbai, India (IST) i.e. GMT+5:30

24. BREACH

In the event that Registrar suspects breach of any of the terms and conditions of this Agreement:

(1) Registrar can immediately, without any notification and without assigning any reasons, suspend / terminate the Registrants access to the OrderBox Server.

(2) The Registrant will be immediately liable for any damages caused by any breach of any of the terms and conditions of this Agreement.

(3) Registrar can immediately, without any notification and without assigning any reasons, delete / suspend / terminate / freeze the Order.

25. NOTICE

(1) Any notice or other communication required or permitted to be delivered to Registrar under this Agreement shall be in writing unless otherwise specified and shall be deemed properly delivered when delivered to contact address specified on the Registrar Website by registered mail or courier. Any communication shall be deemed to have been validly and effectively given, on the date of receiving such communication, if such date is a Business Day and such delivery was made prior to 17:30 (Indian Standard Time) and otherwise on the next Business Day.

(2) Any notice or other communication required or permitted to be delivered to the Registrant under this Agreement shall be in writing unless otherwise specified and shall be deemed properly delivered, given and received when delivered to contact address of the Registrant in the OrderBox Database.

(3) Any notice or other communication to be delivered to any party via email under this agreement shall be deemed to have been properly delivered if sent in case of Registrar to its Legal Contact mentioned on the Registrar Website and in case of the Registrant to their respective email address in the OrderBox Database.

APPENDIX 'A'
TERMS AND CONDITIONS OF ORDERBOX USAGE

This Appendix A covers the terms of access to the OrderBox. Any violation of these terms will constitute a breach of agreement, and grounds for immediate termination of this Agreement.

1. ACCESS TO OrderBox

(1) Registrar may in its ABSOLUTE and UNFETTERED SOLE DISCRETION, temporarily suspend OrderBox Users' access to the OrderBox in the event of significant degradation of the OrderBox, or at any time Registrar may deem necessary.

(2) Registrar may in its ABSOLUTE and UNFETTERED SOLE DISCRETION make modifications to the OrderBox from time to time.

(3) Access to the OrderBox is controlled by authentication information provided by Registrar. Registrar is not responsible for any action in the OrderBox that takes place using this authentication information whether authorized or not.

(4) Registrar is not responsible for any action in the OrderBox by a OrderBox User.

(5) OrderBox User will not attempt to hack, crack, gain unauthorized access, misuse or engage in any practice that may hamper operations of the OrderBox including, without Limitation temporary / permanent slow down of the OrderBox, damage to data, software, operating system, applications, hardware components, network connectivity or any other hardware / software that constitute the OrderBox and architecture needed to continue operation thereof.

(6) OrderBox User will not send or cause the sending of repeated unreasonable network requests to the OrderBox or establish repeated unreasonable connections to the OrderBox. Registrar will in its ABSOLUTE and UNFETTERED SOLE DISCRETION decide what constitutes as a reasonable number of requests or connections.

(7) OrderBox User will take reasonable measures and precautions to ensure secrecy of authentication information.

(8) OrderBox User will take reasonable precautions to protect OrderBox Data from misuse, unauthorized access or disclosure, alteration, or destruction.

(9) Registrar shall not be responsible for damage caused due to the compromise of your Authentication information in any manner OR any authorized/unauthorized use of the Authentication Information.

(10) Registrar shall not be liable for any damages due to downtime or interruption of OrderBox for any duration and any cause whatsoever.

(11) Registrar shall have the right to temporarily or permanently suspend access of a OrderBox User to the OrderBox if Registrar in its ABSOLUTE and UNFETTERED SOLE DISCRETION suspects misuse of the access to the OrderBox, or learns of any possible misuse that has occurred, or will occur with respect to a OrderBox User.

(12) Registrar and Service Providers reserve the right to, in their sole discretion, reject any request, network connection, e-mail, or message, to, or passing through, OrderBox

2. Terms of USAGE OF ORDERBOX

(1) Registrant, or its contractors, employees, directors, officers, representatives, agents and affiliates and OrderBox Users, either directly or indirectly, shall not use or permit use of the OrderBox, directly or indirectly, in violation of any federal, state or local rule, regulation or law, or for any unlawful purpose, or to promote adult-oriented or "offensive" material, or related to any unsolicited bulk e-mail directly or indirectly (such as by referencing an OrderBox provided service within a spam email or as a reply back address), or related to ANY unsolicited marketing efforts offline or online, directly or indirectly, or in a manner injurious to Registrar, Registry Operator, Service Providers or their Resellers, Customers, or their reputation, including but not limited to the following:

(1) Usenet spam (off-topic, bulk posting/cross-posting, advertising in non-commercial newsgroups, etc.);

(2) posting a single article or substantially similar articles to an excessive number of newsgroups (i.e., more than 2-3) or posting of articles which are off-topic (i.e., off-topic according to the newsgroup charter or the article provokes complaints from the readers of the newsgroup for being off-topic);

(3) sending unsolicited mass e-mails (i.e., to more than 10 individuals, generally referred to as spamming) which provokes complaints from any of the recipients; or engaging in spamming from any provider;

(4) offering for sale or otherwise enabling access to software products that facilitate the sending of unsolicited e-mail or facilitate the assembling of multiple e-mail addresses ("spamware");

(5) advertising, transmitting, linking to, or otherwise making available any software, program, product, or service that is designed to violate these terms, including but not limited to the facilitation of the means to spam, initiation of pinging, flooding, mailbombing, denial of service attacks, and piracy of software;

(6) harassment of other individuals utilizing the Internet after being asked to stop by those individuals, a court, a law-enforcement agency and/or Registrar;

(7) impersonating another user or entity or an existing company/user/service or otherwise falsifying one's identity for fraudulent purposes in e-mail, Usenet postings, on IRC, or with any other Internet service, or for the purpose of directing traffic of said user or entity elsewhere;

(8) using OrderBox services to point to or otherwise direct traffic to, directly or indirectly, any material that, in the sole opinion of Registrar, is associated with spamming, bulk e-mail, e-mail harvesting, warez (or links to such material), is in violation of copyright law, or contains material judged, in the sole opinion of Registrar, to be threatening or obscene or inappropriate;

(9) using OrderBox directly or indirectly for any of the below activities activities:

(1) transmitting Unsolicited Commercial e-mail (UCE);

(2) transmitting bulk e-mail;

(3) being listed, or, in our sole opinion is about to be listed, in any Spam Blacklist or DNS Blacklist;

(4) posting bulk Usenet/newsgroup articles;

(5) Denial of Service attacks of any kind;

(6) excessive use of any web service obtained under this agreement beyond reasonable limits as determined by the Registrar in its sole discretion;

(7) copyright or trademark infringement;

(8) unlawful or illegal activities of any kind;

(9) promoting net abuse in any manner (providing software, tools or information which enables, facilitates or otherwise supports net abuse);

(10) causing lossage or creating service degradation for other users whether intentional or inadvertent.

(2) Registrar in its sole discretion will determine what constitutes as violation of appropriate usage including but not limited to all of the above.

(3) Data in the OrderBox Database cannot be used for any purpose other than those listed below, except if explicit written permission has been obtained from Registrar:

(1) to perform services contemplated under this agreement; and

(2) to communicate with Registrar on any matter pertaining to Registrar or its services.

(4) data in the OrderBox Database cannot specifically be used for any purpose listed below:

(1) Mass Mailing or SPAM; and

(2) selling the data.

APPENDIX 'B'
PAYMENT TERMS AND CONDITIONS

(1) Registrar will accept payment for the Order from the Customer or Resellers.

(2) Registrant can refer to http://webhost.mavajsunco.com/domain-registration-pricing for fee charged by the MAVAJ SUN CO for the Order. The Registrant acknowledges that the Registrar or MAVAJ SUN CO reserves the right to change the pricing without any prior notification.

(3) In the event that a payment made via Credit Card or the payment instrument sent by the Customer or Reseller bounces due to Lack of Funds or any other Reason, then

(1) Registrar may immediately suspend OrderBox Users' access to the OrderBox

(2) Registrar has the right to terminate this agreement with immediate effect and without any notice.

(4) Registrar in its ABSOLUTE and UNFETTERED SOLE DISCRETION may delete, suspend, deny, cancel, modify, take ownership of or transfer any or all of the Orders placed of the Registrant as well as stop / suspend / delete / transfer any Orders currently being processed.

(5) Registrar in its ABSOLUTE and UNFETTERED SOLE DISCRETION may Transfer all Orders placed by the Registrant to another Customer, or under Registrar's account.

(6) Registrar in its ABSOLUTE and UNFETTERED SOLE DISCRETION may levy reasonable additional charges for the processing of the Charge-back / Payment Reversal in addition to actual costs of the same.

(7) Registrar shall have the right to initiate any legal proceedings against the Registrant to recover any such liabilities.

APPENDIX 'C'
.COM/.NET/.ORG SPECIFIC CONDITIONS

If the Order is a .COM/.NET/.ORG domain name, the Registrant, must also agree to the following terms:

1. PROVISION OF REGISTRATION DATA

As part of the registration process, you are required to provide us with certain information and to update this information to keep it current, complete and accurate. This information includes:

(1) full name of an authorized contact person, company name, postal address, e-mail address, voice telephone number, and fax number if available of the Registrant;

(2) the primary nameserver and secondary nameserver(s), if any for the domain name;

(3) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the technical contact for the domain name;

(4) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the administrative contact for the domain name;

(5) the name, postal address, e-mail address, voice telephone number, and fax number if available of the billing contact for the domain name; and

2. DOMAIN NAME DISPUTE POLICY

You agree to be bound by the current Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/udrp/udrp.htm that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'D'
.BIZ SPECIFIC CONDITIONS

If the Order is a .BIZ domain name, the Registrant, must also agree to the following terms:

1. CONDITIONS FOR .BIZ REGISTRATIONS

(1) Registrations in the .BIZ TLD must be used or intended to be used primarily for bona fide business or commercial purposes. For purposes of the .BIZ Registration Restrictions ("Restrictions"), "bona fide business or commercial use" shall mean the bona fide use or bona fide intent to use the domain name or any content, software, materials, graphics or other information thereon, to permit Internet users to access one or more host computers through the DNS:

(1) to exchange goods, services, or property of any kind;

(2) in the ordinary course of trade or business; or

(3) to facilitate:

(1) the exchange of goods, services, information, or property of any kind; or

(2) the ordinary course of trade or business.

(2) Registering a domain name solely for the purposes of

(1) selling, trading or leasing the domain name for compensation, or

(2) the unsolicited offering to sell, trade or lease the domain name for compensation shall not constitute a "bona fide business or commercial use" of that domain name.

2. CERTIFICATION FOR .BIZ REGISTRATIONS

(1) As a .BIZ domain name Registrant, you hereby certify to the best of your knowledge that the registered domain name will be used primarily for bona fide business or commercial purposes and not exclusively for personal use or solely for the purposes of selling, trading or leasing the domain name for compensation, or the unsolicited offering to sell, trade or lease the domain name for compensation. For more information on the .BIZ restrictions, which are incorporated herein by reference, please see: http://www.neulevel.com/countdown/registrationRestrictions.html.

(2) The domain name Registrant has the authority to enter into the registration agreement.

(3) The registered domain name is reasonably related to the Registrant's business or intended commercial purpose at the time of registration.

3. PROVISION OF REGISTRATION DATA

As part of the registration process, you are required to provide us with certain information and to update this information to keep it current, complete and accurate. This information includes:

(1) full name of an authorized contact person, company name, postal address, e-mail address, voice telephone number, and fax number if available of the Registrant;

(2) the primary nameserver and secondary nameserver(s), if any for the domain name;

(3) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the technical contact for the domain name;

(4) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the administrative contact for the domain name;

(5) the name, postal address, e-mail address, voice telephone number, and fax number if available of the billing contact for the domain name; and

4. DOMAIN NAME DISPUTE POLICY

You agree to be bound by the dispute policies in the following documents that are incorporated herein and made a part of this Agreement by reference.

The Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/udrp/udrp.htm.

The Start-up Trademark Opposition Policy ("STOP"), available at http://www.neulevel.com/countdown/stop.html

The Restrictions Dispute Resolution Criteria and Rules, available at http://www.neulevel.com/countdown/rdrp.html.

The STOP sets forth the terms and conditions in connection with a dispute between a registrant of a .BIZ domain name with any third party (other than Registry Operator or Registrar) over the registration or use of a .BIZ domain name registered by Registrant that is subject to the Intellectual Property Claim Service. The Intellectual Property Claim Service a service introduced by Registry Operator to notify a trademark or service mark holder ("Claimant") that a second-level domain name has been registered in which that Claimant claims intellectual property rights. In accordance with the STOP and its associated Rules, those Claimants will have the right to challenge registrations through independent ICANN-accredited dispute resolution providers.

The UDRP sets forth the terms and conditions in connection with a dispute between a Registrant and any party other than the Registry Operator or Registrar over the registration and use of an Internet domain name registered by Registrant.

The RDRP sets forth the terms under which any allegation that a domain name is not used primarily for business or commercial purposes shall be enforced on a case-by-case, fact specific basis by an independent ICANN-accredited dispute provider. None of the violations of the Restrictions will be enforced directly by or through Registry Operator. Registry Operator will not review, monitor, or otherwise verify that any particular domain name is being used primarily for business or commercial purposes or that a domain name is being used in compliance with the SUDRP or UDRP processes.

APPENDIX 'E'
.INFO DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .INFO domain name, the Registrant, must also agree to the following terms:

(1) Registrant agrees to submit to proceedings under ICANN's Uniform Domain Name Dispute Policy (UDRP) as laid out at http://www.icann.org/udrp/udrp.htm and comply with the requirements set forth by Afilias for domain names registered during the Sunrise Period, including the mandatory Sunrise Dispute Resolution Policy. These policies are available at http://www.afilias.info. These policies are subject to modification.

(2) Registrant acknowledges that Afilias, the registry operator for .INFO, will have no liability of any kind for any loss or liability resulting from the proceedings and processes relating to the Sunrise Period or the Land Rush Period, including, without limitation:

(1) the ability or inability of a registrant to obtain a Registered Name during these periods, and

(2) the results of any dispute over a Sunrise Registration.

APPENDIX 'F'
.NAME SPECIFIC CONDITIONS

If the Order is a .NAME domain name, or a .NAME Email Forward, the Registrant, must also agree to the following terms:

1. .NAME REGISTRATION RESTRICTIONS

Domain Name and Email Forward Registrations in the .NAME TLD must constitute an individual's "Personal Name". For purposes of the .NAME restrictions (the "Restrictions"), a "Personal Name" is a person's legal name, or a name by which the person is commonly known. A "name by which a person is commonly known" includes, without limitation, a pseudonym used by an author or painter, or a stage name used by a singer or actor.

2. .NAME CERTIFICATIONS

As a .NAME domain name or Email Forward Registrant, you hereby certify to the best of your knowledge that the SLD is your Personal Name.

3. PROVISION OF REGISTRATION DATA

As part of the registration process, you are required to provide us with certain information and to update this information to keep it current, complete and accurate. This information includes the information contained in the Whois directory, including:

(1) full name of an authorized contact person, company name, postal address, e-mail address, voice telephone number, and fax number if available of the Registrant;

(2) the primary nameserver and secondary nameserver(s), if any for the domain name;

(3) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the technical contact for the domain name;

(4) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the administrative contact for the domain name;

(5) the name, postal address, e-mail address, voice telephone number, and fax number if available of the billing contact for the domain name; and

You further understand that the foregoing registration data may be transferred outside of the European Community, such as to the United States, and you expressly consent to such export.

4. DISPUTE POLICY

You agree to be bound by the dispute policies in the following documents that are incorporated herein and made a part of this Agreement by reference:

(1) the Eligibility Requirements (the "Eligibility Requirements"), available at http://www.icann.org/tlds/agreements/name/registry-agmt-appl-03jul01.htm;

(2) the Eligibility Requirements Dispute Resolution Policy (the "ERDRP"), available at http://www.icann.org/tlds/agreements/name/registry-agmt-appm-03jul01.htm; and

(3) the Uniform Domain Name Dispute Resolution Policy (the "UDRP"), available at http://www.icann.org/tlds/agreements/name/registry-agmt-appm-03jul01.htm

The Eligibility Requirements dictate that Personal Name domain names and Personal Name SLD email addresses will be granted on a first-come, first-served basis, except for registrations granted as a result of a dispute resolution proceeding or during the landrush procedures in connection with the opening of the Registry TLD. The following categories of Personal Name Registrations may be registered:

(1) the Personal Name of an individual;

(2) the Personal Name of a fictional character, if you have trademark or service mark rights in that character's Personal Name;

(3) in addition to a Personal Name registration, you may add numeric characters to the beginning or the end of your Personal Name so as to differentiate it from other Personal Names.

The ERDRP applies to challenges to:

(1) registered domain names and SLD email address registrations within .NAME on the grounds that a Registrant does not meet the Eligibility Requirements, and

(2) to Defensive Registrations within .NAME.

The UDRP sets forth the terms and conditions in connection with a dispute between a Registrant and party other than Global Name Registry ("Registry Operator") or Registrar over the registration and use of an Internet domain name registered by a Registrant.

5. .NAME EMAIL FORWARD ADDITIONAL CONDITIONS

If the Order is a .NAME email forward, the Registrant, must also agree to the following additional terms and conditions:

(1) You acknowledge that you are responsible for all use of Email Forwarding, including the content of messages sent through Email Forwarding.

(2) You undertake to familiarize yourself with the content of and to comply with the generally accepted rules for Internet and email usage.

(3) Without prejudice to the foregoing, you undertake not to use Email Forwarding:

(1) to encourage, allow or participate in any form of illegal or unsuitable activity, including but not restricted to the exchange of threatening, obscene or offensive messages, spreading computer viruses, breach of copyright and/or proprietary rights or publishing defamatory material;

(2) to gain illegal access to systems or networks by unauthorized access to or use of the data in systems or networks, including all attempts at guessing passwords, checking or testing the vulnerability of a system or network or breaching the security or access control without the sufficient approval of the owner of the system or network;

(3) to interrupt data traffic to other users, servers or networks, including, but not restricted to, mail bombing, flooding, Denial of Service (DoS) attacks, willful attempts to overload another system or other forms of harassment; or

(4) for spamming, which includes, but is not restricted to, the mass mailing of unsolicited email, junk mail, the use of distribution lists (mailing lists) which include persons who have not specifically given their consent to be placed on such a distribution list

(4) Users are not permitted to provide false names or in any other way to pose as somebody else when using Email Forwarding.

(5) Registry Operator reserves the right to implement additional anti-spam measures, to block spam or mail from systems with a history of abuse from entering Registry Operator's Email Forwarding.

(6) On discontinuing Email Forwarding, Registry Operator is not obliged to store any contents or to forward unsent email to you or a third party.

APPENDIX 'G'
.NAME DEFENSIVE REGISTRATIONS SPECIFIC CONDITIONS

If the Order is a .NAME Defensive Registration, the Registrant, must also agree to the following terms:

1. DEFENSIVE REGISTRATIONS

Defensive Registrations allow owners of nationally registered marks to exclusively pre-register on the .NAME space and create a protective barrier for their trademarks. A "Defensive Registration" is a registration granted to a third party of a specific string on the second or third level, or of a specific set of strings on the second and third levels, which will not resolve within the domain name system but may prevent the registration of the same string(s) on the same level(s) by other third party applicants.

2. PHASES OF DEFENSIVE REGISTRATIONS

(1) As a Defensive Registration Registrant ("Defensive Registrant"), you hereby certify to the best of your knowledge that for Phase I Defensive Registrations ("Phase I Defensive Registrants"), you own valid and enforceable trademark or service mark registrations having national effect that issued prior to December 13, 2014 for strings that are identical to the textual or word elements, using ASCII characters only, subject to the same character and formatting restrictions as apply to all registrations in the Registry TLD. You understand that trademark or service mark registrations from the supplemental or equivalent Registry of any country, or from individual states or provinces of a nation, will not be accepted. Subject to the same character and formatting restrictions as apply to all registrations in the Registry TLD, if a trademark or service mark registration incorporates design elements, the ASCII character portion of that mark may qualify to be a Phase I Defensive Registration.

(2) Phase II Defensive Registrants may apply for a Defensive Registration for any string or combination of strings.

(3) Defensive Registrants, whether Phase I or Phase II shall comply with the following Eligibility Requirements, available at http://www.icann.org/tlds/agreements/name/registry-agmt-appl-03jul01.htm, the summary of which is as follows:

(1) There are two levels of Defensive Registrations, each of which is subject to payment of a separate fee;

(2) Multiple persons or entities may obtain identical or overlapping Defensive Registrations upon payment by each of a separate registration fee;

(3) The Defensive Registrant must provide the information requested in Section 3(i) below;

(4) A Defensive Registration will not be granted if it conflicts with a then-existing Personal Name Registration or other reserved word or string.

3. PROVISION OF REGISTRATION DATA

As part of the registration process, you are required to provide us with certain information and to update this information to keep it current, complete and accurate. You must provide contact information, including name, email address, postal address and telephone number, for use in disputes relating to the Defensive Registration. You understand and agree that this contact information will be provided as part of the Whois record for the Defensive Registration. You further understand that the foregoing registration data may be transferred outside of the European Community, such as to the United States, and you expressly consent to such export.

In addition to the information provided in subsection 1. above, Phase I Defensive Registrants must also provide:

(1) the name, in ASCII characters, of the trademark or service mark being registered;

(2) the date the registration issued;

(3) the country of registration; and

(4) the registration number or other comparable identifier used by the registration authority.

4. DISPUTE POLICY

If you registered a Defensive Registration, you agree that:

(1) the Defensive Registration will be subject to challenge pursuant to the Eligibility Requirements Dispute Resolution Policy ("ERDRP");

(2) if the Defensive Registration is successfully challenged pursuant to the ERDRP, the Defensive Registrant will pay the challenge fees; and

(3) if a challenge is successful, then the Defensive Registration will be subject to the procedures described in Section 2(h) of Appendix L to the agreement of Global Name Registry ("Registry Operator") with the Internet Corporation for Assigned Names and Numbers ("ICANN"), available at http://www.icann.org/tlds/agreements/name/registry-agmt-appl-03jul01.htm;

(4) if a Phase I Defensive Registration is successfully challenged on the basis that it did not meet the applicable Eligibility Requirements, the Defensive Registrant will thereafter be required to demonstrate, at its expense, that it meets the Eligibility Requirements for Phase I Defensive Registrations for all other Phase I Defensive Registrations that it registered within .NAME through any Registrar. In the event that the Defensive Registrant is unable to demonstrate the foregoing with respect to any such Phase I Defensive Registration(s), those Defensive Registration(s) will be cancelled;

(5) The ERDRP applies to, among other things, challenges to Defensive Registrations within .NAME and is available at http://www.icann.org/tlds/agreements/name/registry-agmt-appm-03jul01.htm.

5. CONSENT

Defensive Registrants may be asked to give their consent to allow individuals to share a part of their space. For example, if you have filed a Defensive Registration on PQR (which blocks out ANYSTRING.PQR.name and PQR.ANYSTRING.name), you may be asked to give consent to John Pqr to register JOHN.PQR.name if he can prove that PQR is his name. In such a circumstance, you will have five (5) days to respond to a request for consent.

APPENDIX 'H'
.US DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .US domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) neither the registration of the domain name nor the manner in which it is directly or indirectly used infringes the legal rights of any third party;

(2) you have the requisite power and authority to enter into this Agreement and to perform the obligations hereunder;

(3) you agree that failure to abide by the usTLD Nexus Requirements Policy shall be a basis for cancellation of the domain (http://www.neustar.us/the-ustld-nexus-requirements/);

(4) you are of legal age to enter into this Agreement;

(5) you agree to comply with all . usTLD Administrator Reservation of Rights policy displayed at http://www.neustar.us/ustld-administrator-reservation-of-rights/ and the Policy Statement by usTLD Administrator displayed at http://www.neustar.us/policy-statement-by-ustld-administrator/

(6) you agree to comply with the usTLD Acceptable Use Policy displayed at http://www.neustar.us/ustld-acceptable-use-policy/

(7) you agree to comply with all Registry Operator policies regarding the use of proxy domain name services.  You further agree that if you license the use of a domain name to a third party you are nonetheless the Registered Name Holder and are responsible for providing your own full contact information and for providing and updating accurate technical and administrative contact information pursuant to the Registration Agreement and that you shall accept liability for harm caused by wrongful use of the domain.

(8) you certify that the Registered Name Holder meets the requirements set out in the usTLD Nexus Requirements Policy (http://www.neustar.us/the-ustld-nexus-requirements/) and that the Registered Name Holder is either:

(1) a citizen or permanent resident of the United States of America or any of its possessions or territories, whose primary place of domicile is in the United States of America or any of its possessions; or 

(2) a United States entity or organization that is (i) incorporated within one of the fifty (50) U.S. states, the District of Columbia, or any of the United States possessions or territories or (ii) organized or otherwise constituted under the laws of a state of the United States of America, the District of Columbia or any of its possessions or territories (including a federal, state, or local government of the United States, or a political subdivision thereof); or

(3) a foreign entity or organization that has a bona fide presence in the United States

(9) you consent to the data processing as required by the Whois Accuracy Program Specification (http://www.neustar.us/data-accuracy/) and the .US Privacy Policy (http://www.neustar.us/us-privacy-statement-v-2/).

2. PROVISION OF REGISTRATION DATA

As part of the registration process, you are required to provide us with certain information and to update this information to keep it current, complete and accurate. This information includes:

(1) full name of an authorized contact person, company name, postal address, e-mail address, voice telephone number, and fax number if available of the Registrant;

(2) the primary nameserver and secondary nameserver(s), if any for the domain name;

(3) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the technical contact for the domain name;

(4) the full name, postal address, e-mail address, voice telephone number, and fax number if available of the administrative contact for the domain name;

(5) the name, postal address, e-mail address, voice telephone number, and fax number if available of the billing contact for the domain name; 

(6) any other data NeuStar, as the Registry, requires be submitted to it, including specifically information regarding the primary purpose for which a domain name is registered (e.g., business, education, etc.); and

3. GOVERNMENT USE OF DATA

You understand and agree that the U.S. Government shall have the right to use, disclose, reproduce, prepare derivative works, distribute copies to the public, and perform publicly and display publicly, in any manner and for any purpose whatsoever and to have or permit other to do so, all Data provided by Registrant. "Data" means any recorded information, and includes without limitation, technical data and computer software, regardless of the form or the medium on which it may be recorded.

4. DOMAIN DISPUTE POLICY

You agree to submit to proceedings under Domain Dispute policies set forth by Neustar. These policies are available at http://www.neustar.us and are hereby incorporated and made an integral part of this Agreement.

5. SUSPENSION, CANCELLATION OR TRANSFER

Your registration of the domain name shall be subject to suspension, cancellation, or transfer:

(1) pursuant to any usTLD Administrator adopted specification or policy, or pursuant to any registrar or usTLD Administrator procedure not inconsistent with a usTLD Administrator adopted specification or policy; or

(2) to correct mistakes by Registrar or the usTLD Administrator in registering the name; or

(3) for the resolution of disputes concerning the domain name.

APPENDIX 'I'
.IN DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .IN domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) neither the registration of the domain name nor the manner in which it is directly or indirectly used, infringes the legal rights of any third party, breaks any applicable laws or regulations, including discrimination on the basis of race, language, sex or religion, is used in bad faith or for any unlawful purpose;

(2) your registered domain name is not contrary to public policy and the content of the website does not violate any Indian Laws.

2. DOMAIN DISPUTE POLICY

You agree to be bound by the dispute policies as decided by the .IN Registry and published at http://www.registry.in that are incorporated herein and made a part of this Agreement by reference.

APPENDIX 'J'
.EU DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .EU domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) neither the registration of the domain name nor the manner in which it is directly or indirectly used infringes the legal rights of any third party;

(2) you have the requisite power and authority to enter into this Agreement and to perform the obligations hereunder;

(3) you are registering an .eu domain name as either:

(1) an undertaking having its registered office, central administration or principal place of business within the European Union Community; or

(2) an organisation established within the EU Community without prejudice to the application of national law; or

(3) a natural person resident within the EU Community.

(4) you are of legal age to enter into this Agreement; and

(5) you agree to comply with all applicable laws, regulations and policies of the .EU Registry. The details of the same can be obtained from http://www.eurid.eu/.

2. PROVISION OF REGISTRATION DATA

As part of the registration process, you are required to provide us with certain information and to update this information to keep it current, complete and accurate. This information includes:

(1) the full name of the Registrant; where no name of a company or organisation is specified, the individual requesting registration of the Domain Name will be considered the Registrant; if the name of the company or the organisation is specified, then the company or organisation is considered the Registrant;

(2) address and country within the European Union Community:

(1) where the registered office, central administration or principal place of business of the undertaking of the Registrant is located; or

(2) where the organisation of the Registrant is established; or

(3) where the Registrant resides;

(3) e-mail address of the Registrant;

(4) the telephone number where the Registrant can be contacted.

3. DOMAIN DISPUTE POLICY

You agree to submit to proceedings under Domain Dispute policies set forth by the EU Registry. These policies are available in the EU Regulation 874/2004 at http://www.eurid.eu and are hereby incorporated and made an integral part of this Agreement.

5. SUSPENSION, CANCELLATION OR TRANSFER

Your registration of the domain name shall be subject to suspension, cancellation, or transfer:

(1) pursuant to the rules set forth by the EU Registry within the EU Regulation 874/2004 or any other policy listed at http://www.eurid.eu/; or

(2) to correct mistakes by Registrar or the EU Registry in registering the name; or

(3) for the resolution of disputes concerning the domain name.

APPENDIX 'K'
PRIVACY PROTECTION SERVICE SPECIFIC CONDITIONS

1. DESCRIPTION OF SERVICES

The Privacy Protection Service hides the contact details of the actual owner from appearing in the Whois Lookup Result of his domain name.

2. IMPLEMENTATION DETAILS

(1) Registrant acknowledges and agrees that the contact information being displayed in the Whois of a privacy protected Domain Order will be those designated by the Registrar, and

(1) any mail received via post at this Address would be rejected;

(2) any telephone call received at this Telephone Number, would be greeted with an electronic answering machine requesting the caller to email the email address listed in the Whois of this privacy protected domain name;

(3) the sender of any email to an email address listed in the Whois of this privacy protected domain name, will get an automated response email asking them to visit the URL http://www.privacyprotect.org/ to contact the Registrant, Administrative, Billing or Technical Contact of a privacy protected domain name through an online form. This message would be relayed as an email message via http://www.privacyprotect.org/ to the actual Registrant, Administrative, Billing or Technical Contact email address in the OrderBox Database.

(2) Registrant agrees that we can not guarantee delivery of messages to either the Registrant, Administrative, Billing, Technical Contact, or Customer of a privacy protected Order, and that such message may not be delivered in time or at all, for any reason whatsoever. Registrar and Service Providers disclaim any and all liability associated with non-delivery of any messages relating to the Domain Order and this service.

(3) Registrant understands that the Privacy Protection Service is only available for certain TLDs.

(4) Irrespective of whether Privacy Protection is enabled or not, Registrants are required to fulfill their obligations of providing true and accurate contact information as detailed in the Agreement.

(5) Registrant understands and acknowledges that Registrar in its sole, unfettered discretion, can discontinue providing Privacy Protection Services on the Order for any purpose, including but not limited to:

(1) when required by a valid court order;

(2) when required by the applicable registry rules or policies;

(3) pursuant to any applicable laws, government rules or requirements, requests of law enforcement agency;

(4) when the registrant fails to renew the service;

(5) when required to respond to an ICANN approved UDRP or URS service provider;

(6) when a domain name is suspended for a violation of Registrar-Registrant agreement, Acceptable usage policy, or other Terms of services applicable to the customer, domain name or whois privacy service;

(7) for any other reason that Registrar in its sole discretion deems appropriate to switch off the Privacy Protection Services.

(6) Registrant understands and acknowledges that Registrar in its sole, unfettered discretion, can DISCLOSE the underlying registrant data to a requesting party in the following circumstances:

(1) When required to comply with the applicable registry rules or policies;

(2) When required to respond to a valid subpoena or warrant;

(3) On receiving information request from a Law Enforcement Agency or any Government body authorized to act on behalf of the Law Enforcement Agency.

3. INDEMNITY

Registrant agrees to release, defend, indemnify and hold harmless Registrar, Service Providers, PrivacyProtect.org, and their parent companies, subsidiaries, affiliates, shareholders, agents, directors, officers and employees, from and against any and all claims, demands, liabilities, losses, damages or costs, including reasonable attorney's fees, arising out of or related in any way to the Privacy Protection services provided hereunder.

APPENDIX 'L'
.UK DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .UK domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) you are aware that registering a .UK domain name, involves you contracting with the Nominet which is the .UK Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.nominet.org.uk/go/terms.

(2) you agree to comply with all applicable laws, regulations and policies of Nominet available on their website at http://www.nominet.org.uk/uk-domain-names/registering-uk-domain/legal-details/terms-and-conditions-domain-name-registration.

2. DOMAIN DISPUTE POLICY

You agree to submit to proceedings under the Dispute Resolution Service Policy set forth by Nominet. These policies are available at http://www.nominet.org.uk/disputes/when-use-drs/policy-and-procedure and are hereby incorporated and made an integral part of this Agreement.

APPENDIX 'M'
.TRAVEL DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .TRAVEL domain name, the Registrant, must also agree to the following terms:

1. PROVISION OF REGISTRATION DATA

Over and above the obligations already described in this Agreement, you are required to provide us the UIN (Unique Identification Number), as issued by the .TRAVEL Registry to an entity that is eligible to hold a .travel domain name.

2. DOMAIN DISPUTE POLICY

You agree to be bound by the current .TRAVEL TLD Charter Eligibility Dispute Resolution Policy as well as the Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/udrp/ that are incorporated herein and made a part of this Agreement by reference.

APPENDIX 'N'
.WS DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .WS domain name, the Registrant, must also agree to the following terms:

1. GOVERNMENT USE OF DATA

You understand and agree that the .WS Registry shall have the right to use, disclose, reproduce, prepare derivative works, distribute copies to the public, and perform publicly and display publicly, in any manner and for any purpose whatsoever and to have or permit other to do so, all Data provided by You. "Data" means any recorded information, and includes without limitation, technical data and computer software, regardless of the form or the medium on which it may be recorded.

2. DOMAIN DISPUTE POLICY

You agree to be bound by the current Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/udrp/udrp.htm that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'O'
.COOP DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .COOP domain name, the Registrant, must also agree to:

(1) the terms and conditions of the .COOP Registration Agreement with the .COOP Sponsor DCLLC (DotCoop Limited Liability Company), available at http://www.nic.coop/media/3345/111102_-_registration_agreement.pdf; and

(2) the Verification &amp; Eligibility Policy available at http://www.nic.coop/media/1571/Verificationpolicy.pdf; and

(3) the Charter Eligibility Dispute Resolution Policy ("CEDRP") and DotCoop Domain Name Dispute Resolution Policy ("DCDRP") found at http://www.nic.coop/dispute.asp; and

(4) the Transfer Policy found at http://www.nic.coop/media/1509/DotCoop%20Policy%20on%20Transfer%20of%20Registrations%20between%20Registrars.pdf

all of the above included herein by reference.

Where there is a conflict, contradiction or inconsistency between the provisions of this Appendix (.COOP DOMAIN NAME SPECIFIC CONDITIONS) and this DOMAIN REGISTRANT AGREEMENT, the provisions of this Appendix shall prevail in respect of all .COOP domain name registrations only.

In particular we draw the following to your attention:

1. ELIGIBILITY AND PRIVACY

You agree:

(1) to meet all eligibility requirements mandated by .COOP Sponsor for registration of a .COOP name, as set forth in the .COOP Charter set out in http://www.icann.org/tlds/agreements/coop/sponsorship-agmt-att1-05nov01.htm.

(2) in the event you are found not to be entitled to register a .COOP domain name for failure to meet .COOP Sponsor eligibility requirements, that the domain name may not be registered (and, if already registered, it will be deleted). You release the .COOP Sponsor from any and all liability stemming from deletion of any domain name. Deleted .COOP names will be returned to the pool of names available for registration. The privacy statement, located on the .COOP Sponsor's Web site at http://www.nic.coop/media/5687/privacy_policy_-_120328.pdf and incorporated herein by reference sets forth your and the .COOP Sponsor's rights and responsibilities with regard to your personal information.

2. APPLICABLE POLICIES

You agree to adhere to the .COOP policies set forth on http://www.nic.coop, including but not limited to the requirement that third-and-higher-level domain names within your second level domain may only be used internally by you (absent a written license from the .COOP Sponsor).

3. DOMAIN NAME DISPUTES

You agree that, if your use of our domain name registration services is challenged by a third party, you will be subject to the provisions specified in the .COOP Sponsor's dispute policy as found at http://www.nic.coop/media/3042/.coop_dispute_policy.pdf as it may be modified at the time of the dispute. You agree that in the event a domain name dispute arises with any third party, you will indemnify and hold your .COOP Registrar and the .COOP Sponsor harmless pursuant to the terms and conditions set forth in the .COOP Domain Name Specific Conditions. If the .COOP Registrar or Sponsor are notified that a complaint has been filed with a judicial or administrative body regarding your use of our domain name registration services, you agree not to make any changes to your domain name record without prior approval. Registrar may not allow you to make changes to such domain name record until (i) Registrar is directed to do so by the judicial or administrative body, or (ii) Registrar receives notification by you and the other party contesting your registration and use of our domain name registration services that the dispute has been settled.

APPENDIX 'P'
CentralNIC DOMAIN NAME SPECIFIC CONDITIONS

If the Order is either a AE.ORG, .BAR, BR.COM, CN.COM, COM.DE, DE.COM, EU.COM, GB.COM, GB.NET, GR.COM, HU.COM, .INK, JPN.COM, KR.COM, .LA, NO.COM, QC.COM, .REST, RU.COM, SA.COM, SE.COM, SE.NET, UK.COM, UK.NET, US.COM, UY.COM, .WIKI, .XYZ or ZA.COM domain name, the Registrant, must also agree to the following terms:

1. GOVERNMENT USE OF DATA

You understand and agree that CentralNic shall have the right to use, disclose, reproduce, prepare derivative works, distribute copies to the public, and perform publicly and display publicly, in any manner and for any purpose whatsoever and to have or permit other to do so, all Data provided by Registrant. "Data" means any recorded information, and includes without limitation, technical data and computer software, regardless of the form or the medium on which it may be recorded.

2. DOMAIN DISPUTE POLICY

You agree to submit to proceedings under Domain Dispute policies set forth by CentralNic. These policies are available at http://www.centralnic.com and are hereby incorporated and made an integral part of this Agreement.

APPENDIX 'Q'
.MOBI DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .MOBI domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) you are aware that registering a .MOBI domain name, involves you contracting with mTLD which is the .MOBI Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://mtld.mobi/system/files/Registrar-Registrant+Agreement+Text+%5BJan+09+revision%5D.pdf.

(2) you agree to comply with all applicable laws, regulations and policies of mTLD available on their website at http://www.mtld.mobi/.

2. DOMAIN DISPUTE POLICY

You agree to be bound by the current Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/udrp/udrp.htm that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'R'
.ASIA DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .ASIA domain name, the Registrant, must also agree to the following terms:

1. DEFINITIONS

(1) "Charter Eligibility Declaration Contact" ("CED Contact") is a contact that is designated to make the declaration that it meets the Charter Eligibility Requirement for registering a .ASIA domain name.

(2) "Charter Eligibility Requirement" means the eligibility requirement set out in the .ASIA Charter, that the Registered Name Holder is required to comply with. The policy for such requirement, the "Charter Eligibility Requirement Policy" is stated on DotAsia's website at http://policies.registry.asia.

2. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) you are aware that registering a .ASIA domain name, involves you contracting with the .ASIA Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://policies.registry.asia.

(2) you are aware that every .ASIA domain name must specify a CED Contact, that is a legal entity or natural person in the DotAsia Community. The DotAsia Community is defined based on the geographical boundaries described by the ICANN Asia / Australia / Pacific region (http://www.icann.org/montreal/geo-regions-topic.htm).

(3) you are aware that in the event you do not have a legal entity or natural person in the DotAsia Community, the Registrar allows you to designate a Registrar-assigned CED Contact, to facilitate your .asia domain name registration.

(4) you have made known to the Charter Eligibility Declaration Contact (CED Contact), and the CED Contact has agreed, that the Registrant Contact and the CED Contact will jointly be defined as the Registered Name Holder, and that it shall be jointly responsible for the Registered Name in the event of a dispute or a challenge over the Registered Name Holder's legal entitlement to or the ownership of the Registered Name. The CED Contact shall be bound by the provisions in the DotAsia Organisation Limited's .ASIA Charter Eligibility Requirement Policy published from time to time. Registered Name Holder acting as Registrant Contact agrees that it has obtained an agreement from the CED Contact that the Registrant Contact shall remain the Operating Contact for all operations of the domain, including but not limited to domain transfer and updates.

(5) in the event of a domain name dispute both the CED Contact and the Registrant Contact can be named as the responding party, the CED Contact however is responsible only for acknowledging the dispute proceedings and to refer the case to the Registrant Contact. The Registrant Contact shall remain solely responsible for all operations and liabilities regarding the use of the domain.

3. DOMAIN DISPUTE POLICY

You agree to be bound by the current ICANN's Uniform Domain Name Dispute Resolution Policy (UDRP), available at http://www.icann.org/dndr/udrp/policy.htm and ICANN's Charter Eligibility Dispute Resolution Policy (CEDRP), available at http://www.icann.org/udrp/cedrp-policy.html, that are incorporated herein and made a part of this Agreement by reference.

APPENDIX 'S'
.ME DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .ME domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) you are aware that registering a .ME domain name, involves you contracting with the doMEn, d.o.o. Registry which is the .ME Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.domain.me/.

(2) you agree to comply with all applicable laws, regulations and policies of doMEn, d.o.o. available on their website at http://www.domain.me/.

2. DOMAIN DISPUTE POLICY

You agree to submit to proceedings under the Dispute Resolution Service Policy set forth by doMEn, d.o.o.. These policies are available at http://www.domain.me/ and are hereby incorporated and made an integral part of this Agreement.

APPENDIX 'T'
.TEL DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .TEL domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) you are aware that registering a .TEL domain name, involves you contracting with the telnic which is the .TEL Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.telnic.org/.

(2) you are aware that registering a .TEL domain name, requires you to submit atleast one communications contact such as a telephone number, an email address, an instant-messaging handle or a web link associated with you.

2. DOMAIN DISPUTE POLICY

You agree to be bound by the current Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/udrp/udrp.htm that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'U'
LIST OF TLDS REGISTRAR IS AUTHORIZED TO PROVIDE DOMAIN NAME REGISTRATION AND MANAGEMENT SERVICES

    .COM, .NET (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .ORG (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .BIZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .INFO (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .NAME and .NAME Defensive Registrations and .NAME Mail Forwards (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .US (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .IN (through Registrar Webiq Domains Solutions Pvt Ltd)
    .EU (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .UK (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .TRAVEL (through Registrar Directi Internet Solutions Pvt. Ltd. D/B/A PublicDomainRegistry.com)
    .WS (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .COOP (through Registrar Domains.coop Ltd.)
    CentralNIC (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .MOBI (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .ASIA (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .ME (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .TEL (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .MN, .BZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CC, .TV (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CN (through Registrar PDR Ltd.)
    .NZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CO (through Registrar &lt;#=dotco_serviceprovidername#&gt;)
    .CA (through Registrar PublicDomainRegistry.com Inc)
    .DE (through Registrar Directi Internet Solutions Pvt. Ltd. d/b/a PublicDomainRegistry.com)
    .ES (through Registrar Directi Internet Solutions Pvt. Ltd. d/b/a PublicDomainRegistry.com)
    .AU (through Registrar Public Domain Registry Pty Ltd.)
    .RU (through Registrar RU-Center)
    .XXX (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .PRO (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .SX (through Registrar PDR Ltd.)
    .PW (through Registrar &lt;#=dotpw_serviceprovidername#&gt;)
    .IN.NET (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
    .CO.DE (through Registrar PDR Ltd.)
    .LA (through Registrar &lt;#=centralnicdotla_serviceprovidername#&gt;)
    Donuts (through Registrar PDR Ltd.)
    .CLUB (through Registrar PDR Ltd. d/b/a
PublicDomainRegistry.com)
    .UNO (through Registrar PDR Ltd.)
    .MENU (through Registrar PDR Ltd.)
    .BUZZ (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.LONDON (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.BID (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.TRADE (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	.WEBCAM (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)
	Rightside Registry (through Registrar PDR Ltd.)
	Radix Registry (through Registrar PDR Ltd. d/b/a PublicDomainRegistry.com)




APPENDIX 'V'
.CN DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .CN domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .CN domain name, involves you contracting with the CNNIC which is the .CN Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.cnnic.cn.

2. DOMAIN DISPUTE POLICY

If the Order is a .CN domain name, the Registrant, must also agree to be bound by the current CNNIC Domain Name Dispute Resolution Policy, available at http://www.cnnic.cn/ that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'W'
.NZ DOMAIN NAME SPECIFIC CONDITIONS

Registrar and registrant are bound by the policies, at http://dnc.org.nz/policies, that are incorporated herein and made a part of this Agreement by reference.

In the case of any conflict between .NZ and this agreement, the .NZ terms apply. If the Order is a .NZ domain name the following applies:

1. REGISTER IS THE RECORD

For all purposes the details shown in the .NZ register shall be treated as correct and the authoritative record.

2. CANCELLATION OF A DOMAIN NAME

If we are going to cancel the registration of a domain name registered to you as a result of you not paying our charges relating to its renewal, we will give you fourteen days notice before we initiate action to cancel that domain name.

3. LAW AND JURISDICTION APPLYING TO THIS APPENDIX

To the extent legally permitted, you agree that:

(1) all services of the .NZ Registry are provided under New Zealand law.

(2) any claim or dispute arising out of or in connection with this agreement must be instituted within 60 days from the date the relevant service was supplied to you.

(3) except as otherwise stated, you may take action against us only in a New Zealand court.

4. CANCELLING THE AGREEMENT

We may cancel or suspend this agreement by giving you one month's notice.

5. REGISTRAR-REGISTRAR TRANSFER

The Registrant acknowledges and agrees that during the first five days after initial registration of the Order the Registrant may not be able to transfer the Order to another Registrar.

APPENDIX 'X'
.CO DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .CO domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .CO domain name, involves you contracting with the .CO Internet S.A.S which is the .CO Administrator, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.cointernet.co/.

2. LAW AND JURISDICTION

To the extent legally permitted, you agree that:

(1) all services of the .CO Registry are provided under laws of Colombia.

(2) any disputes, claims or controversies arising out of the registration, ownership, use, transfer, assignment, loss, cancellation, or suspension of any Registered Name or otherwise relating to the .CO TLD between Registrant and the .CO Registry shall be governed exclusively by the laws of Colombia and that any such disputes, claims or controversies shall be brought and heard exclusively in the courts located in Bogota, Colombia.

3. DOMAIN DISPUTE POLICY

If the Order is a .CO domain name, the Registrant acknowledges having read and understood and agrees to be bound by the terms and conditions of the Uniform Domain Name Dispute Resolution Policy adopted by ICANN, available at http://www.icann.org/en/udrp/udrp-policy-24oct99.htm (the "UDRP"), as the same may be amended from time to time and which is hereby incorporated and made an integral part of this Agreement.

APPENDIX 'Y'
.CA DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .CA domain name, the Registrant, must also agree to the terms within the .CA Registrant Agreement displayed at the time of registering a .CA domain name and while assigning a new Registrant Contact for the domain name.

Where there is a conflict, contradiction or inconsistency between the provisions of this Appendix (.CA DOMAIN NAME SPECIFIC CONDITIONS) and this DOMAIN REGISTRANT AGREEMENT, the provisions of this Appendix shall prevail in respect of all .CA domain name registrations only.

APPENDIX 'Z'
.DE DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .DE domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .DE domain name, involves you contracting with the DENIC eG (DENIC) which is the .DE Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.denic.de/en/domains.html.

2. LAW AND JURISDICTION

To the extent legally permitted, you agree that:

(1) all services of the .DE Registry are provided under laws of Germany.

(2) either the Registrant or the Administrative Contact of your .DE domain name is domiciled in Germany and would be legally able to receive German Court documents and/or summons.

(3) any disputes, claims or controversies arising out of the registration, ownership, use, transfer, assignment, loss, cancellation, or suspension of any Registered Name or otherwise relating to the .DE TLD between Registrant and the .DE Registry shall be governed exclusively by the laws of Germany and that any such disputes, claims or controversies shall be brought and heard exclusively in the courts located in Germany.

3. DOMAIN DISPUTE POLICY

If the Order is a .DE domain name, the Registrant, must also agree to be bound by the current DENIC Domain Name Dispute Resolution Policy, available at http://www.denic.de/en/domains.html that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AA'
.ES DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .ES domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .ES domain name, involves you contracting with the Red.es (ESNIC) which is the .ES Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.dominios.es/.

2. LAW AND JURISDICTION

To the extent legally permitted, you agree that:

(1) all services of the .ES Registry are provided under laws of Spain.

(2) any disputes, claims or controversies arising out of the registration, ownership, use, transfer, assignment, loss, cancellation, or suspension of any Registered Name or otherwise relating to the .ES TLD between Registrant and the .ES Registry shall be governed exclusively by the laws of Spain and that any such disputes, claims or controversies shall be brought and heard exclusively in the courts located in Spain.

3. DOMAIN DISPUTE POLICY

If the Order is a .ES domain name, the Registrant, must also agree to be bound by the current ESNIC Domain Name Dispute Resolution Policy, available at http://www.dominios.es/ that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AB'
.AU DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .AU domain name, then the following terms apply:

1. REGISTRANT REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief:

(1) you are aware that auDA (.au Domain Administration Limited, ACN 079 009 340) is the .AU Domain Names Administrator.

(2) you are aware that you must comply with all auDA Published Policies (listed at http://www.auda.org.au), as if they were incorporated into, and form part of, this agreement. In the event of any inconsistency between any auDA Published Policy and this agreement, then the auDA Published Policy will prevail to the extent of such inconsistency.

(3) you are aware that the Registrar acts as agent for auDA for the sole purpose, but only to the extent necessary, to enable auDA to receive the benefit of rights and covenants conferred to it under this agreement. auDA is an intended third party beneficiary of this agreement.

(4) all information provided to register or renew the registration of the domain name (including all supporting documents, if any) are true, complete and correct, and are not misleading in any way, and the application is made in good faith.

(5) you acknowledge that under the auDA Published Policies there are mandatory terms and conditions that apply to all domain names licences, and such terms and conditions are incorporated into, and form part of, this agreement.

(6) you meet and will continue to meet, the eligibility criteria prescribed in auDA Published Policies (http://www.auda.org.au/policy/current-policies/) for the domain name for the duration of the domain name.

(7) you have not previously submitted an application for the domain name with another Registrar using the same eligibility criteria, and the other Registrar has rejected the application.

(8) you are aware that even if the domain name is accepted for registration, the Registrant's entitlement to register the domain name may be challenged by others who claim to have an entitlement to the domain name.

(9) you are aware that auDA or the Registrar may cancel the registration of the domain name if any of the warranties set out above is found to be untrue, incomplete, incorrect or misleading.

(10) you are aware of auDA's WHOIS policy at http://www.auda.org.au/whois-policy/, which sets out auDA's guidelines on the collection, disclosure and use of WHOIS data.

2. LIABILITIES AND INDEMNIFICATION

(1) To the fullest extent permitted by law, auDA will not be liable to Registrant for any direct, indirect, consequential, special, punitive or exemplary losses or damages of any kind (including, without limitation, loss of use, loss or profit, loss or corruption of data, business interruption or indirect costs) suffered by Registrant arising from, as a result of, or otherwise in connection with, any act or omission whatsoever of auDA, its employees, agents or contractors.

(2) Registrant agrees to indemnify, keep indemnified and hold auDA, its employees, agents and contractors harmless from all and any claims or liabilities, arising from, as a result of, or otherwise in connection with, Registrant's registration or use of its .au domain name.

(3) Nothing in this document is intended to exclude the operation of Trade Practices Act 1974.

3. DOMAIN DISPUTE POLICY

You agree to be bound by the current auDRP Dispute Resolution Policy, available at http://www.auda.org.au/policy/current-policies/ that is incorporated herein and made a part of this Agreement by reference.

4. REGISTRAR SUPPORT

First level of support is available through the Registration Partner, from whom you have registered your .AU domain name. Contact details of this organization may be obtained from http://publicdomainregistry.com/support/.

If this organization is not able to provide timely assistance to the domain name owner, you may contact Registrar Public Domain Registry Pty Ltd.'s 24x7 online Support Team at http://resources.publicdomainregistry.com/compliance/.

To know more about your .AU domain name or to get in touch with the .AU Registry, refer http://www.auda.org.au/help/faq-index/.

5. REGISTRAR ADDRESS

Public Domain Registry Pty Ltd.

ACN: 141 141 988
ABN: 25 141 141 988

14, Lever Street, Albion
Brisbane, Queensland 4010
Australia

6. DOMAIN CANCELLATION POLICY

If the domain name must be cancelled for any reason after the Registrar allotted Add Grace period, the domain name registrant can do so by submitting a written application for cancellation of the domain to the Registrar.

To cancel the domain licence:

(1) Organisations or companies listed as the domain registrant must submit their written request along with the legal letterhead of that organisation.

(2)  Individuals or sole traders must submit their written request along with a copy of photo identification.

All requests must be dated, signed and may be submitted by the Registrant via email or any other medium provisioned by the Registrar.

APPENDIX 'AC'
.CC, .TV DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .CC or .TV domain name, then the following terms apply:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .CC or .TV domain name, involves you contracting with Verisign, which is the .CC/.TV Registry, and agreeing to their .CC and .TV registry policies available on their website at http://www.verisigninc.com/en_US/channel-resources/become-a-registrar/verisign-domain-registrar/domain-registration/index.xhtml and you are aware that registering a .CC, .TV domain name, requires you to agree to:

(1) grant Verisign (the .CC, .TV Registry) all necessary licenses and consents to permit Verisign or its agent(s) to:

(1) perform in Verisign's unlimited and sole discretion Malware Scans on your .CC, .TV website.

(2) collect, store, and process data gathered as a result of such Malware Scans.

(3) disclose the results of such Malware Scan (including all data therefrom) to the Registrar. Such information can not be considered as confidential or proprietary.

(4) use the results of such Malware Scan (including all data therefrom) in connection with protecting the integrity, security or stability of the Registry.

(2) disclaim any and all warranties, representations or covenants that such Malware Scan will detect any and all Malware or that Verisign is responsible for notifying the Registrar or the Registrant of any Malware or cleaning any Malware from any Registrant's systems.

2. LIABILITIES AND INDEMNIFICATION

You agree to indemnify, defend and hold harmless Verisign and its affiliates, suppliers, vendors and subcontractors, and, if applicable, any ccTLD registry operators providing services and their respective employees, directors, officers, representatives, agents and assigns ("Verisign Affected Parties") from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses, arising out of or relating to, for any reason whatsoever, any Malware Scan, the failure to conduct a Malware Scan, the failure to detect any Malware, or the use of any data from Malware Scans.

APPENDIX 'AD'
.XXX DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .XXX domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .XXX domain name, involves you contracting with the ICM Registry LLC which is the .XXX Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.icmregistry.com.

2. DOMAIN DISPUTE POLICY

You agree to be bound by the current ICANN's Uniform Domain Name Dispute Resolution Policy available at http://www.icann.org/udrp/udrp.htm, and ICM's Charter Eligibility Dispute Resolution Policy (CEDRP) and ICM's Rapid Evaluation Service (RES) available at the Registry's website, that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AE'
.RU DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .RU domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .RU domain name, involves you contracting with Registrar RU-Center, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at https://www.nic.ru/.

2. LAW AND JURISDICTION

To the extent legally permitted, you agree that all services of Registrar RU-Center are provided under laws of the Russian Federation.

APPENDIX 'AF'
.PRO DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .PRO domain name, the Registrant, must also agree to the following terms:

You are aware that registering a .PRO domain name, involves you contracting with RegistryPro, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://registry.pro/legal/user-terms

1.  INDEMNITY

You agree to hold harmless and indemnify RegistryPro and Registrar, and each of their subsidiaries, affiliates, officers, agents, and employees from and against any third party claim arising from or in any way related to your use of the Service, including any liability or expense arising from all claims, losses, damages (actual and consequential), suits, judgements, litigation costs and attorneys' fees, of every kind and nature. In such a case, Registrar will provide you with written notice of such claim, suit or action.

2.  INCORPORATION OF .PRO RESTRICTIONS AND CHALLENGE PROCESSES

You  acknowledge having read and understood and agree to be bound by the terms and conditions of the following documents, as they may be amended from time to time, which are hereby incorporated and made an integral part of this Agreement.

(A) The Uniform Domain Name Dispute Resolution Policy, available at http://www.icann.org/dndr/udrp/ policy.htm
(B) The Qualification Challenge Policy and Rules, available at http://www.icann.org/dndr/proqcp/policy.htm and http://www.icann.org/dndr/proqcp/uniform-rules.htm;
(C) The .pro TLD restriction requirements, available at http://www.registrypro.pro/qualifications.htm

You represent and warrant that, at all times during the term of domain name registration, you will meet the .pro registration requirements set forth by RegistryPro. You are required to provide prompt notice to the Registrar if you fail to meet such registration requirements. Registrar and/or Registry Operator shall have the right to immediately and without notice to you, suspend, cancel or modify a yourregistration if, at any time you fail to meet the registration requirements.

APPENDIX 'AG'
.SX DOMAIN NAME SPECIFIC CONDITIONS


1. REGISTRANT REPRESENTATIONS AND WARRANTIES

1.1 You represent and certify that, to the best of your knowledge and belief you are aware that registering a .SX domain name, involves you contracting with the SX Registry SA which is the .SX Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.registry.sx/registrars/legal

1.2 Domain Name Holders expressly acknowledge and accept that the Registry shall be entitled (but not obliged) to reject an Application or to delete or transfer a Domain Name Registration:
- that does not contain complete and accurate information as described in these Policies, or is not in compliance with any other provision of these Policies; or
- to protect the integrity and stability of the Shared Registry System, and/or the operation and/or management of the .SX TLD; or
- in order to comply with applicable laws and regulations, and/or any decision by a competent court or administrative authority and/or any dispute resolution service provider the Registry may hereafter retain to oversee the arbitration and mediation of disputes; and/or any other applicable laws, regulations, policies or decrees; or
- to avoid any liability on behalf of the Registry, including their respective affiliates, directors, officers, employees, subcontractors and/or agents; or
- following the outcome of a Sunrise Reconsideration Proceeding.

2. INDEMNIFICATION AND LIMITATION OF LIABILITY

2.1. To the extent allowed under governing law, the Registry shall only be liable in cases where willful misconduct or gross negligence is proven. In no event shall the Registry be held liable for any indirect, consequential or incidental damages or loss of profits, whether contractual, based on tort (including negligence) or otherwise arising, resulting from or related to the submission of an Application, the registration or use of a Domain Name or to the use of the Shared Registry System or Registry Web Site, even if they have been advised of the possibility of such loss or damages, including but not limited to decisions taken by the Registry to register or not to register a Domain Name on the basis of the findings of or information provided by the IP Clearinghouse Operator, as well as the consequences of those decisions.

2.2. To the extent allowed under applicable law, the Registry's aggregate liability for damages shall in any case be limited to the amounts paid by the Accredited Registrar to the Registry in relation to the Application concerned (excluding additional fees paid by the Applicant to the Accredited Registrar or reseller, auction fees and/or reconsideration fees). The Applicant agrees that no greater or other damages may be claimed from the Registry (such as, but not limited to, any fees payable or paid by the Applicant in the context of any proceedings initiated against a decision by the Registry to register or not to register a Domain Name). The Applicant further agrees to submit to a binding arbitration for
disputes arising from these Policies and related to the allocation of Domain Names.

2.3. Applicants and Domain Name Holders shall hold the Registry harmless from claims filed or disputes initiated by third parties, and shall compensate the Registry for any costs or expenses incurred or damages for which they may be held liable as a result of third parties taking action against it on the grounds that the Applications for or the registration or use of the Domain Name by the Applicant infringes the rights of a third party. Applicant agrees to indemnify, keep indemnified and hold the Registry harmless from all and any claims or liabilities, arising from, as a result of, or otherwise in connection with, Applicant's registration or use of its .sx domain name.

2.4. For the purposes of this Article, the term "Registry" shall also refer to its shareholders, directors, employees, members, subcontractors, the IP Clearinghouse Operator and their respective directors, agents, employees and subcontractors.

2.5. The Registry, its directors, employees, contractors and agents (including the IP Clearinghouse Operator and the Auction Provider) are not a party to the agreement between an Accredited Registrar and its Applicants, its Domain Name Holders or any party acting in the name and/or on behalf of such Applicants or Domain Name Holders.

3. DOMAIN DISPUTE POLICY

You agree to be bound by the Uniform Domain Dispute Resolution Policy (UDRP), available at http://www.registry.sx/registrars/legal.html that is incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AH'
.PW DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .PW domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .PW domain name, involves you contracting with the .PW Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.registry.pw/.
Furthermore, you represent and certify that, to the best of your knowledge and belief you are aware of the Domain Abuse Policy for .PW Registrants available on the website http://www.registry.pw/

2.  DOMAIN DISPUTE POLICY

You agree to be bound by the dispute policies as decided by the .PW Registry and published at http://www.registry.pw that are incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AI'
.IN.NET DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .IN.NET domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .IN.NET domain name, involves you contracting with the .IN.NET Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.domains.in.net/.
Furthermore, you represent and certify that, to the best of your knowledge and belief you are aware of the Domain Abuse Policy for .IN.NET Registrants available on the website http://www.domains.in.net/anti-abuse-policy/

2. DOMAIN DISPUTE POLICY

You agree to be bound by the dispute policies as decided by the .IN.NET Registry and published at http://www.domains.in.net/dispute-resolution-policy/ that are incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AJ'
.CO.DE DOMAIN NAME SPECIFIC CONDITIONS

You represent and certify that, to the best of your knowledge and belief you are aware that registering a .CO.DE domain name, involves you contracting with the DNNEXT Registry which is the .CO.DE Registry, and agreeing to their CO.DE REGISTRATION POLICY available on their website at http://dnnext.com/code/ 

APPENDIX 'AK'
.LA DOMAIN NAME SPECIFIC CONDITIONS

1. WHOIS ACCURACY
1.1 The Registrant shall provide to the registrar accurate and reliable contact details and promptly up date them during the term of the .LA domain registration including: full name, name of organisation, association or corporation (if applicable) postal address, email address, voice telephone number, and fax number if available; name of authorized person in the case of Registrant that is an organization association or corporation.

1.2 A Registrant's provision of inaccurate or unreliable information or its failure to promptly update information provided shall constitutes material breach of the registration agreement and shall be a basis for cancellation of the .la registered domain name.

2 REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .LA domain name, involves you contracting with the .LA Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.la/.

3. DOMAIN DISPUTE POLICY
You agree to be bound by the dispute policies as decided by the .LA Registry and published at https://www.la/e/dispute that are incorporated herein and made a part of this Agreement by reference.

APPENDIX 'AL'
DONUTS INC. SPECIFIC CONDITIONS -

If the Order is either a .BIKE, .CLOTHING, .GURU, .HOLDINGS, .PLUMBING, .SINGLES, .VENTURES, .CAMERA, .EQUIPMENT, .ESTATE, .GALLERY, .GRAPHICS, .LIGHTING, .PHOTOGRAPHY, .CONSTRUCTION, .CONTRACTORS, .DIRECTORY, .KITCHEN, .LAND, .TECHNOLOGY, .TODAY, .DIAMONDS, .ENTERPRISES, .TIPS, .VOYAGE, .CAREERS, .PHOTOS, .RECIPES, .SHOES, .CAB, .COMPANY, .DOMAINS, .LIMO, .ACADEMY, .CENTER, .COMPUTER, .MANAGEMENT, .SYSTEMS, .BUILDERS, .EMAIL, .SOLUTIONS, .SUPPORT, .TRAINING, .CAMP, .EDUCATION, .GLASS, .INSTITUTE, .REPAIR, .COFFEE, .FLORIST, .HOUSE, .INTERNATIONAL, .SOLAR, .HOLIDAY, .MARKETING, .CODES, .FARM, .VIAJES, .AGENCY, .BARGAINS, .BOUTIQUE, .CHEAP, .ZONE, .COOL, .WATCH, .WORKS, .EXPERT, .FOUNDATION, .EXPOSED, .CRUISES, .FLIGHTS, .RENTALS, .VACATIONS, .VILLAS, .TIENDA, .CONDOS, .PROPERTIES, .MAISON, .DATING, .EVENTS, .PARTNERS, .PRODUCTIONS, .COMMUNITY, .CATERING, .CARDS, .CLEANING, .TOOLS, .INDUSTRIES, .PARTS, .SUPPLIES, .SUPPLY, .FISH, .REPORT, .VISION, .SERVICES, .CAPITAL, .ENGINEERING, .EXCHANGE, .GRIPE, .ASSOCIATES, .LEASE, .MEDIA, .PICTURES, .REISEN, .TOYS, .UNIVERSITY, .TOWN, .WTF, .FAIL, .FINANCIAL, .LIMITED, .CARE, .CLINIC, .SURGERY, .DENTAL, .TAX, .CASH, .FUND, .INVESTMENTS, .FURNITURE, .DISCOUNT, .FITNESS, .SCHULE, .GRATIS, .CLAIMS, .CREDIT, .CREDITCARD, .DIGITAL, .ACCOUNTANTS, .FINANCE, .INSURE, .LOANS, .CHURCH, .LIFE, .GUIDE, .DIRECT, .PLACE, .DEALS, .CITY, .HEALTHCARE, .RESTAURANT OR .GIFTS domain name, the Registrant, must also agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a DONUTS INC domain name, involves you contracting with the DONUTS INC Registry, and agreeing to their Policies of Domain Name Registration available on their website at http://www.donuts.co/policies/

2. HANDLING OF PERSONAL DATA
Donuts shall handle Personal Data submitted to Donuts by Registrar in accordance with its published privacy policy located at the Registry Website under Policies (the Privacy Policy). Donuts will provide sixty (60) days prior written notice to Registrar of any changes to the Privacy Policy. Donuts may from time to time use data submitted by Registrar for statistical analysis, provided that any such analysis will not disclose individual non-public Personal Data and such non-public Personal Data is only used for internal business purposes. Donuts will not share, sell, rent or otherwise disclose such non-public Personal Data to any third parties.

3. INDEMNIFICATION
Registrant agrees to (within thirty days of demand) indemnify, defend and hold harmless the Registry Operator, Donuts service providers, Registrar and it's respective affiliates and subsidiaries, as well as each of it's respective owners, directors, managers, officers, employees, contractors, service providers and agents from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses (including on appeal), arising out of or relating in any way to the Registrant's domain name registration, including, without limitation, the use, registration, extension, renewal, deletion, and/or transfer thereof and/or the violation of any applicable terms or conditions governing the registration. Registrant shall not enter into any settlement or compromise of any such indemnifiable claim without Registrars prior written consent, which consent shall not be unreasonably withheld and that this indemnification obligation shall survive the termination or expiration of the Registration Agreement for any reason.

4. Domain Dispute Policy
For disputes relating to the use of domain names, Registrant agrees to be bound and confirm to ICANN's Uniform Rapid Suspension System or Uniform Domain Name Dispute Resolution Policy, both as applied and amended at http://newgtlds.icann.org/en/applicants/urs and http://www.icann.org/en/help/dndr/udrp, respectively.

APPENDIX 'AM'
.CLUB DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .CLUB domain name, the Registrant agrees to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .CLUB domain name, involves you contracting with the .CLUB Registry, and agreeing to their Policies of Domain Name Registration available on their website at http://nic.club/Terms/

2. DOMAIN NAME REGISTRATION AGREEMENT
If the Order is a .CLUB domain name, the Registrant, must also agree to the following terms: (a) acknowledge and agree that Registry reserves the right to deny, cancel or transfer any registration or transaction, or place any domain name(s) on registry lock, hold or similar status, as it deems necessary, in its unlimited and sole discretion: (i) to comply with specifications adopted by any industry group generally recognized as authoritative with respect to the Internet (e.g., RFCs), (ii) to correct mistakes made by Registry or any Registrar in connection with a domain name registration, or (iii) for the non-payment of fees to Registry. (b) .CLUB domain name (s) shall not be used for distributing malware, abusively operating botnets, phishing, piracy, trademark or copyright infringement, fraudulent or deceptive practices, counterfeiting or otherwise engaging in activity contrary to applicable law.

3. INDEMNIFICATION
The REGISTERED NAME HOLDER indemnify, defend and hold harmless the Registry Operator and Registry Service Provider and their subcontractors, subsidiaries, affiliates, divisions, shareholders, directors, officers, employees, accountants, attorneys, insurers, agents, predecessors, successors and assigns, from and against any and all claims, demands, damages, losses, costs, expenses, causes of action or other liabilities of any kind, whether known or unknown, including reasonable legal and attorney's fees and expenses, in any way arising out of, relating to, or otherwise in connection with the Registered Name Holder's domain name registration. The registration agreement shall further require that this indemnification obligation survive the termination or expiration of the registration agreement.

4. Domain Dispute Policy
For disputes relating to the use of domain names, Registrant agrees to be bound and confirm to ICANN's Uniform Rapid Suspension System or Uniform Domain Name Dispute Resolution Policy, both as applied and amended at http://newgtlds.icann.org/en/applicants/urs and http://www.icann.org/en/help/dndr/udrp, respectively.

APPENDIX 'AN'
.UNO DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .UNO domain name, the Registrant agrees to the following terms:

1. REPRESENTATIONS AND WARRANTIES.
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .UNO  domain name, involves you contracting with the .UN ORegistry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://unodominio.com/.
Furthermore, you represent and certify that, to the best of your knowledge and belief you are aware of the Domain Abuse Policy for .UNO Registrants available on the website http://www.unodominio.com/policy/Acceptable-Use-and-Anti-Abuse-Policy.pdf

2. DOMAIN NAME REGISTRATION AGREEMENT
If the Order is a .UNO domain name, the Registrant, must also agree to the following terms: (a) acknowledge and agree that Registry reserves the right to deny, cancel or transfer any registration or transaction, or place any domain name(s) on registry lock, hold or similar status, as it deems necessary, in its unlimited and sole discretion: (i) to comply with specifications adopted by any industry group generally recognized as authoritative with respect to the Internet (e.g., RFCs), (ii) to correct mistakes made by Registry or any Registrar in connection with a domain name registration, or (iii) for the non-payment of fees to Registry. (b) comply with Registry's Acceptable Use policies and Terms of Service, if any, as they may be instituted or updated from time to time and published on the Registry website specific to the Registry TLD for the Registered Name. (c) .UNO domain name (s) shall not be used for distributing malware, abusively operating botnets, phishing, piracy, trademark or copyright infringement, fraudulent or deceptive practices, counterfeiting or otherwise engaging in activity contrary to applicable law.

3. INDEMNIFICATION
Registrant agrees to (within thirty days of demand) indemnify, defend and hold harmless the Registry Operator and Registry Service Provider and their subcontractors, subsidiaries, affiliates, divisions, shareholders, directors, officers, employees, accountants, attorneys, insurers, agents, predecessors, successors and assigns, from and against any and all claims, demands, damages, losses, costs, expenses, causes of action or other liabilities of any kind, whether known or unknown, including reasonable legal and attorney's fees and expenses, in any way arising out of, relating to, or otherwise in connection with the Registered Name Holder's domain name registration.  The registration agreement shall further require that this indemnification obligation survive the termination or expiration of the registration agreement.

4. Domain Dispute Policy
For disputes relating to the use of domain names, Registrant agrees to be bound and confirm to ICANN's Uniform Rapid Suspension System or Uniform Domain Name Dispute Resolution Policy, both as applied and amended at http://newgtlds.icann.org/en/applicants/urs and http://www.icann.org/en/help/dndr/udrp, respectively.

APPENDIX 'AO'
.MENU DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .MENU domain name, the Registrant agrees to the following terms:

1. REPRESENTATIONS AND WARRANTIES.
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .MENU  domain name, involves you contracting with the .MENU Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.dot-menu.com/

2. INDEMNIFICATION
Registrant agrees to indemnify, defend and hold harmless Registry Operator, and its subcontractors, directors, officers, employees, affiliates and agents of each of them from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses, arising out of or relating to the Registered Name Holder's domain name registration. The registration agreement shall further require this indemnification obligation survive the termination or expiration of the registration agreement.

3. DOMAIN NAME REGISTRATION AGREEMENT
The Registrant must acknowledge and agree that Registry Operator reserves the right to deny, cancel or transfer any registration or transaction, or place any domain name(s) on registry lock, hold, suspension or similar status, that it deems necessary, in its discretion: (1) to protect the integrity and stability of the registry; (2) to comply with any applicable laws, government rules or requirements, requests of law enforcement, or any dispute resolution process; (3) to comply with any applicable ICANN rules or regulations, including without limitation, the Registry Agreement; (4) to avoid any liability, civil or criminal, on the part of Registry Operator, as well as its affiliates, subsidiaries, officers, directors, and employees; (5) per the terms of the registration agreement; (6) following an occurrence of any of the prohibited activities described in Subsections 3.7.6 above; or (7) to correct mistakes made by Registry Operator or any Registrar in connection with a domain name registration. Registry Operator also reserves the right to place upon registry lock, hold or similar status a domain name during resolution of a dispute; Registry Operator will provide Registrar notice of any cancelation, transfers or changes made to any registration by Registry Operator not initiated by the Registrar.

4. HANDLING OF PERSONAL DATA
The Registrant provides consent to the use, copying, distribution, publication, modification and other processing of the Registered Name Holder's Personal Data by Registry Operator and its designees and agents in a manner consistent with the purposes specified herein, current ICANN policies, and with relevant mandatory local data protection, laws and privacy; also, to the collection and use of Personal Data by Registry Operator, in conformity with the terms of this Agreement and the Registry Agreement, and applicable law;

5. DOMAIN DISPUTE POLICY
For disputes relating to the use of domain names, Registrant agrees to be bound and confirm to ICANN's Uniform Rapid Suspension System or Uniform Domain Name Dispute Resolution Policy, both as applied and amended at http://newgtlds.icann.org/en/applicants/urs and http://www.icann.org/en/help/dndr/udrp, respectively.

6. THIRD PARTY BENEFICIARY
The Registrant agrees to the following provision: "Notwithstanding anything in this Agreement to the contrary, Wedding TLD2, LLC, the Registry Operator of the .MENU TLD, is and shall be an intended third party beneficiary of this Agreement. As such the parties to this agreement acknowledge and agree that the third party beneficiary rights of Wedding TLD2, LLC have vested and that Wedding TLD2, LLC has relied on its third party beneficiary rights under this Agreement in agreeing to the Registrar being a registrar for the .MENU TLD. Additionally, the third party beneficiary  rights of Wedding TLD2, LLC shall survive any termination of this Agreement."

APPENDIX 'AP'
.BUZZ DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .BUZZ domain name, then the following terms apply:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .BUZZ domain name, involves you contracting with the .BUZZ Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.buzznames.biz/

2. DOMAIN NAME REGISTRATION AGREEMENT
The Registrant must  (a) acknowledge and agree that Registry reserves the right to deny, cancel or transfer any registration or transaction, or place any domain name(s) on registry lock, hold or similar status, as it deems necessary, in its unlimited and sole discretion: (i) to comply with specifications adopted by any industry group generally recognized as authoritative with respect to the Internet (e.g., RFCs), (ii) to correct mistakes made by Registry or any Registrar in connection with a domain name registration, or (iii) for the non-payment of fees to Registry.

3. INDEMNIFICATION
The Registrant must agree to indemnify, defend and hold harmless the Registry Operator and Registry Service Provider and their subcontractors, subsidiaries, affiliates, divisions, shareholders, directors, officers, employees, accountants, attorneys, insurers, agents, predecessors, successors and assigns, from and against any and all claims, demands, damages, losses, costs, expenses, causes of action or other liabilities of any kind, whether known or unknown, including reasonable legal and attorney's fees and expenses, in any way arising out of, relating to, or otherwise in connection with the Registered Name Holder's domain name registration. The registration agreement shall further require that this indemnification obligation survive the termination or expiration of the registration agreement.

4. DOMAIN DISPUTE POLICY
For disputes relating to the use of domain names, Registrant agrees to be bound and confirm to ICANN's Uniform Rapid Suspension System or Uniform Domain Name Dispute Resolution Policy, both as applied and amended at http://newgtlds.icann.org/en/applicants/urs and http://www.icann.org/en/help/dndr/udrp, respectively.

APPENDIX 'AQ'
.LONDON DOMAIN NAME SPECIFIC CONDITIONS

If the Order is a .LONDON domain name, then the following terms apply:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a .LONDON domain name, involves you contracting with the .LONDON Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.dotlondondomains.london/terms-conditions/

2. DOMAIN NAME REGISTRATION AGREEMENT
The Registrant acknowledges and agrees that the Registry reserves the right to deny, cancel or transfer any registration or transaction, or place any domain name(s) on registry lock, hold, or similar status as it deems necessary, in its unlimited and sole discretion: (1) to comply with specifications adopted by any industry group generally recognized as authoritative with respect to the Internet (e.g. RFCs), (2) to correct mistakes made by the Registry or any Registrar in connection with a domain name registration, or (3) if required by a URS or UDRP, proceeding; (4) under the terms of the Registry Policies; (4) for the non-payment of fees to the Registry

3. INDEMNIFICATION
The Registrant agrees to indemnify, defend and hold harmless the Registry and its subcontractors, and its and their directors, officers, employees, agents, and affiliates from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses arising out of or relating to, for any reason whatsoever, the Registered Name Holder’s domain name registration. The Registrar’s registration agreement shall further require that this indemnification obligation survive the termination or expiration of the registration agreement

4. OPERATIONAL REQUIREMENTS
The Registered Name Holder complies with (i) ICANN standards, policies, procedures, and practices for which the Registry has monitoring responsibility in accordance with the Registry Agreement or other arrangement with ICANN; and (ii) Operational standards, policies, procedures, and practices for the Registry TLD established from time to time by the Registry in a non-arbitrary manner and applicable to all Registrars (“Operational Requirements”), including affiliates of the Registry, and consistent with the Registry’s Registry Agreement with ICANN, as applicable, upon the Registry’s notification to the Registrar of the establishment of those terms and conditions. Unless shorter notice is deemed necessary by the Registry in exceptional circumstances, additional or revised Operational Requirements shall be effective upon ninety (90) days notice by the Registry to the Registrar

APPENDIX 'AR'
FAMOUS FOUR MEDIA LIMITED SPECIFIC CONDITIONS -

If the Order is either a .BID, .TRADE or .WEBCAM domain name, the Registrant, must agree to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a FAMOUS FOUR MEDIA LIMITED domain name, involves you contracting with the FAMOUS FOUR MEDIA LIMITED Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://www.famousfourmedia.com/about-us/terms-conditions/

2. INDEMNIFICATION
Registrant agrees to indemnify, defend and hold harmless the Registry Operator, and its subcontractors (including the registry back-end services provider) and their respective directors, officers, employees, affiliates and agents of each of them from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses, arising out of or relating to the Registered Name Holder's domain name registration, except due to Registry Operator's negligence, error or omission. The Registered Name Holder shall further agree this indemnification obligation survive the termination or expiration of the registration agreement.

3. INDEMNIFICATION
The Registrant agrees to indemnify, defend and hold harmless the Registry and its subcontractors, and its and their directors, officers, employees, agents, and affiliates from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses arising out of or relating to, for any reason whatsoever, the Registered Name Holder’s domain name registration. The Registrar’s registration agreement shall further require that this indemnification obligation survive the termination or expiration of the registration agreement

APPENDIX 'AS'
Rightside Registry SPECIFIC CONDITIONS -

If the Order is either a .IMMOBILIEN, .NINJA, .FUTBOL, .REVIEWS, .SOCIAL, .PUB, .MODA, .KAUFEN, .CONSULTING, .DEMOCRAT or .DANCE domain name, the Registrant, agrees to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a Rightside Registry domain name, involves you contracting with the FAMOUS FOUR MEDIA LIMITED Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://rightside.co/legal/terms-conditions/

2. INDEMNIFICATION
Registrant agrees to (within thirty (30) days of demand) indemnify, defend and hold harmless the Registry (by express reference), Registrar and their respective affiliates and subsidiaries, as well as each of their respective owners, directors, managers, officers, employees, contractors, service providers and agents from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses (including on appeal), arising out of or relating in any way to the Registrant's domain name registration, including, without limitation, the use, registration, extension, renewal, deletion, and/or transfer thereof and/or the violation of any applicable terms or conditions governing the registration. The Registrant shall not enter into any settlement or compromise of any such indemnifiable claim without Registrar’s or the Registry’s prior written consent, which consent shall not be unreasonably withheld and that this indemnification obligation shall survive the termination or expiration of the Registration Agreement for any reason.

3. DOMAIN NAME REGISTRATION AGREEMENT
The Registrant acknowledges and agrees to the specific terms and conditions which are found on the Registry website under Terms &amp; Conditions. Additionally, the Registrant obliges to provide accurate contact information as well as comply with all applicable rules and laws including those that relate to privacy, data collection, consumer protection, import/export of services and disclosure of data. Moreover, the Registrant should acknowledge that the Registry reserves the right to deny, cancel or transfer any registration or transaction, or place any Registered Name(s) on registry lock, hold or similar status, that it deems necessary, in its discretion (1) to protect the integrity and stability of the Registry TLD registries or the Registry System; (2) to comply with any applicable laws, government rules or requirements, requests of law enforcement, or any dispute resolution process; (3) to avoid any liability, civil or criminal, on the part of the Registry, as well as its affiliates, subsidiaries, officers, directors, and employees; (4) for violations of this Agreement, including, without limitation, any exhibits, attachments, or schedules hereto; or (5) to correct mistakes made by the Registry or any Registrar in connection with a Registered Name registration. The Registry also reserves the right to place a Registered Name on registry hold, registry lock, or similar status during resolution of a dispute.

4. DOMAIN DISPUTE POLICY
For disputes relating to the use of domain names, Registrant agrees to be bound and confirm to ICANN's Uniform Rapid Suspension System or Uniform Domain Name Dispute Resolution Policy, both as applied and amended at http://newgtlds.icann.org/en/applicants/urs and http://www.icann.org/en/help/dndr/udrp, respectively.

APPENDIX 'AT'
RADIX SPECIFIC CONDITIONS -

If the Order is either a .PRESS, .HOST or .WEBSITE domain name, the Registrant agrees to the following terms:

1. REPRESENTATIONS AND WARRANTIES
You represent and certify that, to the best of your knowledge and belief you are aware that registering a RADIX domain name, involves you contracting with the RADIX Registry, and agreeing to their Terms and Conditions of Domain Name Registration available on their website at http://radixregistry.com/policies/

2. DOMAIN NAME REGISTRATION AGREEMENT
By registering a RADIX domain name, the Registrant/Registered Name Holder:
(a) acknowledge and agree that RO reserves the absolute right to deny, cancel, delete or transfer any registration or transaction, or place any domain name(s) on registry lock, hold or similar status, as it deems necessary, in its unlimited and sole discretion: (1) to comply with specifications adopted by any industry group generally recognized as authoritative with respect to the Internet (e.g., RFCs), (2) to correct mistakes made by RO or any registrar in connection with a domain name registration, (3) for the non-payment of fees to RO, (4) to protect the integrity and stability of the Registry System; (5) to comply with any applicable laws, government rules or requirements, requests of law enforcement, or any dispute resolution process; (6) to avoid any liability, civil or criminal, on the part of RO , as well as its affiliates, subsidiaries, officers, directors, and employees. 
(b) comply with all applicable laws including those that relate to privacy, data collection, consumer protection (including in relation to misleading and deceptive conduct) and applicable consumer laws in respect of fair lending, debt collection, organic farming (if applicable), disclosure of data and financial regulations.
(c) acknowledge and agree that registrants who collect and maintain sensitive health and financial data must implement reasonable and appropriate security measures commensurate with the offering of those services, as defined by applicable law. 
(d) warrant that no domain name registration within any Included TLD shall be used to distribute malware, abusively operating botnets, phishing, piracy, trademark or copyright infringement, fraudulent or deceptive practices, counterfeiting or other similar activity and providing consequences for such activities including suspension of the domain name.
(e) comply with all operational standards, procedures, practices and policies for the Included TLD including the Radix Acceptable Use and Anti-Abuse Policy (“AUP”) and all other applicable policies which will be available on the Radix website (www.radixregistry.com), established from time to time by RO in a non-arbitrary manner and applicable to all registrars, including affiliates of RO, and consistent with ICANN’s standards policies, procedures, and practices and RO’s Registry Agreement with ICANN for the Included TLD. Additional or revised RO operational standards, policies, procedures, and practices for the Included TLD shall be effective upon ninety (90) days notice by RO to Registrar unless mandated by ICANN with a shorter notice period.
(f) consent to the use, copying, distribution, publication, modification and other processing of Registrant's Personal Data by RO and its designees and agents, including data escrow requirements, or as specified by ICANN from time to time for new gTLDs.
(g) expressly agree that registration and renewal fees for some domain names in an Included TLD are variable and shall differ from registration and renewal fees for other domain names within that Included TLD. This includes but is not limited to non-standard pricing for Premium Domain Name registration and renewal fees, which differs from the pricing of Standard Domain Names.
(h) agree that registration, renewal and transfers fees for each Included TLD are variable.
(i) be bound by the terms and conditions of the initial launch of the Included TLD, including without limitation the sunrise period and the landrush period, the procedure and process for compliance with ICANN’s rights protection mechanisms including the Trademark Clearing House requirements and any Sunrise Dispute Resolution Policy, and further to acknowledge that RO and/or its service providers have no liability of any kind for any loss or liability resulting from the proceedings and processes relating to the sunrise period or the landrush period, including, without limitation: (a) the ability or inability of a Registrant to obtain a domain name during these periods, and (b) the results of any dispute over a sunrise registration.
(j) indemnify, defend and hold harmless RO, RO’s Registry Service Provider and its subcontractors, and its and their directors, officers, employees, agents, and affiliates from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses arising out of or relating in any way, for any reason whatsoever, to the Registered Name Holder's domain name registration, any breach of the Registration Agreement with Registrar and any use of the domain name. The Registration Agreement shall further require that this indemnification obligation survive the termination or expiration of the Registration Agreement and this Agreement.

3. NON-UNIFORM RENEWAL REGISTRATION PRICING
The Registrant agrees that the Included TLDs will have non-uniform renewal registration pricing such that the Registration Fee for a domain name registration renewal may differ from other domain names in the same or other Included TLDs (e.g., renewal registration Fee is $7 for one domain name and $13 for a different domain name).

4. OPERATIONAL REQUIREMENTS
The Registered Name Holder is obliged to comply with each of the following requirements:
(a) ICANN standards, policies, procedures, and practices for which RO has monitoring responsibility in accordance with the Registry Agreement or other arrangement with ICANN; and
(b) Operational standards, policies, procedures, and practices for the Included TLD established from time to time by RO in a non-arbitrary manner and applicable to all registrars ("Operational Requirements"), including affiliates of RO, and consistent with RO's Registry Agreement with ICANN, as applicable, upon RO's notification to Registrar of the establishment of those terms and conditions.
                </textarea>
            </td>
        </tr>

        <tr>
            <td align="center">
                <input type="button" name="back_button" value="Back" class="frmButton" onclick="javascript:history.go(-1);">
            </td>
        </tr>

    </tbody></table>
<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>