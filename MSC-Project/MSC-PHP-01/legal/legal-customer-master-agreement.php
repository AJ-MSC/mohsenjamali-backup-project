<?php

include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<table class="dataTable" cellspacing="0" width="100%" align="center" border="0">
    <tbody><tr>
            <td><div class="ui-heading">Legal Agreements</div></td>
        </tr>
        <tr align="">
            <td align="">
                <textarea rows="26" name="newcontent" cols="145" readonly="">CUSTOMER MASTER AGREEMENT

This Customer Master Agreement is made, entered into and executed on 13 December, 2014 (hereinafter referred to as the "Effective Date")

BETWEEN:-

MAVAJ SUN CO (hereinafter referred to as "Parent") AND you (hereinafter referred to as "Customer"). If you are entering into this Agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to these terms and conditions, in which case the term "Customer" shall refer to such entity.

(The Parent and the Customer may be referred to individually as a "Party" and collectively as the "Parties").

WHEREAS the Parent provides various Products and Services;

AND WHEREAS the Customer wishes to purchase Parent's Products and Services

NOW, THEREFORE, for and in consideration of the mutual promises, benefits and covenants contained herein and for other good and valuable consideration, the receipt, adequacy and sufficiency of which are hereby acknowledged, Parent and the Customer, intending to be legally bound, hereby agree as follows:

1. DEFINITIONS

(1) "Advance Account" refers to the credit balance maintained by the Customer with the Parent.

(2) "Agreement" refers to this Customer Master Agreement alongwith all its appendices, extensions and amendments at any given point in time.

(3) "Business Day" refers to a working day between Mondays to Friday excluding all Public Holidays.

(4) "Clear Balance" refers to credit in the Customer Advance Account after deducting any accrued liabilities, Locked Funds and debited amounts.

(5) "Confidential Information", as used in this Agreement shall mean all data, information and materials including, without limitation, computer software, data, information, databases, protocols, reference implementation, documentation, functional and interface specifications, provided by Parent to the Customer under this Agreement, whether written, transmitted, oral, through the Parent Website or otherwise, that is marked as Confidential.

(6) "Customer Contact Details" refers to the Contact Details of the Customer as listed in the OrderBox Database.

(7) "Customer Control Panel" refers to the set of Web-based interfaces provided by the Parent and its Service Providers to the Customer which allows him to Manage Orders.

(8) "Customer Product Agreement Extension" refers to the latest version of a Specific Customer Product Agreement Extension as posted in the Customer Control Panel or on the Parent Website.

(9) "OrderBox" refers to the set of Servers, Software, Interfaces, Parent Products and API that is provided for use directly or indirectly under this Agreement by the Parent and/or its Service Providers.

(10) "OrderBox Database" is the collection of data elements stored on the OrderBox Servers.

(11) "OrderBox Servers" refer to Machines / Servers that Parent or its Service Providers maintain to fulfill services and operations of the OrderBox.

(12) "OrderBox User" refers to the Customer and any Agent, Employee, Contractee of the Customer or any other Legal Entity, that has been provided access to the "OrderBox" by the Customer, directly or indirectly.

(13) "Order" refers to a Parent Product purchased by the Customer having a unique Order ID in the OrderBox Database.

(14) "Parent Products" refer to all Products and Services of Parent which it has provided/rendered/sold, or is providing/rendering/selling.

(15) "Parent Servers" refer to web servers, Mailing List Servers, Database Servers, OrderBox Servers and any other Machines / Servers that Parent or its Service Providers Operate, for the OrderBox, the Parent Website, the Parent Mailing Lists, Parent Products and any other operations required to fulfill services and operations of Parent.

(16) "Parent Website" refers to mavajsunco.com.

(17) "Service Providers" refers individually and collectively to any Artificial Juridical Persons, Company, Concern, Corporation, Enterprise, Firm, Individual, Institute, Institution, Organization, Person, Society, Trust or any other Legal Entity that Parent or its Service Providers (recursively) may, directly or indirectly, Engage / Employ / Outsource / Contract for the fulfillment / provision / purchase of Parent Products, OrderBox, and any other services and operations of Parent.

(18) "Prohibited Persons refers to individuals, organizations or entities located in certain sanctioned countries (each a "Sanctioned Country") and certain individuals, organizations, entities, or domain names, including without limitation, "Specially Designated Nationals" ("SDN"), as listed by the government of the United States of America through the Department of the Treasury's Office of Foreign Assets Control ("OFAC"), with whom all or certain commercial activities are prohibited.

2. CUSTOMER PRODUCT AGREEMENT EXTENSIONS

(1) The Customer may purchase various Parent Products in the course of their relationship with Parent under this Agreement, by submitting to Parent, in a form and manner prescribed by Parent, one or more Customer Product Agreement Extensions, which shall then be included as a part of this Agreement.

(2) Any conflicting definitions, terms and conditions in a Customer Product Agreement Extension shall take precedence over the same definition, terms and conditions in this Agreement, and shall be applied only to that Customer Product Agreement Extension.

(3) The Customer agrees to adhere to the SiteLock Terms and Conditions, available at https://www.sitelock.com/terms.php, that are incorporated herein and made a part of this Agreement by reference.

(4) The Customer agrees to adhere to the CodeGuard Terms and Conditions, available at https://codeguard.com/pages/terms-of-service, that are incorporated herein and made a part of this Agreement by reference.

3. OBLIGATIONS OF PARENT

Parent shall make available the latest versions of this Agreement and Customer Product Agreement Extensions in the Customer Control Panel or on the Parent Website.

4. OBLIGATIONS OF THE CUSTOMER

(1) The Customer acknowledges that in the event of any dispute and/or discrepancy concerning any data element of an Order or the Customer in the OrderBox Database, the data element in the OrderBox Database records shall prevail.

(2) The Customer acknowledges that all information of the Customer in the OrderBox, including authentication information is accessible to Parent and its Service Providers

(3) The Customer shall comply with all terms or conditions established by Parent and/or its Service Providers from time to time.

(4) The Customer agree to provide, maintain and update, current, complete and accurate information for all the data elements about the Customer in the OrderBox Database.

(5) Customer acknowledges that Parent Products maybe obtained through Service Providers, and as such, changes in structure, or contracts may occur, and as a result services may be adversely affected. Customer acknowledges and agrees that Parent shall not have any liability associated with any such.

(6) During the term of this Agreement and for three years thereafter, the Customer shall maintain the following records relating to its dealings with Parent and their Agents or Authorized Representatives:-

(1) In electronic, paper or microfilm form, all written communications with respect to Parent Products.

(2) In electronic form, records of the accounts of all, current / past Orders with the Customer, including dates and amounts of all payments, discount, credits and refunds.

The Customer shall make these records available for inspection by Parent upon reasonable notice not exceeding 14 days.

(7) Customer shall not transact with or act on behalf of any Prohibited Person. If Customer is a Prohibited Person, Customer is prohibited from registering or signing up with, subscribing to, or using any Parent Product, or participating in the Customer program. Any violation of this provision ("OFAC Provision") as determined in Parent's sole discretion, may result in the suspension and/or termination of the Customer account and the termination of this Agreement without a refund or compensation of any kind to Customer.

5. REPRESENTATIONS AND WARRANTIES

Parent and Customer represent and warrant that:-

(1) they have all requisite power and authority to execute, deliver and perform their obligations under this Agreement;

(2) This Agreement has been duly and validly executed and delivered and constitutes a legal, valid and binding obligation, enforceable against the Customer and Parent in accordance with its terms;

(3) The execution, delivery, and performance of this Agreement and the consummation by Parent and the Customer of the transactions contemplated hereby will not, with or without the giving of notice, the lapse of time, or both, conflict with or violate:-

(1) any provision of law, rule, or regulation;

(2) any order, judgment, or decree;

(3) any provision of corporate by-laws or other documents; or

(4) any agreement or other instrument.

(4) the execution, performance and delivery of this Agreement has been duly authorized by the Customer and Parent;

(5) No consent, approval, or authorization of, or exemption by, or filing with, any governmental authority or any third party is required to be obtained or made in connection with the execution, delivery, and performance of this Agreement or the taking of any other action contemplated hereby;

The Customer represents and warrants that:

(1) the Customer has read and understood every clause of this Agreement

(2) the Customer has independently evaluated the desirability of the service and is not relying on any representation agreement, guarantee or statement other than as set forth in this agreement

(3) the Customer is not a Prohibited Person and is not acting on behalf of a Prohibited Person; and

(4) the Customer is eligible, to enter into this Contract according to the laws of the Customer's country

6. RIGHTS OF PARENT AND SERVICE PROVIDERS

(1) Parent and Service Providers may change any information, including Authentication Information of the Customer in the OrderBox Database upon receiving authorization from the Customer in any form as maybe prescribed by Parent from time to time.

(2) Parent and Service Providers may provide/send any information in the OrderBox Database, about the Customer, including Authentication information

(1) to the Customer Contact Details

(2) to any authorised representative, agent, contractee, employee of the Customer upon receiving authorization in any form as maybe prescribed by Parent from time to time

(3) to the Service Providers

(3) Parent and Service Providers in its own discretion can at any point of time temporarily or permanently cease to sell a Parent Product

(4) Parent reserves the right to change pricing, minimum order levels, and discounts,  of any Parent Product , at any time.

(5) Parent and Service Providers, in their sole discretion, expressly reserve the right to deny any Order or cancel an Order within 30 days of processing the same. In such case Parent may refund the fees charged for the Order, after deducting any processing charges for the same.

(6) Parent and Service Providers, in their sole discretion, without notice, expressly reserve the right to modify, upgrade, freeze the OrderBox, and its associated Services.

(7) Notwithstanding anything to the contrary, Parent and Service Providers, in their sole discretion, expressly reserve the right to without notice or refund, access, delete, suspend, deny, cancel, modify, intercept and analyze traffic of, copy, backup, access data of, redirect, log usage of, monitor, limit access to, limit access of, take ownership of or transfer any Order, or to delete, suspend, freeze, modify OrderBox Users' access to OrderBox, or to modify, upgrade, suspend, freeze OrderBox, or to publish, transmit, share data in the OrderBox Database with any person or entity, or to contact any entity in the OrderBox Database, in order to recover any Payment from the Customer for any service rendered by the Parent including services rendered outside the scope of this agreement for which the Customer has been notified and requested to remit payment, or to correct mistakes made by Parent or its Service Providers in processing or executing an Order, or in the case of any breach or violation or threatened breach or violation of this Agreement, or incase Parent learns of a possibility of breach or violation of this Agreement which Parent in its sole discretion determines to be appropriate, or incase of Termination of this Agreement, or if Parent learns of any such event which Parent reasonably determines would lead to Termination of this Agreement or would constitute as Breach thereof, or to protect the integrity and stability of the Parent Products and the OrderBox, or to comply with any applicable laws, government rules or requirements, requests of law enforcement, or in compliance with any dispute resolution process, or in compliance with any agreements executed by Parent, or to avoid any liability, civil or criminal, on the part of Parent and/or Service Providers, as well as their affiliates, subsidiaries, officers, directors and employees, or if the Customer and/or its Agents or any other authorised representatives of the Customer violate any applicable laws/government rules/usage policies, including but not limited to, intellectual property, copyright, patent, or Parent learns of the possibility of any such violation, or authorisation from the Customer in any manner that Parent deems satisfactory, or for any appropriate reason. The Customer agrees that Parent and Service Providers, and the contractors, employees, directors, officers, representatives, agents and affiliates, of Parent and Service Providers, are not liable for loss or damages that may result from any of the above.

(8) Incase of Orders involving web services, Parent and Service Providers can choose to redirect any Order to any IP Address including, without limitation, to an IP address which hosts a parking page or a commercial search engine for the purpose of monetization, if an Order has expired, or is suspended, or does not contain valid information to direct it to any destination. Customer acknowledges that Parent and Service Providers cannot and do not check to see whether such a redirection, infringes any legal rights including but not limited to intellectual property rights, privacy rights, trademark rights, of Customer, or that the content displayed due to such redirection is inappropriate, or in violation of any federal, state or local rule, regulation or law, or injurious to Customer or any third party, or their reputation and as such is not responsible for any damages caused directly or indirectly as a result of such redirection.

(9) Parent has the right to rectify any mistakes in the data in the OrderBox Database with retrospective effect.

(10) Parent and Service Providers reserve the right to prohibit the use of any of their services in connection with any Country-Code Top Level Domain Name ("ccTLD") of any Sanctioned Country.

(11) Parent and Service Providers expressly reserve the right to suspend or terminate Customer's account, without prior notice and without issuing a refund or compensation of any kind, if Parent or Service Provider determines in its sole discretion, that Customer has violated the OFAC Provision in Section 4. Parent and Service Provider shall not be liable for any loss or damages resulting from such action whether such loss or damage is incurred by the Customer, or a third party. Parent will not directly or indirectly refund any amounts to any Prohibited Person, including without limitation, any amounts in a Customer's Advance Account. 

7. TERM OF AGREEMENT AND RENEWAL

The term of this Agreement shall be 1 (ONE) YEAR from the Effective Date and will automatically renew for successive 1 (ONE) YEAR Renewal Term (hereinafter referred to each a "Renewal Term" and cumulatively the "Term"). The Term shall continue until the earlier to occur of the following:

(1) the Agreement is terminated as provided for in Section 8 (TERMINATION OF AGREEMENT); and

(2) The Customer elects not to renew at the end of the Initial Term or any Renewal Term.

8. TERMINATION OF AGREEMENT

(1) Either Party may terminate this Agreement and/or any Customer Product Agreement Extension at any time by

(1) giving a 30 (Thirty) days notice of termination delivered as per Section 26 (NOTICE).

(2) With immediate effect, if the other Party is adjudged insolvent or bankrupt, or if proceedings are instituted by or against a Party seeking relief, reorganization or arrangement or compromise or settlement under any laws relating to insolvency, or seeking any assignment for the benefit of creditors, or seeking the appointment of a receiver, liquidator or trustee of a Party's property or assets or the liquidation, dissolution or winding up of a Party's Business.

(2) Parent may Terminate this Agreement and/or any Customer Product Agreement Extension by notifying the Customer in writing, as of the date specified in such notice of termination under the following circumstances

(1) In the event that the Customer or an Agent / Employee / Authorized Representative of the Customer materially breaches any term of this Agreement and/or any Customer Product Agreement Extension, including any of its representations, warranties, covenants and agreements hereunder

(2) There was a material misrepresentation and/or material inaccuracy, and/or materially misleading statement in Customer's Application to Parent and/or any material accompanying the application.

(3) With immediate effect if :-

(1) the Customer is convicted of a felony or other serious offense related to financial activities, or is judged by a court to have committed fraud or breach of fiduciary duty, or is the subject of a judicial determination that Parent reasonably deems as the substantive equivalent of any of these; or

(2) the Customer is disciplined by the government of its domicile for conduct involving dishonesty or misuse of funds of others.

(3) as provided for in Appendix 'A' and Appendix 'C'

(4) if Any officer or director of the Customer is convicted of a felony or of a misdemeanor related to financial activities, or is judged by a court to have committed fraud or breach of fiduciary duty, or is the subject of a judicial determination that Parent deems as the substantive equivalent of any of these;

(3) Customer may Terminate this Agreement and/or any Customer Product Agreement Extension by notifying Parent in writing, as of the date of receipt of such notice, in the event that the Customer does not agree with any revision to the Agreement or any Customer Product Agreement Extension made as per Section 14 (RIGHT TO SUBSTITUTE UPDATED AGREEMENT AND Customer Product Agreement EXTENSIONS) within 30 days of such revision.

(4) Any Product Agreement Extension shall terminate with immediate effect in the event that

(1) Parent ceases to sell the particular Parent Product covered under that Product Agreement Extension

(2) Parents contract with Service Provider for  the particular Parent Product terminates or expires without renewal

(5) Effect of Termination of this Agreement

(1) Parent shall suspend all OrderBox Users' access to the OrderBox, Parent Servers and all Parent Products and Services, under this agreement and all Customer Product Agreement Extensions, immediately upon receiving Termination notice from the Customer or upon learning of any event, which Parent reasonably determines, would lead to Termination of the Agreement.

(2) Upon expiration or termination of this Agreement, all Customer Product Agreement Extensions signed by the Customer shall deemed to have been Terminated with immediate effect

(3) Upon expiration or termination of this Agreement, Parent may complete the processing of all Orders requested to be processed, in the order that they were requested to be processed, by the Customer prior to the date of such expiration or termination, provided that the Customer's Advance Account with Parent has Clear Balance sufficient to carry out these Orders. If Parent is unable to fulfill these Orders then the charges levied to the Customer for these Orders will be reversed

(6) Effect of Termination of any Customer Product Agreement Extension

(1) Parent may suspend OrderBox Users' access to applicable Parent Products and Services , and the OrderBox immediately upon receiving Termination notice from the Customer or upon learning of any event, which Parent reasonably determines, would lead to Termination of any Customer Product Agreement Extension

(2) Upon expiration or termination of any Customer Product Agreement Extension, Parent may complete the processing of all Orders, of that Parent Product, in the order that they were requested to be processed, by the Customer prior to the date of such expiration or termination, provided that Parent is in a position to fulfill these Orders, and the Customer's Advance Account with Parent has Clear Balance sufficient to carry out these Orders. If Parent is unable to fulfill these Orders then the charges levied to the Customer for these Orders will be reversed

(3) Parent may transfer all Orders falling under the purview of the specific Customer Product Agreement Extension to another Customer or Parent.

(7) Any pending balance due from the Customer at the time of termination of this Agreement or any Customer Product Agreement Extension will be immediately payable.

(8) Neither Party shall be liable to the other for damages of any sort resulting solely from terminating this Agreement or any Customer Product Agreement Extension in accordance with its terms, unless specified otherwise.The Customer however shall be liable for any damage arising from any breach by it of this Agreement or any Customer Product Agreement Extension.

9. FEES / RENEWAL

(1) Customer shall pay all applicable fees/advances as per the Payment Terms and Conditions set out in Appendix 'C'

(2) Parent will charge a non-refundable fee for an Order unless stated otherwise in any Product Agreement Extension. The applicable fees will be displayed in the Customer Control Panel or on the Parent Website and during the Ordering Process. Parent has the right to revise this pricing at anytime. Any such revision or change will be binding and effective immediately on posting of the revision in the Customer Control Panel or on the Parent Website or on notification to the Customer via email to the Customer.

(3) Customer acknowledges that it is the Customer's responsibility to keep records and maintain reminders regarding the expiry of any Order. As a convenience to the Customer, and not as a binding commitment, we may notify the Customer of any expiring Orders, via an email message  sent to the contact information associated with the Customer in the OrderBox database. Should renewal fees go unpaid for an Order, the Order will expire.



(4) Customer acknowledges that after expiration of the term of an Order, Customer has no rights on such Order, or any information associated with such Order, and that ownership of such Order now passes on to Parent. Parent and Service Providers may make any modifications to said Order or any information associated with said Order. Parent and Service Providers may intercept any network/communication requests to such Order and process them in any manner in their sole discretion. Parent and Service Providers may choose to monetize such requests in any fashion at their sole discretion. Parent and Service Providers may choose to display any appropriate message, and/or send any response to any user making a network/communication request, for or concerning said Order. Parent and Service Providers may choose to delete said Order at anytime after expiry upon their sole discretion. Parent and Service Providers may choose to transfer the ownership of the Order to any third party in their sole discretion. Customer acknowledges that Parent and Service Providers shall not liable to Customer or any third party for any action performed under this clause.

(5) Parent at its sole discretion may allow the renewal of the Order after Order expiry, and such renewal term will start as on the date of expiry of the Order, unless otherwise specified. Such process may be charged separately. Such renewal after the expiry of the Order may not result in exact reinstatement of the Order in the same form as it was prior to expiry.

(6) Parent makes no guarantees about the number of days, after deletion of an Order, after which the same Order will once again become available for purchase.

10. LIMITATION OF LIABILITY

IN NO EVENT WILL PARENT OR SERVICE PROVIDERS OR CONTRACTORS OR THIRD PARTY BENEFICIARIES BE LIABLE TO THE CUSTOMER FOR ANY LOSS OF REGISTRATION AND USE OF DOMAIN NAME, OR FOR INTERRUPTIONS OF BUSINESS, OR ANY SPECIAL, INDIRECT, ANCILLARY, INCIDENTAL, PUNITIVE, EXEMPLARY OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES RESULTING FROM LOSS OF PROFITS, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, REGARDLESS OF THE FORM OF ACTION WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), OR OTHERWISE, EVEN IF PARENT AND/OR ITS SERVICE PROVIDERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

PARENT FURTHER DISCLAIMS ANY AND ALL LOSS OR LIABILITY RESULTING FROM, BUT NOT LIMITED TO:

(1) LOSS OR LIABILITY RESULTING FROM THE UNAUTHORIZED USE OR MISUSE OF AUTHENTICATION INFORMATION;

(2) LOSS OR LIABILITY RESULTING FROM FORCE MAJEURE EVENTS;

(3) LOSS OR LIABILITY RESULTING FROM ACCESS DELAYS OR ACCESS INTERRUPTIONS;

(4) LOSS OR LIABILITY RESULTING FROM NON-DELIVERY OF DATA OR DATA MISS-DELIVERY;

(5) LOSS OR LIABILITY RESULTING FROM ERRORS, OMISSIONS, OR MISSTATEMENTS IN ANY AND ALL INFORMATION OR PARENT PRODUCT(S) PROVIDED UNDER THIS AGREEMENT;

(6) LOSS OR LIABILITY RESULTING FROM THE INTERRUPTION OF SERVICE.

If any legal action or other legal proceeding (including arbitration) relating to the performance under this Agreement or the enforcement of any provision of this Agreement is brought against Parent by the Customer, then in no event will the liability of Parent exceed actual amount paid to Parent by the Customer for the Order in question minus direct expenses incurred with respect to that Order.

BOTH PARTIES ACKNOWLEDGE THAT THE CONSIDERATION AGREED UPON BY THE PARTIES IS BASED IN PART UPON THESE LIMITATIONS, AND THAT THESE LIMITATIONS WILL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY REMEDY. IN NO EVENT WILL THE LIABILITY OF THE PARENT RELATING TO THIS AGREEMENT EXCEED TOTAL AMOUNT PAID TO PARENT BY THE CUSTOMER DURING THE MOST RECENT THREE (3) MONTH PERIOD PRECEDING THE EVENTS GIVING RISE TO SUCH LIABILITY.

11. INDEMNIFICATION

(1) The Customer, at their own expense, will indemnify, defend and hold harmless, Parent, Service Providers, and the contactors, employees, directors, officers, representatives, agents and affiliates, of Parent, and Service Providers, against any claim, suit, action, or other proceeding brought against Parent or Service Providers based on or arising from any claim or alleged claim, of third parties relating to or arising under this Agreement, Parent Products provided hereunder or use of the Parent Products, including without limitation:-

(1) infringement by either the Customer, or someone else using a Parent Product with the Customer's computer, of any intellectual property or other proprietary right of any person or entity

(2) arising out of any breach by the Customer of this Agreement.

(3) relating to or arising out of any Order or use of any Order

(4) relating to any action of Parent as permitted by this Agreement

(5) relating to any action of Parent carried out on behalf of Customer as described in this Agreement

(2) Parent will not enter into any settlement or compromise of any such indemnifiable claim without Customer's prior written consent, which shall not be unreasonably withheld.

(3) The Customer will pay any and all costs, damages, and expenses, including, but not limited to, actual attorneys' fees and costs awarded against or otherwise incurred by Parent in connection with or arising from any such indemnifiable claim, suit, action or proceeding.

12. INTELLECTUAL PROPERTY

Subject to the provisions of this Agreement, each Party will continue to independently own his/her/its intellectual property, including all patents, trademarks, trade names, domain names, service marks, copyrights, trade secrets, proprietary processes and all other forms of intellectual property. Any improvements to existing intellectual property will continue to be owned by the Party already holding such intellectual property.

Without limiting the generality of the foregoing, no commercial use rights or any licenses under any patent, patent application, copyright, trademark, know-how, trade secret, or any other intellectual proprietary rights are granted by Parent to the Customer, or by any disclosure of any Confidential Information to the Customer under this Agreement.

Customer shall further ensure that the Customer does not infringe any intellectual property rights or other rights of any person or entity, or does not publish any content that is libelous or illegal while using services under this Agreement. Customer acknowledges that Parent cannot and does not check to see whether any services or the use of the services by the Customer under this Agreement, infringes legal rights of others.

13. OWNERSHIP AND USE OF DATA

(1) Customer agrees and acknowledges that Parent owns all data, compilation, collective and similar rights, title and interests worldwide in the OrderBox Database, and all information and derivative works generated from the OrderBox Database.

(2) Parent and Service Providers and their designees/agents have the right to backup, copy, publish, disclose, use, sell, modify, process this data in any form and manner as maybe required for compliance of any agreements executed by Parent or Service Providers, or in order to fulfill services under this Agreement, or for any other appropriate reason.

14. DELAYS OR OMISSIONS; WAIVERS

No failure on the part of any Party to exercise any power, right, privilege or remedy under this Agreement, and no delay on the part of any Party in exercising any power, right, privilege or remedy under this Agreement, shall operate as a waiver of such power, right, privilege or remedy; and no single or partial exercise or waiver of any such power, right, privilege or remedy shall preclude any other or further exercise thereof or of any other power, right, privilege or remedy.

No Party shall be deemed to have waived any claim arising out of this Agreement, or any power, right, privilege or remedy under this Agreement, unless the waiver of such claim, power, right, privilege or remedy is expressly set forth in a written instrument duly executed and delivered on behalf of such Party; and any such waiver shall not be applicable or have any effect except in the specific instance in which it is given.

No waiver of any of the provisions of this Agreement shall be deemed to constitute a waiver of any other provision (whether or not similar), nor shall such waiver constitute a waiver or continuing waiver unless otherwise expressly provided in writing duly executed and delivered.

15. RIGHT TO SUBSTITUTE UPDATED AGREEMENT

(1) During the period of this Agreement, the Customer agrees that Parent may:-

(1) revise the terms and conditions of this Agreement; and

(2) change the services provided under this Agreement

(2) Any such revision or change will be binding and effective immediately on posting of the revision in the Customer Control Panel or on the Parent Website

(3) The Customer agrees to review the Customer Control Panel and Parent Website including the agreements, periodically, to be aware of any such revisions

(4) If the Customer does not agree with any revision, the Customer may terminate this Agreement according to Section 8(3) of this Agreement

(5) The Customer agrees that, continuing use of the services under this Agreement following notice of any revision, will constitute as an acceptance of any such revisions or changes

(6) The Customer shall execute, in a form and manner prescribed by Parent, a supplementary agreement incorporating the amendments to or revisions of the Agreement and/or Customer Product Agreement Extension

(7) The length of the term of the substituted agreement will be calculated as if it is commenced on the date the original Agreement began and the original Agreement will be deemed terminated.

(8) It will be the Customer's responsibility to communicate any changes in the agreement and any obligations/duties covered by these changes to the Customer's Agents / Employees / Authorised Representatives.

16. CONFIDENTIALITY

All Confidential Information shall be governed by the Confidentiality Agreement as attached in Appendix 'B'.

17. PUBLICITY

The Customer shall not create, publish, distribute, or permit any written / Oral / electronic material that makes reference to us or our Service Providers or uses any of Parent's registered Trademarks / Service Marks or our Service Providers' registered Trademarks / Service Marks without first submitting such material to us and our Service Providers and receiving prior written consent.

The Customer gives Parent the right to recommend / suggest the Customer's name and details to Customers / Visitors to the Parent Website, and Prospective Customers and use the Customer's name in marketing / promotional material with regards to Parent Products.

18. TAXES

The Customer shall be responsible for sales tax, consumption tax, transfer duty, custom duty, octroi duty, excise duty, income tax, and all other taxes and duties, whether international, national, state or local, however designated, which are levied or imposed or may be levied or imposed, with respect to this Agreement and the Parent Products.

19. FORCE MAJEURE

Neither party shall be liable to the other for any loss or damage resulting from any cause beyond its reasonable control (a "Force Majeure Event") including, but not limited to, insurrection or civil disorder, riot, war or military operations, national or local emergency, acts or directives or omissions of government or other competent authority, compliance with any statutory obligation or executive order, strike, lock-out, work stoppage, industrial disputes of any kind (whether or not involving either party's employees), any Act of God, fire, lightning, explosion, flood, earthquake, eruption of volcano, storm, subsidence, weather of exceptional severity, equipment or facilities breakages / shortages which are being experienced by providers of telecommunications services generally, or other similar force beyond such Party's reasonable control, and acts or omissions of persons for whom neither party is responsible. Upon occurrence of a Force Majeure Event and to the extent such occurrence interferes with either party's performance of this Agreement, such party shall be excused from performance of its obligations (other than payment obligations) during the first three months of such interference, provided that such party uses best efforts to avoid or remove such causes of non performance as soon as possible.

20. ASSIGNMENT / SUBLICENSE

Except as otherwise expressly provided herein, the provisions of this Agreement shall inure to the benefit of and be binding upon, the successors and assigns of the Parties. The Customer shall not assign, sublicense or transfer its rights or obligations under this Agreement to any third person(s)/party without the prior written consent of the Parent.

21. CUSTOMER - CUSTOMER TRANSFER

(1) Parent may transfer the Order of the Customer to another Person, Organisation or any other Legal entity under the following circumstances:-

(1) Authorization from the Customer and/or their Agent or Authorized Representative in a manner prescribed by Parent from time to time;

(2) On receiving orders from a competent Court, Law Enforcement Agency, or recognized Regulatory body;

(3) Breach of Contract;

(4) Termination of this Agreement;

(5) Parent learns of any such event, which Parent reasonably determines would lead to Termination of this Agreement, or would constitute as Breach thereof.

(2) In the above circumstances the Customer shall extend full cooperation to Parent in transferring the Order of the Customer.

22. DISCLAIMER

THE ORDERBOX, PARENT SERVERS AND ANY OTHER SOFTWARE / API / SPECIFICATION / DOCUMENTATION / APPLICATION SERVICES IS PROVIDED ON "AS IS" AND "WHERE IS" BASIS AND WITHOUT ANY WARRANTY OF ANY KIND.

PARENT AND SERVICE PROVIDERS EXPRESSLY DISCLAIM ALL WARRANTIES AND / OR CONDITIONS, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY OR SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS AND QUALITY/AVAILABILITY OF TECHNICAL SUPPORT.

PARENT AND SERVICE PROVIDERS ASSUME NO RESPONSIBILITY AND SHALL NOT BE LIABLE FOR ANY DAMAGES TO, OR VIRUSES THAT MAY AFFECT, YOUR COMPUTER EQUIPMENT OR OTHER PROPERTY IN CONNECTION WITH YOUR ACCESS TO, USE OF, ORDERBOX OR BY ACCESSING PARENT SERVERS. WITHOUT LIMITING THE FOREGOING, PARENT AND SERVICE PROVIDERS DO NOT REPRESENT, WARRANT OR GUARANTEE THAT (A) ANY INFORMATION/DATA/DOWNLOAD AVAILABLE ON OR THROUGH ORDERBOX OR PARENT SERVERS WILL BE FREE OF INFECTION BY VIRUSES, WORMS, TROJAN HORSES OR ANYTHING ELSE MANIFESTING DESTRUCTIVE PROPERTIES; OR (B) THE INFORMATION AVAILABLE ON OR THROUGH THE ORDERBOX/PARENT SERVERS WILL NOT CONTAIN ADULT-ORIENTED MATERIAL OR MATERIAL WHICH SOME INDIVIDUALS MAY DEEM OBJECTIONABLE; OR (C) THE FUNCTIONS OR SERVICES PERFORMED BY PARENT AND SERVICE PROVIDERS WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR THAT DEFECTS IN THE ORDERBOX WILL BE CORRECTED; OR (D) THE SERVICE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS OR (E) THE SERVICES PROVIDED UNDER THIS AGREEMENT OPERATE IN COMBINATION WITH ANY SPECIFIC HARDWARE, SOFTWARE, SYSTEM OR DATA. OR (F) YOU WILL RECEIVE NOTIFICATIONS, REMINDERS OR ALERTS FOR ANY EVENTS FROM THE SYSTEM INCLUDING BUT NOT LIMITED TO ANY MODIFICATION TO YOUR ORDER, ANY TRANSACTION IN YOUR ACCOUNT, ANY EXPIRY OF AN ORDER

PARENT AND SERVICE PROVIDERS MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE SUITABILITY OF THE INFORMATION AVAILABLE OR WITH RESPECT TO ITS LEGITIMACY, LEGALITY, VALIDITY, QUALITY, STABILITY, COMPLETENESS, ACCURACY OR RELIABILITY. PARENT AND SERVICE PROVIDERS DO NOT ENDORSE, VERIFY OR OTHERWISE CERTIFY THE CONTENT OF ANY SUCH INFORMATION. SOME JURISDICTIONS DO NOT ALLOW THE WAIVER OF IMPLIED WARRANTIES, SO THE FOREGOING EXCLUSIONS, AS TO IMPLIED WARRANTIES, MAY NOT APPLY TO YOU.

FURTHERMORE, PARENT NEITHER WARRANTS NOR MAKES ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE ORDERBOX, ORDERBOX SERVERS, PARENT WEBSITE AND ANY OTHER SOFTWARE / API / SPECIFICATION / DOCUMENTATION / APPLICATION SERVICES IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.

23. JURISDICTION &amp; ATTORNEY'S FEES

This Agreement shall be governed by and interpreted and enforced in accordance with the laws of the Country, State and City where Parent is incorporated, applicable therein without reference to rules governing choice of laws. Any action relating to this Agreement must be brought in a court in the city, state, country where Parent is incorporated. Parent reserves the right to enforce the law in the Country/State/District where the Registered/Corporate/Branch Office, or Place of Management of the Customer is situated as per the laws of that Country/State/District.

If any legal action or other legal proceeding relating to the performance under this Agreement or the enforcement of any provision of this Agreement is brought against either Party hereto, the prevailing Party shall be entitled to recover reasonable attorneys' fees, costs and disbursements (in addition to any other relief to which the prevailing Party may be entitled.

24. MISCELLANEOUS

(1) Any reference in this Agreement to gender shall include all genders, and words importing the singular number only shall include the plural and vice versa.

(2) There are no representations, warranties, conditions or other agreements, express or implied, statutory or otherwise, between the Parties in connection with the subject matter of this Agreement, except as specifically set forth herein.

(3) The Parties shall attempt to resolve any disputes between them prior to resorting to litigation through mutual understanding or a mutually acceptable Arbitrator.

(4) This Agreement shall inure to the benefit of and be binding upon Parent and the Customer as well as all respective successors and permitted assigns.

(5) Survival: In the event of termination of this Agreement for any reason, Sections 1, 4, 6, 8(5), 8(6), 8(7), 8(8), 9, 10, 11, 12, 13, 14, 16, 17, 18, 21, 22, 23, 24(3), 24(5), 24(7), 24(11), 25(2) and all Sections of Appendix A, and all Sections of Appendix B, and all Sections of Appendix C and any Sections covered separately under a Survival clause in any Customer Product Agreement Extension shall survive..

(6) This Agreement does not provide and shall not be construed to provide third parties (i.e. non-parties to this Agreement), with any remedy, claim, and cause of action or privilege against Parent.

(7) The Customer, Parent, and its Service Providers are independent contractors, and nothing in this Agreement will create any partnership, joint venture, agency, franchise, and sales representative or employment relationship between the parties.

(8) Further Assurances: Each Party hereto shall execute and/or cause to be delivered to the other Party hereto such instruments and other documents, and shall take such other actions, as such other Party may reasonably request for the purpose of carrying out or evidencing any of the transactions contemplated / carried out, by / as a result of, this Agreement.

(9) Construction: The Parties agree that any rule of construction to the effect that ambiguities are to be resolved against the drafting Party shall not be applied in the construction or interpretation of this Agreement.

(10) Entire Agreement; Severability: This Agreement, which includes Appendix A, Appendix B, Appendix C and each executed Customer Product Agreement Extension constitutes the entire agreement between the Parties concerning the subject matter hereof and supersedes any prior agreements, representations, statements, negotiations, understandings, proposals or undertakings, oral or written, with respect to the subject matter expressly set forth herein. If any provision of this Agreement shall be held to be illegal, invalid or unenforceable, each Party agrees that such provision shall be enforced to the maximum extent permissible so as to effect the intent of the Parties, and the validity, legality and enforceability of the remaining provisions of this Agreement shall not in any way be affected or impaired thereby. If necessary to effect the intent of the Parties, the Parties shall negotiate in good faith to amend this Agreement to replace the unenforceable language with enforceable language that reflects such intent as closely as possible.

(11) The division of this Agreement into Sections, Subsections, Appendices, Extensions and other Subdivisions and the insertion of headings are for convenience of reference only and shall not affect or be used in the construction or interpretation of this Agreement.

(12) This agreement may be executed in counterparts.

(13) Language. All notices, designations, and specifications made under this Agreement shall be made in the English Language only.

(14) Dates and Times. All dates and times relevant to this Agreement or its performance shall be computed based on the date and time observed in the city of the Registered office of the Parent

25. BREACH

In the event that Parent suspects breach of any of the terms and conditions of this Agreement:

(1) Parent can immediately, without any notification and without assigning any reasons, suspend / terminate the OrderBox Users' access to all Parent Products and Services and the OrderBox.

(2) The Customer will be immediately liable for any damages caused by any breach of any of the terms and conditions of this Agreement.

26. NOTICE

(1) Any notice or other communication required or permitted to be delivered to Parent under this Agreement shall be in writing unless otherwise specified and shall be deemed properly delivered, when sent to Parent's contact address specified in the Customer Control Panel or on the Parent Website by registered mail or courier. Any communication shall be deemed to have been validly and effectively given, on the date of receiving such communication, if such date is a Business Day and such delivery was made prior to 17:30 hours local time, and otherwise on the next Business Day.

(2) Any notice or other communication to be delivered to Parent via email under this agreement shall be deemed to have been properly delivered if sent to its Legal Contact mentioned in the Customer Control Panel or on the Parent Website.

(3) Any notice or other communication required or permitted to be delivered to the Customer under this Agreement shall be deemed properly delivered, given and received when delivered to email address or contact address of the Customer in the OrderBox Database.

(4) Other than those notices mentioned in this agreement, Parent is NOT required to communicate with the Customer in any respect about services provided under this agreement. As a convenience to the Customer, Parent may proactively send notices about aspects with regards to services rendered under this Agreement, however these notices may be discontinued by Parent at anytime.

APPENDIX 'A'
ACCEPTABLE USAGE POLICIES

This Appendix A covers the terms of access to the OrderBox. Any violation of these terms will constitute a breach of agreement, and grounds for immediate termination of this Agreement.

1. ACCESS TO OrderBox

(1) Parent may in its ABSOLUTE and UNFETTERED SOLE DISCRETION, temporarily suspend OrderBox Users' access to the OrderBox in the event of significant degradation of the OrderBox, or at any time Parent may deem necessary.

(2) Parent may in its ABSOLUTE and UNFETTERED SOLE DISCRETION make modifications to the OrderBox from time to time.

(3) Access to the OrderBox is controlled by authentication information provided by Parent. Parent is not responsible for any action in the OrderBox that takes place using this authentication information whether authorized or not.

(4) Parent is not responsible for any action in the OrderBox by a OrderBox User

(5) OrderBox User will not attempt to hack, crack, gain unauthorized access, misuse or engage in any practice that may hamper operations of the OrderBox including, without Limitation temporary / permanent slow down of the OrderBox, damage to data, software, operating system, applications, hardware components, network connectivity or any other hardware / software that constitute the OrderBox and architecture needed to continue operation thereof.

(6) OrderBox User will not send or cause the sending of repeated unreasonable network requests to the OrderBox or establish repeated unreasonable connections to the OrderBox. Parent will in its ABSOLUTE and UNFETTERED SOLE DISCRETION decide what constitutes as a reasonable number of requests or connections.

(7) OrderBox User will take reasonable measures and precautions to ensure secrecy of authentication information.

(8) OrderBox User will take reasonable precautions to protect OrderBox Data from misuse, unauthorized access or disclosure, alteration, or destruction.

(9) Parent shall not be responsible for damage caused due to the compromise of your Authentication information in any manner OR any authorized/unauthorized use of the Authentication Information.

(10) Parent shall not be liable for any damages due to downtime or interruption of OrderBox for any duration and any cause whatsoever.

(11) Parent shall have the right to temporarily or permanently suspend access of a OrderBox User to the OrderBox if Parent in its ABSOLUTE and UNFETTERED SOLE DISCRETION suspects misuse of the access to the OrderBox, or learns of any possible misuse that has occurred, or will occur with respect to a OrderBox User.

(12) Parent and Service Providers reserve the right to, in their sole discretion, reject any request, network connection, e-mail, or message, to, or passing through, OrderBox

2. Terms of USAGE OF OrderBox

(1) Customer, or its contractors, employees, directors, officers, representatives, agents and affiliates and OrderBox Users, either directly or indirectly, shall not use or permit use of the OrderBox or an Order, directly or indirectly, in violation of any federal, state or local rule, regulation or law, or for any unlawful purpose, or in a manner injurious to Parent, Service Providers or their Resellers, Customers and OrderBox Users, or their reputation, including but not limited to the following activities -

(1) Usenet spam (off-topic, bulk posting/cross-posting, advertising in non-commercial newsgroups, etc.)

(2) Posting a single article or substantially similar articles to an excessive number of newsgroups (i.e., more than 2-3) or posting of articles which are off-topic (i.e., off-topic according to the newsgroup charter or the article provokes complaints from the readers of the newsgroup for being off-topic)

(3) Sending unsolicited mass e-mails (i.e., to more than 10 individuals, generally referred to as spamming) which provokes complaints from any of the recipients; or engaging in spamming from any provider

(4) Offering for sale or otherwise enabling access to software products that facilitate the sending of unsolicited e-mail or facilitate the assembling of multiple e-mail addresses ("spamware")

(5) Advertising, transmitting, linking to, or otherwise making available any software, program, product, or service that is designed to violate these terms, including but not limited to the facilitation of the means to spam, initiation of pinging, flooding, mailbombing, denial of service attacks, and piracy of software

(6) Harassment of other individuals utilizing the Internet after being asked to stop by those individuals, a court, a law-enforcement agency and/or Parent

(7) Impersonating another user or entity or an existing company/user/service or otherwise falsifying one's identity for fraudulent purposes in e-mail, Usenet postings, on IRC, or with any other Internet service, or for the purpose of directing traffic of said user or entity elsewhere

(8) Pointing to or otherwise directing traffic to, directly or indirectly, any material that, in the sole opinion of Parent, is associated with spamming, bulk e-mail, e-mail harvesting, warez (or links to such material), is in violation of copyright law, or contains material judged, in the sole opinion of Parent, to be threatening or obscene or inappropriate

(9) Engaging in or solicit illegal activities, or to conduct any other activity that infringes the rights of Parent, Service Providers or any other third party

(10) Making foul or profane expressions, or impersonating another person with fraudulent or malicious intent, or to annoy, abuse, threaten, or harass that person

(11) Transmitting Unsolicited Commercial e-mail (UCE)

(12) Transmitting bulk e-mail

(13) Being listed, or, in our sole opinion is about to be listed, in any Spam Blacklist or DNS Blacklist

(14) Posting bulk Usenet/newsgroup articles

(15) Denial of Service attacks of any kind

(16) Excessive use of any web service obtained under this agreement beyond reasonable limits as determined by the Parent in its sole discretion

(17) Copyright or trademark infringement

(18) Unlawful or illegal activities of any kind

(19) Promoting net abuse in any manner (providing software, tools or information which enables, facilitates or otherwise supports net abuse)

(20) Causing lossage or creating service degradation for other users whether intentional or inadvertent.

(21) Distributing chain letters

(22) Sending large or multiple files or messages to a single recipient with malicious intent

(23) Cross-posting articles to an excessive number of, or inappropriate, newsgroups, forums, mailing lists or websites

(24) Phishing (identity theft), pharming, distribution of virus or malware, child pornography, Fast Flux techniques, running Botnet command and control, network attacks, money laundering schemes (Ponzi, Pyramid, Money Mule, etc.), or illegal distribution of prescription medications, including, but not limited to, promotion, marketing, or sale of prescription medications without a valid prescription

(25) Referencing an OrderBox provided service or an Order within a spam email

(26) Hosting, transmitting, providing, publishing, or storing illegal content, including but not limited to the following material, information, messages, data or images:

(1) libelous or defamatory content

(2) content that violates any privacy right

(3) content which threatens physical harm or property damage

(4) content which is obscene, pornographic, salacious, explicitly erotic or offensive

(5) content that violates applicable intellectual property laws or regulations, including but not limited to, the transmission of copyrighted material or trade secrets and the infringement of patents and trademarks

(6) content which violates any export, re-export or import laws and regulations of any jurisdiction

(7) hacker programs or archives, "warez", passwords or "cracks"

(8) internet relay chat servers ("IRCs") IRC bots

(9) any content which Parent in its sole discretion determines as illegal, unlawful, or otherwise inappropriate

(2) Parent in its sole discretion will determine what constitutes as violation of appropriate usage including but not limited to all of the above.

(3) Data in the OrderBox Database cannot be used for any purpose other than those listed below, except if explicit written permission has been obtained from Parent:-

1. To perform services contemplated under this agreement; and

2. To communicate with Parent on any matter pertaining to Parent or its services

(3) Data in the OrderBox Database cannot specifically be used for any purpose listed below :-

1. Mass Mailing or SPAM; and

2. Selling the data


APPENDIX 'B'
CONFIDENTIALITY

Customer's use and disclosure of Confidential Information is subject to the following terms and conditions:-

(1) With respect to the Confidential Information, the Customer agree that:

(1) The Customer shall treat as strictly confidential, and use all reasonable efforts, including implementing reasonable physical security measures and operating procedures, to preserve the secrecy and confidentiality of, all Confidential Information received from Parent.

(2) The Customer shall make no disclosures whatsoever of any Confidential Information to others, provided however, that if the Customer are a corporation, partnership, or similar entity, disclosure is permitted to the their officers and employees who have a demonstrable need to know such Confidential Information, provided that the Customer shall advise such personnel of the confidential nature of the Confidential Information and of the procedures required to maintain the confidentiality thereof; and

(3) The Customer shall not modify or remove any confidentiality legends and/or copyright notices appearing on any Confidential Information of Parent.

(2) The obligations set forth in this Appendix shall be continuing, provided, however, that this Appendix imposes no obligation upon the Customer with respect to information that:

(1) is disclosed with Parent's prior written approval; or

(2) is or has entered the public domain in its integrated and aggregated form through no fault of the receiving party; or

(3) is known by the Customer prior to the time of disclosure in its integrated and aggregated form; or

(4) is independently developed  by the Customer without use of the Confidential Information; or

(5) is made generally available by Parent without restriction on disclosure.

(3) In the event the Customer is required by law, regulation or court order to disclose any of Parent's Confidential Information, the Customer will promptly notify Parent in writing prior to making any such disclosure in order to facilitate Parent seeking a protective order or other appropriate remedy from the proper authority, at the Customer' expense. The Customer agree to cooperate with Parent in seeking such order or other remedy. The Customer further agree that if Parent is not successful in precluding the requesting legal body from requiring the disclosure of the Confidential Information, it will furnish only that portion of the Confidential Information, which is legally required.

(4) In the event of any termination of this Agreement, all Confidential Information, including all copies, partial copies of Confidential Information, copied portions contained in derivative works, in the Customer' possession shall be immediately returned to Parent or destroyed. Within 30 (Thirty) days of termination of this Agreement, the Customer will certify in writing, to Parent the Customer' compliance with this provision.

(5) The Customer shall provide full voluntary disclosure to Parent of any and all unauthorized disclosures and/or unauthorized uses of any Confidential Information; and the obligations of this Appendix shall survive such termination and remain in full force and effect.

(6) The Customer duties under this Appendix shall expire five (5) years after the information is received or earlier, upon written agreement of the parties.

(7) The Customer agrees that Parent shall be entitled to seek all available legal and equitable remedies for the breach by either of the Customer of all of these clauses in this Appendix at the cost of the Customer.

APPENDIX 'C'
PAYMENT TERMS AND CONDITIONS


1. ADVANCE ACCOUNT

(1) Prior to purchasing any Parent Products, the Customer shall maintain an Advance Account with Parent.

(2) As and when, the Customer purchases Parent Products, the Customer's Advance Account balance shall be reduced as per the then current pricing of that Parent Product as mentioned in the Customer Control Panel or on the Parent Website or during the ordering process.

(3) Parent shall maintain a record of Customer's Advance Account balance, which shall be accessible by the Customer. If the Customer's Advance Account balance is insufficient for processing any Order then that Order may not be processed.

(4) The Advance Account will maintain the Customer Credit in both the Accounting Currency and Selling Currency of the Parent's choice. Parent has the right to modify the currency at anytime.

(5) Any negative balance in the Customer's Advance Account will be immediately payable. If a Customer does not remedy a negative balance in their account within 24 hours, Parent has the right to terminate this agreement with immediate effect and without any notice. Upon such termination or otherwise Parent shall continue to have the right to initiate any legal proceedings against the Customer to recover any negative balance in the Customer's Advance Account.

(6) Parent shall have the right to set-off any payment received from the Customer, or Sub-Customer, or Lower Tier Sub-Customer, or Customer against any negative balance in the Customer's Advance Account.

(7) Any discrepancy, mistake, error in the credit / debit / amount in the Customer Transactions / Advance Account maybe corrected by Parent at anytime

2. PAYMENT TERMS

(1) Parent will accept payments from the Customer only by means specified in the Customer Control Panel

(2) Parent will credit all payments received to the Customers Advance Account after deducting all bank charges, processing charges and any other charges which Parent may choose to levy upon its sole discretion, within reasonable time of receiving the credit in Parent's Account. The exchange rate will be determined by Parent through a reasonable source. The exchange rate determined by Parent shall be undisputable.

(3) It is the Customer's responsibility to provide the Customer Username to Parent to be credited for the payment. The absence of the Customer Username along with reasonable information will delay the corresponding credit to the Advance Account.

(4) In the event that the Customer charges back a payment made via Credit Card or the payment instrument sent by the Customer bounces due to Lack of Funds or any other Reason, then

(1) Parent may immediately suspend OrderBox Users' access to the OrderBox

(2) Parent has the right to terminate this agreement with immediate effect and without any notice.

(3) Parent in its ABSOLUTE and UNFETTERED SOLE DISCRETION may delete, suspend, deny, cancel, modify, take ownership of or transfer any or all of the Orders placed by the Customer, as well as stop / suspend / delete / transfer any Orders currently being processed.

(4) Parent in its ABSOLUTE and UNFETTERED SOLE DISCRETION may Transfer all Orders placed by the Customer to any other Customer, or under Parent's account.

(5) Parent in its ABSOLUTE and UNFETTERED SOLE DISCRETION may levy reasonable additional charges for the processing of the Charge-back / Payment Reversal in addition to actual costs of the same.

(6) Any negative balance in the Customers Advance Account shall become immediately payable

(7) Parent shall have the right to initiate any legal proceedings against the Customer to recover any such liabilities.

3. PRICING TERMS

(1) All pricing in this Agreement as well as every Customer Product Agreement Extension refers to the price at which the Customer may Purchase the corresponding Parent Product. This is excluding taxes, surcharges or any other costs.

(2) Parent may at any time change the price of any Parent Product with reasonable notification to the Customer.

4. REFUNDS AND REIMBURSEMENT TERMS

(1) All Clear Balance pending in the Advance Account maybe refunded to the Customer, on request of the Customer unless otherwise indicated, including without limitation, if Customer has violated the OFAC Provision in Section 4 or if Customer has violated any other term of this Agreement. Such request must be sent to Parent in the manner prescribed by Parent.

(2) All bank charges applicable and a reasonable processing fee will be deducted from this amount. All Refunds and Reimbursements will take up to 14 Business Days from the date of receipt of the request, to process.

(3) Parent will not be responsible for any differences in the reimbursement amount due to Fluctuation in International Currency rates. Parent will determine in its sole discretion appropriate conversion rates for currency exchange

(4) Parent will not refund any amount that has already been debited to the Customers Advance Account under any circumstances.
                </textarea>
            </td>
        </tr>

        <tr>
            <td align="center">
                <input type="button" name="back_button" value="Back" class="frmButton" onclick="javascript:history.go(-1);">
            </td>
        </tr>

    </tbody></table>

<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>