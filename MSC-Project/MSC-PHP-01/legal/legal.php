<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<div>
    <div>
        <!-- ### Template legal agreement starts here ### -->
        <div >
            <table class="dataTable section" cellspacing="0" width="100%" align="center" border="0">
                <tbody><tr>
                        <td>
                            <div><h3>Legal Agreements</h3></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p><a href="legal-customer-master-agreement.php">
                                    <b>Customer Master Agreement</b></a><br>
                                This is the main Customer Master agreement that would apply to you as our
                                Customer. Apart
                                from this Master Agreement, the following Product Specific Agreements may
                                also apply to you depending on the Products and Services you buy.</p></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p><a href="legal-registrar-registrant-agreement-for-domain-names.php">
                                    <b>Registrar Registrant Agreement for Domain Names</b></a><br>
                                This represents the Agreement between the Registrant (Owner) of a Domain
                                Name and the Registrar. If you register a domain name through us, this
                                Agreement will apply to the person whose information you filled in the Owner
                                section during the Registration process</p></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p><a href="legal-customer-agreement-for-domain-names.php">
                                    <b>Customer Agreement for Domain Names</b></a><br>
                                This represents an addendum to the Customer Master Agreement between yourself and us for  Domain
                                Registration</p> </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p><a href="legal-customer-agreement-for-web-services.php">
                                    <b>Customer Agreement for Web Services</b></a><br>
                                This represents an addendum to the Customer Master Agreement between yourself and us for  Domain / Mail
                                Forwarding and Managed DNS</p> </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p><a href="legal-customer-agreement-for-digital-certificates.php">
                                    <b>Customer Agreement for Digital Certificates</b></a><br>
                                This represents the Customer Agreement for Digital Certificates</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p><a href="legal-customer-hosting-product-agreement.php">
                                    <b>Customer Hosting Product Agreement</b></a><br>
                                This represents the Customer Agreement for Hosting</p>
                        </td>
                    </tr>
                </tbody></table>
            <br><br>
            <p>
                <b style="font-size: 9pt;">#</b> Registrant shall further endeavor to familiarize themselves with the <a href="http://www.icann.org/en/registrars/registrant-rights-responsibilities-en.htm" target="_blank"> Registrant Rights and Responsibilities </a>applicable while registering gTLDs.
            </p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="footer-img">
                <tbody><tr><td>&nbsp;</td></tr>
                </tbody></table>
        </div>


        <!-- ### Template legal.html ends here ### -->

    </div>
</div>

<?php
//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?> 

