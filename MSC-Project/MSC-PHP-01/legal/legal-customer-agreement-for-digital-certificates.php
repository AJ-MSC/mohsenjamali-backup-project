<?php

include_once  $_SERVER['DOCUMENT_ROOT'].'/common/header.php';
//Start Section
?>
<br>
<!--<? $breadcrumb; ?>-->

<table class="dataTable" cellspacing="0" width="100%" align="center" border="0">
    <tbody><tr>
            <td><div class="ui-heading">Legal Agreements</div></td>
        </tr>
        <tr align="">
            <td align="">
                <textarea rows="26" name="newcontent" cols="145" readonly="">CUSTOMER DIGITAL CERTIFICATE PRODUCT AGREEMENT EXTENSION

MAVAJ SUN CO (hereinafter referred to as "Parent") AND you (hereinafter referred to as "Customer")

HAVE

entered into a Customer Master Agreement ("Agreement") effective from 13 December, 2014 of which this "Digital Certificate Product Agreement Extension" is a part.

WHEREAS, Parent sells digital certificates of Thawte;

NOW, THEREFORE, Parent and the Customer, hereby agree as follows:

1. Customer Election. Customer hereby elects to purchase Thawte digital certificates through Parent

2. Parent's Acceptance. Parent hereby accepts Customer's election to purchase Thawte digital certificates through Parent.
                </textarea>
            </td>
        </tr>

        <tr>
            <td align="center">
                <input type="button" name="back_button" value="Back" class="frmButton" onclick="javascript:history.go(-1);">
            </td>
        </tr>

    </tbody></table>

<?php

//End section
include_once  $_SERVER['DOCUMENT_ROOT'].'/common/footer.php';
?>